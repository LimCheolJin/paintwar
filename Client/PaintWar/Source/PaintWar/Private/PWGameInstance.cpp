// Fill out your copyright notice in the Description page of Project Settings.


#include "PWGameInstance.h"
#include "Engine/TextureRenderTarget2D.h"
#include "PWNetWork.h"
#include "Protocol.h"
#include "PWGameMode.h"
#include "PWGameTimeManager.h"

UPWGameInstance::UPWGameInstance()
{
	
}
UPWGameInstance::~UPWGameInstance()
{// 다른 플레이어들의 정보임
	/*for (auto& m : m_PlayerInformations)
	{
		delete m.second;
	}
	m_PlayerInformations.clear();*/
	
	//delm_MyPlayerInfo;
	if (IsConnect == true && (PWNetWork != nullptr))
		PWNetWork->DisConnect();

	IsConnect = false;
	PWNetWork = nullptr;

	if (!m_PlayerInformations.empty())
	{
		for (auto& client : m_PlayerInformations)
		{
			delete client.second;
			client.second = nullptr;
		}
	}
}


void UPWGameInstance::OnStart()
{
	Super::OnStart();

	IsConnect = false;
	CurrentGameMode = EGameMode::NONE;
	PWLOG(Error, TEXT("GAMEINSTANCE BEGINPLAY"));
	//PWLOG(Error, TEXT("UPWGameInstance's OnStart()"));
}

void UPWGameInstance::FinishDestroy()
{
	Super::FinishDestroy();

	PWLOG(Error, TEXT("GAMEINSTANCE ENDPLAY"));
	//PWLOG(Error, TEXT("UPWGameInstance's FinishDestroy()"));
	
	if(IsConnect == true)
		PWNetWork->DisConnect();

	IsConnect = false;
	PWNetWork = nullptr;

	if (!m_PlayerInformations.empty())
	{
		for (auto& client : m_PlayerInformations)
		{
			delete client.second;
			client.second = nullptr;
		}
	}
	//delete m_MyPlayerInfo;
	//m_MyPlayerInfo = nullptr;
}

void UPWGameInstance::SetCurrentSelectCharacter(ESelectCharacter SelectCharacter)
{
	CurrentSelectCharacter = SelectCharacter;
	//PWLOG(Error, TEXT("UPWGameInstance'S SetCurrentSelectCharacter "));
	if (CurrentGameMode == EGameMode::MULTI)
	{
		m_MyPlayerInfo->m_eCharacterType = CurrentSelectCharacter;//(e_PlayerCharacterType)SelectCharacter;
	}
}
void UPWGameInstance::SetCurrentSelectTeam(ETeam SelectTeam)
{
	CurrentSelectTeam = SelectTeam;
	//PWLOG(Error, TEXT("UPWGameInstance'S SetCurrentSelectTeam "));
	if (CurrentGameMode == EGameMode::MULTI)
	{
		m_MyPlayerInfo->m_eTeamType = CurrentSelectTeam;//(e_PlayerTeamType)SelectTeam;
	}
}

ESelectCharacter UPWGameInstance::GetCurrentSelectCharacter() const
{
	return CurrentSelectCharacter;
}

ETeam UPWGameInstance::GetCurrentSelectTeam() const
{
	return CurrentSelectTeam; //Class'/Script/PaintWar.PWLoginGameMode'
}

EGameMode UPWGameInstance::GetCurrentGameMode() const
{
	return CurrentGameMode;
}
void UPWGameInstance::SetCurrentGameMode(EGameMode Mode)
{
	CurrentGameMode = Mode;
}

TArray<UTextureRenderTarget2D*> UPWGameInstance::GetAllRenderTargets() const
{
	return AllRenderTargets;
}

void UPWGameInstance::InsertRenderTarget(UTextureRenderTarget2D* RenderTarget)
{
	AllRenderTargets.Emplace(RenderTarget);
}

int UPWGameInstance::GetGreenResultCount() const
{
	return GreenResultCount;
}

int UPWGameInstance::GetRedResultCount() const
{
	return RedResultCount;
}


void UPWGameInstance::SetGreenResultCount(int count)
{
	GreenResultCount = count;
}

void UPWGameInstance::SetRedResultCount(int count)
{
	RedResultCount = count;
}


void UPWGameInstance::ClearRenderTargets()
{
	PWLOG(Error, TEXT("ClearRenderTargetsF "));

	/*for (int i = 0; i < AllRenderTargets.Num(); ++i)
		AllRenderTargets[i] = nullptr;*/
	
	AllRenderTargets.Empty(0);
	/*AllRenderTargets.Reset();
	AllRenderTargets.Shrink();*/
}

//////////////////////////For NetWorking Functions//////////////////// 
bool UPWGameInstance::InitNetWork()
{
	//PWLOG(Error, TEXT("GameInstance's InitNetWork "));
	PWNetWork = PWNetWork::GetNetWork();
	PWNetWork->SetPWGameInstance(this);
	if (PWNetWork->InitClientSocket() == false)
		return false;

	return true;
}
bool UPWGameInstance::Connect(FString serverIP)
{
	//뭔가연결이있다.
	//PWLOG(Error, TEXT("GameInstance's Connect "));
	if (IsConnect == true)
		return false;

	if (PWNetWork->Connect(TCHAR_TO_ANSI(*serverIP)) == false)
		return false;
	if (PWNetWork->CreateEventSelect() == false)
		return false;
	if (PWNetWork->StartThread() == false)
		return false;


	IsConnect = true;
	//PWNetWork->Send_Login();
	//Send_Login();

	return true;

}
void UPWGameInstance::Recv_LoginPacketProcess(char* LoginPacket)
{
	//PWLOG(Error, TEXT("GameInstance's Recv_LoginPACKETProcess "));
	SC_PACKET_LOGIN_OK packet{};
	memcpy(&packet, LoginPacket, sizeof(SC_PACKET_LOGIN_OK));
	if (PWNetWork != nullptr)
	{
		//if (m_PlayerInformations.empty()) //최초의 내가 로그인했을때 나의 정보를 따로가지고 잇기위함이다. 
		//{
		m_MyPlayerInfo = new PlayerInfo();
		m_MyPlayerInfo->m_ID = packet.m_ID;
		m_PlayerInformations[packet.m_ID] = m_MyPlayerInfo;
		m_PlayerInformations[packet.m_ID]->m_IsRoomHead = packet.m_IsRoomHead;
		//m_PlayerInformations[packet.m_ID]->Login_ID = const_cast<char*>(TCHAR_TO_ANSI(*GetLoginID()));
		memcpy(m_PlayerInformations[packet.m_ID]->Login_ID, TCHAR_TO_ANSI(*GetLoginID()), MAX_ID_LEN);

		
	}
}

void UPWGameInstance::Recv_OtherPlayerEnterPacketProcess(char* OtherEnterPacket)
{
	SC_PACKET_ENTER packet{};
	memcpy(&packet, OtherEnterPacket, sizeof(SC_PACKET_ENTER));

	if (PWNetWork != nullptr)
	{
		if (packet.m_ID != m_MyPlayerInfo->m_ID)
		{
			m_PlayerInformations[packet.m_ID] = new PlayerInfo();
			m_PlayerInformations[packet.m_ID]->m_ID = packet.m_ID;
			m_PlayerInformations[packet.m_ID]->m_IsRoomHead = packet.m_IsRoomHead;
			memcpy(m_PlayerInformations[packet.m_ID]->Login_ID, packet.m_LoginID, MAX_ID_LEN);
			//PWLOG(Error, TEXT("Other ID is %d "), m_PlayerInformations[packet.m_ID]->m_ID);
		}
	}
}
void UPWGameInstance::Recv_SelectTeamTypePacketProcess(char* TeamTypePacket)
{
	char* buf = TeamTypePacket;
	SC_PACKET_SELECT_TEAM packet{};
	memcpy(&packet, buf, sizeof(SC_PACKET_SELECT_TEAM));

	if (PWNetWork != nullptr)
	{
		if (packet.m_ID != m_MyPlayerInfo->m_ID)
			m_PlayerInformations[packet.m_ID]->m_eTeamType = packet.m_eTeamType;
		else
		{
			m_MyPlayerInfo->m_eTeamType = packet.m_eTeamType;
		}
	}
}


void UPWGameInstance::Recv_SelectCharacterTypePacketProcess(char* CharacterTypePacket)
{
	SC_PACKET_SELECT_CHARACTERTYPE packet{};
	memcpy(&packet, CharacterTypePacket, sizeof(SC_PACKET_SELECT_CHARACTERTYPE));
	// 비었을때 하는게 맞나싶음.
	if (PWNetWork != nullptr)
	{
		if (packet.m_ID != m_MyPlayerInfo->m_ID)
			m_PlayerInformations[packet.m_ID]->m_eCharacterType = packet.m_eCharacterType;
		else
			m_MyPlayerInfo->m_eCharacterType = packet.m_eCharacterType;
	}
}

void UPWGameInstance::Recv_OtherPlayerLeavePacketProcess(char* OtherLeavePacket)
{
	SC_PACKET_LEAVE packet{};
	memcpy(&packet, OtherLeavePacket, sizeof(SC_PACKET_LEAVE));

	if (PWNetWork != nullptr)
	{
		if (m_PlayerInformations[packet.m_ID] != nullptr)
		{
			delete m_PlayerInformations[packet.m_ID];
			m_PlayerInformations.erase(packet.m_ID);
		}
		//일단주석 0423
		//Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()))->Recv_OtherPlayerLeavePacketProcess(packet.m_ID);
		//PWLOG(Error, TEXT("Player's ID: %d  DisConnecting"), packet.m_ID);
	}
}

void UPWGameInstance::Recv_StartGamePacketProcess(char* StartGamePacket)
{
	SC_PACKET_START_GAME packet{};
	memcpy(&packet, StartGamePacket, sizeof(SC_PACKET_START_GAME));
	if (PWNetWork != nullptr)
	{
		if (packet.m_StartGame == true)
		{
			IsGameStart = packet.m_StartGame;
			//UGameplayStatics::OpenLevel(this, FName("TestLevel"));
		}
	}
}
void UPWGameInstance::Recv_GameEndPacketProcess(char* EndPacket)
{
	SC_PACKET_GAME_END* packet = reinterpret_cast<SC_PACKET_GAME_END*>(EndPacket);
	if (PWNetWork != nullptr)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("Game ENd  "), false));
		//잠시 주석
		auto gmode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		auto timer = gmode->GetGameTimer(); 
		if (timer == nullptr)
			return;
		timer->SetIsGameEnd(true);
		this->SetIsGameStart(false);
		gmode->SetIsMultiGameStart(false);
		m_MyPlayerInfo->m_IsResulter = packet->m_Resulter;
	}

}

void UPWGameInstance::Recv_GameResultPacketProcess(char* Packet)
{
	SC_PACKET_GAME_RESULT* packet = reinterpret_cast<SC_PACKET_GAME_RESULT*>(Packet);
	if (PWNetWork != nullptr)
	{
		RedResultCount = packet->m_ResultRedCount;
		GreenResultCount = packet->m_ResultGreenCount;
		auto gmode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		auto timer = gmode->GetGameTimer();
		if (timer == nullptr)
			return;
		timer->SetIsResultCompleteEnd(true);
	}
}
bool UPWGameInstance::Send_SelectTeamTypePacket()
{
	PWLOG(Error, TEXT("GameInstance's Send_SelectTeamTypePacket "));
	if (IsConnect == false)
		return false;
	if (PWNetWork->Get_m_bIsRun() == false)
		return false;

	if (PWNetWork != nullptr)
	{
		if (PWNetWork->Send_SelectTeamTypePacket(m_MyPlayerInfo->m_eTeamType, m_MyPlayerInfo->m_ID) == false)
			return false;
	}
	return true;
}

bool UPWGameInstance::Send_SelectCharacterTypePacket()
{
	//PWLOG(Error, TEXT("GameInstance's Send_SelectCharacterTypePacket "));
	if (IsConnect == false)
		return false;
	if (PWNetWork->Get_m_bIsRun() == false)
		return false;

	if (PWNetWork != nullptr)
	{
		if (PWNetWork->Send_SelectCharacterTypePacket((m_MyPlayerInfo->m_eCharacterType), m_MyPlayerInfo->m_ID) == false)
			return false;
	}
	return true;
}
bool UPWGameInstance::Send_InitLogin()
{
	//PWLOG(Error, TEXT("GameInstance's Send_Login "));
	if (IsConnect == false)
		return false;
	if (PWNetWork->Get_m_bIsRun() == false)
		return false;
	
	if (PWNetWork != nullptr)
	{
		if (PWNetWork->Send_Login() == false)
			return false;
	}
	return true;
}

bool UPWGameInstance::Send_ReadyInfo()
{
	//PWLOG(Error, TEXT("GameInstance's Send_ReadyInfo "));
	if (IsConnect == false)
		return false;
	if (PWNetWork->Get_m_bIsRun() == false)
		return false;

	if (m_MyPlayerInfo->m_PlayerState == e_GameStateInfo::e_Ready)
		m_MyPlayerInfo->m_PlayerState = e_GameStateInfo::e_UnReady;
	else if (m_MyPlayerInfo->m_PlayerState == e_GameStateInfo::e_UnReady)
		m_MyPlayerInfo->m_PlayerState = e_GameStateInfo::e_Ready;
	else if (m_MyPlayerInfo->m_PlayerState == e_GameStateInfo::e_None)
		m_MyPlayerInfo->m_PlayerState = e_GameStateInfo::e_Ready;

	if (PWNetWork != nullptr)
	{
		if (PWNetWork->Send_ReadyPacket(m_MyPlayerInfo->m_PlayerState, m_MyPlayerInfo->m_ID) == false)
			return false;
	}
	return true;
}

bool UPWGameInstance::Send_ResultGame()
{
	if (IsConnect == false)
		return false;
	if (PWNetWork->Get_m_bIsRun() == false)
		return false;

	if (PWNetWork != nullptr)
	{
		if (PWNetWork->Send_ResultGamePacket(RedResultCount, GreenResultCount, m_MyPlayerInfo->m_ID) == false)
			return false;
	}
	return true;
}
bool UPWGameInstance::GetIsGameStart() const
{
	return IsGameStart;
}

void UPWGameInstance::SetIsGameStart(bool start)
{
	IsGameStart = start;
}

FString  UPWGameInstance::GetLoginID() {
	return LoginID;
}

void  UPWGameInstance::SetLoginID(FString id) {
	LoginID = id;
}
void UPWGameInstance::ForRestartInit()
{
	for (auto& player : m_PlayerInformations)
	{
		player.second->m_PlayerState = e_GameStateInfo::e_None;
		player.second->m_eCharacterType = ESelectCharacter::NONE;
		player.second->m_eTeamType = ETeam::NONE;
	}
}