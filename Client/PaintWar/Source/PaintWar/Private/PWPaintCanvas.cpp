// Fill out your copyright notice in the Description page of Project Settings.


#include "PWPaintCanvas.h"
#include "PWGameInstance.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInterface.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Engine/Canvas.h"

// Sets default values
APWPaintCanvas::APWPaintCanvas()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//StaticMesh'/Game/StarterContent/Shapes/Shape_Plane.Shape_Plane'
	CanvasMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CanvasMesh"));
	//StaticMesh'/Game/TestAssets/MeshPainterMethod/test_box.test_box' //StaticMesh'/Game/TestAssets/testmap/1234.1234'
	//StaticMesh'/Game/TestAssets/testmap/testmap0225/testmap_rescaleon_200225.testmap_rescaleon_200225'
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/testmap0225/testmap_rescaleon_200225.testmap_rescaleon_200225"));
	//StaticMesh'/Game/TestAssets/testmap/map_uv_11062poly_200225.map_uv_11062poly_200225'
	//StaticMesh'/Game/TestAssets/testmap/map_low_retopo_uv_200213.map_low_retopo_uv_200213'
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/StarterContent/Shapes/Shape_Plane.Shape_Plane"));
	//StaticMesh'/Game/TestAssets/testmap/testmap0225/testmap0226/test_dettachobj_uv_200226_Object003.test_dettachobj_uv_200226_Object003'
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/map_200224.map_200224"));//("/Game/TestAssets/testmap/map_uv_11062poly_200225.map_uv_11062poly_200225"));
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/map_uv_11062poly_200225.map_uv_11062poly_200225"));
	//if (ST_CANVAS.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon' //StaticMesh'/Game/TestAssets/testmap/testmap0227/0227_17_43/map_low_4096texture_200227.map_low_4096texture_200227'
	//{//루트지정해주기//StaticMesh'/Game/TestAssets/testmap/testmap0227/map5cm_uv_48105p_200226_left_1st.map5cm_uv_48105p_200226_left_1st'
	//	CanvasMesh->SetStaticMesh(ST_CANVAS.Object);
	//}
	//CanvasMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	CanvasMesh->SetGenerateOverlapEvents(true);
	CanvasMesh->SetCollisionProfileName("PaintCanvas");
	

	//static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_Canvas(TEXT("/Game/TestAssets/TestFloorPaint/M_Floor.M_Floor"));
	//if (M_Canvas.Succeeded())
	//{
	//	CanvasMaterial = M_Canvas.Object;
	//}
	//InitCanvasMaterial;
	
	//Material'/Game/TestAssets/TestFloorPaint/M_InitFloor.M_InitFloor'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_InitCanvas(TEXT("/Game/TestAssets/TestFloorPaint/M_InitFloor.M_InitFloor"));
	if (M_InitCanvas.Succeeded())
	{
		InitCanvasMaterial = M_InitCanvas.Object;
	}
	//Texture2D'/Game/TestAssets/TestFloorPaint/uvpractice.uvpractice'
	//StaticMesh'/Game/TestAssets/testmap/1234.1234'
	/*static ConstructorHelpers::FObjectFinder<UTexture2D> T_InitCanvas(TEXT("/Game/TestAssets/TestFloorPaint/uvpractice.uvpractice"));
	if (T_InitCanvas.Succeeded())
	{
		InitTexture = T_InitCanvas.Object;
	}*/
	//static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_Brush(TEXT("/Game/TestAssets/TestFloorPaint/M_Brush.M_Brush"));
	//if (M_Brush.Succeeded())
	//{
	//	BrushMaterial = M_Brush.Object;
	//}

	//BrushTexture = CreateDefaultSubobject<UTexture2D>(TEXT("BrushTexture"));
	//static ConstructorHelpers::FObjectFinder<UTexture2D> T_Brush(TEXT("/Game/TestAssets/TestFloorPaint/T_RedPaint.T_RedPaint"));
	//if (T_Brush.Succeeded())
	//{
	//	BrushTexture = T_Brush.Object;
	//	PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	//}
	Canvas = CreateDefaultSubobject<UCanvas>(TEXT("Canvas"));

}

// Called when the game starts or when spawned
void APWPaintCanvas::BeginPlay()
{
	Super::BeginPlay();

	PWLOG(Warning, TEXT("CanvasBeginPlay"));
	auto PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());

	RenderTarget = UKismetRenderingLibrary::CreateRenderTarget2D(GetWorld(), 2048, 2048);//, ETextureRenderTargetFormat::RTF_RGBA8); // 6개
	
	PWGameInstance->InsertRenderTarget(RenderTarget);

	CanvasMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), CanvasMaterial);
	/*BrushMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), BrushMaterial);*/
	//InitCanvasMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), InitCanvasMaterial);


	//UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), RenderTarget, InitCanvasMaterial);
	CanvasMaterialInstance->SetTextureParameterValue(FName("RenderTarget"), RenderTarget);
	CanvasMesh->SetMaterial(0, CanvasMaterialInstance);

	//BrushMaterialInstance->SetTextureParameterValue(FName("BrushTexture"), BrushTexture);
	//RenderTarget->ConstructTexture2D(this, FString("testTexture2D"), EObjectFlags::RF_NoFlags);
	//나중에 조건문 추가하기 0이면 빨강 1이면 파랑을 불러오도록 위의 생성자 T_Brush 참고하기
	
	//UKismetRenderingLibrary::ClearRenderTarget2D(GetWorld(), RenderTarget, FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
}

// Called every frame
void APWPaintCanvas::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APWPaintCanvas::DrawBrush(FVector2D DrawLocation, float BrushSize, UMaterialInstanceDynamic* BrushMaterialInstance)
{
	//UCanvas* Canvas;

	FDrawToRenderTargetContext Context;
	FVector2D Size;
	//RenderTarget->canvas
	UKismetRenderingLibrary::BeginDrawCanvasToRenderTarget(GetWorld(), RenderTarget, Canvas, Size, Context);
	
	//PWLOG(Warning, TEXT("%f, %f "), Size.X, Size.Y);
	//나중에 아래의 슈도코드와 같이 추가하면될거같음.
	//if(shooter의 총알이면)
	//{}
	//else if(Bomber의 폭탄이면)
	//{}
	//else if(Roller의 롤러이면)
	//{}

	if (Canvas)
	{
		//Canvas->K2_DrawMaterial(BrushMaterialInstance, ((Size * DrawLocation) - (BrushSize / 12.0f)), FVector2D(BrushSize, BrushSize), FVector2D(0.0f, 0.0f));
		Canvas->K2_DrawMaterial(BrushMaterialInstance, ((Size * DrawLocation) - (BrushSize / 2.0f)), FVector2D(BrushSize,BrushSize) , FVector2D(0.0f, 0.0f));

	}
	UKismetRenderingLibrary::EndDrawCanvasToRenderTarget(GetWorld(), Context);

	//PWLOG(Warning, TEXT("Brush Complete !")); 
}

void APWPaintCanvas::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	PWLOG(Warning, TEXT("ENDPLAY PAINT"));
}