// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"

/**
 * 
 */
class PAINTWAR_API PWGameEndThread : public FRunnable
{
public:
	PWGameEndThread();
	~PWGameEndThread();

	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
	virtual void Exit();

	//bool StartThread(TArray<FLinearColor>& ColorBuffer, int StartLoop, int EndLoop, long long* RedResultCount, long long* GreenResultCount, FCriticalSection* mutex);
	bool StartThread(TArray<FLinearColor>& ColorBuffer,  long long* RedResultCount, long long* GreenResultCount, FCriticalSection* mutex);

	TArray<FColor> Pixels;
	//void ReadPixels();

	class FRunnableThread* GetGameEndThread() const;
private:
	class FRunnableThread* GameEndThread;

	TArray<FLinearColor> ColorBuffer;

	long long* GreenResultCount;
	long long* RedResultCount;

	int StartLoop;

	int EndLoop;

	FCriticalSection* ResultMutex;

};
