// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PaintMap/Left/Body/PWPaintLeft_2nd.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPaintLeft_2nd() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintLeft_2nd_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintLeft_2nd();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWPaintLeft_2nd::StaticRegisterNativesAPWPaintLeft_2nd()
	{
	}
	UClass* Z_Construct_UClass_APWPaintLeft_2nd_NoRegister()
	{
		return APWPaintLeft_2nd::StaticClass();
	}
	struct Z_Construct_UClass_APWPaintLeft_2nd_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPaintLeft_2nd_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWPaintCanvas,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintLeft_2nd_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaintMap/Left/Body/PWPaintLeft_2nd.h" },
		{ "ModuleRelativePath", "Public/PaintMap/Left/Body/PWPaintLeft_2nd.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPaintLeft_2nd_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPaintLeft_2nd>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPaintLeft_2nd_Statics::ClassParams = {
		&APWPaintLeft_2nd::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWPaintLeft_2nd_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPaintLeft_2nd_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPaintLeft_2nd()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPaintLeft_2nd_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPaintLeft_2nd, 2295797112);
	template<> PAINTWAR_API UClass* StaticClass<APWPaintLeft_2nd>()
	{
		return APWPaintLeft_2nd::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPaintLeft_2nd(Z_Construct_UClass_APWPaintLeft_2nd, &APWPaintLeft_2nd::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPaintLeft_2nd"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPaintLeft_2nd);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
