// Fill out your copyright notice in the Description page of Project Settings.


#include "PWRollerAnimInstance.h"
#include "PWRollerCharacter.h"



UPWRollerAnimInstance::UPWRollerAnimInstance()
{
	IsPainting = false;

}

void UPWRollerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	//현재 폰이 롤러캐릭터이면?
	UPWAnimInstance::NativeUpdateAnimation(DeltaSeconds);

	auto RollerCharacter = Cast<APWRollerCharacter>(TryGetPawnOwner());

	if (RollerCharacter != nullptr)
	{
		IsPainting = RollerCharacter->GetbPainting();
	}
}
void UPWRollerAnimInstance::AnimNotify_ReloadNotify()
{
	OnReloadNotify.Broadcast();
}

void UPWRollerAnimInstance::AnimNotify_PaintNotify()
{
	OnPaintNotify.Broadcast();
}