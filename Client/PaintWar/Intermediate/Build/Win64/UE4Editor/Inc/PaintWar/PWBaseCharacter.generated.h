// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APWBaseWeapon;
enum class ETeam : uint8;
class APWBaseCharacter;
#ifdef PAINTWAR_PWBaseCharacter_generated_h
#error "PWBaseCharacter.generated.h already included, missing '#pragma once' in PWBaseCharacter.h"
#endif
#define PAINTWAR_PWBaseCharacter_generated_h

#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSkillON) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetSkillON(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSkillCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetSkillCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(APWBaseWeapon**)Z_Param__Result=P_THIS->GetWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLoginID) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_id); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetLoginID(Z_Param_id); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoginID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetLoginID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsCharacterFalling) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IsCharacterFalling(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDeath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetDeath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetKill) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetKill(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAmountPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetAmountPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIsReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execQuitReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->QuitReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ETeam*)Z_Param__Result=P_THIS->GetTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathProcess) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathProcess(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRespawnEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RespawnEvent(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execKillEvent) \
	{ \
		P_GET_OBJECT(APWBaseCharacter,Z_Param_KillInstigator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->KillEvent(Z_Param_KillInstigator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathEvent(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSkillON) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetSkillON(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSkillCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetSkillCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(APWBaseWeapon**)Z_Param__Result=P_THIS->GetWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLoginID) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_id); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetLoginID(Z_Param_id); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoginID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetLoginID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsCharacterFalling) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IsCharacterFalling(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDeath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetDeath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetKill) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetKill(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAmountPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetAmountPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIsReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execQuitReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->QuitReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartReload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ETeam*)Z_Param__Result=P_THIS->GetTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathProcess) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathProcess(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRespawnEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RespawnEvent(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execKillEvent) \
	{ \
		P_GET_OBJECT(APWBaseCharacter,Z_Param_KillInstigator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->KillEvent(Z_Param_KillInstigator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathEvent(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBaseCharacter(); \
	friend struct Z_Construct_UClass_APWBaseCharacter_Statics; \
public: \
	DECLARE_CLASS(APWBaseCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_INCLASS \
private: \
	static void StaticRegisterNativesAPWBaseCharacter(); \
	friend struct Z_Construct_UClass_APWBaseCharacter_Statics; \
public: \
	DECLARE_CLASS(APWBaseCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBaseCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBaseCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseCharacter(APWBaseCharacter&&); \
	NO_API APWBaseCharacter(const APWBaseCharacter&); \
public:


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseCharacter(APWBaseCharacter&&); \
	NO_API APWBaseCharacter(const APWBaseCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBaseCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ReloadHandle() { return STRUCT_OFFSET(APWBaseCharacter, ReloadHandle); } \
	FORCEINLINE static uint32 __PPO__Weapon() { return STRUCT_OFFSET(APWBaseCharacter, Weapon); } \
	FORCEINLINE static uint32 __PPO__HealthPoint() { return STRUCT_OFFSET(APWBaseCharacter, HealthPoint); } \
	FORCEINLINE static uint32 __PPO__CurrentCharacterState() { return STRUCT_OFFSET(APWBaseCharacter, CurrentCharacterState); } \
	FORCEINLINE static uint32 __PPO__RedTeamCharacterMaterialInstance() { return STRUCT_OFFSET(APWBaseCharacter, RedTeamCharacterMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__GreenTeamCharacterMaterialInstance() { return STRUCT_OFFSET(APWBaseCharacter, GreenTeamCharacterMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__SkillMaterialInstance() { return STRUCT_OFFSET(APWBaseCharacter, SkillMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__CharacterType() { return STRUCT_OFFSET(APWBaseCharacter, CharacterType); } \
	FORCEINLINE static uint32 __PPO__SpawnPointArray() { return STRUCT_OFFSET(APWBaseCharacter, SpawnPointArray); } \
	FORCEINLINE static uint32 __PPO__SpawnPointLocation() { return STRUCT_OFFSET(APWBaseCharacter, SpawnPointLocation); } \
	FORCEINLINE static uint32 __PPO__CurrentTeam() { return STRUCT_OFFSET(APWBaseCharacter, CurrentTeam); }


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_21_PROLOG
#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_INCLASS \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBaseCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Characters_PWBaseCharacter_h


#define FOREACH_ENUM_ECHARACTERSTATE(op) \
	op(ECharacterState::ALIVE) \
	op(ECharacterState::DEATH) 

enum class ECharacterState;
template<> PAINTWAR_API UEnum* StaticEnum<ECharacterState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
