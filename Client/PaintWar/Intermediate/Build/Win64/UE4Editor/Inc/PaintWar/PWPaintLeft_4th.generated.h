// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintLeft_4th_generated_h
#error "PWPaintLeft_4th.generated.h already included, missing '#pragma once' in PWPaintLeft_4th.h"
#endif
#define PAINTWAR_PWPaintLeft_4th_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_4th(); \
	friend struct Z_Construct_UClass_APWPaintLeft_4th_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_4th, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_4th)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_4th(); \
	friend struct Z_Construct_UClass_APWPaintLeft_4th_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_4th, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_4th)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintLeft_4th(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintLeft_4th) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_4th); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_4th); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_4th(APWPaintLeft_4th&&); \
	NO_API APWPaintLeft_4th(const APWPaintLeft_4th&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_4th(APWPaintLeft_4th&&); \
	NO_API APWPaintLeft_4th(const APWPaintLeft_4th&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_4th); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_4th); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintLeft_4th)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintLeft_4th>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_4th_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
