// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PaintMap/Right/Bridge/PWPaintRight_Bridge4.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPaintRight_Bridge4() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintRight_Bridge4_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintRight_Bridge4();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWPaintRight_Bridge4::StaticRegisterNativesAPWPaintRight_Bridge4()
	{
	}
	UClass* Z_Construct_UClass_APWPaintRight_Bridge4_NoRegister()
	{
		return APWPaintRight_Bridge4::StaticClass();
	}
	struct Z_Construct_UClass_APWPaintRight_Bridge4_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPaintRight_Bridge4_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWPaintCanvas,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintRight_Bridge4_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaintMap/Right/Bridge/PWPaintRight_Bridge4.h" },
		{ "ModuleRelativePath", "Public/PaintMap/Right/Bridge/PWPaintRight_Bridge4.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPaintRight_Bridge4_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPaintRight_Bridge4>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPaintRight_Bridge4_Statics::ClassParams = {
		&APWPaintRight_Bridge4::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWPaintRight_Bridge4_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPaintRight_Bridge4_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPaintRight_Bridge4()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPaintRight_Bridge4_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPaintRight_Bridge4, 2067123410);
	template<> PAINTWAR_API UClass* StaticClass<APWPaintRight_Bridge4>()
	{
		return APWPaintRight_Bridge4::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPaintRight_Bridge4(Z_Construct_UClass_APWPaintRight_Bridge4, &APWPaintRight_Bridge4::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPaintRight_Bridge4"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPaintRight_Bridge4);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
