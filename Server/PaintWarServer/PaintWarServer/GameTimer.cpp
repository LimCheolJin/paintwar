#include "GameTimer.h"
#include <iostream>
#include <chrono>
#include <thread>

using namespace std;
using namespace chrono;

GameTimer::GameTimer()
{
	m_TimerCount = 300;
}


GameTimer::~GameTimer()
{
}

void GameTimer::ProcessTimer()
{
	--m_TimerCount;
}


int GameTimer::GetTimerCount() const
{
	return m_TimerCount;
}
void GameTimer::SetTimerCount(int count)
{
	m_TimerCount = count;
}