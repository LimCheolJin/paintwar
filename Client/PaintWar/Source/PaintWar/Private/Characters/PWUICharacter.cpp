// Fill out your copyright notice in the Description page of Project Settings.


#include "PWUICharacter.h"
#include "PWBomberWeapon.h"
#include "PWShooterWeapon.h"
#include "PWRollerWeapon.h"

// Sets default values
APWUICharacter::APWUICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM"));
	if (SK_MESH.Succeeded()) //성공하면SkeletalMesh'/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM'
	{
		GetMesh()->SetSkeletalMesh(SK_MESH.Object); // set 스켈레탈메쉬
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_GUN(TEXT("/Game/TestAssets/TestWeapon/Gun/gun_200324.gun_200324"));
	if (ST_GUN.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		Gun  = ST_GUN.Object; //StaticMesh'/Game/TestAssets/TestWeapon/Gun/gun_200324.gun_200324'
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_BOMB(TEXT("/Game/TestAssets/TestWeapon/Weapons/Gun/FPWeapon/Bomb/Bomb_01.Bomb_01"));
	if (ST_BOMB.Succeeded())
	{
		Bomb = ST_BOMB.Object;
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_ROLLER(TEXT("/Game/TestAssets/TestWeapon/Roller/roller_final_200323_bonche.roller_final_200323_bonche"));
	if (ST_ROLLER.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		Roller = ST_ROLLER.Object;
	}

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);

	//Scale
	GetMesh()->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	const auto& CapsuleComponent = GetCapsuleComponent();
	//Scale
	//CapsuleComponent->SetCapsuleSize(30.0f, 90.0f);
	// 원래 3.0f 9.0f였음.
	CapsuleComponent->SetCapsuleSize(4.0f, 8.0f);
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -9.0f), FRotator(0.0f, -90.0f, 0.0f));
	CapsuleComponent->SetCollisionProfileName(TEXT("Pawn"));

	/*FName WeaponSocket(TEXT("hand_rSocketBomber"));

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	Weapon = GetWorld()->SpawnActor<APWBomberWeapon>(FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (nullptr != Weapon)
	{
		Weapon->SetOwner(this);
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	}*/

}

// Called when the game starts or when spawned
void APWUICharacter::BeginPlay()
{
	Super::BeginPlay();
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	Weapon = GetWorld()->SpawnActor<APWBomberWeapon>(FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (nullptr != Weapon)
	{
		Weapon->SetOwner(this);
		//Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	}
}

// Called every frame
void APWUICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APWUICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void APWUICharacter::SetShooterWeapon()
{

	Weapon->WeaponSTMesh->SetStaticMesh(Gun); //StaticMesh'/Game/TestAssets/TestWeapon/Gun/gun_200324.gun_200324'
	//Weapon->WeaponSTMesh->SetRelativeScale3D(FVector(4.0f, 4.0f, 4.0f));
	Weapon->WeaponSTMesh->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	FName WeaponSocket(TEXT("hand_rSocketShooter"));

	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	
}

void APWUICharacter::SetBomberWeapon()
{
	
	Weapon->WeaponSTMesh->SetStaticMesh(Bomb);
	//Weapon->WeaponSTMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	Weapon->WeaponSTMesh->SetWorldScale3D(FVector(0.125f, 0.125f, 0.125f));
	FName WeaponSocket(TEXT("hand_rSocketBomber"));

	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
}

void APWUICharacter::SetRollerWeapon()
{
	
	Weapon->WeaponSTMesh->SetStaticMesh(Roller);
	//Weapon->WeaponSTMesh->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));
	Weapon->WeaponSTMesh->SetWorldScale3D(FVector(0.25f, 0.25f, 0.25f));
	FName WeaponSocket(TEXT("hand_rSocketRoller"));

	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
}

APWBaseWeapon* APWUICharacter::GetUIWeapon() const
{
	return Weapon;
}