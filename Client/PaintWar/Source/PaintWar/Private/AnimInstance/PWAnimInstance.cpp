// Fill out your copyright notice in the Description page of Project Settings.


#include "PWAnimInstance.h"
#include "PWRollerCharacter.h"

UPWAnimInstance::UPWAnimInstance()
{
	CurrentPawnSpeed = 0.0f;
	IsInAir = false;
	IsDead = false;
}

void UPWAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//현재 이 애님인스턴스를 적용하고있는 pawn을 가져온다.
	auto Pawn = TryGetPawnOwner();

	//유효한지 체크

	if (IsValid(Pawn))
	{
		//체크완료하면 현재 Pawn의 speed를 CurrentPawnSpeed에 set해준다.
		CurrentPawnSpeed = Pawn->GetVelocity().Size();

		//폰을 캐릭터로 캐스팅하는이유: MovementComponent에 접근하기위함.
		auto Character = Cast<APWBaseCharacter>(Pawn);
		if (nullptr != Character)
		{
			IsInAir = Character->GetMovementComponent()->IsFalling();
			
			IsReload = Character->GetIsReload();

			if (Character->GetCurrentCharacterState() == ECharacterState::DEATH)
			{
				IsDead = true;
			}
			else if (Character->GetCurrentCharacterState() == ECharacterState::ALIVE)
			{
				IsDead = false;
			}
			
		}


		////현재 폰이 롤러캐릭터이면?
		//auto RollerCharacter = Cast<APWRollerCharacter>(Character);
		//
		//if (RollerCharacter != nullptr)
		//{
		//	IsPainting = RollerCharacter->GetbPainting();
		//}
	}

	

}