// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APWBaseWeapon;
#ifdef PAINTWAR_PWUICharacter_generated_h
#error "PWUICharacter.generated.h already included, missing '#pragma once' in PWUICharacter.h"
#endif
#define PAINTWAR_PWUICharacter_generated_h

#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetUIWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(APWBaseWeapon**)Z_Param__Result=P_THIS->GetUIWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetRollerWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetRollerWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetBomberWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetBomberWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetShooterWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetShooterWeapon(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetUIWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(APWBaseWeapon**)Z_Param__Result=P_THIS->GetUIWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetRollerWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetRollerWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetBomberWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetBomberWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetShooterWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetShooterWeapon(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWUICharacter(); \
	friend struct Z_Construct_UClass_APWUICharacter_Statics; \
public: \
	DECLARE_CLASS(APWUICharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWUICharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWUICharacter(); \
	friend struct Z_Construct_UClass_APWUICharacter_Statics; \
public: \
	DECLARE_CLASS(APWUICharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWUICharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWUICharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWUICharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWUICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWUICharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWUICharacter(APWUICharacter&&); \
	NO_API APWUICharacter(const APWUICharacter&); \
public:


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWUICharacter(APWUICharacter&&); \
	NO_API APWUICharacter(const APWUICharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWUICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWUICharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWUICharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWUICharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Characters_PWUICharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
