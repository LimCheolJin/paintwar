// Fill out your copyright notice in the Description page of Project Settings.


#include "PWShooterCharacter.h"
#include "PWShooterWeapon.h"
#include "PWShooterProjectile.h"
#include "PWSpawnPoint.h"
#include "Materials/MaterialInstanceConstant.h"
#include "Components/ArrowComponent.h"
#include "PWShooterAIController.h"
#include "PWGameInstance.h"
#include "PWGameMode.h"
#include "PWShooterAnimInstance.h"
#include "Materials/MaterialInstance.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"

APWShooterCharacter::APWShooterCharacter()
{
	
	
	//this->SetReplicates(true);
	static ConstructorHelpers::FClassFinder<UAnimInstance> Shooter_Anim(TEXT("/Game/SpaceExplorer/Anims/BP_ShooterCharacterAnim.BP_ShooterCharacterAnim_C"));
	if (Shooter_Anim.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(Shooter_Anim.Class);
	}

	IsFire = false;
	CharacterType = ESelectCharacter::SHOOTER;

	AmountPaint = 100.0f;

	AIControllerClass = APWShooterAIController::StaticClass();
	//AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;


	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetGreen(TEXT("/Game/Particles/P_ShooterRed.P_ShooterRed"));
	if (ParticleAssetGreen.Succeeded())
	{
		//PaintParticleRight->SetTemplate(ParticleAssetRight.Object);
		RedParticle = ParticleAssetGreen.Object;
	} //왼오른 둘다 같은거면 하나로만 불러와서쓸수잇을거같음 일단 2개
	
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetRed(TEXT("/Game/Particles/P_ShooterGreen.P_ShooterGreen"));
	if (ParticleAssetRed.Succeeded())
	{
		//PaintParticleRight->SetTemplate(ParticleAssetRight.Object);
		GreenParticle = ParticleAssetRed.Object;
			
	} //왼오른 둘다 같은거면 하나로만 불러와서쓸수잇을거같음 일단 2개
	SkillTime = 0;

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSoundObject(TEXT("/Game/Sound/ShooterAttackSound.ShooterAttackSound"));
	if (AttackSoundObject.Succeeded())
	{
		AttackSound = AttackSoundObject.Object;

		AttackSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AttackSoundComponent"));
		AttackSoundComponent->SetupAttachment(RootComponent);

		AttackSoundComponent->SetSound(AttackSound);
	}


}

void APWShooterCharacter::BeginPlay()
{
	//APWBaseCharacter::BeginPlay();
	Super::BeginPlay();
	PWLOG(Error, TEXT("Shooter BeginPlay Start"));
	TSubclassOf<AActor> ClassParam = APWSpawnPoint::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassParam, SpawnPointArray);

	if (!GetName().Compare( FString("RedShooter")))   //같으면 0을반환함.
	{
		SetTeam(ETeam::RED);
		PWLOG(Warning, TEXT("RED"));
		GetMesh()->SetMaterial(0, RedTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if(SpawnPoint->GetTeamColor() == ETeam::RED)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}
	else if (!GetName().Compare( FString("GreenShooter") ))
	{
		SetTeam(ETeam::GREEN);
		PWLOG(Warning, TEXT("GREEN"));
		GetMesh()->SetMaterial(0, GreenTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if(SpawnPoint->GetTeamColor() == ETeam::GREEN)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}
	

	FName WeaponSocket(TEXT("hand_rSocketShooter"));

	//auto CurWeapon = GetWorld()->SpawnActor<APWShooterWeapon>(FVector(-10.0f, -18.0f, 32.0f), FRotator(-90.0f, 0.0f,0.0f));
	//auto CurWeapon = GetWorld()->SpawnActor<APWShooterWeapon>(FVector::ZeroVector, FRotator::ZeroRotator);
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	Weapon = GetWorld()->SpawnActor<APWShooterWeapon>(FVector::ZeroVector, FRotator::ZeroRotator,SpawnParams);
	if (nullptr != Weapon)
	{
		Weapon->SetOwner(this);
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	}

	ShooterAnim = Cast<UPWShooterAnimInstance>(GetMesh()->GetAnimInstance());
	if (ShooterAnim != nullptr)
	{
		ShooterAnim->OnFireNotify.AddUObject(this, &APWShooterCharacter::OnFire);
		ShooterAnim->OnFireNotify1.AddUObject(this, &APWShooterCharacter::OnFire);
		ShooterAnim->OnReloadNotify.AddUObject(this, &APWBaseCharacter::ReloadProcess);
	}

	AttackSoundComponent->Stop();
}
void APWShooterCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	//auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	//if (IsFire == true)//PWGameMode->Get_m_MyCharacter() == this)
	//	OnFire();

	
	//if (this->CurrentCharacterState == ECharacterState::DEATH)
	//	return;
	//auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	//if (PWGameMode->Get_m_MyCharacter() != this)
	//	return;
	//if (AmountPaint <= 0)
	//	IsFire = false;
	//if (IsFire)
	//{
	//	//PWLOG(Error, TEXT("OnFire()"));
	//	//멀티면 내가쐇다는것을 알려줘야햔다.
	//	//auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

	//	//게임모드가 멀티이면서 내캐릭터와 같으면 보내줘야한다.
	//	if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI && (PWGameMode->Get_m_MyCharacter() == this))
	//	{

	//		//PWGameMode
	//		PWGameMode->SendShooterFire(IsFire);
	//	}

	//	auto ProjectileSpawnPointInstance = Cast<APWShooterWeapon>(Weapon)->GetProjectileSpawnPoint();
	//	auto TempTransform = ProjectileSpawnPointInstance->GetComponentTransform();
	//	FVector ProjectileLocation = TempTransform.GetLocation();
	//	FRotator ProjectileRotation = TempTransform.GetRotation().Rotator();
	//	FActorSpawnParameters SpawnParams;
	//	SpawnParams.Owner = this; // 일단무기로 해놧음 예제는 무기가아니고 캐릭터임
	//	//SpawnParams.Instigator = Instigator;

	//	//PWLOG(Warning, CollisionSphere->GetGenerateOverlapEvents() ? TEXT("overlap True") : TEXT("overlap False"));
	//	//총알과총알끼리는 충돌안되게해야댐.

	//	//파티클활성화
	//	Cast<APWShooterWeapon>(Weapon)->GetShooterParticle()->SetActive(true);

	//	auto Projectile = GetWorld()->SpawnActor<APWShooterProjectile>(ProjectileLocation, ProjectileRotation, SpawnParams);

	//	if (Projectile)
	//	{
	//		Projectile->SetOwner(this);
	//		//하드코딩했음 이거나중에 고칩시다.
	//		FVector Direction = ProjectileRotation.Vector();
	//		Projectile->FireInDirection(Direction);

	//	}
	//	else
	//	{
	//		//PWLOG(Warning, TEXT("Projectile None"));
	//	}

	//	//PWLOG(Warning, TEXT("Amount Paint is %d"), AmountPaint);

	//	//잠시 주석해놓자. 
	//	//--AmountPaint;
	//	/*if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::SINGLE)
	//		GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &APWShooterCharacter::OnFire, 0.1f, false);
	//	else if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI)
	//	{
	//		if (PWGameMode->Get_m_MyCharacter() == this)
	//			GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &APWShooterCharacter::OnFire, 0.1f, false);
	//	}*/
	//}
}
void APWShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APWBaseCharacter::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &APWShooterCharacter::Fire);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &APWShooterCharacter::UnAbleFire);
}
void APWShooterCharacter::OnFire()
{

	
	if (this->CurrentCharacterState == ECharacterState::DEATH)
		return;
	auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (AmountPaint <= 0.0f)
	{
		IsFire = false;
		
		//게임모드가 멀티이면서 내캐릭터와 같으면 보내줘야한다.
		if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI && (PWGameMode->Get_m_MyCharacter() == this))
		{

			//PWGameMode
			PWGameMode->SendShooterFire(IsFire);
		}
		return;
	}
	
	/*if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI && (PWGameMode->Get_m_MyCharacter() == this) )*/
	//{
		auto ProjectileSpawnPointInstance = Cast<APWShooterWeapon>(Weapon)->GetProjectileSpawnPoint();
		auto TempTransform = ProjectileSpawnPointInstance->GetComponentTransform();
		FVector ProjectileLocation = TempTransform.GetLocation();
		FRotator ProjectileRotation = TempTransform.GetRotation().Rotator();
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		auto Projectile = GetWorld()->SpawnActor<APWShooterProjectile>(ProjectileLocation, ProjectileRotation, SpawnParams);
		if (Projectile)
		{
			Projectile->SetOwner(this);
			//하드코딩했음 이거나중에 고칩시다.
			FVector Direction = ProjectileRotation.Vector();
			Projectile->FireInDirection(Direction);
			if(SkillON == true)
				Projectile->Skill();
		}
		AmountPaint -= 0.5f;

		UsedPaint += 0.5f;

		if (UsedPaint >= 100) {
			if (SkillCount < 3)
				SkillCount += 1;
			UsedPaint = 0;
		}

		
		////////////////////////////////파티클

		if (CurrentTeam == ETeam::RED) {
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), RedParticle, TempTransform);
		}
		else
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GreenParticle, TempTransform);
		}		

	//}

	
}
void APWShooterCharacter::Fire()
{
	AttackSoundComponent->Play();
	if (IsFire == false)
	{
		IsFire = true;
		PWLOG(Error, TEXT("APWShooterCharacter's Fire()"));
		//OnFire();
		auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		//게임모드가 멀티이면서 내캐릭터와 같으면 보내줘야한다.
		if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI && (PWGameMode->Get_m_MyCharacter() == this))
		{

			//PWGameMode
			PWGameMode->SendShooterFire(IsFire);
		}
		//PWLOG(Warning, TEXT("%d"), AmountPaint);
	}
}
 
bool APWShooterCharacter::GetIsFire() const
{
	return IsFire;
}

void APWShooterCharacter::UnAbleFire()
{
	AttackSoundComponent->Stop();
	if (IsFire == true)
	{
		IsFire = false;
		//파티클 비활성화
		Cast<APWShooterWeapon>(Weapon)->GetShooterParticle()->SetActive(false);
		//PWLOG(Error, TEXT("UnAbleFire()"));

		auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		//게임모드가 멀티이면서 내캐릭터와 같으면 보내줘야한다.
		if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI && (PWGameMode->Get_m_MyCharacter() == this))
		{

			//PWGameMode
			PWGameMode->SendShooterFire(IsFire);
		}
	}
}

void APWShooterCharacter::SpawnShooterProjectile(FVector Pos)
{
	auto ProjectileSpawnPointInstance = Cast<APWShooterWeapon>(Weapon)->GetProjectileSpawnPoint();
	auto& TempTransform = ProjectileSpawnPointInstance->GetComponentTransform();

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;


	auto Projectile = GetWorld()->SpawnActor<APWShooterProjectile>(Pos, TempTransform.Rotator(), SpawnParams);
	if (Projectile)
	{
		Projectile->SetOwner(this);
		//하드코딩했음 이거나중에 고칩시다.
		FVector Direction = TempTransform.Rotator().Vector();
		Projectile->FireInDirection(Direction);
	}

}


void APWShooterCharacter::SetIsFire(bool fire)
{
	IsFire = fire;
}


void APWShooterCharacter::Skill()
{

	if (SkillCount >= 1) {
		GetWorldTimerManager().SetTimer(SkillTimerHandle, this, &APWShooterCharacter::SKillCountDown, 1.0f, true);
		SkillTime=5;
		SkillON = true;
		SkillCount -= 1;
	}
}

void APWShooterCharacter::SKillCountDown() {

	SkillTime--;
	if (SkillTime <= 0)
	{
		GetWorldTimerManager().ClearTimer(SkillTimerHandle);
		SkillON = false;
	}
}