// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PaintWar_generated_h
#error "PaintWar.generated.h already included, missing '#pragma once' in PaintWar.h"
#endif
#define PAINTWAR_PaintWar_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintWar_h


#define FOREACH_ENUM_EGAMEMODE(op) \
	op(EGameMode::NONE) \
	op(EGameMode::SINGLE) \
	op(EGameMode::MULTI) 

enum class EGameMode : uint8;
template<> PAINTWAR_API UEnum* StaticEnum<EGameMode>();

#define FOREACH_ENUM_ESELECTCHARACTER(op) \
	op(ESelectCharacter::NONE) \
	op(ESelectCharacter::SHOOTER) \
	op(ESelectCharacter::ROLLER) \
	op(ESelectCharacter::BOMBER) 

enum class ESelectCharacter : uint8;
template<> PAINTWAR_API UEnum* StaticEnum<ESelectCharacter>();

#define FOREACH_ENUM_ETEAM(op) \
	op(ETeam::NONE) \
	op(ETeam::RED) \
	op(ETeam::GREEN) 

enum class ETeam : uint8;
template<> PAINTWAR_API UEnum* StaticEnum<ETeam>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
