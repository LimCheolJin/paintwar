// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EditableTextBox.h"
#include "TextBlock.h"
#include "ProgressBar.h"
#include "ScrollBox.h"
#include "TextBlock.h"
#include "PaintWar.h"
#include "PWNetWork.h"
#include "Blueprint/UserWidget.h"
#include "PWUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API UPWUserWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;

	UPROPERTY()
	UTextBlock* LoginIDText;
	void SetLoginID(FString id);

	UPROPERTY()
	UScrollBox* SBChatLog;
	void RecvChat( FString id);

	void SetChat(wchar_t* recvchat);

	UPROPERTY()
	UEditableTextBox* TBChatText;

	void SetChatFocus();

	class PWNetWork* m_PWNetWork;
};
