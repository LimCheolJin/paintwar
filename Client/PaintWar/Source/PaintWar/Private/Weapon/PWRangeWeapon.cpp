// Fill out your copyright notice in the Description page of Project Settings.


#include "PWRangeWeapon.h"
#include "Components/ArrowComponent.h"

// Sets default values
APWRangeWeapon::APWRangeWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Scale
	
}
UArrowComponent* APWRangeWeapon::GetProjectileSpawnPoint()
{
	return ProjectileSpawnPoint;
}

void APWRangeWeapon::Tick(float DeltaTime)
{
	APWBaseWeapon::Tick(DeltaTime);
}