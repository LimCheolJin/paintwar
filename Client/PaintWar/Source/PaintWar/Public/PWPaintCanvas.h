// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWPaintCanvas.generated.h"

UCLASS(Blueprintable, BlueprintType)
class PAINTWAR_API APWPaintCanvas : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWPaintCanvas();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = Canvas)
	UStaticMeshComponent* CanvasMesh;

	UPROPERTY(BlueprintReadWrite, Category = Canvas)
	class UTextureRenderTarget2D* RenderTarget;

	UPROPERTY(BlueprintReadWrite, Category = Canvas)
	class UMaterialInterface* CanvasMaterial;


	//UPROPERTY(BlueprintReadWrite, Category = Canvas)
	//class UMaterialInterface* BrushMaterial;
	////class UMaterialInstanceDynamic* BrushMaterial;

	//UPROPERTY(BlueprintReadWrite, Category = Canvas)
	//class UTexture2D* BrushTexture;

	UPROPERTY()
	class UMaterialInstanceDynamic* CanvasMaterialInstance;

	//UPROPERTY()
	//class UMaterialInstanceDynamic* BrushMaterialInstance;

	UPROPERTY()
	class UMaterialInterface* InitCanvasMaterial;

	UPROPERTY()
	class UMaterialInstanceDynamic* InitCanvasMaterialInstance;

	UPROPERTY()
	class UTexture2D* InitTexture;

	UPROPERTY()
	class UCanvas* Canvas;
public:
	void DrawBrush(FVector2D DrawLocation, float BrushSize, class UMaterialInstanceDynamic* BrushMaterialInstance);
};
