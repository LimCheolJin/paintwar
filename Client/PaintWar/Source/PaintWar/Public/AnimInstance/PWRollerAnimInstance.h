// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWAnimInstance.h"
#include "PWRollerAnimInstance.generated.h"

/**
 * 
 */

DECLARE_MULTICAST_DELEGATE(FOnReloadNotifyDelegate);
DECLARE_MULTICAST_DELEGATE(FOnPaintNotifyDelegate);

UCLASS()
class PAINTWAR_API UPWRollerAnimInstance : public UPWAnimInstance
{
	GENERATED_BODY()
public:
	UPWRollerAnimInstance();


	FOnReloadNotifyDelegate OnReloadNotify;

	FOnPaintNotifyDelegate OnPaintNotify;


	UFUNCTION()
	void AnimNotify_ReloadNotify();

	UFUNCTION()
	void AnimNotify_PaintNotify();
protected:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
private:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsPainting;
};
