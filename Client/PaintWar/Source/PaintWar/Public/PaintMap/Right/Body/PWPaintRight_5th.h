// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintRight_5th.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintRight_5th : public APWPaintCanvas
{
	GENERATED_BODY()
public:
	APWPaintRight_5th();
};
