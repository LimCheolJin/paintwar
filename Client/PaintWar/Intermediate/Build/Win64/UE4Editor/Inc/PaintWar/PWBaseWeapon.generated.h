// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMeshComponent;
#ifdef PAINTWAR_PWBaseWeapon_generated_h
#error "PWBaseWeapon.generated.h already included, missing '#pragma once' in PWBaseWeapon.h"
#endif
#define PAINTWAR_PWBaseWeapon_generated_h

#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeaponSTMesh) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UStaticMeshComponent**)Z_Param__Result=P_THIS->GetWeaponSTMesh(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeaponSTMesh) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UStaticMeshComponent**)Z_Param__Result=P_THIS->GetWeaponSTMesh(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBaseWeapon(); \
	friend struct Z_Construct_UClass_APWBaseWeapon_Statics; \
public: \
	DECLARE_CLASS(APWBaseWeapon, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWBaseWeapon(); \
	friend struct Z_Construct_UClass_APWBaseWeapon_Statics; \
public: \
	DECLARE_CLASS(APWBaseWeapon, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBaseWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBaseWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseWeapon(APWBaseWeapon&&); \
	NO_API APWBaseWeapon(const APWBaseWeapon&); \
public:


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseWeapon(APWBaseWeapon&&); \
	NO_API APWBaseWeapon(const APWBaseWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBaseWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBaseWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Weapon_PWBaseWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
