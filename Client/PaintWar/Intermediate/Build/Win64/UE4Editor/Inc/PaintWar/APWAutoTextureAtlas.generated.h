// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_APWAutoTextureAtlas_generated_h
#error "APWAutoTextureAtlas.generated.h already included, missing '#pragma once' in APWAutoTextureAtlas.h"
#endif
#define PAINTWAR_APWAutoTextureAtlas_generated_h

#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAPWAutoTextureAtlas(); \
	friend struct Z_Construct_UClass_AAPWAutoTextureAtlas_Statics; \
public: \
	DECLARE_CLASS(AAPWAutoTextureAtlas, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(AAPWAutoTextureAtlas)


#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAPWAutoTextureAtlas(); \
	friend struct Z_Construct_UClass_AAPWAutoTextureAtlas_Statics; \
public: \
	DECLARE_CLASS(AAPWAutoTextureAtlas, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(AAPWAutoTextureAtlas)


#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAPWAutoTextureAtlas(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAPWAutoTextureAtlas) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAPWAutoTextureAtlas); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAPWAutoTextureAtlas); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAPWAutoTextureAtlas(AAPWAutoTextureAtlas&&); \
	NO_API AAPWAutoTextureAtlas(const AAPWAutoTextureAtlas&); \
public:


#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAPWAutoTextureAtlas(AAPWAutoTextureAtlas&&); \
	NO_API AAPWAutoTextureAtlas(const AAPWAutoTextureAtlas&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAPWAutoTextureAtlas); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAPWAutoTextureAtlas); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAPWAutoTextureAtlas)


#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class AAPWAutoTextureAtlas>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_APWAutoTextureAtlas_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
