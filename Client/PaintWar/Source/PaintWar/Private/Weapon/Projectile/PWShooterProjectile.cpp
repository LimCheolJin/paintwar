// Fill out your copyright notice in the Description page of Project Settings.

#include "PWShooterProjectile.h"
#include "Engine/Engine.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PWPaintCanvas.h"
#include "Materials/MaterialInstanceConstant.h"
#include "PWBaseCharacter.h"
#include "PWShooterCharacter.h"
#include "PWPaintLeft_1st.h"
#include "PWPaintLeft_2nd.h"
#include "PWPaintLeft_3rd.h"
#include "PWPaintLeft_4th.h"
#include "PWPaintLeft_5th.h"
#include "PWPaintLeft_6th.h"
#include "PWPaintLeft_Bridge1.h"
#include "PWPaintLeft_Bridge2.h"
#include "PWPaintLeft_Bridge3.h"
#include "PWPaintLeft_Bridge4.h"
#include "PWPaintLeft_Bridge5.h"
#include "PWPaintRight_1st.h"
#include "PWPaintRight_2nd.h"
#include "PWPaintRight_3rd.h"
#include "PWPaintRight_4th.h"
#include "PWPaintRight_5th.h"
#include "PWPaintRight_6th.h"
#include "PWPaintRight_Bridge1.h"
#include "PWPaintRight_Bridge2.h"
#include "PWPaintRight_Bridge3.h"
#include "PWPaintRight_Bridge4.h"
#include "PWPaintRight_Bridge5.h"


#include "DrawDebugHelpers.h"

APWShooterProjectile::APWShooterProjectile()
{
	//Scale
	//this->SetReplicates(true);
	CollisionSphere->InitSphereRadius(2.5f);  //0421 
	//CollisionSphere->InitSphereRadius(5.0f);
	//CollisionSphere->InitSphereRadius(6.0f);
	//CollisionSphere->SetGenerateOverlapEvents(true); //"/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"
	///Game/TestAssets/TestWeapon/Bomb/bomb_200324.bomb_200324
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_PROJECTILE(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if (ST_PROJECTILE.Succeeded())
	{
		ProjectileMesh->SetStaticMesh(ST_PROJECTILE.Object);
	}
	
	//Scale

	ProjectileMesh->SetRelativeScale3D(FVector(0.045f, 0.045f, 0.045f)); //0420
	
	//ProjectileMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
	//ProjectileMesh->bNeverDistanceCull = true;
	//Scale
	//ProjectileMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -15.0f)); // ���ͷΕ���
	//ProjectileMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -1.0f));
	ProjectileMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -2.0f)); // 0420
	

	ProjectileMovementComponent->SetUpdatedComponent(CollisionSphere); // ���ͷΕ���
	
	//Scale
	//ProjectileMovementComponent->InitialSpeed = 1500.0f;
	ProjectileMovementComponent->InitialSpeed = 600.0f;
	//ProjectileMovementComponent->MaxSpeed = 1500.0f;
	ProjectileMovementComponent->MaxSpeed = 600.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;

	//CollisionSphere->SetCollisionProfileName(TEXT("ShooterProjectile"));
	//CollisionSphere->BodyInstance.SetCollisionProfileName(TEXT("ShooterProjectile")); // 0421
	CollisionSphere->BodyInstance.SetCollisionProfileName(TEXT("BomberProjectile"));
	CollisionSphere->SetGenerateOverlapEvents(true);

	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_MeshRed(TEXT("/Game/TestAssets/Materials/M_RedColor_Inst.M_RedColor_Inst"));
	if (M_MeshRed.Succeeded())//
	{
		ProjectileMeshRedMaterial = M_MeshRed.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_MeshGreen(TEXT("/Game/TestAssets/Materials/M_GreenColor_Inst.M_GreenColor_Inst"));
	if (M_MeshGreen.Succeeded())
	{
		ProjectileMeshGreenMaterial = M_MeshGreen.Object;
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> T_ShooterBrushMRed(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/RedSplat2.RedSplat2"));
	if (T_ShooterBrushMRed.Succeeded())
	{
		BrushTextureRed = T_ShooterBrushMRed.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> T_ShooterBrushMGreen(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/GreenSplat2.GreenSplat2"));
	if (T_ShooterBrushMGreen.Succeeded())
	{
		BrushTextureGreen = T_ShooterBrushMGreen.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	//��ƼŬ
	static ConstructorHelpers::FObjectFinder<UParticleSystem> P_PaintRedParticle(TEXT("/Game/Particles/P_SplashRed.P_SplashRed"));
	if (P_PaintRedParticle.Succeeded())
	{
		//ProjectileParticle->SetTemplate(P_PaintRedParticle.Object);
		ParticleRed = P_PaintRedParticle.Object;
	}
	static ConstructorHelpers::FObjectFinder<UParticleSystem> P_PaintGreenParticle(TEXT("/Game/Particles/P_SplashGreen.P_SplashGreen"));
	if (P_PaintGreenParticle.Succeeded())
	{
		//ProjectileParticle->SetTemplate(P_PaintRedParticle.Object);
		ParticleGreen = P_PaintGreenParticle.Object;
	}
	ProjectileParticle->SetupAttachment(RootComponent);
	ProjectileParticle->bAutoActivate = false;
	//


	//BrushSize = 400.0f;
	BrushSize = 200.0f;
	Damage = 10.0f;
}

void APWShooterProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{//���Ϳ� �չ��� �θ��� base���� Onhit�� �����Ҽ������� ���� �����Ѵ�.
	if (GetOwner() == Hit.GetActor()) //otherActor�� �� �����̶��?
	{
		//PWLOG(Warning, TEXT("SELF OVER LAP"));
		return;
	}

	const auto& BaseCharacter = Cast<APWBaseCharacter>(OtherActor);
	const auto& Owner = Cast<APWBaseCharacter>(GetOwner());

	//PWLOG(Warning, TEXT("Other Actor's name is %s "), *OtherActor->GetName());
	if (Cast<APWPaintCanvas>(Hit.GetActor()))//Cast<APWPaintCanvas>(OtherActor) != nullptr)
	{

		ProjectileParticle->SetActive(true);
		ProjectileMesh->SetHiddenInGame(true);

		FVector TraceStart;
		FVector TraceEnd;
		FHitResult HitResult;

		TraceStart = GetActorLocation();
		TraceEnd = GetActorLocation() + (GetActorForwardVector() * 20.0f); //���� * 10.0f

		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.bTraceComplex = true;
		CollisionQueryParams.AddIgnoredActor(this);
		CollisionQueryParams.bReturnFaceIndex = true;

		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, true);
		//if( UKismetSystemLibrary::LineTraceSingle(this, TraceStart, TraceEnd, ETraceTypeQuery::TraceTypeQuery1, true, TempArr, EDrawDebugTrace::Persistent, HitResult, true)
		if ((GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams)))
		{

			FVector2D UV{ 0.0f, 0.0f };
			//GetActor�� �̸�����
			//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("paint canvas overlapped - %d"), Cast<APWShooterCharacter>(GetOwner())->Get_m_ID(), false));
			auto HitActor = HitResult.GetActor();

			if (Cast<APWPaintLeft_1st>(HitActor)) // �̺κ� if - else if �� 22���� ��� ��������.. ..............
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_1st>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_4th>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}

			//PWLOG(Warning, TEXT("Trace Succeed !!"));
		}
		else
		{
			//PWLOG(Warning, TEXT("failed Line Trace"));
		}
	}
	else if (BaseCharacter != nullptr && (Owner->GetTeam() != BaseCharacter->GetTeam()))
	{
		BaseCharacter->TakenDamage(Cast<APWBaseCharacter>(this->GetOwner()), this->Damage);
	}
	
	this->SetLifeSpan(0.5f);
	//Destroy();
}

void APWShooterProjectile::BeginPlay()
{

	APWBaseProjectile::BeginPlay();

	//CollisionSphere->OnComponentHit.AddDynamic(this, &APWShooterProjectile::OnHit);
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APWShooterProjectile::OnOverlapBegin); //0421
	CollisionSphere->OnComponentHit.AddDynamic(this, &APWShooterProjectile::OnHit);
	//this->SetLifeSpan(1.0f); 0421

	if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::RED)
	{

		ProjectileMesh->SetMaterial(0, ProjectileMeshRedMaterial);
		ProjectileParticle->SetTemplate(ParticleRed);

	}
	else if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::GREEN)
	{
		ProjectileMesh->SetMaterial(0, ProjectileMeshGreenMaterial);
		ProjectileParticle->SetTemplate(ParticleGreen);
	}
	ProjectileParticle->SetRelativeScale3D(FVector(0.4f, 0.4f, 0.4f));

}

void APWShooterProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{// ������ ó���� �����̴�. hit�� overlap ���߿� �ϳ��� ����ϰ�������
	// �ݸ��� ������ �Ϻ��ϰԵ����ʾƼ� �Ѿ˰� ĳ���Ͱ� overlap �ǹ�����

	// **�̺κ��� ���� ������� �ʴ� �ڵ��̴� ���� �����ؾߵȴ�. 2020.02.17

	if (GetOwner() == SweepResult.GetActor()) //otherActor�� �� �����̶��?
	{
		//PWLOG(Warning, TEXT("SELF OVER LAP"));
		return;
	}
	const auto& BaseCharacter = Cast<APWBaseCharacter>(OtherActor);
	const auto& Owner = Cast<APWBaseCharacter>(GetOwner());

	//PWLOG(Warning, TEXT("Other Actor's name is %s "), *OtherActor->GetName());
	
	if (BaseCharacter != nullptr && (Owner->GetTeam() != BaseCharacter->GetTeam()))
	{
		BaseCharacter->TakenDamage(Cast<APWBaseCharacter>(this->GetOwner()) ,this->Damage);
		Destroy();
		return;
	}
	
}

void APWShooterProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}


void APWShooterProjectile::Skill()
{
	ProjectileMesh->SetRelativeScale3D(FVector(0.072f, 0.072f, 0.072f)); //0420
	BrushSize = 300.0f;
	Damage = 50.0f;
	ProjectileParticle->SetRelativeScale3D(FVector(0.6f, 0.6f, 0.6f));
}

