// Fill out your copyright notice in the Description page of Project Settings.


#include "PWPlayerController.h"
#include "PWRollerCharacter.h"
#include "PWShooterCharacter.h"
#include "PWBomberCharacter.h"
#include "PWGameInstance.h"
#include "PWRollerAIController.h"
#include "PWBomberAIController.h"
#include "PWShooterAIController.h"
#include "PWGameMode.h"
#include "Blueprint/UserWidget.h"
#include "PWUserWidget.h"
#include "Components/Widget.h"

//플레이어가 입장하면 게임모드에의해 아래와같은순서로 관련액터들이 생성되고
//게임플레이를 위한 설정이 갖춰진다.

//1. 플레이어 컨트롤러의 생성
//2. 플레이어 폰의 생성
//3. 플레이어 컨트롤러가 플레이어 폰을 빙의
//4. 게임의 시작
//플레이어로그인 - PostLogin 이벤트(플레이어가 조종할 폰을 생성, 플레이어 컨트롤러가 해당폰에 빙의)
//폰과 플레이어 컨트롤러 생성 시점 - 각 액터의 PostInitializeComponents 함수로 파악가능
//빙의 진행시점 - 플레이어컨트롤러의 OnPossess,  폰의 PossessedBy

//접미사 _C는 블루프린트 에셋의 클래스정보를 각져올수있다.

void APWPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	PWLOG(Warning, TEXT("PlayerController's PostInitialize"));
}

void APWPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	PWLOG(Warning, TEXT("PlayerController's OnPossess"));
}


void APWPlayerController::BeginPlay()
{
	Super::BeginPlay();
	//입력모드를 set해준다.	
	PWLOG(Error, TEXT("PlayerController'sBeginPlay"));
	FInputModeGameOnly InputMode;
	SetInputMode(InputMode);
	
	//만약 possess한 캐릭터가 현재 존재하면 unpossess 해줘야할거같음.
	Mycontroller = GetWorld()->GetFirstPlayerController();

	FStringClassReference BloodWidgetClassRef(TEXT("/Game/UI/BloodEffect.BloodEffect_C"));
	if (UClass* BloodWidgetClass = BloodWidgetClassRef.TryLoadClass<UUserWidget>())
	{

		BloodWidget = CreateWidget<UUserWidget>(GetWorld(), BloodWidgetClass);
		BloodWidget->AddToPlayerScreen();
		WidgetColor = { 1.0f,1.0f,1.0f,0.0f };
		BloodWidget->SetColorAndOpacity(WidgetColor);
	}

	FStringClassReference BulletWidgetClassRef(TEXT("/Game/UI/BulletWidget.BulletWidget_C"));
	if (UClass* BulletWidgetClass = BulletWidgetClassRef.TryLoadClass<UUserWidget>())
	{
		BulletWidget = CreateWidget<UUserWidget>(GetWorld(), BulletWidgetClass);
	}

	FStringClassReference EscWidgetClassRef(TEXT("/Game/UI/GameMenu.GameMenu_C"));
	if (UClass* EscWidgetClass = EscWidgetClassRef.TryLoadClass<UUserWidget>())
	{
		EscMenuWidget = CreateWidget<UUserWidget>(GetWorld(), EscWidgetClass);
	}

	FStringClassReference ChatLogWidgetClassRef(TEXT("/Game/UI/ChatLogWidget.ChatLogWidget_C"));
	if (UClass* ChatLogWidgetClass = ChatLogWidgetClassRef.TryLoadClass<UUserWidget>())
	{
		ChatLogWidget = CreateWidget<UUserWidget>(GetWorld(), ChatLogWidgetClass);
	}

	FStringClassReference SkillCountWidgetClassRef(TEXT("/Game/UI/SkillCountWidget.SkillCountWidget_C"));
	if (UClass* SkillCountWidgetClass = SkillCountWidgetClassRef.TryLoadClass<UUserWidget>())
	{
		SkillCountWidget = CreateWidget<UUserWidget>(GetWorld(), SkillCountWidgetClass);
	}

	const auto& PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());
	/*TSubclassOf<AActor> ClassShooter = APWShooterCharacter::StaticClass();
	TSubclassOf<AActor> ClassRoller = APWRollerCharacter::StaticClass();
	TSubclassOf<AActor> ClassBomber = APWBomberCharacter::StaticClass();*/
	PlayerCameraManagerClass = APlayerCameraManager::StaticClass();
	PlayerCameraManager->ViewPitchMax = 89.0f;
	PlayerCameraManager->ViewPitchMin = -40.0f;
	TSubclassOf<AActor> ClassCharacter = APWBaseCharacter::StaticClass();

	TArray<AActor*> OutputActors;
	
	if (PWGameInstance != nullptr)
	{
		//////////////////////////////////////////////////나중에 게임시작하면 켜지는걸로
		BulletWidget->AddToPlayerScreen();
		SkillCountWidget->AddToPlayerScreen();


		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassCharacter, OutputActors);

		PWLOG(Warning, TEXT("BaseCharacter's Num is %d"), OutputActors.Num());
		if (PWGameInstance->GetCurrentGameMode() == EGameMode::MULTI && PWGameInstance->GetIsGameStart())
		{
			for (const auto& Actor : OutputActors)
			{

				//루프돌려서 게임인스턴스의 선택캐릭터정보와 팀정보를 월드상에 모든 BaseCharacter중에서 팀정보와 CharacterType을 비교하여 찾는다.
				//Find()가 있긴한데 Array라서 어케 찾는지 모르겠네 
				const auto& BaseCharacter = Cast<APWBaseCharacter>(Actor);
				if ((BaseCharacter->GetTeam() == PWGameInstance->GetCurrentSelectTeam()) && (BaseCharacter->GetCharacterType() == PWGameInstance->GetCurrentSelectCharacter()))
				{//찾았으면 Possess()
					auto MyGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
					
					int n = BaseCharacter->Get_m_ID();
					MyGameMode->SetCurrentPlayerID(n);
					auto players = MyGameMode->Get_m_Clients();
					auto it = players.find(n);
					MyGameMode->Set_m_MyCharacter(it->second);
					BaseCharacter->SetLoginID(PWGameInstance->GetLoginID());
					OnPossess(BaseCharacter);
					
					break;
				}
				
			}
			
		}
		else if (PWGameInstance->GetCurrentGameMode() == EGameMode::SINGLE)
		{
			for (const auto& Actor : OutputActors)
			{

				//루프돌려서 게임인스턴스의 선택캐릭터정보와 팀정보를 월드상에 모든 BaseCharacter중에서 팀정보와 CharacterType을 비교하여 찾는다.
				//Find()가 있긴한데 Array라서 어케 찾는지 모르겠네 
				const auto& BaseCharacter = Cast<APWBaseCharacter>(Actor);
				if ((BaseCharacter->GetTeam() == PWGameInstance->GetCurrentSelectTeam()) && (BaseCharacter->GetCharacterType() == PWGameInstance->GetCurrentSelectCharacter()))
				{//찾았으면 Possess()
					//AI에 이미 빙의되어있으면 풀자. 풀고해야한다.
					if (BaseCharacter->IsControlled())
					{
						auto controller = Cast<APWBaseAIController>(BaseCharacter->GetController());
						if(controller != nullptr)
							controller->OnUnPossess();
					}

					BaseCharacter->SetLoginID(PWGameInstance->GetLoginID());
					OnPossess(BaseCharacter);
					break;
				}

				//시작하면으로 바꿔야함
				

				//else if(BaseCharacter->GetCharacterType() == ESelectCharacter::SHOOTER)
				//{
				//	BaseCharacter->AIControllerClass = APWShooterAIController::StaticClass();
				//	Cast<APWShooterAIController>(BaseCharacter->AIControllerClass)->OnPossess(BaseCharacter);
				//	/*BaseCharacter->AIControllerClass = APWShooterAIController::StaticClass();
				//	BaseCharacter->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;*/
				//}
				//else if (BaseCharacter->GetCharacterType() == ESelectCharacter::BOMBER)
				//{
				//	BaseCharacter->AIControllerClass = APWBomberAIController::StaticClass();
				//	Cast<APWBomberAIController>(BaseCharacter->AIControllerClass)->OnPossess(BaseCharacter);
				//	/*BaseCharacter->AIControllerClass = APWBomberAIController::StaticClass();
				//	BaseCharacter->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;*/
				//}
				//else if (BaseCharacter->GetCharacterType() == ESelectCharacter::ROLLER)
				//{
				//	BaseCharacter->AIControllerClass = APWRollerAIController::StaticClass();
				//	Cast<APWRollerAIController>(BaseCharacter->AIControllerClass)->OnPossess(BaseCharacter);
				//	/*BaseCharacter->AIControllerClass = APWRollerAIController::StaticClass();
				//	BaseCharacter->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;*/
				//}
			}
		}
	}
	
}

void APWPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	////액션매핑 바인드
	InputComponent->BindAction(TEXT("Menu"), EInputEvent::IE_Pressed, this, &APWPlayerController::TabMenuOpen);
	InputComponent->BindAction(TEXT("Menu"), EInputEvent::IE_Released, this, &APWPlayerController::TabMenuClose);
	InputComponent->BindAction(TEXT("ESCMenu"), EInputEvent::IE_Pressed, this, &APWPlayerController::EscMenuOpen);
	InputComponent->BindAction(TEXT("Enter"), EInputEvent::IE_Pressed, this, &APWPlayerController::EnterMenuOpen);

}

void APWPlayerController::EnterMenuOpen()
{
	
	FInputModeGameAndUI UIinputmode;
	
	auto ChatUserWidget = Cast<UPWUserWidget>(ChatLogWidget);
	auto Mycharacter = Cast<APWBaseCharacter>(GetCharacter());

	if (!ChatLogWidget->IsVisible()) {
		Mycontroller->bShowMouseCursor = true;
		Mycontroller->bEnableClickEvents = true;
		Mycontroller->SetInputMode(UIinputmode);
		ChatLogWidget->AddToPlayerScreen();
		ChatUserWidget->bIsFocusable = true;
		ChatUserWidget->SetChatFocus();	
	}
	
}

void APWPlayerController::EnterMenuClose()
{
	auto ChatUserWidget = Cast<UPWUserWidget>(ChatLogWidget);
	auto Mycharacter = Cast<APWBaseCharacter>(GetCharacter());
	FInputModeGameOnly GameInputMode;
	ChatLogWidget->RemoveFromViewport();
	Mycontroller->bShowMouseCursor = false;
	Mycontroller->bEnableClickEvents = false;
	Mycontroller->SetInputMode(GameInputMode);
	
	//메세지 보냄
	ChatUserWidget->RecvChat(Mycharacter->GetLoginID());
	
}

void APWPlayerController::EnterMenuHide()
{
	auto ChatUserWidget = Cast<UPWUserWidget>(ChatLogWidget);
	auto Mycharacter = Cast<APWBaseCharacter>(GetCharacter());
	FInputModeGameOnly GameInputMode;

	ChatLogWidget->RemoveFromViewport();
	Mycontroller->bShowMouseCursor = false;
	Mycontroller->bEnableClickEvents = false;
	Mycontroller->SetInputMode(GameInputMode);

}

void APWPlayerController::RecvChatSet(wchar_t* recvchat)
{
	auto ChatUserWidget = Cast<UPWUserWidget>(ChatLogWidget);
	auto MyGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

	if (ChatUserWidget == nullptr)
	{
		PWLOG(Error, TEXT("chatuserwidget null"));
	}
	else	
		ChatUserWidget->SetChat(recvchat); //메세지 도착

}

void APWPlayerController::TabMenuOpen()
{

	FStringClassReference WidgetClassRef(TEXT("/Game/UI/TabMenu.TabMenu_C"));
	if (UClass* WidgetClass = WidgetClassRef.TryLoadClass<UUserWidget>())
	{
		TabMenuWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);
		if(GetPawn() != nullptr)
			TabMenuWidget->AddToPlayerScreen();
	}

}

void APWPlayerController::TabMenuClose()
{

	TabMenuWidget->RemoveFromViewport();
}

void APWPlayerController::EscMenuOpen()
{
	FInputModeUIOnly inputmode;
	if (GetPawn() != nullptr) {
		if (!EscMenuWidget->IsVisible()) {
			EscMenuWidget->AddToPlayerScreen();
			Mycontroller->bShowMouseCursor = true;
			Mycontroller->bEnableClickEvents = true;
			Mycontroller->SetInputMode(inputmode);
		}
	}
}

void APWPlayerController::EscMenuClose()
{
	FInputModeGameOnly inputmode;
	EscMenuWidget->RemoveFromViewport();
	Mycontroller->bShowMouseCursor = false;
	Mycontroller->bEnableClickEvents = false;
	Mycontroller->SetInputMode(inputmode);
}


void APWPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
	PWLOG(Error, TEXT("PlayerController UnPossess"));
}

void APWPlayerController::FindCharacters()
{
	const auto& PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());

	TSubclassOf<AActor> ClassCharacter = APWBaseCharacter::StaticClass();

	TArray<AActor*> OutputActors;

	if (PWGameInstance != nullptr)
	{

		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassCharacter, OutputActors);

		PWLOG(Warning, TEXT("BaseCharacter's Num is %d"), OutputActors.Num());
		if (PWGameInstance->GetCurrentGameMode() == EGameMode::MULTI && PWGameInstance->GetIsGameStart())
		{
			for (const auto& Actor : OutputActors)
			{

				//루프돌려서 게임인스턴스의 선택캐릭터정보와 팀정보를 월드상에 모든 BaseCharacter중에서 팀정보와 CharacterType을 비교하여 찾는다.
				//Find()가 있긴한데 Array라서 어케 찾는지 모르겠네 
				const auto& BaseCharacter = Cast<APWBaseCharacter>(Actor);
				if ((BaseCharacter->GetTeam() == PWGameInstance->GetCurrentSelectTeam()) && (BaseCharacter->GetCharacterType() == PWGameInstance->GetCurrentSelectCharacter()))
				{//찾았으면 Possess()
					auto MyGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

					if (BaseCharacter->IsControlled())
					{
						auto controller = Cast<APWBaseAIController>(BaseCharacter->GetController());
						if (controller != nullptr)
							controller->OnUnPossess();
					}
					int n = BaseCharacter->Get_m_ID();
					MyGameMode->SetCurrentPlayerID(n);
					auto players = MyGameMode->Get_m_Clients();
					auto it = players.find(n);
					//BaseCharacter->SetLoginID(PWGameInstance->GetLoginID());
					MyGameMode->Set_m_MyCharacter(it->second);
					BaseCharacter->SetLoginID(PWGameInstance->GetLoginID());
					BaseCharacter->SetMyLoginID_OnWidget();
					OnPossess(BaseCharacter);

					break;
				}

			}

		}
		else if (PWGameInstance->GetCurrentGameMode() == EGameMode::SINGLE)
		{
			for (const auto& Actor : OutputActors)
			{

				//루프돌려서 게임인스턴스의 선택캐릭터정보와 팀정보를 월드상에 모든 BaseCharacter중에서 팀정보와 CharacterType을 비교하여 찾는다.
				//Find()가 있긴한데 Array라서 어케 찾는지 모르겠네 
				const auto& BaseCharacter = Cast<APWBaseCharacter>(Actor);
				if ((BaseCharacter->GetTeam() == PWGameInstance->GetCurrentSelectTeam()) && (BaseCharacter->GetCharacterType() == PWGameInstance->GetCurrentSelectCharacter()))
				{//찾았으면 Possess()
					//AI에 이미 빙의되어있으면 풀자. 풀고해야한다.
					if (BaseCharacter->IsControlled())
					{
						auto controller = Cast<APWBaseAIController>(BaseCharacter->GetController());
						if (controller != nullptr)
							controller->OnUnPossess();
					}
					BaseCharacter->SetLoginID(PWGameInstance->GetLoginID());
					BaseCharacter->SetMyLoginID_OnWidget();
					OnPossess(BaseCharacter);
					break;
				}
			}
		}
	}

}


void APWPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	auto MyGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

	if (MyGameMode->ChatSet == true) {
		RecvChatSet(MyGameMode->m_RecvChat);
		MyGameMode->ChatSet = false;
	}

}