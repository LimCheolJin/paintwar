// Fill out your copyright notice in the Description page of Project Settings.


#include "PWRollerWeapon.h"
#include "PWRollerCharacter.h"
#include "PWBomberCharacter.h"
#include "PWShooterCharacter.h"
#include "PWPaintCanvas.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInterface.h"
#include "Materials/MaterialInstanceConstant.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "Components/ArrowComponent.h"

#include "PWPaintLeft_1st.h"
#include "PWPaintLeft_2nd.h"
#include "PWPaintLeft_3rd.h"
#include "PWPaintLeft_4th.h"
#include "PWPaintLeft_5th.h"
#include "PWPaintLeft_6th.h"
#include "PWPaintLeft_Bridge1.h"
#include "PWPaintLeft_Bridge2.h"
#include "PWPaintLeft_Bridge3.h"
#include "PWPaintLeft_Bridge4.h"
#include "PWPaintLeft_Bridge5.h"
#include "PWPaintRight_1st.h"
#include "PWPaintRight_2nd.h"
#include "PWPaintRight_3rd.h"
#include "PWPaintRight_4th.h"
#include "PWPaintRight_5th.h"
#include "PWPaintRight_6th.h"
#include "PWPaintRight_Bridge1.h"
#include "PWPaintRight_Bridge2.h"
#include "PWPaintRight_Bridge3.h"
#include "PWPaintRight_Bridge4.h"
#include "PWPaintRight_Bridge5.h"


// Sets default values
APWRollerWeapon::APWRollerWeapon()
{ //StaticMesh'/Game/TestAssets/TestWeapon/Roller/roller_final_200323_bonche.roller_final_200323_bonche'
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_ROLLER(TEXT("/Game/TestAssets/TestWeapon/roller_test.roller_test"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_ROLLER(TEXT("/Game/TestAssets/TestWeapon/Roller/roller_final_200323_bonche.roller_final_200323_bonche"));
	if (ST_ROLLER.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		WeaponSTMesh->SetStaticMesh(ST_ROLLER.Object);
	}

	//StaticMesh'/Game/TestAssets/TestWeapon/Roller/rollerhead_chagepivot_test.rollerhead_chagepivot_test'
	//StaticMesh'/Game/TestAssets/TestWeapon/Roller/rollerhead_chagepivot_200324.rollerhead_chagepivot_200324'
	//StaticMesh'/Game/TestAssets/TestWeapon/Roller/roller_final_200323_rollerhead.roller_final_200323_rollerhead'
	RollerHeadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RollerHead"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_ROLLERHEAD(TEXT("/Game/TestAssets/TestWeapon/Roller/rollerhead_chagepivot_test.rollerhead_chagepivot_test"));
	if (ST_ROLLERHEAD.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		RollerHeadMesh->SetStaticMesh(ST_ROLLERHEAD.Object);
	}

	//WeaponSTMesh->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	WeaponSTMesh->SetRelativeScale3D(FVector(0.25f, 0.25f, 0.25f));
	RollerHeadMesh->SetupAttachment(RootComponent);
	RollerHeadMesh->SetCollisionProfileName(TEXT("NoCollision"));
	RollerHeadMesh->SetRelativeLocation(FVector(38.0f, -6.0f, 0.0f));
	RollerHeadMesh->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
	RollerCollision->bHiddenInGame = false;
	//RollerCollision->SetRelativeLocation(FVector(0.0f, 0.0f, 77.0f));
	
	//RollerCollision->SetBoxExtent(FVector(8.0f, 40.0f, 9.0f));
	RollerCollision->SetBoxExtent(FVector(5.0f, 40.0f, 5.0f));
	//RollerCollision->SetRelativeLocation(FVector(40.0f, 0.0f, 77.0f));
	RollerCollision->SetRelativeLocation(FVector(40.0f, -4.0f, 1.0f));
	//RollerCollision->SetCollisionProfileName(TEXT("BlockAll"));
	RollerCollision->SetCollisionProfileName(TEXT("Weapon"));

	RollerCollision->SetGenerateOverlapEvents(true);

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_BrushRoller(TEXT("/Game/TestAssets/TestFloorPaint/M_BrushRoller.M_BrushRoller"));
	if(M_BrushRoller.Succeeded())
	{
		BrushMaterial = M_BrushRoller.Object;
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> T_RollerBrushMRed(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/RedSplat2.RedSplat2"));
	if (T_RollerBrushMRed.Succeeded())
	{
		BrushTextureRed = T_RollerBrushMRed.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> T_RollerBrushMGreen(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/GreenSplat2.GreenSplat2"));
	if (T_RollerBrushMGreen.Succeeded())
	{
		BrushTextureGreen = T_RollerBrushMGreen.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_RollerRedHead(TEXT("/Game/TestAssets/TestWeapon/M_RollerMaterialRedInst.M_RollerMaterialRedInst"));
	if (M_RollerRedHead.Succeeded())
	{
		RollerRedHead = M_RollerRedHead.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_RollerGreenHead(TEXT("/Game/TestAssets/TestWeapon/M_RollerMaterialGreenInst.M_RollerMaterialGreenInst"));
	if (M_RollerGreenHead.Succeeded())
	{
		RollerGreenHead = M_RollerGreenHead.Object;
	}
	//Material'/Game/TestAssets/TestWeapon/Roller/M_RedRoller.M_RedRoller'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_RollerRedTeam(TEXT("/Game/TestAssets/TestWeapon/Roller/M_RedRoller.M_RedRoller"));
	if (M_RollerRedTeam.Succeeded())
	{
		RollerRed = M_RollerRedTeam.Object;
	}

	//MaterialInstanceConstant'/Game/SpaceExplorer/Materials/SpaceExplorerMat_GreenTeam.SpaceExplorerMat_GreenTeam'
	//Material'/Game/TestAssets/TestWeapon/Roller/M_GreenRoller.M_GreenRoller'
	//static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_RollerGreenTeam(TEXT("/Game/TestAssets/TestWeapon/M_RollerMaterialGreenInst.M_RollerMaterialGreenInst"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_RollerGreenTeam(TEXT("/Game/TestAssets/TestWeapon/Roller/M_GreenRoller.M_GreenRoller"));
	if (M_RollerGreenTeam.Succeeded())
	{
		RollerGreen = M_RollerGreenTeam.Object;
	}

	RollerDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("RollerDirection"));
	RollerDirection->SetupAttachment(RootComponent);

	//RollerDirection->SetRelativeLocation(FVector(0.0f, 0.0f, 80.0f));
	//RollerDirection->SetRelativeLocation(FVector(-20.0f, 0.0f, 80.0f));
	RollerDirection->SetRelativeLocation(FVector(-20.0f, -4.0f, 0.0f));
	RollerDirection->bHiddenInGame = false;

	//파티클
	PaintParticleLeft = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Left ParticleSystem"));
	PaintParticleRight = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Right ParticleSystem"));
	PaintParticleLeft->SetupAttachment(RootComponent);
	PaintParticleRight->SetupAttachment(RootComponent);
	PaintParticleLeft->SetRelativeScale3D(FVector(5.0f, 5.0f, 0.1f));
	PaintParticleRight->SetRelativeScale3D(FVector(5.0f, 5.0f, 0.1f));
	//PaintParticleLeft->SetRelativeLocation(FVector(0.0f, 0.0f, 150.0f));
	PaintParticleLeft->SetRelativeLocation(FVector(40.0f, -40.0f, 0.0f));
	PaintParticleLeft->SetRelativeRotation(FRotator(0.0f, -90.0f, -90.0f));
	PaintParticleRight->SetRelativeLocation(FVector(40.0f, 36.0f, 0.0f));
	PaintParticleRight->SetRelativeRotation(FRotator(0.0f, 90.0f, 90.0f));

	PaintParticleLeft->bAutoActivate = false;
	PaintParticleRight->bAutoActivate = false; //ParticleSystem'/Game/TestAssets/testPaintParticle/Liquid.Liquid'
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetRed(TEXT("/Game/TestAssets/testPaintParticle/LiquidRed.LiquidRed"));
	if (ParticleAssetRed.Succeeded())
	{
		//PaintParticleLeft->SetTemplate(ParticleAssetLeft.Object);
		ParticleRed = ParticleAssetRed.Object;
	}

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetGreen(TEXT("/Game/TestAssets/testPaintParticle/LiquidGreen.LiquidGreen"));
	if (ParticleAssetGreen.Succeeded())
	{
		//PaintParticleRight->SetTemplate(ParticleAssetRight.Object);
		ParticleGreen = ParticleAssetGreen.Object;
	} //왼오른 둘다 같은거면 하나로만 불러와서쓸수잇을거같음 일단 2개

	//BrushSize = 600.0f;
	BrushSize = 300.0f;
	//bOverlapWithOther = false;
	Damage = 100.0f;

	//롤러무기 헤드의 회전률이다.
	RollerHeadRotateRate = 30.0f;
}

void APWRollerWeapon::BeginPlay()
{
	APWMeleeWeapon::BeginPlay();
	RollerCollision->OnComponentBeginOverlap.AddDynamic(this, &APWRollerWeapon::OnOverlapBegin);
	RollerCollision->OnComponentEndOverlap.AddDynamic(this, &APWRollerWeapon::OnOverlapEnd);
	//RollerCollision->OnComponentHit.AddDynamic(this, &APWRollerWeapon::OnHit);

	BrushMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), BrushMaterial);
	//BrushMaterialInstance->SetVectorParameterValue(FName("Color"), FLinearColor(1.0f, 0.0f, 0.0f));
	if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::RED)
	{
		WeaponSTMesh->SetMaterial(0, RollerRed);
		RollerHeadMesh->SetMaterial(0, RollerRedHead);
		BrushMaterialInstance->SetTextureParameterValue(FName(TEXT("BrushTexture")), BrushTextureRed);
		PaintParticleLeft->SetTemplate(ParticleRed);
		PaintParticleRight->SetTemplate(ParticleRed);
	}
	else if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::GREEN)
	{

		WeaponSTMesh->SetMaterial(0, RollerGreen);
		RollerHeadMesh->SetMaterial(0, RollerGreenHead);
		BrushMaterialInstance->SetTextureParameterValue(FName("BrushTexture"), BrushTextureGreen);
		PaintParticleLeft->SetTemplate(ParticleGreen);
		PaintParticleRight->SetTemplate(ParticleGreen);
	}

	//ProcessPaintHandle, this, &APWRollerWeapon::ProcessPaint, 0.1f, false
	GetWorldTimerManager().SetTimer(AmountPaintCountingHandle, this, &APWRollerWeapon::CountAmountPaint, 2.5f, true);
}

void APWRollerWeapon::CountAmountPaint()
{
	const auto& OwnerCharacter = Cast<APWRollerCharacter>(GetOwner());
	if (OwnerCharacter->GetbPainting()) //&& bOverlapWithOther) //&& TraceResult)
	{
		OwnerCharacter->DiscountAmountPaint(10.0f);
	}

}

void APWRollerWeapon::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	auto RollerCharacter = Cast<APWRollerCharacter>(GetOwner());
	bool IsPaintingKeyDown = RollerCharacter->GetbPainting();
	if (IsPaintingKeyDown)
	{
		//페인팅 키가 눌려진 상태에서 hit를 했는데 그것이 슈터캐릭이거나 붐버 캐릭이면 
		if ( Cast<APWShooterCharacter>(OtherActor) || Cast<APWBomberCharacter>(OtherActor))
		{
			//죽이는 로직을 여기작성.
		}
	}
	PWLOG(Warning, TEXT("%s, Roller Hit!!"), *OtherActor->GetName() );
}
void APWRollerWeapon::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//bOverlapWithOther = true;
	const auto& BaseCharacter = Cast<APWBaseCharacter>(OtherActor);


	if (GetOwner() == OtherActor || BaseCharacter == nullptr) //otherActor가 내 주인이라면?
	{
		//PWLOG(Warning, TEXT("SELF OVER LAP"));
		return;
	}

	auto RollerCharacter = Cast<APWRollerCharacter>(GetOwner());
	bool IsPaintingKeyDown = RollerCharacter->GetbPainting();

	if (IsPaintingKeyDown && (RollerCharacter->GetTeam() != BaseCharacter->GetTeam())) //bOverlapWithOther && (RollerCharacter->GetTeam() != BaseCharacter->GetTeam()) )
	{
		
		if (BaseCharacter != nullptr)
		{
			BaseCharacter->TakenDamage(Cast<APWBaseCharacter>(this->GetOwner()), this->Damage);
		}
	}
	
	//PWLOG(Warning, TEXT("RollerWeapon Overlap Begin"));
}

void APWRollerWeapon::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	//02/28 주석처리하였음잠시, 03/03 잠시주석풀었음.
	//bOverlapWithOther = false;


	//PWLOG(Warning, TEXT("RollerWeapon Overlap End"));
	//PWLOG(Warning, TEXT("I m %s overlapEnd"), *this->GetName());
	//PWLOG(Warning, TEXT("%s overlapEnd"), *OtherActor->GetName());
}

void APWRollerWeapon::Tick(float DeltaTime)
{
	APWMeleeWeapon::Tick(DeltaTime);

}

void APWRollerWeapon::ProcessPaint()
{
	auto RollerCharacter = Cast<APWRollerCharacter>(GetOwner());
	//if (!RollerCharacter)
	//{
	//	return;
	//}
	if (RollerCharacter->GetAmountPaint() <= 0.0f)
		return;

	//PWLOG(Warning, TEXT("%d"), RollerCharacter->GetAmountPaint());
	bool IsPaintingKeyDown = RollerCharacter->GetbPainting();

	if (IsPaintingKeyDown) //&& bOverlapWithOther)
	{//페인팅 키가 눌러져있으면서
		
		/////페인팅 키가 눌러져있으면 RollerHead는 계속해서 돌아가야한다.
		//RollerHeadMesh->SetRelativeRotation(FRotator(0.0f, (RollerHeadRotateRate+=RollerHeadRotateRate) ,0.0f));
		//if (RollerHeadRotateRate >= 720.0f)
			//RollerHeadRotateRate = 0.0f;
		//RollerHeadRotateRate += 20.0f;
		auto RotateQuat = FRotator(0.0f, 0.0f, RollerHeadRotateRate).Quaternion();
		RollerHeadMesh->AddLocalRotation(RotateQuat);
		//RollerHeadMesh->SetWorldRotation(FRotator(RollerHeadRotateRate, 0.0f, 0.0f) );
		//PWLOG(Error, TEXT("Owner's color is %d RollerHead Rotate Rate is %f"), Cast<APWBaseCharacter>(GetOwner())->GetTeam() ,RollerHeadRotateRate);
		////
		FVector TraceStart;
		FVector TraceEnd;
		FHitResult HitResult;

		TraceStart = RollerDirection->GetComponentLocation();
		TraceEnd = TraceStart + (RollerDirection->GetForwardVector() * 25.0f);

		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.bTraceComplex = true;
		CollisionQueryParams.AddIgnoredActor(this);
		CollisionQueryParams.bReturnFaceIndex = true;

		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, true);
		TraceResult = (GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams));
		if ( TraceResult )
		{
			//PWLOG(Warning, TEXT("linetrace count is %d"), testint);
			FVector2D UV{ 0.0f, 0.0f };
			auto HitActor = HitResult.GetActor();


			PaintParticleLeft->SetActive(true);
			PaintParticleRight->SetActive(true);
			if (Cast<APWPaintCanvas>(HitActor) == nullptr)
				return;
			if (Cast<APWBaseCharacter>(HitActor) != nullptr)
			{
				if (RollerCharacter->GetTeam() != Cast<APWBaseCharacter>(HitActor)->GetTeam())
				{
					Cast<APWBaseCharacter>(HitActor)->TakenDamage(RollerCharacter, this->Damage);
				}
			}
			if (Cast<APWPaintLeft_1st>(HitActor)) // 이부분 if - else if 로 22개의 경우 나눠야함.. ..............
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_1st>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_4th>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
		}
		else
		{
			//PWLOG(Warning, TEXT("LiNE TRACE IS FAILED"));
		}
		//GetWorld()->GetTimerManager().SetTimer(ProcessPaintHandle, this, &APWRollerWeapon::ProcessPaint, 0.1f, false);
	}
	else
	{
		PaintParticleLeft->SetActive(false);
		PaintParticleRight->SetActive(false);
		GetWorld()->GetTimerManager().ClearTimer(ProcessPaintHandle);
	}
}