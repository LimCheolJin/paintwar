// Fill out your copyright notice in the Description page of Project Settings.


#include "PWUserWidget.h"
#include "WidgetTree.h"

void UPWUserWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UPWUserWidget::SetLoginID(FString id)
{
	LoginIDText = this->WidgetTree->FindWidget<UTextBlock>("IDTextBox");
	LoginIDText->SetText(FText::FromString(id));
}

void UPWUserWidget::RecvChat(FString id)
{
	SBChatLog = this->WidgetTree->FindWidget<UScrollBox>("ChatLog");
	TBChatText = this->WidgetTree->FindWidget<UEditableTextBox>("ChatText");


	//id : 채팅 합쳐줌
	id.Append(" : ");
	id.Append(TBChatText->GetText().ToString());
	
	FStringClassReference AddChatWidgetClassRef(TEXT("/Game/UI/AddChatWidget.AddChatWidget_C"));
	UClass* AddChatWidgetClass = AddChatWidgetClassRef.TryLoadClass<UUserWidget>();
	TSubclassOf<UUserWidget> tempstaticclass = AddChatWidgetClass;
	auto AddChatWidget = CreateWidget(this, tempstaticclass);
	auto Widget = AddChatWidget->GetWidgetFromName(FName("AddText"));
	auto TBnew = Cast<UTextBlock>(Widget);
	TBnew->SetText(FText::FromString(id));

	SBChatLog->AddChild(AddChatWidget); //여기서 로그에 쌓임

	m_PWNetWork = PWNetWork::GetNetWork();
	m_PWNetWork->Send_ChatPacket(id);

	TBChatText->SetText(FText::FromString(""));
}

void UPWUserWidget::SetChat(wchar_t* recvchat)
{
	SBChatLog = this->WidgetTree->FindWidget<UScrollBox>("ChatLog");
	TBChatText = this->WidgetTree->FindWidget<UEditableTextBox>("ChatText");

	FString myString(recvchat);

	FStringClassReference AddChatWidgetClassRef(TEXT("/Game/UI/AddChatWidget.AddChatWidget_C"));
	UClass* AddChatWidgetClass = AddChatWidgetClassRef.TryLoadClass<UUserWidget>();
	TSubclassOf<UUserWidget> tempstaticclass = AddChatWidgetClass;
	auto AddChatWidget = CreateWidget(this, tempstaticclass);
	auto Widget = AddChatWidget->GetWidgetFromName(FName("AddText"));
	auto TBnew = Cast<UTextBlock>(Widget);
	TBnew->SetText(FText::FromString(myString));

	SBChatLog->AddChild(AddChatWidget); //여기서 로그에 쌓임
}


void UPWUserWidget::SetChatFocus()
{
	TBChatText = this->WidgetTree->FindWidget<UEditableTextBox>("ChatText");
	TBChatText->SetKeyboardFocus();
}
