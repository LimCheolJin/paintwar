// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWSpawnPoint.generated.h"

UCLASS()
class PAINTWAR_API APWSpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWSpawnPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

public: //오브젝트들의 위치조정을 위해 잠시 public 나중에 private해야ㅎ마.
	UPROPERTY(VisibleAnywhere, Category = Trigger)
	class UBoxComponent* TriggerBox;

	ETeam GetTeamColor() const;

	void SetTeamColor(ETeam Color);

private:
	UPROPERTY(EditAnywhere, Category = Team)
	ETeam TeamColor;


	//무적상태를 만들고 싶으면 무적상태만 블로킹 만들기

	
	

};
