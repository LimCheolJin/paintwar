// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWLoginGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWLoginGameMode() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWLoginGameMode_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWLoginGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWLoginGameMode::StaticRegisterNativesAPWLoginGameMode()
	{
	}
	UClass* Z_Construct_UClass_APWLoginGameMode_NoRegister()
	{
		return APWLoginGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APWLoginGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWLoginGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWLoginGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PWLoginGameMode.h" },
		{ "ModuleRelativePath", "Public/PWLoginGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWLoginGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWLoginGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWLoginGameMode_Statics::ClassParams = {
		&APWLoginGameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_APWLoginGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWLoginGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWLoginGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWLoginGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWLoginGameMode, 727540097);
	template<> PAINTWAR_API UClass* StaticClass<APWLoginGameMode>()
	{
		return APWLoginGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWLoginGameMode(Z_Construct_UClass_APWLoginGameMode, &APWLoginGameMode::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWLoginGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWLoginGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
