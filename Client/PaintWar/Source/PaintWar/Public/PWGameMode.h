// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "PaintWar.h"
#include "GameFramework/GameModeBase.h"
#include <iostream>
#include <map>
#include "PWGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	//생성자
	APWGameMode();
	~APWGameMode();
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	//GameModeBase의 PostLogin 오버라이딩
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Tick(float DeltaTime) override;
	EGameMode GetCurrentGameMode() const;

	void SetCurrentGameMode(EGameMode Mode);

	UFUNCTION(BlueprintCallable)
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	UPROPERTY()
	UUserWidget* CurrentWidget;

	/*UFUNCTION(BlueprintCallable)
	void SetMyCharacter(int32 n);*/

	/*UFUNCTION(BlueprintCallable)
	void SetMode(int32 n);*/

	class UUserWidget* MainHudWidget;

private:
	EGameMode CurrentGameMode;

	ESelectCharacter CurrentCharacter;

	ETeam CurrentTeam;

	UPROPERTY()
	class APWSpawnPoint* RedTeamSpawnPoint;

	UPROPERTY()
	class APWSpawnPoint* GreenTeamSpawnPoint;


	UPROPERTY()
	class UPWGameInstance* PWGameInstance;

	/*UPROPERTY()
		class APWShooterCharacter* ShooterCharacterRed;

	UPROPERTY()
		class APWShooterCharacter* ShooterCharacterGreen;

	UPROPERTY()
		class APWRollerCharacter* RollerCharacterRed;

	UPROPERTY()
		class APWRollerCharacter* RollerCharacterGreen;

	UPROPERTY()
		class APWBomberCharacter* BomberCharacterRed;

	UPROPERTY()
		class APWBomberCharacter* BomberCharacterGreen;*/

	UPROPERTY()
	TArray<class APWBaseCharacter*> ExistOnGameLevelCharacters;

public:
	//네트워크를 위한 함수들
	std::map<int, class APWBaseCharacter*> Get_m_Clients();
	
	int GetCurrentPlayerID() const;

	void SetCurrentPlayerID(int id);

	class APWBaseCharacter* Get_m_MyCharacter() const;

	void Set_m_MyCharacter(class APWBaseCharacter*);

	void SetIsMultiGameStart(bool);
private:
	//네트워크를 위한 변수들
	std::map<int, class APWBaseCharacter*> m_AllCharacters;

	std::map<int, ObjectInformations> m_TempPlayerInfoes;  //정보를 받아와서 업데이트 해주기위한 자료구조임 .

	int CurrentPlayerID;
	
	class APWBaseCharacter* m_MyCharacter;

	UPROPERTY()
	FTimerHandle SendTimerHandle;

	class PWNetWork* m_PWNetWork;

	bool IsMultiGameStart;

	UPROPERTY()
	class APWGameTimeManager* m_GameTimer;

	//ESCENETYPE CurrentGameSceneInfo;
public:
	wchar_t m_RecvChat[32];
	bool ChatSet = false;

	class APWPlayerController* PWController;

	class APWGameTimeManager* GetGameTimer() const;

	//네트워크를 위한 함수들
	void Recv_PlayerMovePacketProcess(char*);

	void Recv_ShooterFirePacketProcess(char*);

	void Recv_RollerPaintPacketProcess(char*);

	void Recv_BomberThrowPacketProcess(char*);

	void Recv_ReloadingPacketProcess(char* );

	void Recv_GameTimerPacketProcess(char*);

	void Recv_ChatPacketProcess(char*,bool);

	//void Recv_GameEndPacketProcess(char* EndPacket);

	void Recv_OtherPlayerLeavePacketProcess(int);

	UFUNCTION()
	void SendWorldInfo();

	void SendPlayerInfo();

	void SendShooterFire(bool);

	void SendRollerPaint(bool);

	void SendBomberThrow(bool);
	
	void SendReloadInfo(bool IsReload);

	void UpdatePlayers();

	class UTextureRenderTarget2D* m_LeftBaseRenderTarget;
	class UTextureRenderTarget2D* m_RightBaseRenderTarget;
};

