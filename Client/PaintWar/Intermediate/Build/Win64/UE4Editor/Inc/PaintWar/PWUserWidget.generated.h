// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWUserWidget_generated_h
#error "PWUserWidget.generated.h already included, missing '#pragma once' in PWUserWidget.h"
#endif
#define PAINTWAR_PWUserWidget_generated_h

#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWUserWidget(); \
	friend struct Z_Construct_UClass_UPWUserWidget_Statics; \
public: \
	DECLARE_CLASS(UPWUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWUserWidget)


#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPWUserWidget(); \
	friend struct Z_Construct_UClass_UPWUserWidget_Statics; \
public: \
	DECLARE_CLASS(UPWUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWUserWidget)


#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWUserWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWUserWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWUserWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWUserWidget(UPWUserWidget&&); \
	NO_API UPWUserWidget(const UPWUserWidget&); \
public:


#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWUserWidget(UPWUserWidget&&); \
	NO_API UPWUserWidget(const UPWUserWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWUserWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWUserWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWUserWidget)


#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_18_PROLOG
#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_INCLASS \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWUserWidget_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWUserWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWUserWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
