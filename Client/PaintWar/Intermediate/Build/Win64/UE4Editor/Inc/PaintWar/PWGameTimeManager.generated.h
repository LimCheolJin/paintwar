// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWGameTimeManager_generated_h
#error "PWGameTimeManager.generated.h already included, missing '#pragma once' in PWGameTimeManager.h"
#endif
#define PAINTWAR_PWGameTimeManager_generated_h

#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowCountDown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowCountDown(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowCountDown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowCountDown(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWGameTimeManager(); \
	friend struct Z_Construct_UClass_APWGameTimeManager_Statics; \
public: \
	DECLARE_CLASS(APWGameTimeManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWGameTimeManager)


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWGameTimeManager(); \
	friend struct Z_Construct_UClass_APWGameTimeManager_Statics; \
public: \
	DECLARE_CLASS(APWGameTimeManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWGameTimeManager)


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWGameTimeManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWGameTimeManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWGameTimeManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWGameTimeManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWGameTimeManager(APWGameTimeManager&&); \
	NO_API APWGameTimeManager(const APWGameTimeManager&); \
public:


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWGameTimeManager(APWGameTimeManager&&); \
	NO_API APWGameTimeManager(const APWGameTimeManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWGameTimeManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWGameTimeManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWGameTimeManager)


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CountDownHandle() { return STRUCT_OFFSET(APWGameTimeManager, CountDownHandle); }


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameTimeManager_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWGameTimeManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWGameTimeManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
