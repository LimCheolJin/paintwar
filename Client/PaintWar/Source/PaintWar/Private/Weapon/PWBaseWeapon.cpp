// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBaseWeapon.h"

// Sets default values
APWBaseWeapon::APWBaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponSTMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WEAPON"));
	RootComponent = WeaponSTMesh;


	
	WeaponSTMesh->SetCollisionProfileName(TEXT("NoCollision"));
	
}

// Called when the game starts or when spawned
void APWBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APWBaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UStaticMeshComponent* APWBaseWeapon::GetWeaponSTMesh()
{
	return WeaponSTMesh;
}

