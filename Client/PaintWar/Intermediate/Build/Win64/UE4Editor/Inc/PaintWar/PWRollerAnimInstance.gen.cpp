// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/AnimInstance/PWRollerAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWRollerAnimInstance() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_UPWRollerAnimInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWRollerAnimInstance();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWAnimInstance();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify();
// End Cross Module References
	void UPWRollerAnimInstance::StaticRegisterNativesUPWRollerAnimInstance()
	{
		UClass* Class = UPWRollerAnimInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AnimNotify_PaintNotify", &UPWRollerAnimInstance::execAnimNotify_PaintNotify },
			{ "AnimNotify_ReloadNotify", &UPWRollerAnimInstance::execAnimNotify_ReloadNotify },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimInstance/PWRollerAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWRollerAnimInstance, nullptr, "AnimNotify_PaintNotify", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimInstance/PWRollerAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWRollerAnimInstance, nullptr, "AnimNotify_ReloadNotify", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPWRollerAnimInstance_NoRegister()
	{
		return UPWRollerAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPWRollerAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsPainting_MetaData[];
#endif
		static void NewProp_IsPainting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsPainting;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPWRollerAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPWAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPWRollerAnimInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_PaintNotify, "AnimNotify_PaintNotify" }, // 1508586585
		{ &Z_Construct_UFunction_UPWRollerAnimInstance_AnimNotify_ReloadNotify, "AnimNotify_ReloadNotify" }, // 2957355525
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWRollerAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "AnimInstance/PWRollerAnimInstance.h" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWRollerAnimInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWRollerAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting_SetBit(void* Obj)
	{
		((UPWRollerAnimInstance*)Obj)->IsPainting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting = { "IsPainting", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPWRollerAnimInstance), &Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPWRollerAnimInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWRollerAnimInstance_Statics::NewProp_IsPainting,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPWRollerAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPWRollerAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPWRollerAnimInstance_Statics::ClassParams = {
		&UPWRollerAnimInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPWRollerAnimInstance_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UPWRollerAnimInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPWRollerAnimInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPWRollerAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPWRollerAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPWRollerAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPWRollerAnimInstance, 2156117342);
	template<> PAINTWAR_API UClass* StaticClass<UPWRollerAnimInstance>()
	{
		return UPWRollerAnimInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPWRollerAnimInstance(Z_Construct_UClass_UPWRollerAnimInstance, &UPWRollerAnimInstance::StaticClass, TEXT("/Script/PaintWar"), TEXT("UPWRollerAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPWRollerAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
