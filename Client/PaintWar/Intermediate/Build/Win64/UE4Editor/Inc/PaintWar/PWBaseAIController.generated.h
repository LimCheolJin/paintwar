// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBaseAIController_generated_h
#error "PWBaseAIController.generated.h already included, missing '#pragma once' in PWBaseAIController.h"
#endif
#define PAINTWAR_PWBaseAIController_generated_h

#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBaseAIController(); \
	friend struct Z_Construct_UClass_APWBaseAIController_Statics; \
public: \
	DECLARE_CLASS(APWBaseAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAPWBaseAIController(); \
	friend struct Z_Construct_UClass_APWBaseAIController_Statics; \
public: \
	DECLARE_CLASS(APWBaseAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBaseAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBaseAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseAIController(APWBaseAIController&&); \
	NO_API APWBaseAIController(const APWBaseAIController&); \
public:


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseAIController(APWBaseAIController&&); \
	NO_API APWBaseAIController(const APWBaseAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBaseAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_14_PROLOG
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_INCLASS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBaseAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AIControllers_PWBaseAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
