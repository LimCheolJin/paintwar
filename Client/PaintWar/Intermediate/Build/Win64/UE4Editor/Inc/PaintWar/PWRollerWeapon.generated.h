// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
struct FVector;
#ifdef PAINTWAR_PWRollerWeapon_generated_h
#error "PWRollerWeapon.generated.h already included, missing '#pragma once' in PWRollerWeapon.h"
#endif
#define PAINTWAR_PWRollerWeapon_generated_h

#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execProcessPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ProcessPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCountAmountPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CountAmountPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execProcessPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ProcessPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCountAmountPaint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CountAmountPaint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWRollerWeapon(); \
	friend struct Z_Construct_UClass_APWRollerWeapon_Statics; \
public: \
	DECLARE_CLASS(APWRollerWeapon, APWMeleeWeapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWRollerWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAPWRollerWeapon(); \
	friend struct Z_Construct_UClass_APWRollerWeapon_Statics; \
public: \
	DECLARE_CLASS(APWRollerWeapon, APWMeleeWeapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWRollerWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWRollerWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWRollerWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWRollerWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWRollerWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWRollerWeapon(APWRollerWeapon&&); \
	NO_API APWRollerWeapon(const APWRollerWeapon&); \
public:


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWRollerWeapon(APWRollerWeapon&&); \
	NO_API APWRollerWeapon(const APWRollerWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWRollerWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWRollerWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWRollerWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RollerHeadMesh() { return STRUCT_OFFSET(APWRollerWeapon, RollerHeadMesh); } \
	FORCEINLINE static uint32 __PPO__BrushSize() { return STRUCT_OFFSET(APWRollerWeapon, BrushSize); } \
	FORCEINLINE static uint32 __PPO__BrushMaterial() { return STRUCT_OFFSET(APWRollerWeapon, BrushMaterial); } \
	FORCEINLINE static uint32 __PPO__BrushMaterialInstance() { return STRUCT_OFFSET(APWRollerWeapon, BrushMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__RollerDirection() { return STRUCT_OFFSET(APWRollerWeapon, RollerDirection); } \
	FORCEINLINE static uint32 __PPO__RollerRed() { return STRUCT_OFFSET(APWRollerWeapon, RollerRed); } \
	FORCEINLINE static uint32 __PPO__RollerRedHead() { return STRUCT_OFFSET(APWRollerWeapon, RollerRedHead); } \
	FORCEINLINE static uint32 __PPO__RollerGreen() { return STRUCT_OFFSET(APWRollerWeapon, RollerGreen); } \
	FORCEINLINE static uint32 __PPO__RollerGreenHead() { return STRUCT_OFFSET(APWRollerWeapon, RollerGreenHead); } \
	FORCEINLINE static uint32 __PPO__BrushTextureRed() { return STRUCT_OFFSET(APWRollerWeapon, BrushTextureRed); } \
	FORCEINLINE static uint32 __PPO__BrushTextureGreen() { return STRUCT_OFFSET(APWRollerWeapon, BrushTextureGreen); } \
	FORCEINLINE static uint32 __PPO__PaintParticleLeft() { return STRUCT_OFFSET(APWRollerWeapon, PaintParticleLeft); } \
	FORCEINLINE static uint32 __PPO__PaintParticleRight() { return STRUCT_OFFSET(APWRollerWeapon, PaintParticleRight); } \
	FORCEINLINE static uint32 __PPO__ParticleRed() { return STRUCT_OFFSET(APWRollerWeapon, ParticleRed); } \
	FORCEINLINE static uint32 __PPO__ParticleGreen() { return STRUCT_OFFSET(APWRollerWeapon, ParticleGreen); } \
	FORCEINLINE static uint32 __PPO__AmountPaintCountingHandle() { return STRUCT_OFFSET(APWRollerWeapon, AmountPaintCountingHandle); } \
	FORCEINLINE static uint32 __PPO__ProcessPaintHandle() { return STRUCT_OFFSET(APWRollerWeapon, ProcessPaintHandle); }


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_8_PROLOG
#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_INCLASS \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWRollerWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Weapon_PWRollerWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
