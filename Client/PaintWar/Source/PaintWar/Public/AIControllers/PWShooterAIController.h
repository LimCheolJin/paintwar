// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWBaseAIController.h"
#include "PWShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWShooterAIController : public APWBaseAIController
{
	GENERATED_BODY()
public:
	APWShooterAIController();
public:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;

};
