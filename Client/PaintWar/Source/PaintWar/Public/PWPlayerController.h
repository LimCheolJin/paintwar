// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/PlayerController.h"
#include "PWPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPlayerController : public APlayerController
{
	GENERATED_BODY()

		//퍼블릭
public:
	virtual void Tick(float DeltaTime) override;

	//플레이어컨트롤러가 생성되는 시점
	virtual void PostInitializeComponents() override;

	//플레이어컨트롤러가 빙의하는 시점
	virtual void OnPossess(APawn* aPawn) override;

	virtual void OnUnPossess() override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	void TabMenuOpen();
	void TabMenuClose();
	void EscMenuOpen();

	UFUNCTION(BlueprintCallable)
	void EnterMenuOpen();

	UFUNCTION(BlueprintCallable)
		void EnterMenuClose();

	UFUNCTION(BlueprintCallable)
		void EnterMenuHide();

	UFUNCTION(BlueprintCallable)
		void EscMenuClose();
	
	void RecvChatSet(wchar_t*);

	UPROPERTY()
		class APlayerController* Mycontroller;

	UPROPERTY()
		class UUserWidget* TabMenuWidget;

	UPROPERTY()
		class UUserWidget* EscMenuWidget;

	UPROPERTY()
		class UUserWidget* BloodWidget;

	UPROPERTY()
		class UUserWidget* ChatLogWidget;

	FLinearColor WidgetColor;

	void FindCharacters();

	UPROPERTY()
		class UUserWidget* BulletWidget;

	UPROPERTY()
		class UUserWidget* SkillCountWidget;

	//프로텍티드
protected:
	//시작할때 최초에 한번만 실행하도록 하는 함수.
	virtual void BeginPlay() override;


	//프라이빗
private:

};

