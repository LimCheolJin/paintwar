// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintRight_6th.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintRight_6th : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintRight_6th();
};
