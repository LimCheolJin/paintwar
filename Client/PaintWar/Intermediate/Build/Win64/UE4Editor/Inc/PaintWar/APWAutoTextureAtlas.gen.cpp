// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/APWAutoTextureAtlas.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPWAutoTextureAtlas() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_AAPWAutoTextureAtlas_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_AAPWAutoTextureAtlas();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	void AAPWAutoTextureAtlas::StaticRegisterNativesAAPWAutoTextureAtlas()
	{
	}
	UClass* Z_Construct_UClass_AAPWAutoTextureAtlas_NoRegister()
	{
		return AAPWAutoTextureAtlas::StaticClass();
	}
	struct Z_Construct_UClass_AAPWAutoTextureAtlas_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_TestTextures_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_m_TestTextures;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_TestTextures_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "APWAutoTextureAtlas.h" },
		{ "ModuleRelativePath", "Public/APWAutoTextureAtlas.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures_MetaData[] = {
		{ "Category", "APWAutoTextureAtlas" },
		{ "ModuleRelativePath", "Public/APWAutoTextureAtlas.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures = { "m_TestTextures", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAPWAutoTextureAtlas, m_TestTextures), METADATA_PARAMS(Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures_Inner = { "m_TestTextures", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::NewProp_m_TestTextures_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAPWAutoTextureAtlas>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::ClassParams = {
		&AAPWAutoTextureAtlas::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAPWAutoTextureAtlas()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAPWAutoTextureAtlas_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAPWAutoTextureAtlas, 4289266424);
	template<> PAINTWAR_API UClass* StaticClass<AAPWAutoTextureAtlas>()
	{
		return AAPWAutoTextureAtlas::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAPWAutoTextureAtlas(Z_Construct_UClass_AAPWAutoTextureAtlas, &AAPWAutoTextureAtlas::StaticClass, TEXT("/Script/PaintWar"), TEXT("AAPWAutoTextureAtlas"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAPWAutoTextureAtlas);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
