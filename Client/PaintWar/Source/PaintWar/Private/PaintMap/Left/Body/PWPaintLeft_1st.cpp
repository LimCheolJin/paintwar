// Fill out your copyright notice in the Description page of Project Settings.


#include "PWPaintLeft_1st.h"



APWPaintLeft_1st::APWPaintLeft_1st()
{
	//StaticMesh'/Game/TestAssets/testmap/TestMap0320/map_uv7_200319_left_1+2.map_uv7_200319_left_1+2'
	//StaticMesh'/Game/TestAssets/testmap/TestMap0314/map_uv6_navimesh_low_200314_left_1+2.map_uv6_navimesh_low_200314_left_1+2'
	//StaticMesh'/Game/TestAssets/testmap/TestMap0313/map_test_navimesh_200313_left_1+2.map_test_navimesh_200313_left_1+2'
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/testmap0227/eachFBX/left_1st.left_1st"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/TestMap0320/map_uv7_200319_left_1+2.map_uv7_200319_left_1+2"));
	if (ST_CANVAS.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWesapon.RollerWeapon'
	{//루트지정해주기//StaticMesh'/Game/TestAssets/testmap/testmap0227/map5cm_uv_48105p_200226_left_1st.map5cm_uv_48105p_200226_left_1st'
		CanvasMesh->SetStaticMesh(ST_CANVAS.Object);
	}//StaticMesh'/Game/TestAssets/testmap/testMap0318/map_low_uv7_200316_left_1+2.map_low_uv7_200316_left_1+2'
	CanvasMesh->bAllowCullDistanceVolume = 0;
	//Material'/Game/TestAssets/TestFloorPaint/M_FloorMateriales/M_FloorLeft1/M_FloorLeft1.M_FloorLeft1'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_Canvas(TEXT("/Game/TestAssets/TestFloorPaint/M_FloorMateriales/M_FloorLeft1/M_FloorLeft1.M_FloorLeft1"));
	if (M_Canvas.Succeeded())
	{
		CanvasMaterial = M_Canvas.Object;
	}
}