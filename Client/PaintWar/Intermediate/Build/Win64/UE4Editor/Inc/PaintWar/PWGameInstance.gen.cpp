// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWGameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWGameInstance() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_UPWGameInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_Connect();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_EGameMode();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ESelectCharacter();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ETeam();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetIsGameStart();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetLoginID();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_GetRedResultCount();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_InitNetWork();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_Send_InitLogin();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetLoginID();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWGameInstance_SetRedResultCount();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	void UPWGameInstance::StaticRegisterNativesUPWGameInstance()
	{
		UClass* Class = UPWGameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Connect", &UPWGameInstance::execConnect },
			{ "GetCurrentGameMode", &UPWGameInstance::execGetCurrentGameMode },
			{ "GetCurrentSelectCharacter", &UPWGameInstance::execGetCurrentSelectCharacter },
			{ "GetCurrentSelectTeam", &UPWGameInstance::execGetCurrentSelectTeam },
			{ "GetGreenResultCount", &UPWGameInstance::execGetGreenResultCount },
			{ "GetIsGameStart", &UPWGameInstance::execGetIsGameStart },
			{ "GetLoginID", &UPWGameInstance::execGetLoginID },
			{ "GetRedResultCount", &UPWGameInstance::execGetRedResultCount },
			{ "InitNetWork", &UPWGameInstance::execInitNetWork },
			{ "Send_InitLogin", &UPWGameInstance::execSend_InitLogin },
			{ "Send_ReadyInfo", &UPWGameInstance::execSend_ReadyInfo },
			{ "Send_SelectCharacterTypePacket", &UPWGameInstance::execSend_SelectCharacterTypePacket },
			{ "Send_SelectTeamTypePacket", &UPWGameInstance::execSend_SelectTeamTypePacket },
			{ "SetCurrentGameMode", &UPWGameInstance::execSetCurrentGameMode },
			{ "SetCurrentSelectCharacter", &UPWGameInstance::execSetCurrentSelectCharacter },
			{ "SetCurrentSelectTeam", &UPWGameInstance::execSetCurrentSelectTeam },
			{ "SetGreenResultCount", &UPWGameInstance::execSetGreenResultCount },
			{ "SetLoginID", &UPWGameInstance::execSetLoginID },
			{ "SetRedResultCount", &UPWGameInstance::execSetRedResultCount },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPWGameInstance_Connect_Statics
	{
		struct PWGameInstance_eventConnect_Parms
		{
			FString serverIP;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_serverIP;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventConnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventConnect_Parms), &Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_serverIP = { "serverIP", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventConnect_Parms, serverIP), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_Connect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Connect_Statics::NewProp_serverIP,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_Connect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_Connect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "Connect", sizeof(PWGameInstance_eventConnect_Parms), Z_Construct_UFunction_UPWGameInstance_Connect_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Connect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_Connect_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Connect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_Connect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_Connect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics
	{
		struct PWGameInstance_eventGetCurrentGameMode_Parms
		{
			EGameMode ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetCurrentGameMode_Parms, ReturnValue), Z_Construct_UEnum_PaintWar_EGameMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetCurrentGameMode", sizeof(PWGameInstance_eventGetCurrentGameMode_Parms), Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics
	{
		struct PWGameInstance_eventGetCurrentSelectCharacter_Parms
		{
			ESelectCharacter ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetCurrentSelectCharacter_Parms, ReturnValue), Z_Construct_UEnum_PaintWar_ESelectCharacter, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetCurrentSelectCharacter", sizeof(PWGameInstance_eventGetCurrentSelectCharacter_Parms), Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics
	{
		struct PWGameInstance_eventGetCurrentSelectTeam_Parms
		{
			ETeam ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetCurrentSelectTeam_Parms, ReturnValue), Z_Construct_UEnum_PaintWar_ETeam, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetCurrentSelectTeam", sizeof(PWGameInstance_eventGetCurrentSelectTeam_Parms), Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics
	{
		struct PWGameInstance_eventGetGreenResultCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetGreenResultCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetGreenResultCount", sizeof(PWGameInstance_eventGetGreenResultCount_Parms), Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics
	{
		struct PWGameInstance_eventGetIsGameStart_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventGetIsGameStart_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventGetIsGameStart_Parms), &Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetIsGameStart", sizeof(PWGameInstance_eventGetIsGameStart_Parms), Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetIsGameStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetIsGameStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics
	{
		struct PWGameInstance_eventGetLoginID_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetLoginID_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetLoginID", sizeof(PWGameInstance_eventGetLoginID_Parms), Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetLoginID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetLoginID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics
	{
		struct PWGameInstance_eventGetRedResultCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventGetRedResultCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "GetRedResultCount", sizeof(PWGameInstance_eventGetRedResultCount_Parms), Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_GetRedResultCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_GetRedResultCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics
	{
		struct PWGameInstance_eventInitNetWork_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventInitNetWork_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventInitNetWork_Parms), &Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
		{ "ToolTip", "For NetWork Functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "InitNetWork", sizeof(PWGameInstance_eventInitNetWork_Parms), Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_InitNetWork()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_InitNetWork_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics
	{
		struct PWGameInstance_eventSend_InitLogin_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventSend_InitLogin_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventSend_InitLogin_Parms), &Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "Send_InitLogin", sizeof(PWGameInstance_eventSend_InitLogin_Parms), Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_Send_InitLogin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_Send_InitLogin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics
	{
		struct PWGameInstance_eventSend_ReadyInfo_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventSend_ReadyInfo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventSend_ReadyInfo_Parms), &Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "Send_ReadyInfo", sizeof(PWGameInstance_eventSend_ReadyInfo_Parms), Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics
	{
		struct PWGameInstance_eventSend_SelectCharacterTypePacket_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventSend_SelectCharacterTypePacket_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventSend_SelectCharacterTypePacket_Parms), &Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "Send_SelectCharacterTypePacket", sizeof(PWGameInstance_eventSend_SelectCharacterTypePacket_Parms), Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics
	{
		struct PWGameInstance_eventSend_SelectTeamTypePacket_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWGameInstance_eventSend_SelectTeamTypePacket_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWGameInstance_eventSend_SelectTeamTypePacket_Parms), &Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
		{ "ToolTip", "Send packet Process Functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "Send_SelectTeamTypePacket", sizeof(PWGameInstance_eventSend_SelectTeamTypePacket_Parms), Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics
	{
		struct PWGameInstance_eventSetCurrentGameMode_Parms
		{
			EGameMode Mode;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetCurrentGameMode_Parms, Mode), Z_Construct_UEnum_PaintWar_EGameMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::NewProp_Mode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::NewProp_Mode_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetCurrentGameMode", sizeof(PWGameInstance_eventSetCurrentGameMode_Parms), Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics
	{
		struct PWGameInstance_eventSetCurrentSelectCharacter_Parms
		{
			ESelectCharacter SelectCharacter;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectCharacter;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SelectCharacter_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::NewProp_SelectCharacter = { "SelectCharacter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetCurrentSelectCharacter_Parms, SelectCharacter), Z_Construct_UEnum_PaintWar_ESelectCharacter, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::NewProp_SelectCharacter_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::NewProp_SelectCharacter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::NewProp_SelectCharacter_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetCurrentSelectCharacter", sizeof(PWGameInstance_eventSetCurrentSelectCharacter_Parms), Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics
	{
		struct PWGameInstance_eventSetCurrentSelectTeam_Parms
		{
			ETeam SelectTeam;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectTeam;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SelectTeam_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::NewProp_SelectTeam = { "SelectTeam", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetCurrentSelectTeam_Parms, SelectTeam), Z_Construct_UEnum_PaintWar_ETeam, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::NewProp_SelectTeam_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::NewProp_SelectTeam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::NewProp_SelectTeam_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetCurrentSelectTeam", sizeof(PWGameInstance_eventSetCurrentSelectTeam_Parms), Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics
	{
		struct PWGameInstance_eventSetGreenResultCount_Parms
		{
			int32 num;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_num;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::NewProp_num = { "num", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetGreenResultCount_Parms, num), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::NewProp_num,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetGreenResultCount", sizeof(PWGameInstance_eventSetGreenResultCount_Parms), Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics
	{
		struct PWGameInstance_eventSetLoginID_Parms
		{
			FString id;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetLoginID_Parms, id), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::NewProp_id,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetLoginID", sizeof(PWGameInstance_eventSetLoginID_Parms), Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetLoginID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetLoginID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics
	{
		struct PWGameInstance_eventSetRedResultCount_Parms
		{
			int32 num;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_num;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::NewProp_num = { "num", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameInstance_eventSetRedResultCount_Parms, num), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::NewProp_num,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWGameInstance, nullptr, "SetRedResultCount", sizeof(PWGameInstance_eventSetRedResultCount_Parms), Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWGameInstance_SetRedResultCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWGameInstance_SetRedResultCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPWGameInstance_NoRegister()
	{
		return UPWGameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPWGameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllRenderTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllRenderTargets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllRenderTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentGameMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentGameMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentGameMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentSelectTeam_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentSelectTeam;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentSelectTeam_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentSelectCharacter_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentSelectCharacter;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentSelectCharacter_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPWGameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPWGameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPWGameInstance_Connect, "Connect" }, // 368898381
		{ &Z_Construct_UFunction_UPWGameInstance_GetCurrentGameMode, "GetCurrentGameMode" }, // 1249237674
		{ &Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectCharacter, "GetCurrentSelectCharacter" }, // 2173026965
		{ &Z_Construct_UFunction_UPWGameInstance_GetCurrentSelectTeam, "GetCurrentSelectTeam" }, // 3704203593
		{ &Z_Construct_UFunction_UPWGameInstance_GetGreenResultCount, "GetGreenResultCount" }, // 1065581627
		{ &Z_Construct_UFunction_UPWGameInstance_GetIsGameStart, "GetIsGameStart" }, // 676636256
		{ &Z_Construct_UFunction_UPWGameInstance_GetLoginID, "GetLoginID" }, // 583404702
		{ &Z_Construct_UFunction_UPWGameInstance_GetRedResultCount, "GetRedResultCount" }, // 2643525644
		{ &Z_Construct_UFunction_UPWGameInstance_InitNetWork, "InitNetWork" }, // 4252042546
		{ &Z_Construct_UFunction_UPWGameInstance_Send_InitLogin, "Send_InitLogin" }, // 2942971600
		{ &Z_Construct_UFunction_UPWGameInstance_Send_ReadyInfo, "Send_ReadyInfo" }, // 2515296007
		{ &Z_Construct_UFunction_UPWGameInstance_Send_SelectCharacterTypePacket, "Send_SelectCharacterTypePacket" }, // 50101440
		{ &Z_Construct_UFunction_UPWGameInstance_Send_SelectTeamTypePacket, "Send_SelectTeamTypePacket" }, // 3185034566
		{ &Z_Construct_UFunction_UPWGameInstance_SetCurrentGameMode, "SetCurrentGameMode" }, // 161832802
		{ &Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectCharacter, "SetCurrentSelectCharacter" }, // 1197874661
		{ &Z_Construct_UFunction_UPWGameInstance_SetCurrentSelectTeam, "SetCurrentSelectTeam" }, // 1354009174
		{ &Z_Construct_UFunction_UPWGameInstance_SetGreenResultCount, "SetGreenResultCount" }, // 633873410
		{ &Z_Construct_UFunction_UPWGameInstance_SetLoginID, "SetLoginID" }, // 408582367
		{ &Z_Construct_UFunction_UPWGameInstance_SetRedResultCount, "SetRedResultCount" }, // 3214837897
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWGameInstance_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PWGameInstance.h" },
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
		{ "ToolTip", "????\xc5\xb8?\xd9\xb5??? ???\xce\xb8??? 300?\xca\xb0? ?????\xda\xbf? ?\xcb\xbb??\xcf\xb1?????." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets = { "AllRenderTargets", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWGameInstance, AllRenderTargets), METADATA_PARAMS(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets_Inner = { "AllRenderTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode = { "CurrentGameMode", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWGameInstance, CurrentGameMode), Z_Construct_UEnum_PaintWar_EGameMode, METADATA_PARAMS(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam = { "CurrentSelectTeam", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWGameInstance, CurrentSelectTeam), Z_Construct_UEnum_PaintWar_ETeam, METADATA_PARAMS(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter = { "CurrentSelectCharacter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWGameInstance, CurrentSelectCharacter), Z_Construct_UEnum_PaintWar_ESelectCharacter, METADATA_PARAMS(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPWGameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_AllRenderTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentGameMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectTeam_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWGameInstance_Statics::NewProp_CurrentSelectCharacter_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPWGameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPWGameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPWGameInstance_Statics::ClassParams = {
		&UPWGameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPWGameInstance_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPWGameInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPWGameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPWGameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPWGameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPWGameInstance, 852717674);
	template<> PAINTWAR_API UClass* StaticClass<UPWGameInstance>()
	{
		return UPWGameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPWGameInstance(Z_Construct_UClass_UPWGameInstance, &UPWGameInstance::StaticClass, TEXT("/Script/PaintWar"), TEXT("UPWGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPWGameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
