// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBomberCharacter_generated_h
#error "PWBomberCharacter.generated.h already included, missing '#pragma once' in PWBomberCharacter.h"
#endif
#define PAINTWAR_PWBomberCharacter_generated_h

#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnBomberProjectile) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnBomberProjectile(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUnAbleFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnAbleFire(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnBomberProjectile) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnBomberProjectile(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUnAbleFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnAbleFire(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBomberCharacter(); \
	friend struct Z_Construct_UClass_APWBomberCharacter_Statics; \
public: \
	DECLARE_CLASS(APWBomberCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWBomberCharacter(); \
	friend struct Z_Construct_UClass_APWBomberCharacter_Statics; \
public: \
	DECLARE_CLASS(APWBomberCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBomberCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBomberCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberCharacter(APWBomberCharacter&&); \
	NO_API APWBomberCharacter(const APWBomberCharacter&); \
public:


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberCharacter(APWBomberCharacter&&); \
	NO_API APWBomberCharacter(const APWBomberCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBomberCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Projectile() { return STRUCT_OFFSET(APWBomberCharacter, Projectile); } \
	FORCEINLINE static uint32 __PPO__BomberAnim() { return STRUCT_OFFSET(APWBomberCharacter, BomberAnim); }


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBomberCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Characters_PWBomberCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
