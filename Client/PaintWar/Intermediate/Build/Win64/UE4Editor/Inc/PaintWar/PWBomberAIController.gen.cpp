// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/AIControllers/PWBomberAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWBomberAIController() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberAIController_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberAIController();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseAIController();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWBomberAIController::StaticRegisterNativesAPWBomberAIController()
	{
	}
	UClass* Z_Construct_UClass_APWBomberAIController_NoRegister()
	{
		return APWBomberAIController::StaticClass();
	}
	struct Z_Construct_UClass_APWBomberAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWBomberAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBomberAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "AIControllers/PWBomberAIController.h" },
		{ "ModuleRelativePath", "Public/AIControllers/PWBomberAIController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWBomberAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWBomberAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWBomberAIController_Statics::ClassParams = {
		&APWBomberAIController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWBomberAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWBomberAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWBomberAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWBomberAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWBomberAIController, 3094622923);
	template<> PAINTWAR_API UClass* StaticClass<APWBomberAIController>()
	{
		return APWBomberAIController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWBomberAIController(Z_Construct_UClass_APWBomberAIController, &APWBomberAIController::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWBomberAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWBomberAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
