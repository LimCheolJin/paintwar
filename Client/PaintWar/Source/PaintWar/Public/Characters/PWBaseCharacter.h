// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "Components/AudioComponent.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/Character.h"
#include "PWBaseCharacter.generated.h"

DECLARE_DELEGATE(FDeathDelegate);
DECLARE_DELEGATE_OneParam(FKillEventDelegate, class APWBaseCharacter*);

UENUM()
enum class ECharacterState
{
	ALIVE		UMETA(DisplayName = "Alive"), 
	DEATH		UMETA(DisplayName = "Death")
};

UCLASS()
class PAINTWAR_API APWBaseCharacter : public ACharacter
{
	GENERATED_BODY()
	

public:
	// Sets default values for this character's properties
	APWBaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//카메라의 스프링암 부모가될 것임
	UPROPERTY(VisibleAnywhere, Category = Camera)
	USpringArmComponent* SpringArm;

	//카메라
	UPROPERTY(VisibleAnywhere, Category = Camera)
	UCameraComponent* Camera;

	//카메라 세팅을 위해 만든 함수.
	void SetCameraSetting();
	void TakenDamage(class APWBaseCharacter* DamageInstigator, float Damage);

	FKillEventDelegate KillEventDeleSingle;
	FDeathDelegate DeathDeleSingle;

	UFUNCTION()
	void DeathEvent();

	UFUNCTION()
	void KillEvent(class APWBaseCharacter* KillInstigator);

	UFUNCTION()
	virtual void RespawnEvent();

	UFUNCTION()
	void DeathProcess();

	float GetHealthPoint() const;

	UFUNCTION(BlueprintCallable)
	ETeam GetTeam() const;

	void SetTeam(ETeam Team);

	ESelectCharacter GetCharacterType() const;

	void SetCharacterType(ESelectCharacter Type);

	ECharacterState GetCurrentCharacterState() const;

	void SetCurrentCharacterState(ECharacterState state);

	UFUNCTION(BlueprintCallable)
	void StartReload();

	UFUNCTION(BlueprintCallable)
	void QuitReload();

	void ReloadProcess();

	UFUNCTION(BlueprintCallable)
	bool GetIsReload() const;

	UFUNCTION(BlueprintCallable)
	float GetAmountPaint() const;

	void SetAmountPaint(int);

	void DiscountAmountPaint(float UsingPaintAmount);

	void ZoomIn();
	void ZoomOut();

	UFUNCTION(BlueprintCallable)
	int GetKill();
	UFUNCTION(BlueprintCallable)
	int GetDeath();

	FLinearColor WidgetColor;

	UFUNCTION()
	void IsCharacterFalling();

	void AddKillCount(class APWBaseCharacter* );
	void AddDeathCount();

	int GetKillCount() const;
	int GetDeathCount() const;
	
	void SetKillCount(int);

	UFUNCTION(BlueprintCallable)
	FString GetLoginID();
	UFUNCTION(BlueprintCallable)
	void SetLoginID(FString id);
	
	UPROPERTY(VisibleAnywhere, Category = UI)
	class UWidgetComponent* IDWidget;

	void SetMyLoginID_OnWidget();

	UFUNCTION(BlueprintCallable)
	APWBaseWeapon* GetWeapon() const;

	int SkillCount;
	UFUNCTION(BlueprintCallable)
	int GetSkillCount() const;
	void SetSkillCount(int);

	float UsedPaint;
	UFUNCTION(BlueprintCallable)
	bool GetSkillON() const;
	void SetSkillON(bool) ;


	bool SkillON;

	virtual void Skill();
	virtual void Skilloff();

	UPROPERTY()
		FTimerHandle SkillTimerHandle;

	///////////////////사운드

	UPROPERTY()
		class USoundCue* AttackSound;

	UAudioComponent* AttackSoundComponent;

private:
	//Right Input
	void MoveRight(float NewAxisValue);

	//Forward Input
	void MoveForward(float NewAxisValue);

	//마우스 회전을 위한함수들 추가
	void LookUp(float NewAxisValue);
	void Turn(float NewAxisValue);
	virtual void Jump() override;

	virtual void Fire() { };  //순수가상함수
	

	struct FTimerHandle DeathEventTimerHandle;
	struct FTimerHandle RespawnEventTimerHandle;
	struct FTimerHandle IsFallingEventTimerHandle;


	bool IsReload;

	UPROPERTY()
	FTimerHandle ReloadHandle;

	//kill 카운트
	int Kill = 0;

	//death 카운트
	int Death = 0;


//무기 부착을 위함.
protected:
	UPROPERTY()
	class APWBaseWeapon* Weapon;

	UPROPERTY()
	float HealthPoint;

	UPROPERTY()
	ECharacterState CurrentCharacterState;

	UPROPERTY()//#include "Materials/MaterialInstanceConstant.h"
	class UMaterialInstanceConstant* RedTeamCharacterMaterialInstance;

	UPROPERTY()
	class UMaterialInstanceConstant* GreenTeamCharacterMaterialInstance;

	UPROPERTY()
	class UMaterialInstanceConstant* SkillMaterialInstance;


	UPROPERTY(EditAnywhere, Category = Type)
	ESelectCharacter CharacterType;

	UPROPERTY()
	TArray<AActor*> SpawnPointArray;

	UPROPERTY()
	FVector SpawnPointLocation;

	UPROPERTY(EditAnywhere, Category = Team)
	ETeam CurrentTeam;
	
	float AmountPaint;
	
	bool IsMove;

	//네트워크를 위한변수
	int m_ID;
	FString LoginID;
public:
	int Get_m_ID() const;

	void Set_m_ID(int ID);


};
