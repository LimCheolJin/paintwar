// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PAINTWAR_PWJumpPad_generated_h
#error "PWJumpPad.generated.h already included, missing '#pragma once' in PWJumpPad.h"
#endif
#define PAINTWAR_PWJumpPad_generated_h

#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->BeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->BeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWJumpPad(); \
	friend struct Z_Construct_UClass_APWJumpPad_Statics; \
public: \
	DECLARE_CLASS(APWJumpPad, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWJumpPad)


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWJumpPad(); \
	friend struct Z_Construct_UClass_APWJumpPad_Statics; \
public: \
	DECLARE_CLASS(APWJumpPad, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWJumpPad)


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWJumpPad(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWJumpPad) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWJumpPad); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWJumpPad); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWJumpPad(APWJumpPad&&); \
	NO_API APWJumpPad(const APWJumpPad&); \
public:


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWJumpPad(APWJumpPad&&); \
	NO_API APWJumpPad(const APWJumpPad&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWJumpPad); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWJumpPad); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWJumpPad)


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__JumpPadMesh() { return STRUCT_OFFSET(APWJumpPad, JumpPadMesh); } \
	FORCEINLINE static uint32 __PPO__JumpHeight() { return STRUCT_OFFSET(APWJumpPad, JumpHeight); } \
	FORCEINLINE static uint32 __PPO__CollisionBox() { return STRUCT_OFFSET(APWJumpPad, CollisionBox); }


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWJumpPad>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Obstacles_PWJumpPad_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
