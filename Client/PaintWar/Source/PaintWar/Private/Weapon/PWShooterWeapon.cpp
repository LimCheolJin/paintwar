// Fill out your copyright notice in the Description page of Project Settings.


#include "PWShooterWeapon.h"
#include "Components/ArrowComponent.h"
#include "PWShooterCharacter.h"
#include "PWShooterProjectile.h"
#include "Materials/MaterialInstanceConstant.h"

// Sets default values
APWShooterWeapon::APWShooterWeapon()
{
	//this->SetReplicates(true);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_GUN(TEXT("/Game/TestAssets/TestWeapon/Gun/gun_200324.gun_200324"));
	if (ST_GUN.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		WeaponSTMesh->SetStaticMesh(ST_GUN.Object); //StaticMesh'/Game/TestAssets/TestWeapon/Gun/gun_200324.gun_200324'
	}
	//붙히고 돌리기 메쉬에 그리고나서 스켈레톤에서 프리뷰로 소켓확인
	//잘적용되게하고 스폰포인트로부터 총알 스폰하게하기.

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_ShooterRedTeam(TEXT("/Game/TestAssets/TestWeapon/Gun/M_GunRed.M_GunRed"));
	if (M_ShooterRedTeam.Succeeded())
	{
		ShooterRed = M_ShooterRedTeam.Object;
	} //Material'/Game/TestAssets/TestWeapon/Gun/M_GunGreen.M_GunGreen'

	//Material'/Game/TestAssets/TestWeapon/Gun/M_GunRed.M_GunRed'
	//MaterialInstanceConstant'/Game/SpaceExplorer/Materials/SpaceExplorerMat_GreenTeam.SpaceExplorerMat_GreenTeam'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_ShooterGreenTeam(TEXT("/Game/TestAssets/TestWeapon/Gun/M_GunGreen.M_GunGreen"));
	if (M_ShooterGreenTeam.Succeeded())
	{
		ShooterGreen = M_ShooterGreenTeam.Object;
	}

	//static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetRed(TEXT("/Game/TestAssets/testPaintParticle/LiquidRed.LiquidRed"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetRed(TEXT("/Game/Particles/P_ShooterRed.P_ShooterRed"));
	if (ParticleAssetRed.Succeeded())
	{
		//PaintParticleLeft->SetTemplate(ParticleAssetLeft.Object);
		ParticleRed = ParticleAssetRed.Object;
	}

	//static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetGreen(TEXT("/Game/TestAssets/testPaintParticle/LiquidGreen.LiquidGreen"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAssetGreen(TEXT("/Game/Particles/P_ShooterGreen.P_ShooterGreen"));
	if (ParticleAssetGreen.Succeeded())
	{
		//PaintParticleRight->SetTemplate(ParticleAssetRight.Object);
		ParticleGreen = ParticleAssetGreen.Object;
	} //왼오른 둘다 같은거면 하나로만 불러와서쓸수잇을거같음 일단 2개

	////Scale
	//WeaponSTMesh->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	WeaponSTMesh->SetRelativeScale3D(FVector(0.6f, 0.6f, 0.6f));
	ShooterProjectileParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ShooterParticle"));
	ShooterProjectileParticle->SetupAttachment(WeaponSTMesh);
	ShooterProjectileParticle->bAutoActivate = false;
	//ShooterProjectileParticle->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));
	ShooterProjectileParticle->SetRelativeScale3D(FVector(0.6f, 0.6f, 0.6f));
	ShooterProjectileParticle->SetRelativeRotation(FRotator(-90.0f, -90.0f, 90.0f));
	ShooterProjectileParticle->SetRelativeLocation(FVector(6.0f, 0.0f, 0.0f));
	ProjectileSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileSpawnPoint"));
	ProjectileSpawnPoint->SetupAttachment(WeaponSTMesh);
	ProjectileSpawnPoint->SetRelativeLocation(FVector(7.5f, 0.0f, 0.0f));
	ProjectileSpawnPoint->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));

	//Scale
	//ProjectileSpawnPoint->SetRelativeLocation(FVector(20.0f, 0.0f, 0.0f));//x 20.0f
	//ProjectileSpawnPoint->SetRelativeLocation(FVector(25.0f, 0.0f, 0.0f));
	ProjectileSpawnPoint->bHiddenInGame = false;
}
void APWShooterWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::RED)
	{
		WeaponSTMesh->SetMaterial(0, ShooterRed);
		ShooterProjectileParticle->SetTemplate(ParticleRed);
	}
	else if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::GREEN)
	{
		WeaponSTMesh->SetMaterial(0, ShooterGreen);
		ShooterProjectileParticle->SetTemplate(ParticleGreen);
	}
	//ShooterProjectileParticle->SetActive(true);

}
void APWShooterWeapon::Tick(float DeltaTime)
{
	
}


UParticleSystemComponent* APWShooterWeapon::GetShooterParticle() const
{
	return ShooterProjectileParticle;
}