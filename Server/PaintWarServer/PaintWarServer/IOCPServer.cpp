
#pragma once

#include "IOCPServer.h"
#include <iostream>
#include <string>
using namespace std;

void IOCPServer::mainLoop()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	m_ListenSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN ServerAddr;
	memset(&ServerAddr, 0x00, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(SERVERPORT);
	ServerAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	::bind(m_ListenSock, reinterpret_cast<sockaddr*>(&ServerAddr), sizeof(ServerAddr));

	listen(m_ListenSock, SOMAXCONN);

	//iocp 생성
	m_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	// 엑셉트도 iocp로 받기위해 등록.
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(m_ListenSock), m_iocp, 999, 0);

	//동기 accpet랑 다르게 미리 소켓을 만들어놓고 이를이용해 클라이언트와 통신한다.
	SOCKET Client_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER Accept_over;
	ZeroMemory(&Accept_over.Over, sizeof(Accept_over.Over));
	Accept_over.Op = OP_ACCEPT;
	AcceptEx(m_ListenSock, Client_Sock, Accept_over.IO_Buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &Accept_over.Over);

	while (true)
	{
		DWORD iobytes;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(m_iocp, &iobytes, &key, &over, INFINITE);
		//완료한 결과를 담고잇는 overlapped구조체를 확장 overlapped구조체로 탈바꿈
		EXOVER* exover = reinterpret_cast<EXOVER*>(over);
		int user_id = static_cast<int>(key);
		CLIENT& Client = m_Clients[user_id];
		
		//Accept시 id를 999로
		m_Clients[user_id].m_ID = user_id;
		switch (exover->Op)
		{
		case OP_RECV:
		{//recv가 완료했으면 패킷처리

			if (iobytes == 0)
			{
				Disconnect(user_id);
			}
			else
			{
				char* ptr = exover->IO_Buf;
				static size_t in_packet_size = 0;
				while (0 != iobytes)
				{
					if (0 == in_packet_size)
						in_packet_size = ptr[0];
					if (iobytes + Client.m_PrevSize >= in_packet_size)
					{
						memcpy(Client.m_PacketBuf + Client.m_PrevSize, ptr, in_packet_size - Client.m_PrevSize);
						ProcessPacket(user_id, Client.m_PacketBuf);
						ptr += in_packet_size - Client.m_PrevSize;
						iobytes -= in_packet_size - Client.m_PrevSize;
						in_packet_size = 0;
						Client.m_PrevSize = 0;
					}
					else
					{
						memcpy(Client.m_PacketBuf + Client.m_PrevSize, ptr, iobytes);
						Client.m_PrevSize += iobytes;
						iobytes = 0;
						break;
					}
				}

				//ProcessPacket(user_id, exover->IO_Buf);
				ZeroMemory(&Client.m_RecvOver.Over, sizeof(Client.m_RecvOver.Over));
				DWORD flags = 0;

				WSARecv(Client.m_Sock, &Client.m_RecvOver.WSABuf, 1, NULL, &flags, &Client.m_RecvOver.Over, NULL);
			}
		}
			break;
		case OP_SEND:
		{//센드가 완료됐으면 오버랩구조체 메모리를 반환
			if (iobytes == 0)
				Disconnect(user_id);
			delete exover;
		}
			break;
		case OP_ACCEPT:
		{
			int id = m_current_user_id++;

			CreateIoCompletionPort(reinterpret_cast<HANDLE>(Client_Sock), m_iocp, id, 0); //key값으로 id를 준다.

			m_current_user_id = m_current_user_id;// % MAXPLAYER; //아이디가 초과하는것을 방지하기위함.
			CLIENT& newClient = m_Clients[id];
			newClient.m_ID = id;
			newClient.m_PrevSize = 0;
			newClient.m_RecvOver.Op = OP_RECV;
			ZeroMemory(&newClient.m_RecvOver.Over, sizeof(newClient.m_RecvOver.Over));
			newClient.m_RecvOver.WSABuf.buf = newClient.m_RecvOver.IO_Buf;
			newClient.m_RecvOver.WSABuf.len = MAXBUFFER;
			newClient.m_Sock = Client_Sock;

			// accpet하고난뒤에 초기화할 정보들을 이곳에 추가.
			cout << "Client" << id << " 접속" << endl;
			//

			DWORD flags = 0;
			WSARecv(newClient.m_Sock, &newClient.m_RecvOver.WSABuf, 1, NULL, &flags, &newClient.m_RecvOver.Over, NULL);


			

			//계속해서 Accept를 받기위함.
			Client_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			ZeroMemory(&Accept_over.Over, sizeof(Accept_over.Over));
			AcceptEx(m_ListenSock, Client_Sock, Accept_over.IO_Buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &Accept_over.Over);

		}
			break;
		}
	}

	return;
}
void IOCPServer::Disconnect(int user_id)
{
	m_Clients.erase(user_id);
	cout << "[Disconnect] user_id: " << user_id << endl;
	if (m_Clients.begin()->second.m_ID == 999)
	{
		if (m_GameTimer != nullptr)
			StopTimer();
	}
	for (auto& client : m_Clients)
	{
		Send_Leave_Packet(client.second.m_ID,user_id);
	}
}
void IOCPServer::ProcessPacket(int user_id, char* buf)
{
	e_PacketType packetType = static_cast<e_PacketType>(buf[1]);
	//cout << (int)buf[1] << endl;
	switch (packetType)
	{
	case e_PacketType::e_LogInPacket:
	{
		//로그인 패킷처리 후 해당 클라이언트에게 로그인ok 패킷 전송.
		CS_PACKET_LOGIN* packet = reinterpret_cast<CS_PACKET_LOGIN*>(buf);
		//strcpy_s(m_Clients[user_id].name, packet->)
		m_Clients[user_id].m_ID = user_id;
		memcpy(m_Clients[user_id].name, packet->m_LoginID, MAX_ID_LEN);
		string a = string(m_Clients[user_id].name);
		cout << "아이디 : " << a << endl;
		//m_Clients[user_id].name
		if(user_id == 0)
			m_Clients[user_id].m_IsRoomHead = true;
		else
			m_Clients[user_id].m_IsRoomHead = false;

		Send_Login_OK_Packet(user_id);
		Enter_Game(user_id); // 다른플레이어와 나를위함.
	}
		break;
	case e_PacketType::e_SelectTeamPacket:
	{
		//받은 후 해당클라이언트의 정보를  서버의 client 배열에 set해주고 이 정보를 다른 클라이언트가 있으면 다른 클라이언트들에게 브로드캐스트
		CS_PACKET_SELECT_TEAM* packet = reinterpret_cast<CS_PACKET_SELECT_TEAM*>(buf);
		m_Clients[user_id].TeamType = packet->m_eTeamType; 
		
		Select_Team_Process(user_id);
		cout << "클라이언트 user id: " << user_id << " 팀 선택: " << (int)packet->m_eTeamType << endl;
	}
		break;
	case e_PacketType::e_SelectCharacterTypePacket:
	{
		//받은 후 해당클라이언트의 정보를  서버의 client 배열에 set해주고 이 정보를 다른 클라이언트가 있으면 다른 클라이언트들에게 브로드캐스트
		CS_PACKET_SELECT_CHARACTERTYPE* packet = reinterpret_cast<CS_PACKET_SELECT_CHARACTERTYPE*>(buf);
		m_Clients[user_id].CharacterType = packet->m_eCharacterType;

		Select_CharacterType_Process(user_id);
		cout << "클라이언트 user id: " << user_id << " 캐릭터 선택: " << (int)packet->m_eCharacterType << endl;
	}
		break;
	case e_PacketType::e_ReadyPacket:
	{
		CS_PACKET_READY* packet = reinterpret_cast<CS_PACKET_READY*>(buf);
		m_Clients[user_id].m_ID = packet->m_ID;
		m_Clients[user_id].GameStateinfo = packet->m_PlayerState;
		cout << "클라이언트 user id: " << user_id << " Player State info is " << (int)packet->m_PlayerState << endl;

		if (Check_IsAllReady())
		{//이안에 오면 모두 레디도하고 모두 캐릭터,팀선택도 했따는 것임.
			Send_StartGame();
			cout << "모든 클라이언트 레디 완료" << endl;
		}

	}
		break;
	case e_PacketType::e_PlayerInfoPacket:
	{
		CS_PACKET_PLAYER_INFO* packet = reinterpret_cast<CS_PACKET_PLAYER_INFO*>(buf);
		m_Clients[user_id].m_Player.m_PlayerInfo = packet->m_ObjInfo;
		m_Clients[user_id].m_Player.SKillCount = packet->SkillCount;
		m_Clients[user_id].m_Player.SkillON = packet->SkillON;
		m_Clients[user_id].m_Player.AmountPaint = packet->m_AmountPaint;
		/*ObjectInfo& info = m_Clients[user_id].m_Player.m_PlayerInfo;
		cout << "[클라이언트 user id: " << user_id << " PlayerInfo]" << endl;
		cout << "Position: " << info.m_ObjectPosition.x << ", " << info.m_ObjectPosition.y << ", " << info.m_ObjectPosition.z << endl;
		cout << "Rotation: " << info.m_ObjectRotation.Pitch << ", " << info.m_ObjectRotation.Yaw << ", " << info.m_ObjectRotation.Roll << endl;
		cout << "Velocity: " << info.m_ObjectVelocity.vx << ", " << info.m_ObjectVelocity.vy << ", " << info.m_ObjectVelocity.vz << endl;
		cout << "------------------------------------------------------------------" << endl;*/

		//받아왓으니 다른플레이어들에게 전송해줘야한다.
		Move_Character_Process(user_id);
	}
		break;
	case e_PacketType::e_ShooterFire:
	{
		CS_PACKET_SHOOTER_FIRE* packet = reinterpret_cast<CS_PACKET_SHOOTER_FIRE*>(buf);
		m_Clients[user_id].m_Player.IsFire = packet->m_IsFire;
		//cout << "shooterfire is ID is " << user_id <<",        "<<packet->m_IsFire << endl;
		Shooter_Fire_Process(user_id);
	}
		break;
	case e_PacketType::e_RollerPaint:
	{
		CS_PACKET_ROLLER_PAINT* packet = reinterpret_cast<CS_PACKET_ROLLER_PAINT*>(buf);
		m_Clients[user_id].m_Player.IsRolling = packet->m_IsRolling;

		Roller_Paint_Process(user_id);
	}
		break;
	case e_PacketType::e_BomberThrow:
	{
		CS_PACKET_BOBMER_THROW* packet = reinterpret_cast<CS_PACKET_BOBMER_THROW*>(buf);
		m_Clients[user_id].m_Player.IsThrowing = packet->m_IsThrowing;

		Bomber_Throw_Process(user_id);
	}
		break;
	case e_PacketType::e_Reloading:
	{
		CS_PACKET_RELOAD* packet = reinterpret_cast<CS_PACKET_RELOAD*>(buf);
		m_Clients[user_id].m_Player.IsReloading = packet->m_IsReloading;

		Reloading_Process(user_id);
	}
		break;
	case e_PacketType::e_ResultGame:
	{
		CS_PACKET_GAME_RESULT* packet = reinterpret_cast<CS_PACKET_GAME_RESULT*>(buf);
		
		GameResult_Process(user_id, packet->m_ResultRedCount,packet->m_ResultGreenCount);
		
	}
		break;
	case e_PacketType::e_ChatPacket:
	{
		CS_PACKET_CHAT* packet = reinterpret_cast<CS_PACKET_CHAT*>(buf);
		
		CLIENT cl;
		SC_PACKET_CHAT scpacket{};
		scpacket.m_ePacketType = e_PacketType::e_ChatPacket;
		wcscpy(scpacket.m_Chat, packet->m_Chat);
		scpacket.m_PacketSize = sizeof(SC_PACKET_CHAT);

		for (auto& Client : m_Clients)
		{
			if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
			{
				cl = Client.second;
				Send_Packet(cl.m_ID, &scpacket);


			}
		}
	}
		break;
	default:
		cout << "Unknown Packet Type Error";
		DebugBreak();
		exit(-1);
	}

}

void IOCPServer::Send_Login_OK_Packet(int user_id)
{
	SC_PACKET_LOGIN_OK packet;
	packet.m_PacketSize = sizeof(packet);
	packet.m_ePacketType = e_PacketType::e_LoginOKPacket;
	packet.m_ID = user_id;
	packet.m_IsRoomHead = m_Clients[user_id].m_IsRoomHead;


	Send_Packet(user_id, &packet);

}
void IOCPServer::Send_Packet(int user_id, void* p)
{
	char* buf = reinterpret_cast<char*>(p);
	CLIENT& user = m_Clients[user_id];

	//별도의 센드용 확장오버랩구조체를 생성한다.
	EXOVER* exover = new EXOVER;
	exover->Op = OP_SEND;
	ZeroMemory(&exover->Over, sizeof(exover->Over));
	exover->WSABuf.buf = exover->IO_Buf;
	exover->WSABuf.len = buf[0];
	memcpy(exover->IO_Buf, buf, buf[0]);

	WSASend(user.m_Sock, &exover->WSABuf, 1, NULL, 0, &exover->Over, NULL);
}
void IOCPServer::Enter_Game(int user_id)
{
	for (auto& client : m_Clients)
	{
		if ((user_id != client.second.m_ID) && (client.second.m_ID != 999))
		{
			Send_Enter_Packet(user_id, client.second.m_ID);//나에게 다른클라를
			Send_Enter_Packet(client.second.m_ID, user_id);//다른클라에게 나를
		}
	}
}
void IOCPServer::Send_Enter_Packet(int user_id, int otherid)
{
	SC_PACKET_ENTER p;
	p.m_ID = otherid;
	p.m_PacketSize = sizeof(SC_PACKET_ENTER);
	p.m_ePacketType = e_PacketType::e_EnterPacket;
	p.m_IsRoomHead = m_Clients[otherid].m_IsRoomHead;
	memcpy(p.m_LoginID, m_Clients[otherid].name, MAX_ID_LEN);

	cout << "[Enter] user_id: " << user_id << ", otherid: " << otherid << endl;
	Send_Packet(user_id, &p);
}

void IOCPServer::Select_Team_Process(int user_id)
{
	for (auto& client : m_Clients)
	{
		if ((user_id != client.second.m_ID) && (client.second.m_ID != 999))
		{
			Send_TeamType_Packet(user_id, client.second.m_ID);//나에게 다른클라를
			Send_TeamType_Packet(client.second.m_ID, user_id);//다른클라에게 나를
		}
	}
}
void IOCPServer::Select_CharacterType_Process(int user_id)
{
	for (auto& client : m_Clients)
	{
		if ((user_id != client.second.m_ID) && (client.second.m_ID != 999))
		{
			Send_CharacterType_Packet(user_id, client.second.m_ID);//나에게 다른클라를
			Send_CharacterType_Packet(client.second.m_ID, user_id);//다른클라에게 나를
		}
	}
}
void IOCPServer::Move_Character_Process(int user_id)
{
	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			//자기한태 남들의 정보를 보낼필요가 없다지금?
			//Send_PlayerInfo_Packet(user_id, Client.second.m_ID);
			Send_PlayerInfo_Packet(Client.second.m_ID, user_id);
		}
	}
}
void IOCPServer::Shooter_Fire_Process(int user_id)
{
	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			Send_ShooterFire_Packet(Client.second.m_ID, user_id);
		}
	}
}
void IOCPServer::Roller_Paint_Process(int user_id)
{
	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			Send_RollerPaint_Packet(Client.second.m_ID, user_id);
		}
	}
}
void IOCPServer::Bomber_Throw_Process(int user_id)
{
	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			Send_BomberThrow_Packet(Client.second.m_ID, user_id);
		}
	}
}
void IOCPServer::Reloading_Process(int user_id)
{
	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			Send_Reloading_Packet(Client.second.m_ID, user_id);
		}
	}
}




void IOCPServer::GameResult_Process(int user_id, int red, int green)
{
	CLIENT cl;
	SC_PACKET_GAME_RESULT packet;
	packet.m_ePacketType = e_PacketType::e_ResultGame;
	packet.m_ResultGreenCount = green;
	packet.m_ResultRedCount = red;
	packet.m_PacketSize = sizeof(SC_PACKET_GAME_RESULT);

	for (auto& Client : m_Clients)
	{
		if ((user_id != Client.second.m_ID) && (Client.second.m_ID != 999))
		{
			cl = Client.second;
			Send_Packet(cl.m_ID, &packet);
			
			
		}
	}
}


void IOCPServer::Send_TeamType_Packet(int user_id, int Other_id)
{
	SC_PACKET_SELECT_TEAM p;
	p.m_ID = Other_id;
	p.m_PacketSize = sizeof(SC_PACKET_SELECT_TEAM);
	p.m_ePacketType= e_PacketType::e_SelectTeamPacket;
	p.m_eTeamType = m_Clients[Other_id].TeamType;

	Send_Packet(user_id, &p);
	cout << "[Team Type] user_id: " << user_id << ", otherid: " << Other_id << endl;
}
void IOCPServer::Send_CharacterType_Packet(int user_id, int Other_id)
{
	SC_PACKET_SELECT_CHARACTERTYPE p;
	p.m_ID = Other_id;
	p.m_PacketSize = sizeof(SC_PACKET_SELECT_CHARACTERTYPE);
	p.m_ePacketType = e_PacketType::e_SelectCharacterTypePacket;
	p.m_eCharacterType = m_Clients[Other_id].CharacterType;

	Send_Packet(user_id, &p);
	cout << "[CharacterType] user_id: " << user_id << ", otherid: " << Other_id << endl;
}
void IOCPServer::Send_Leave_Packet(int user_id, int Other_id)
{
	SC_PACKET_LEAVE p;
	p.m_ePacketType = e_PacketType::e_LeavePacket;
	p.m_PacketSize = sizeof(SC_PACKET_LEAVE);
	p.m_ID = Other_id;

	Send_Packet(user_id, &p);
}

void IOCPServer::Send_PlayerInfo_Packet(int user_id, int Other_id)
{
	SC_PACKET_PLAYER_MOVE p;
	p.m_ePacketType = e_PacketType::e_PlayerMovePacket;
	p.m_PacketSize = sizeof(SC_PACKET_PLAYER_MOVE);
	p.m_ID = Other_id;
	p.m_ObjInfo = m_Clients[Other_id].m_Player.m_PlayerInfo;
	p.m_SkillON = m_Clients[Other_id].m_Player.SkillON;
	p.m_SkillCount = m_Clients[Other_id].m_Player.SKillCount;
	p.m_AmountPaint = m_Clients[Other_id].m_Player.AmountPaint;
	Send_Packet(user_id, &p);
	//여기부터하기 
}
void IOCPServer::Send_ShooterFire_Packet(int user_id, int Other_id)
{
	SC_PACKET_SHOOTER_FIRE p;
	p.m_ePacketType = e_PacketType::e_ShooterFire;
	p.m_ID = Other_id;
	p.m_IsFire = m_Clients[Other_id].m_Player.IsFire;
	p.m_PacketSize = sizeof(SC_PACKET_SHOOTER_FIRE);
	//cout << "m_Clients[" << Other_id << "].m_Player.IsFire" << m_Clients[Other_id].m_Player.IsFire << endl;
	Send_Packet(user_id, &p);
}
void IOCPServer::Send_RollerPaint_Packet(int user_id, int Other_id)
{
	SC_PACKET_ROLLER_PAINT p;
	p.m_ePacketType = e_PacketType::e_RollerPaint;
	p.m_ID = Other_id;
	p.m_IsRolling = m_Clients[Other_id].m_Player.IsRolling;
	p.m_PacketSize = sizeof(SC_PACKET_ROLLER_PAINT);

	Send_Packet(user_id, &p);
}
void IOCPServer::Send_BomberThrow_Packet(int user_id, int Other_id)
{
	SC_PACKET_BOBMER_THROW p;
	p.m_ePacketType = e_PacketType::e_BomberThrow;
	p.m_ID = Other_id;
	p.m_IsThrowing = m_Clients[Other_id].m_Player.IsThrowing;
	p.m_PacketSize = sizeof(SC_PACKET_BOBMER_THROW);

	Send_Packet(user_id, &p);
}
void IOCPServer::Send_Reloading_Packet(int user_id, int Other_id)
{
	SC_PACKET_RELOAD p;
	p.m_ePacketType = e_PacketType::e_Reloading;
	p.m_ID = Other_id;
	p.m_IsReloading = m_Clients[Other_id].m_Player.IsReloading;
	p.m_PacketSize = sizeof(SC_PACKET_RELOAD);

	Send_Packet(user_id, &p);
}

bool IOCPServer::Check_IsAllReady()
{
	//캐릭터타입도 none이 아니고 team도 none이 아니고 state가 Ready이면 true 하나라도아니면 false
	CLIENT cl;
	int readyCount = 0;
	for (auto& Client: m_Clients)
	{
		if (Client.second.m_ID != 999)
		{
			cl = Client.second;
			if ((cl.CharacterType != e_PlayerCharacterType::e_None) && (cl.TeamType != e_PlayerTeamType::e_None) && (cl.GameStateinfo == e_GameStateInfo::e_Ready))
				readyCount++;
		}
	}
	if (readyCount == MAXPLAYER)
		return true;
	else
		return false;
	//return true;
}
void IOCPServer::Send_StartGame()
{
	CLIENT cl;
	SC_PACKET_START_GAME packet;
	packet.m_ePacketType = e_PacketType::e_StartGamePacket;
	packet.m_PacketSize = sizeof(SC_PACKET_START_GAME);
	packet.m_StartGame = true;
	for (auto& Client : m_Clients)
	{
		cl = Client.second;
		if (cl.m_ID != 999)
		{
			Send_Packet(cl.m_ID, &packet);
		}
	}

	StartTimer();
}
void IOCPServer::Send_TimerPacket()
{
	CLIENT cl;
	SC_PACKET_GAME_TIMER packet;
	int count = m_GameTimer->GetTimerCount();
	packet.m_ePacketType = e_PacketType::e_GameTimer;
	packet.m_PacketSize = sizeof(SC_PACKET_GAME_TIMER);
	packet.m_TimerCount = count;
	//packet.m_TimerCount = m_GameTimer->GetTimerCount();

	for (auto& Client : m_Clients)
	{
		cl = Client.second;
		if (cl.m_ID != 999)
		{
			Send_Packet(cl.m_ID, &packet);
		}
	}
}
void IOCPServer::Send_EndGame()
{
	CLIENT cl;
	SC_PACKET_GAME_END packet;
	packet.m_ePacketType = e_PacketType::e_EndGamePacket;
	packet.m_PacketSize = sizeof(SC_PACKET_GAME_END);
	

	for (auto& Client : m_Clients)
	{
		cl = Client.second;
		if (cl.m_ID != 999)
		{
			packet.m_ID = cl.m_ID;
			auto it = m_Clients.find(cl.m_ID);
			if (it == m_Clients.begin())
				packet.m_Resulter = true;
			else
				packet.m_Resulter = false;

			Send_Packet(cl.m_ID, &packet);
		}
	}
}

void IOCPServer::StartTimer()
{
	if (m_GameTimerThread != nullptr)
	{
		m_GameTimerThread->join();
		delete m_GameTimerThread;
		m_GameTimerThread = nullptr;
	}
	if (m_GameTimer == nullptr)
		m_GameTimer = new GameTimer;

	//////////////////쓰레드어케할지생각해보고고치자.
	//m_TimerThread = new thread([&]() { ProcessTimer(); return 0; });
	//shared_ptr<thread> GameTimerThread = make_shared<thread>(ProcessTimer);
	m_GameTimerThread = new thread{ [&]() { ProcessTimer(); return 0; } };

	
	
}
void IOCPServer::StopTimer()
{
	m_IsGameStart = false;
	//m_GameTimerThread->join();
	if (m_GameTimer != nullptr)
	{
		delete m_GameTimer;
		m_GameTimer = nullptr;
	}
	for (auto& player : m_Clients)
	{
		//player.second.GameStateinfo = e_GameStateInfo::e_None;
		player.second.CharacterType = e_PlayerCharacterType::e_None;
		player.second.TeamType = e_PlayerTeamType::e_None;
	}

	////딜리트를 못해주고잇음 .. 터져서
	//
	//delete m_GameTimerThread;
	//m_GameTimerThread = nullptr;
}
void IOCPServer::ProcessTimer()
{
	m_IsGameStart = true;
	while (m_IsGameStart)
	{
		while (m_GameTimerThread == nullptr) {} //메인쓰레드에서 게임타이머쓰레드에 메모리를 할당하기전에 밑에 sleep_for을 호출하면안되므로
		this_thread::sleep_for(1s);
		m_GameTimer->ProcessTimer();
		Send_TimerPacket();
		if (m_GameTimer->GetTimerCount() < 1)
		{
			Send_EndGame();
			StopTimer();
		}

	}
	return;
}
//void IOCPServer::Broad_Cast(int user_id, void* p)
//{
//	
//	for (auto& cl : m_Clients)
//	{
//		if (user_id != cl.second.m_ID)
//		{
//			CLIENT& Client = cl.second;
//			Send_Packet(Client.m_ID, p);
//		}
//	}
//}
