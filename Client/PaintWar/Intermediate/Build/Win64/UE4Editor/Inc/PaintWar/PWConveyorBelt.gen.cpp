// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Obstacles/PWConveyorBelt.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWConveyorBelt() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWConveyorBelt_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWConveyorBelt();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void APWConveyorBelt::StaticRegisterNativesAPWConveyorBelt()
	{
	}
	UClass* Z_Construct_UClass_APWConveyorBelt_NoRegister()
	{
		return APWConveyorBelt::StaticClass();
	}
	struct Z_Construct_UClass_APWConveyorBelt_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConveyorDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConveyorDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBox;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConveyorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConveyorMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWConveyorBelt_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWConveyorBelt_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Obstacles/PWConveyorBelt.h" },
		{ "ModuleRelativePath", "Public/Obstacles/PWConveyorBelt.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_Speed_MetaData[] = {
		{ "Category", "PWConveyorBelt" },
		{ "ModuleRelativePath", "Public/Obstacles/PWConveyorBelt.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_Speed = { "Speed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWConveyorBelt, Speed), METADATA_PARAMS(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_Speed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorDirection_MetaData[] = {
		{ "Category", "Arrow" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Obstacles/PWConveyorBelt.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorDirection = { "ConveyorDirection", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWConveyorBelt, ConveyorDirection), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_CollisionBox_MetaData[] = {
		{ "Category", "Collision" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Obstacles/PWConveyorBelt.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_CollisionBox = { "CollisionBox", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWConveyorBelt, CollisionBox), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_CollisionBox_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_CollisionBox_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Obstacles/PWConveyorBelt.h" },
		{ "ToolTip", "private:" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorMesh = { "ConveyorMesh", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWConveyorBelt, ConveyorMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWConveyorBelt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_CollisionBox,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWConveyorBelt_Statics::NewProp_ConveyorMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWConveyorBelt_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWConveyorBelt>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWConveyorBelt_Statics::ClassParams = {
		&APWConveyorBelt::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APWConveyorBelt_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWConveyorBelt_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWConveyorBelt_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWConveyorBelt()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWConveyorBelt_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWConveyorBelt, 457467749);
	template<> PAINTWAR_API UClass* StaticClass<APWConveyorBelt>()
	{
		return APWConveyorBelt::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWConveyorBelt(Z_Construct_UClass_APWConveyorBelt, &APWConveyorBelt::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWConveyorBelt"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWConveyorBelt);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
