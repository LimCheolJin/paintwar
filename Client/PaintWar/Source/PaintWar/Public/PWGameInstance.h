// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "Engine/GameInstance.h"
#include <iostream>
#include <map>
#include "Protocol.h"
#include "PWGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API UPWGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPWGameInstance();
	~UPWGameInstance();

	virtual void OnStart() override;

	virtual void FinishDestroy() override;

	UFUNCTION(BlueprintCallable)
	void SetCurrentSelectCharacter(ESelectCharacter SelectCharacter);

	UFUNCTION(BlueprintCallable)
	void SetCurrentSelectTeam(ETeam SelectTeam);

	UFUNCTION(BlueprintCallable)
	void SetCurrentGameMode(EGameMode Mode);

	UFUNCTION(BlueprintCallable)
	ESelectCharacter GetCurrentSelectCharacter() const;

	UFUNCTION(BlueprintCallable)
	ETeam GetCurrentSelectTeam() const;

	UFUNCTION(BlueprintCallable)
	EGameMode GetCurrentGameMode() const;

	UFUNCTION(BlueprintCallable)
	void SetRedResultCount(int num);

	UFUNCTION(BlueprintCallable)
	void SetGreenResultCount(int num);

	UFUNCTION(BlueprintCallable)
	int GetRedResultCount() const;

	UFUNCTION(BlueprintCallable)
	int GetGreenResultCount() const;

	UFUNCTION(BlueprintCallable)
	FString GetLoginID();

	UFUNCTION(BlueprintCallable)
	void SetLoginID(FString id);

private:
	//선택을 안하고 래디를 할 경우를 대비해서 초기화 해놈
	int RedResultCount;
	int GreenResultCount;
	FString LoginID;

	UPROPERTY()
	ESelectCharacter CurrentSelectCharacter = ESelectCharacter::NONE;

	UPROPERTY()
	ETeam			 CurrentSelectTeam = ETeam::NONE;

	UPROPERTY()
	EGameMode		 CurrentGameMode; //= EGameMode::SINGLE;

private:
	//렌더타겟들을 모두모아 300초가 지난뒤에 검사하기위함.
	UPROPERTY()
	TArray<class UTextureRenderTarget2D*> AllRenderTargets;

	bool StartGameScene;
	
public:
	TArray<class UTextureRenderTarget2D*> GetAllRenderTargets() const;

	void InsertRenderTarget(class UTextureRenderTarget2D*);

	void ClearRenderTargets();

public:
	//For NetWork Functions

	UFUNCTION(BlueprintCallable)
	bool InitNetWork();

	UFUNCTION(BlueprintCallable)
	bool Connect(FString serverIP);
	
	//Recv packet Process Functions//

	void Recv_LoginPacketProcess(char* LoginPacket);


	void Recv_SelectTeamTypePacketProcess(char* TeamTypePacket);


	void Recv_SelectCharacterTypePacketProcess(char* CharacterTypePacket);

	void Recv_OtherPlayerEnterPacketProcess(char* OtherEnterPacket);

	void Recv_OtherPlayerLeavePacketProcess(char* OtherLeavePacket);

	void Recv_StartGamePacketProcess(char* StartGamePacket);

	void Recv_GameEndPacketProcess(char* EndPacket);

	void Recv_GameResultPacketProcess(char* Packet);
	///////////////////////////////

	//Send packet Process Functions//
	UFUNCTION(BlueprintCallable)
	bool Send_SelectTeamTypePacket();

	UFUNCTION(BlueprintCallable)
	bool Send_SelectCharacterTypePacket();

	UFUNCTION(BlueprintCallable)
	bool Send_InitLogin();

	UFUNCTION(BlueprintCallable)
	bool Send_ReadyInfo();
	//////////////////////////////////

	UFUNCTION(BlueprintCallable)
	bool GetIsGameStart() const;

	void SetIsGameStart(bool);

	bool Send_ResultGame();

	//잠시 퍼블릭
	std::map<int, struct PlayerInfo*> m_PlayerInformations; // 다른 플레이어들의 정보임
	//잠시 퍼블릭
	struct PlayerInfo* m_MyPlayerInfo;   //클라이언트 나 자신임

	void ForRestartInit();
private:
	//For NetWork Variables
	bool IsConnect;

	class PWNetWork* PWNetWork;

	bool IsGameStart;

};
