// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PaintWar.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintWar() {}
// Cross Module References
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_EGameMode();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ESelectCharacter();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ETeam();
// End Cross Module References
	static UEnum* EGameMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PaintWar_EGameMode, Z_Construct_UPackage__Script_PaintWar(), TEXT("EGameMode"));
		}
		return Singleton;
	}
	template<> PAINTWAR_API UEnum* StaticEnum<EGameMode>()
	{
		return EGameMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGameMode(EGameMode_StaticEnum, TEXT("/Script/PaintWar"), TEXT("EGameMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PaintWar_EGameMode_Hash() { return 2290939115U; }
	UEnum* Z_Construct_UEnum_PaintWar_EGameMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PaintWar();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGameMode"), 0, Get_Z_Construct_UEnum_PaintWar_EGameMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGameMode::NONE", (int64)EGameMode::NONE },
				{ "EGameMode::SINGLE", (int64)EGameMode::SINGLE },
				{ "EGameMode::MULTI", (int64)EGameMode::MULTI },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/PaintWar.h" },
				{ "MULTI.DisplayName", "MULTIGameMode" },
				{ "NONE.DisplayName", "NoneGameMode" },
				{ "SINGLE.DisplayName", "SingleGameMode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PaintWar,
				nullptr,
				"EGameMode",
				"EGameMode",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESelectCharacter_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PaintWar_ESelectCharacter, Z_Construct_UPackage__Script_PaintWar(), TEXT("ESelectCharacter"));
		}
		return Singleton;
	}
	template<> PAINTWAR_API UEnum* StaticEnum<ESelectCharacter>()
	{
		return ESelectCharacter_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESelectCharacter(ESelectCharacter_StaticEnum, TEXT("/Script/PaintWar"), TEXT("ESelectCharacter"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PaintWar_ESelectCharacter_Hash() { return 43280042U; }
	UEnum* Z_Construct_UEnum_PaintWar_ESelectCharacter()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PaintWar();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESelectCharacter"), 0, Get_Z_Construct_UEnum_PaintWar_ESelectCharacter_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESelectCharacter::NONE", (int64)ESelectCharacter::NONE },
				{ "ESelectCharacter::SHOOTER", (int64)ESelectCharacter::SHOOTER },
				{ "ESelectCharacter::ROLLER", (int64)ESelectCharacter::ROLLER },
				{ "ESelectCharacter::BOMBER", (int64)ESelectCharacter::BOMBER },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "BOMBER.DisplayName", "BomberCharacter" },
				{ "ModuleRelativePath", "Public/PaintWar.h" },
				{ "NONE.DisplayName", "Character None" },
				{ "ROLLER.DisplayName", "RollerCharacter" },
				{ "SHOOTER.DisplayName", "ShooterCharacter" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PaintWar,
				nullptr,
				"ESelectCharacter",
				"ESelectCharacter",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETeam_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PaintWar_ETeam, Z_Construct_UPackage__Script_PaintWar(), TEXT("ETeam"));
		}
		return Singleton;
	}
	template<> PAINTWAR_API UEnum* StaticEnum<ETeam>()
	{
		return ETeam_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETeam(ETeam_StaticEnum, TEXT("/Script/PaintWar"), TEXT("ETeam"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PaintWar_ETeam_Hash() { return 1195395067U; }
	UEnum* Z_Construct_UEnum_PaintWar_ETeam()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PaintWar();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETeam"), 0, Get_Z_Construct_UEnum_PaintWar_ETeam_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETeam::NONE", (int64)ETeam::NONE },
				{ "ETeam::RED", (int64)ETeam::RED },
				{ "ETeam::GREEN", (int64)ETeam::GREEN },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "GREEN.DisplayName", "GreenTeam" },
				{ "ModuleRelativePath", "Public/PaintWar.h" },
				{ "NONE.DisplayName", "TeamNone" },
				{ "RED.DisplayName", "RedTeam" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PaintWar,
				nullptr,
				"ETeam",
				"ETeam",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
