// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(PaintWar, Log, All);
//LOG를 사용한 함수의 실행 시점을 파악할때 유용하다.
#define PWLOG_CALLINFO (FString(__FUNCTION__) + TEXT("(") + FString::FromInt(__LINE__) + TEXT(")") )
#define PWLOG_S(Verbosity) UE_LOG(PaintWar, Verbosity, TEXT("%s"), *PWLOG_CALLINFO)
#define PWLOG(Verbosity, Format, ...) UE_LOG(PaintWar, Verbosity, TEXT("%s %s"), *PWLOG_CALLINFO, *FString::Printf(Format, ##__VA_ARGS__))
#define PWCHECK(Expr, ...) {if(!(Expr)){PWLOG(Error, TEXT("ASSERTION : %s"), TEXT("'"#Expr"'"));return __VA_ARGS__;}}


UENUM(BlueprintType)
enum class ETeam : uint8
{
	NONE	UMETA(DisplayName = "TeamNone"),
	RED		UMETA(DisplayName = "RedTeam"),
	GREEN		UMETA(DisplayName = "GreenTeam")
};

UENUM(BlueprintType)
enum class ESelectCharacter : uint8
{
	NONE		UMETA(DisplayName = "Character None"),
	SHOOTER		UMETA(DisplayName = "ShooterCharacter"),
	ROLLER		UMETA(DisplayName = "RollerCharacter"),
	BOMBER		UMETA(DisplayName = "BomberCharacter")
};

UENUM(BlueprintType)
enum class EGameMode : uint8
{
	NONE		UMETA(DisplayName = "NoneGameMode"),
	SINGLE		UMETA(DisplayName = "SingleGameMode"),
	MULTI		UMETA(DisplayName = "MULTIGameMode")
};

//UENUM(BlueprintType)
//enum class ESCENETYPE : uint8
//{
//	NONE			UMETA(DisplayName = "GAME SCENE NONE"),
//	LOGIN			UMETA(DisplayName = "LOGIN"),
//	SELECTGAMEMODE	UMETA(DisplayName = "SELECTGAMEMODE"),
//	SINGLELOBBY		UMETA(DisplayName = "SINGLELOBBY"),
//	MULTILOBBY		UMETA(DisplayName = "MULTILOBBY"),
//	SELECTROOM		UMETA(DisplayName = "SELECTROOM"),
//	GAMESCENE		UMETA(DisplayName = "GAME SCENE"),
//	GAMEENDSCENE	UMETA(DisplayName = "GAME END SCENE")
//};