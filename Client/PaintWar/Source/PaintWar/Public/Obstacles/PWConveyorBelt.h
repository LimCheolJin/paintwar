// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWConveyorBelt.generated.h"

UCLASS()
class PAINTWAR_API APWConveyorBelt : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWConveyorBelt();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public: // private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	UStaticMeshComponent* ConveyorMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
	UBoxComponent* CollisionBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Arrow)
	UArrowComponent* ConveyorDirection;

	UPROPERTY(EditAnywhere)
	float Speed;
};
