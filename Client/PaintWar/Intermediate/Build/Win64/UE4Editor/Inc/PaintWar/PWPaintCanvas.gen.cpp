// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWPaintCanvas.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPaintCanvas() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	ENGINE_API UClass* Z_Construct_UClass_UCanvas_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void APWPaintCanvas::StaticRegisterNativesAPWPaintCanvas()
	{
	}
	UClass* Z_Construct_UClass_APWPaintCanvas_NoRegister()
	{
		return APWPaintCanvas::StaticClass();
	}
	struct Z_Construct_UClass_APWPaintCanvas_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Canvas_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Canvas;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InitTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitCanvasMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InitCanvasMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitCanvasMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InitCanvasMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanvasMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CanvasMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanvasMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CanvasMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanvasMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CanvasMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPaintCanvas_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "PWPaintCanvas.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_Canvas_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_Canvas = { "Canvas", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, Canvas), Z_Construct_UClass_UCanvas_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_Canvas_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_Canvas_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitTexture_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitTexture = { "InitTexture", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, InitTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitTexture_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterialInstance = { "InitCanvasMaterialInstance", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, InitCanvasMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
		{ "ToolTip", "UPROPERTY()\nclass UMaterialInstanceDynamic* BrushMaterialInstance;" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterial = { "InitCanvasMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, InitCanvasMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
		{ "ToolTip", "UPROPERTY(BlueprintReadWrite, Category = Canvas)\nclass UTexture2D* BrushTexture;" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterialInstance = { "CanvasMaterialInstance", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, CanvasMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterial_MetaData[] = {
		{ "Category", "Canvas" },
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterial = { "CanvasMaterial", nullptr, (EPropertyFlags)0x0020080000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, CanvasMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_RenderTarget_MetaData[] = {
		{ "Category", "Canvas" },
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0020080000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_RenderTarget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_RenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMesh_MetaData[] = {
		{ "Category", "Canvas" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPaintCanvas.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMesh = { "CanvasMesh", nullptr, (EPropertyFlags)0x002008000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPaintCanvas, CanvasMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWPaintCanvas_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_Canvas,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_InitCanvasMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPaintCanvas_Statics::NewProp_CanvasMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPaintCanvas_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPaintCanvas>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPaintCanvas_Statics::ClassParams = {
		&APWPaintCanvas::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APWPaintCanvas_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWPaintCanvas_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPaintCanvas_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPaintCanvas()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPaintCanvas_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPaintCanvas, 2257613424);
	template<> PAINTWAR_API UClass* StaticClass<APWPaintCanvas>()
	{
		return APWPaintCanvas::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPaintCanvas(Z_Construct_UClass_APWPaintCanvas, &APWPaintCanvas::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPaintCanvas"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPaintCanvas);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
