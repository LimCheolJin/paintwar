// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Weapon/PWBaseWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWBaseWeapon() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseWeapon_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseWeapon();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void APWBaseWeapon::StaticRegisterNativesAPWBaseWeapon()
	{
		UClass* Class = APWBaseWeapon::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWeaponSTMesh", &APWBaseWeapon::execGetWeaponSTMesh },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics
	{
		struct PWBaseWeapon_eventGetWeaponSTMesh_Parms
		{
			UStaticMeshComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseWeapon_eventGetWeaponSTMesh_Parms, ReturnValue), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWBaseWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseWeapon, nullptr, "GetWeaponSTMesh", sizeof(PWBaseWeapon_eventGetWeaponSTMesh_Parms), Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWBaseWeapon_NoRegister()
	{
		return APWBaseWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APWBaseWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponSTMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeaponSTMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWBaseWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWBaseWeapon_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWBaseWeapon_GetWeaponSTMesh, "GetWeaponSTMesh" }, // 1318806763
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/PWBaseWeapon.h" },
		{ "ModuleRelativePath", "Public/Weapon/PWBaseWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_WeaponSTMesh_MetaData[] = {
		{ "Category", "Weapon" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWBaseWeapon.h" },
		{ "ToolTip", "?????? ????\xc6\xbd?\xde\xbd????????\xd8\xb5\xd0\xbb?????. ???\xcc\xb7?\xc5\xbb?\xde\xbd????????\xd9\xb8?\n?\xda\xb5\xe5\xb8\xa6 ???????\xca\xbf\xe4\xb0\xa1?????\xc5\xb0???." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_WeaponSTMesh = { "WeaponSTMesh", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseWeapon, WeaponSTMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_WeaponSTMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_WeaponSTMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "Public/Weapon/PWBaseWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseWeapon, Damage), METADATA_PARAMS(Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_Damage_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_Damage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWBaseWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_WeaponSTMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseWeapon_Statics::NewProp_Damage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWBaseWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWBaseWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWBaseWeapon_Statics::ClassParams = {
		&APWBaseWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWBaseWeapon_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWBaseWeapon_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWBaseWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWBaseWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWBaseWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWBaseWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWBaseWeapon, 1505434833);
	template<> PAINTWAR_API UClass* StaticClass<APWBaseWeapon>()
	{
		return APWBaseWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWBaseWeapon(Z_Construct_UClass_APWBaseWeapon, &APWBaseWeapon::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWBaseWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWBaseWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
