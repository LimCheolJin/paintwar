// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Characters/PWUICharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWUICharacter() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWUICharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWUICharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWUICharacter_GetUIWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseWeapon_NoRegister();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWUICharacter_SetBomberWeapon();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWUICharacter_SetRollerWeapon();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWUICharacter_SetShooterWeapon();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
	void APWUICharacter::StaticRegisterNativesAPWUICharacter()
	{
		UClass* Class = APWUICharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetUIWeapon", &APWUICharacter::execGetUIWeapon },
			{ "SetBomberWeapon", &APWUICharacter::execSetBomberWeapon },
			{ "SetRollerWeapon", &APWUICharacter::execSetRollerWeapon },
			{ "SetShooterWeapon", &APWUICharacter::execSetShooterWeapon },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics
	{
		struct PWUICharacter_eventGetUIWeapon_Parms
		{
			APWBaseWeapon* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWUICharacter_eventGetUIWeapon_Parms, ReturnValue), Z_Construct_UClass_APWBaseWeapon_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWUICharacter, nullptr, "GetUIWeapon", sizeof(PWUICharacter_eventGetUIWeapon_Parms), Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWUICharacter_GetUIWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWUICharacter_GetUIWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWUICharacter, nullptr, "SetBomberWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWUICharacter_SetBomberWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWUICharacter_SetBomberWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWUICharacter, nullptr, "SetRollerWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWUICharacter_SetRollerWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWUICharacter_SetRollerWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWUICharacter, nullptr, "SetShooterWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWUICharacter_SetShooterWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWUICharacter_SetShooterWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWUICharacter_NoRegister()
	{
		return APWUICharacter::StaticClass();
	}
	struct Z_Construct_UClass_APWUICharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bomb_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bomb;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Roller_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Roller;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gun_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Gun;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weapon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Weapon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWUICharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWUICharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWUICharacter_GetUIWeapon, "GetUIWeapon" }, // 2841202146
		{ &Z_Construct_UFunction_APWUICharacter_SetBomberWeapon, "SetBomberWeapon" }, // 2834905283
		{ &Z_Construct_UFunction_APWUICharacter_SetRollerWeapon, "SetRollerWeapon" }, // 2297030132
		{ &Z_Construct_UFunction_APWUICharacter_SetShooterWeapon, "SetShooterWeapon" }, // 1757375616
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWUICharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Characters/PWUICharacter.h" },
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWUICharacter_Statics::NewProp_Bomb_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWUICharacter_Statics::NewProp_Bomb = { "Bomb", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWUICharacter, Bomb), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Bomb_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Bomb_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWUICharacter_Statics::NewProp_Roller_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWUICharacter_Statics::NewProp_Roller = { "Roller", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWUICharacter, Roller), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Roller_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Roller_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWUICharacter_Statics::NewProp_Gun_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWUICharacter_Statics::NewProp_Gun = { "Gun", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWUICharacter, Gun), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Gun_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Gun_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWUICharacter_Statics::NewProp_Weapon_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWUICharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWUICharacter_Statics::NewProp_Weapon = { "Weapon", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWUICharacter, Weapon), Z_Construct_UClass_APWBaseWeapon_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Weapon_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::NewProp_Weapon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWUICharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWUICharacter_Statics::NewProp_Bomb,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWUICharacter_Statics::NewProp_Roller,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWUICharacter_Statics::NewProp_Gun,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWUICharacter_Statics::NewProp_Weapon,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWUICharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWUICharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWUICharacter_Statics::ClassParams = {
		&APWUICharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWUICharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWUICharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWUICharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWUICharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWUICharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWUICharacter, 2015058615);
	template<> PAINTWAR_API UClass* StaticClass<APWUICharacter>()
	{
		return APWUICharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWUICharacter(Z_Construct_UClass_APWUICharacter, &APWUICharacter::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWUICharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWUICharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
