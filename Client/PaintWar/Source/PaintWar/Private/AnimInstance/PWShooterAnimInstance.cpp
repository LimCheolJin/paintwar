// Fill out your copyright notice in the Description page of Project Settings.


#include "PWShooterAnimInstance.h"
#include "PWShooterCharacter.h"

UPWShooterAnimInstance::UPWShooterAnimInstance()
{
	IsFire = false;

	//static ConstructorHelpers::FObjectFinder<UAnimMontage> ANIM_MONTAGE(TEXT("/Game/SpaceExplorer/Anims/testMontage.testMontage"));
	//if (ANIM_MONTAGE.Succeeded())
	//{
	//	AnimMontage = ANIM_MONTAGE.Object;
	//}

	//	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM"));
	//if (SK_MESH.Succeeded()) //�����ϸ�SkeletalMesh'/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM'
	//{
	//	GetMesh()->SetSkeletalMesh(SK_MESH.Object); // set ���̷�Ż�޽�
	//}
}
void UPWShooterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	UPWAnimInstance::NativeUpdateAnimation(DeltaSeconds);

	auto ShooterCharacter = Cast<APWShooterCharacter>(TryGetPawnOwner());

	if (ShooterCharacter != nullptr)
	{
		IsFire = ShooterCharacter->GetIsFire();

		/*if (!Montage_IsPlaying(AnimMontage))
		{
			if (IsFire)
			{
				Montage_Play(AnimMontage);
			}
		}*/
	}
}
void UPWShooterAnimInstance::AnimNotify_ReloadNotify()
{
	OnReloadNotify.Broadcast();
}

void UPWShooterAnimInstance::AnimNotify_FireNotify()
{

	OnFireNotify.Broadcast();
}

void UPWShooterAnimInstance::AnimNotify_FireNotify1()
{
	OnFireNotify1.Broadcast();
}