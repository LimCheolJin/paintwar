// Fill out your copyright notice in the Description page of Project Settings.


#include "PWRollerAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "PWRollerCharacter.h"
#include "PWGameInstance.h"



APWRollerAIController::APWRollerAIController()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT("/Game/behavior/AIRoller_BB"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT("/Game/behavior/AIRoller_BT"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;
	}
}

void APWRollerAIController::BeginPlay()
{
	APWBaseAIController::BeginPlay();
	//auto PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());

	//TSubclassOf<AActor> ClassCharacter = APWRollerCharacter::StaticClass();

	//TArray<AActor*> OutputActors;
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassCharacter, OutputActors);
	//if (PWGameInstance->GetCurrentGameMode() == EGameMode::SINGLE)
	//{
	//	for (const auto& Actor : OutputActors)
	//	{

	//		//루프돌려서 게임인스턴스의 선택캐릭터정보와 팀정보를 월드상에 모든 BaseCharacter중에서 팀정보와 CharacterType을 비교하여 찾는다.
	//		//Find()가 있긴한데 Array라서 어케 찾는지 모르겠네 
	//		const auto& BaseCharacter = Cast<APWRollerCharacter>(Actor);
	//		if ((BaseCharacter->GetCharacterType() == ESelectCharacter::ROLLER) && (BaseCharacter->GetCharacterType() != PWGameInstance->GetCurrentSelectCharacter()) && (BaseCharacter->GetTeam() != PWGameInstance->GetCurrentSelectTeam()))
	//		{//찾았으면 Possess()
	//			//OnPossess(BaseCharacter);
	//			OnPossess(BaseCharacter);
	//		}
	//		else
	//		{

	//		}
	//	}
	//}
}

void APWRollerAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (UseBlackboard(BBAsset, Blackboard))
	{
		if (!RunBehaviorTree(BTAsset))
		{
			PWLOG(Error, TEXT("AIerror"));
		}
	}
	pawn = Cast<APWBaseCharacter>(AController::GetPawn());
	pawn->SetLoginID("RollerAI");
	pawn->SetMyLoginID_OnWidget();
}
