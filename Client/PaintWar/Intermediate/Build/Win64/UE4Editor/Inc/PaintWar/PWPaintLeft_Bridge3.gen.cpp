// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PaintMap/Left/Bridge/PWPaintLeft_Bridge3.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPaintLeft_Bridge3() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintLeft_Bridge3_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintLeft_Bridge3();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWPaintLeft_Bridge3::StaticRegisterNativesAPWPaintLeft_Bridge3()
	{
	}
	UClass* Z_Construct_UClass_APWPaintLeft_Bridge3_NoRegister()
	{
		return APWPaintLeft_Bridge3::StaticClass();
	}
	struct Z_Construct_UClass_APWPaintLeft_Bridge3_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWPaintCanvas,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaintMap/Left/Bridge/PWPaintLeft_Bridge3.h" },
		{ "ModuleRelativePath", "Public/PaintMap/Left/Bridge/PWPaintLeft_Bridge3.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPaintLeft_Bridge3>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::ClassParams = {
		&APWPaintLeft_Bridge3::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPaintLeft_Bridge3()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPaintLeft_Bridge3_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPaintLeft_Bridge3, 3411222214);
	template<> PAINTWAR_API UClass* StaticClass<APWPaintLeft_Bridge3>()
	{
		return APWPaintLeft_Bridge3::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPaintLeft_Bridge3(Z_Construct_UClass_APWPaintLeft_Bridge3, &APWPaintLeft_Bridge3::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPaintLeft_Bridge3"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPaintLeft_Bridge3);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
