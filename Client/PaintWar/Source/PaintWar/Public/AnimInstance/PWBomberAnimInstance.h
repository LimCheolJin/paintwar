// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "AnimInstance/PWAnimInstance.h"
#include "PWBomberAnimInstance.generated.h"

/**
 * 
 */

DECLARE_MULTICAST_DELEGATE(FOnFireNotifyDelegate);
DECLARE_MULTICAST_DELEGATE(FOnReloadNotifyDelegate);

UCLASS()
class PAINTWAR_API UPWBomberAnimInstance : public UPWAnimInstance
{
	GENERATED_BODY()

public:
	UPWBomberAnimInstance();

	FOnFireNotifyDelegate OnFireNotify;
	FOnReloadNotifyDelegate OnReloadNotify;
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsFire;

	UFUNCTION()
	void AnimNotify_FireNotify();

	UFUNCTION()
	void AnimNotify_ReloadNotify();
protected:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

};
