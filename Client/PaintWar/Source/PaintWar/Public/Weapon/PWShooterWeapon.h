// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWRangeWeapon.h"
#include "PWShooterWeapon.generated.h"

UCLASS()
class PAINTWAR_API APWShooterWeapon : public APWRangeWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWShooterWeapon();
	
	UParticleSystemComponent* GetShooterParticle() const;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, Category = Material)
	class UMaterialInterface* ShooterRed;

	UPROPERTY(EditAnywhere, Category = Material)
	class UMaterialInterface* ShooterGreen;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* ParticleRed;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* ParticleGreen;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystemComponent* ShooterProjectileParticle;

};
