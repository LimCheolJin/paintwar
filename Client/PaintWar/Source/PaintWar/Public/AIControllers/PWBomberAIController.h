// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWBaseAIController.h"
#include "PWBomberAIController.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWBomberAIController : public  APWBaseAIController
{
	GENERATED_BODY()

public:
	APWBomberAIController();

public:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;



};
