// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "Weapon/Projectile/PWBaseProjectile.h"
#include "PWShooterProjectile.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWShooterProjectile : public APWBaseProjectile
{
	GENERATED_BODY()

public:
	APWShooterProjectile();

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void Skill();
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	/*UPROPERTY()
	class APWPaintCanvas* CanvasActor;*/

private:
	UPROPERTY()
		class UParticleSystem* ParticleRed;

	UPROPERTY()
		class UParticleSystem* ParticleGreen;
};
