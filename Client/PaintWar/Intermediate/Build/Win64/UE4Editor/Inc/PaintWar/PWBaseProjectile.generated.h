// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBaseProjectile_generated_h
#error "PWBaseProjectile.generated.h already included, missing '#pragma once' in PWBaseProjectile.h"
#endif
#define PAINTWAR_PWBaseProjectile_generated_h

#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBaseProjectile(); \
	friend struct Z_Construct_UClass_APWBaseProjectile_Statics; \
public: \
	DECLARE_CLASS(APWBaseProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWBaseProjectile(); \
	friend struct Z_Construct_UClass_APWBaseProjectile_Statics; \
public: \
	DECLARE_CLASS(APWBaseProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBaseProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBaseProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBaseProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseProjectile(APWBaseProjectile&&); \
	NO_API APWBaseProjectile(const APWBaseProjectile&); \
public:


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBaseProjectile(APWBaseProjectile&&); \
	NO_API APWBaseProjectile(const APWBaseProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBaseProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBaseProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBaseProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionSphere() { return STRUCT_OFFSET(APWBaseProjectile, CollisionSphere); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovementComponent() { return STRUCT_OFFSET(APWBaseProjectile, ProjectileMovementComponent); } \
	FORCEINLINE static uint32 __PPO__ProjectileMesh() { return STRUCT_OFFSET(APWBaseProjectile, ProjectileMesh); } \
	FORCEINLINE static uint32 __PPO__BrushSize() { return STRUCT_OFFSET(APWBaseProjectile, BrushSize); } \
	FORCEINLINE static uint32 __PPO__BrushMaterial() { return STRUCT_OFFSET(APWBaseProjectile, BrushMaterial); } \
	FORCEINLINE static uint32 __PPO__BrushMaterialInstance() { return STRUCT_OFFSET(APWBaseProjectile, BrushMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__BrushTextureRed() { return STRUCT_OFFSET(APWBaseProjectile, BrushTextureRed); } \
	FORCEINLINE static uint32 __PPO__BrushTextureGreen() { return STRUCT_OFFSET(APWBaseProjectile, BrushTextureGreen); } \
	FORCEINLINE static uint32 __PPO__ProjectileMeshGreenMaterial() { return STRUCT_OFFSET(APWBaseProjectile, ProjectileMeshGreenMaterial); } \
	FORCEINLINE static uint32 __PPO__ProjectileMeshRedMaterial() { return STRUCT_OFFSET(APWBaseProjectile, ProjectileMeshRedMaterial); } \
	FORCEINLINE static uint32 __PPO__ProjectileParticle() { return STRUCT_OFFSET(APWBaseProjectile, ProjectileParticle); }


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBaseProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWBaseProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
