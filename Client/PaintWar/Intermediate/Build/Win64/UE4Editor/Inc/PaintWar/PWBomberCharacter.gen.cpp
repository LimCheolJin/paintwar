// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Characters/PWBomberCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWBomberCharacter() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberCharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberCharacter();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBomberCharacter_Fire();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBomberCharacter_UnAbleFire();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWBomberAnimInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberProjectile_NoRegister();
// End Cross Module References
	void APWBomberCharacter::StaticRegisterNativesAPWBomberCharacter()
	{
		UClass* Class = APWBomberCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Fire", &APWBomberCharacter::execFire },
			{ "SpawnBomberProjectile", &APWBomberCharacter::execSpawnBomberProjectile },
			{ "UnAbleFire", &APWBomberCharacter::execUnAbleFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWBomberCharacter_Fire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBomberCharacter_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBomberCharacter_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBomberCharacter, nullptr, "Fire", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBomberCharacter_Fire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBomberCharacter_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBomberCharacter_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBomberCharacter_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBomberCharacter, nullptr, "SpawnBomberProjectile", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBomberCharacter, nullptr, "UnAbleFire", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBomberCharacter_UnAbleFire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBomberCharacter_UnAbleFire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWBomberCharacter_NoRegister()
	{
		return APWBomberCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APWBomberCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BomberAnim_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BomberAnim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Projectile_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Projectile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWBomberCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseCharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWBomberCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWBomberCharacter_Fire, "Fire" }, // 3795139633
		{ &Z_Construct_UFunction_APWBomberCharacter_SpawnBomberProjectile, "SpawnBomberProjectile" }, // 1548079796
		{ &Z_Construct_UFunction_APWBomberCharacter_UnAbleFire, "UnAbleFire" }, // 649616565
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBomberCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Characters/PWBomberCharacter.h" },
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_BomberAnim_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_BomberAnim = { "BomberAnim", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBomberCharacter, BomberAnim), Z_Construct_UClass_UPWBomberAnimInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_BomberAnim_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_BomberAnim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_Projectile_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBomberCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_Projectile = { "Projectile", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBomberCharacter, Projectile), Z_Construct_UClass_APWBomberProjectile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_Projectile_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_Projectile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWBomberCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_BomberAnim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBomberCharacter_Statics::NewProp_Projectile,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWBomberCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWBomberCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWBomberCharacter_Statics::ClassParams = {
		&APWBomberCharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWBomberCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWBomberCharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWBomberCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWBomberCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWBomberCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWBomberCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWBomberCharacter, 1214552933);
	template<> PAINTWAR_API UClass* StaticClass<APWBomberCharacter>()
	{
		return APWBomberCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWBomberCharacter(Z_Construct_UClass_APWBomberCharacter, &APWBomberCharacter::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWBomberCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWBomberCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
