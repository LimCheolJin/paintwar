// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "PWBaseCharacter.h"
#include "PWBaseAIController.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWBaseAIController : public AAIController
{
	GENERATED_BODY()
public:
	APWBaseAIController();

public:
	virtual void BeginPlay() override;

	virtual void OnUnPossess() override;

	UPROPERTY()
	class UBehaviorTree* BTAsset;

	UPROPERTY()
	class UBlackboardData* BBAsset;

	APWBaseCharacter* pawn;
};
