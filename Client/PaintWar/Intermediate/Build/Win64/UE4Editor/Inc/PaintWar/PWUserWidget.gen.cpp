// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWUserWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWUserWidget() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_UPWUserWidget_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWUserWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	UMG_API UClass* Z_Construct_UClass_UEditableTextBox_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UScrollBox_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
// End Cross Module References
	void UPWUserWidget::StaticRegisterNativesUPWUserWidget()
	{
	}
	UClass* Z_Construct_UClass_UPWUserWidget_NoRegister()
	{
		return UPWUserWidget::StaticClass();
	}
	struct Z_Construct_UClass_UPWUserWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TBChatText_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TBChatText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SBChatLog_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SBChatLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LoginIDText_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LoginIDText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPWUserWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWUserWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PWUserWidget.h" },
		{ "ModuleRelativePath", "Public/PWUserWidget.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWUserWidget_Statics::NewProp_TBChatText_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWUserWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPWUserWidget_Statics::NewProp_TBChatText = { "TBChatText", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWUserWidget, TBChatText), Z_Construct_UClass_UEditableTextBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_TBChatText_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_TBChatText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWUserWidget_Statics::NewProp_SBChatLog_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWUserWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPWUserWidget_Statics::NewProp_SBChatLog = { "SBChatLog", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWUserWidget, SBChatLog), Z_Construct_UClass_UScrollBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_SBChatLog_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_SBChatLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWUserWidget_Statics::NewProp_LoginIDText_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWUserWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPWUserWidget_Statics::NewProp_LoginIDText = { "LoginIDText", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWUserWidget, LoginIDText), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_LoginIDText_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWUserWidget_Statics::NewProp_LoginIDText_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPWUserWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWUserWidget_Statics::NewProp_TBChatText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWUserWidget_Statics::NewProp_SBChatLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWUserWidget_Statics::NewProp_LoginIDText,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPWUserWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPWUserWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPWUserWidget_Statics::ClassParams = {
		&UPWUserWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPWUserWidget_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UPWUserWidget_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPWUserWidget_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPWUserWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPWUserWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPWUserWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPWUserWidget, 1536006135);
	template<> PAINTWAR_API UClass* StaticClass<UPWUserWidget>()
	{
		return UPWUserWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPWUserWidget(Z_Construct_UClass_UPWUserWidget, &UPWUserWidget::StaticClass, TEXT("/Script/PaintWar"), TEXT("UPWUserWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPWUserWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
