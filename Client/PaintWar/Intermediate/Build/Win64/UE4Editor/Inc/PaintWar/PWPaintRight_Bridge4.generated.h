// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintRight_Bridge4_generated_h
#error "PWPaintRight_Bridge4.generated.h already included, missing '#pragma once' in PWPaintRight_Bridge4.h"
#endif
#define PAINTWAR_PWPaintRight_Bridge4_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge4(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge4_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge4, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge4)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge4(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge4_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge4, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge4)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintRight_Bridge4(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintRight_Bridge4) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge4); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge4); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge4(APWPaintRight_Bridge4&&); \
	NO_API APWPaintRight_Bridge4(const APWPaintRight_Bridge4&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge4(APWPaintRight_Bridge4&&); \
	NO_API APWPaintRight_Bridge4(const APWPaintRight_Bridge4&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge4); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge4); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintRight_Bridge4)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintRight_Bridge4>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge4_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
