// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBaseProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Materials/MaterialInstanceConstant.h"
#include "Materials/MaterialInterface.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "PWBaseCharacter.h"

// Sets default values
APWBaseProjectile::APWBaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileStaticMesh"));
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	RootComponent = CollisionSphere;

	ProjectileMesh->SetCollisionProfileName(TEXT("NoCollision"));

	ProjectileMesh->SetupAttachment(CollisionSphere);

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_Brush(TEXT("/Game/TestAssets/TestFloorPaint/M_Brush.M_Brush"));
	if (M_Brush.Succeeded())
	{
		BrushMaterial = M_Brush.Object;
	}

	BrushTextureRed = CreateDefaultSubobject<UTexture2D>(TEXT("BrushTextureRed"));
	BrushTextureGreen = CreateDefaultSubobject<UTexture2D>(TEXT("BrushTextureGreen"));
	
	//static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_MeshRed(TEXT("/Game/TestAssets/Materials/M_RedColor_Inst.M_RedColor_Inst"));
	//if (M_MeshRed.Succeeded())//
	//{
	//	ProjectileMeshRedMaterial = M_MeshRed.Object;
	//}

	//static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_MeshGreen(TEXT("/Game/TestAssets/Materials/M_GreenColor_Inst.M_GreenColor_Inst"));
	//if (M_MeshGreen.Succeeded())
	//{
	//	ProjectileMeshGreenMaterial = M_MeshGreen.Object;
	//}

	//��ƼŬ
	ProjectileParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));

}

// Called when the game starts or when spawned
void APWBaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	BrushMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), BrushMaterial);
	
	if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::RED)
	{
		BrushMaterialInstance->SetTextureParameterValue(FName("BrushTexture"), BrushTextureRed);
		
		//ProjectileMesh->SetMaterial(0, ProjectileMeshRedMaterial);

	}
	else if (Cast<APWBaseCharacter>(GetOwner())->GetTeam() == ETeam::GREEN)
	{
		BrushMaterialInstance->SetTextureParameterValue(FName("BrushTexture"), BrushTextureGreen);
		//ProjectileMesh->SetMaterial(0, ProjectileMeshGreenMaterial);
	}
	
}

// Called every frame
void APWBaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APWBaseProjectile::FireInDirection(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}


USphereComponent* APWBaseProjectile::GetSphereCollision() const
{
	return CollisionSphere;
}

UProjectileMovementComponent* APWBaseProjectile::GetProjectileMovementComponent() const
{
	return ProjectileMovementComponent;
}