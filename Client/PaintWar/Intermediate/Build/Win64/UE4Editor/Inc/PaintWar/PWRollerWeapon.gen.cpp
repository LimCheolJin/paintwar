// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Weapon/PWRollerWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWRollerWeapon() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerWeapon_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWMeleeWeapon();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerWeapon_OnHit();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerWeapon_ProcessPaint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceConstant_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void APWRollerWeapon::StaticRegisterNativesAPWRollerWeapon()
	{
		UClass* Class = APWRollerWeapon::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CountAmountPaint", &APWRollerWeapon::execCountAmountPaint },
			{ "OnHit", &APWRollerWeapon::execOnHit },
			{ "OnOverlapBegin", &APWRollerWeapon::execOnOverlapBegin },
			{ "OnOverlapEnd", &APWRollerWeapon::execOnOverlapEnd },
			{ "ProcessPaint", &APWRollerWeapon::execProcessPaint },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerWeapon, nullptr, "CountAmountPaint", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics
	{
		struct PWRollerWeapon_eventOnHit_Parms
		{
			UPrimitiveComponent* HitComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			FVector NormalImpulse;
			FHitResult Hit;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Hit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Hit;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NormalImpulse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HitComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_Hit_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_Hit = { "Hit", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnHit_Parms, Hit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_Hit_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_Hit_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_NormalImpulse = { "NormalImpulse", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnHit_Parms, NormalImpulse), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnHit_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnHit_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_HitComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_HitComponent = { "HitComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnHit_Parms, HitComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_HitComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_HitComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_Hit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_NormalImpulse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::NewProp_HitComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerWeapon, nullptr, "OnHit", sizeof(PWRollerWeapon_eventOnHit_Parms), Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerWeapon_OnHit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerWeapon_OnHit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics
	{
		struct PWRollerWeapon_eventOnOverlapBegin_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapBegin_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_SweepResult_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_SweepResult_MetaData)) };
	void Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((PWRollerWeapon_eventOnOverlapBegin_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWRollerWeapon_eventOnOverlapBegin_Parms), &Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapBegin_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapBegin_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapBegin_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapBegin_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OverlappedComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_SweepResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::NewProp_OverlappedComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerWeapon, nullptr, "OnOverlapBegin", sizeof(PWRollerWeapon_eventOnOverlapBegin_Parms), Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics
	{
		struct PWRollerWeapon_eventOnOverlapEnd_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapEnd_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapEnd_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapEnd_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWRollerWeapon_eventOnOverlapEnd_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OverlappedComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::NewProp_OverlappedComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerWeapon, nullptr, "OnOverlapEnd", sizeof(PWRollerWeapon_eventOnOverlapEnd_Parms), Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerWeapon, nullptr, "ProcessPaint", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerWeapon_ProcessPaint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerWeapon_ProcessPaint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWRollerWeapon_NoRegister()
	{
		return APWRollerWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APWRollerWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProcessPaintHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProcessPaintHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmountPaintCountingHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AmountPaintCountingHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintParticleRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintParticleRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintParticleLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintParticleLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushTextureGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushTextureGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushTextureRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushTextureRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerGreenHead_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerGreenHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerRedHead_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerRedHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrushSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerHeadMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerHeadMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWRollerWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWMeleeWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWRollerWeapon_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWRollerWeapon_CountAmountPaint, "CountAmountPaint" }, // 3900533868
		{ &Z_Construct_UFunction_APWRollerWeapon_OnHit, "OnHit" }, // 496028208
		{ &Z_Construct_UFunction_APWRollerWeapon_OnOverlapBegin, "OnOverlapBegin" }, // 3954195567
		{ &Z_Construct_UFunction_APWRollerWeapon_OnOverlapEnd, "OnOverlapEnd" }, // 2264714472
		{ &Z_Construct_UFunction_APWRollerWeapon_ProcessPaint, "ProcessPaint" }, // 485988478
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/PWRollerWeapon.h" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ProcessPaintHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ProcessPaintHandle = { "ProcessPaintHandle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, ProcessPaintHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ProcessPaintHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ProcessPaintHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_AmountPaintCountingHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_AmountPaintCountingHandle = { "AmountPaintCountingHandle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, AmountPaintCountingHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_AmountPaintCountingHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_AmountPaintCountingHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleGreen_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleGreen = { "ParticleGreen", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, ParticleGreen), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleGreen_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleRed_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleRed = { "ParticleRed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, ParticleRed), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleRed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleRight_MetaData[] = {
		{ "Category", "Particle" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleRight = { "PaintParticleRight", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, PaintParticleRight), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleLeft_MetaData[] = {
		{ "Category", "Particle" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleLeft = { "PaintParticleLeft", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, PaintParticleLeft), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureGreen_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureGreen = { "BrushTextureGreen", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, BrushTextureGreen), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureGreen_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureRed_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureRed = { "BrushTextureRed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, BrushTextureRed), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureRed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreenHead_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreenHead = { "RollerGreenHead", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerGreenHead), Z_Construct_UClass_UMaterialInstanceConstant_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreenHead_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreenHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreen_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreen = { "RollerGreen", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerGreen), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreen_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRedHead_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRedHead = { "RollerRedHead", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerRedHead), Z_Construct_UClass_UMaterialInstanceConstant_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRedHead_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRedHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRed_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRed = { "RollerRed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerRed), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerDirection_MetaData[] = {
		{ "Category", "Direction" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerDirection = { "RollerDirection", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerDirection), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterialInstance = { "BrushMaterialInstance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, BrushMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterial = { "BrushMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, BrushMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushSize_MetaData[] = {
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushSize = { "BrushSize", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, BrushSize), METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerHeadMesh_MetaData[] = {
		{ "Category", "PWRollerWeapon" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWRollerWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerHeadMesh = { "RollerHeadMesh", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerWeapon, RollerHeadMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerHeadMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerHeadMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWRollerWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ProcessPaintHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_AmountPaintCountingHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_ParticleRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_PaintParticleLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushTextureRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreenHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRedHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_BrushSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerWeapon_Statics::NewProp_RollerHeadMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWRollerWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWRollerWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWRollerWeapon_Statics::ClassParams = {
		&APWRollerWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWRollerWeapon_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWRollerWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWRollerWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWRollerWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWRollerWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWRollerWeapon, 457883794);
	template<> PAINTWAR_API UClass* StaticClass<APWRollerWeapon>()
	{
		return APWRollerWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWRollerWeapon(Z_Construct_UClass_APWRollerWeapon, &APWRollerWeapon::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWRollerWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWRollerWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
