// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Character.h"
#include "PWUICharacter.generated.h"

UCLASS()
class PAINTWAR_API APWUICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APWUICharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UPROPERTY()
	class APWBaseWeapon* Weapon;

	UPROPERTY()
		class UStaticMesh* Gun;

	UPROPERTY()
		class UStaticMesh* Roller;

	UPROPERTY()
		class UStaticMesh* Bomb;

	UFUNCTION(BlueprintCallable)
	void SetShooterWeapon();

	UFUNCTION(BlueprintCallable)
	void SetBomberWeapon();

	UFUNCTION(BlueprintCallable)
	void SetRollerWeapon();

	UFUNCTION(BlueprintCallable)
	class APWBaseWeapon* GetUIWeapon() const;
};
