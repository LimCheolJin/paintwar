// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWBaseCharacter.h"
#include "PWBomberCharacter.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWBomberCharacter : public APWBaseCharacter
{
	GENERATED_BODY()
public:
	APWBomberCharacter();
	bool GetIsFire() const;

	UFUNCTION(BlueprintCallable)
	void UnAbleFire();

	UFUNCTION()
	void SpawnBomberProjectile();

	UFUNCTION(BlueprintCallable)
	virtual void Fire() override;

	void SetIsFire(bool);

	virtual void Skill() override;
	int SkillTime;
	void SKillCountDown();
protected:
	virtual void BeginPlay() override;

	bool IsFire;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
private:


	UPROPERTY()
	class APWBomberProjectile* Projectile;

	UPROPERTY()
	class UPWBomberAnimInstance* BomberAnim;

};
