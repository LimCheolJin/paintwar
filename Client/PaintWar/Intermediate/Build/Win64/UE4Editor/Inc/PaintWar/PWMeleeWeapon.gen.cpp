// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Weapon/PWMeleeWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWMeleeWeapon() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWMeleeWeapon_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWMeleeWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseWeapon();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
// End Cross Module References
	void APWMeleeWeapon::StaticRegisterNativesAPWMeleeWeapon()
	{
	}
	UClass* Z_Construct_UClass_APWMeleeWeapon_NoRegister()
	{
		return APWMeleeWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APWMeleeWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerCollision_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerCollision;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWMeleeWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWMeleeWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/PWMeleeWeapon.h" },
		{ "ModuleRelativePath", "Public/Weapon/PWMeleeWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWMeleeWeapon_Statics::NewProp_RollerCollision_MetaData[] = {
		{ "Category", "Roller" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWMeleeWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWMeleeWeapon_Statics::NewProp_RollerCollision = { "RollerCollision", nullptr, (EPropertyFlags)0x0020080000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWMeleeWeapon, RollerCollision), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWMeleeWeapon_Statics::NewProp_RollerCollision_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWMeleeWeapon_Statics::NewProp_RollerCollision_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWMeleeWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWMeleeWeapon_Statics::NewProp_RollerCollision,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWMeleeWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWMeleeWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWMeleeWeapon_Statics::ClassParams = {
		&APWMeleeWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APWMeleeWeapon_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_APWMeleeWeapon_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWMeleeWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWMeleeWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWMeleeWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWMeleeWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWMeleeWeapon, 1671923528);
	template<> PAINTWAR_API UClass* StaticClass<APWMeleeWeapon>()
	{
		return APWMeleeWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWMeleeWeapon(Z_Construct_UClass_APWMeleeWeapon, &APWMeleeWeapon::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWMeleeWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWMeleeWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
