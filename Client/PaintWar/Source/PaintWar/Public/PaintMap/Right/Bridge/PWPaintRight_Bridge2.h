// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintRight_Bridge2.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintRight_Bridge2 : public APWPaintCanvas
{
	GENERATED_BODY()

public:
	APWPaintRight_Bridge2();
	
};
