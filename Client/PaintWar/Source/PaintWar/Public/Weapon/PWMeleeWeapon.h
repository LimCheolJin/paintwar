// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWBaseWeapon.h"
#include "PWMeleeWeapon.generated.h"

UCLASS()
class PAINTWAR_API APWMeleeWeapon : public APWBaseWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWMeleeWeapon();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = Roller)
	UBoxComponent* RollerCollision;
};
