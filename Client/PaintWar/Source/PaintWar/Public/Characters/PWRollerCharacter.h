// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWBaseCharacter.h"
#include "PWGameTimeManager.h"
#include "PWRollerCharacter.generated.h"


UCLASS()
class PAINTWAR_API APWRollerCharacter : public APWBaseCharacter
{
	GENERATED_BODY()
		
protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void RespawnEvent() override;

	bool bPainting;
public:
	APWRollerCharacter();


	UPROPERTY(EditAnywhere)
	class UPostProcessComponent* RollerPostProcessComponent;

	UFUNCTION(BlueprintCallable)
	void AblePainting();

	void UnablePainting();
	bool GetbPainting() const;

	void SetbPainting(bool);

	UFUNCTION()
	void OnPainting();

	virtual void Skill() override;
	virtual void Skilloff();

	int SkillTime;

	virtual void Tick(float DeltaTime) override;

	void SKillCountDown();

private:


	UPROPERTY()
	class UPWRollerAnimInstance* RollerAnim;
};
