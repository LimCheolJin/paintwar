// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWLoginGameMode_generated_h
#error "PWLoginGameMode.generated.h already included, missing '#pragma once' in PWLoginGameMode.h"
#endif
#define PAINTWAR_PWLoginGameMode_generated_h

#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWLoginGameMode(); \
	friend struct Z_Construct_UClass_APWLoginGameMode_Statics; \
public: \
	DECLARE_CLASS(APWLoginGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWLoginGameMode)


#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWLoginGameMode(); \
	friend struct Z_Construct_UClass_APWLoginGameMode_Statics; \
public: \
	DECLARE_CLASS(APWLoginGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWLoginGameMode)


#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWLoginGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWLoginGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWLoginGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWLoginGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWLoginGameMode(APWLoginGameMode&&); \
	NO_API APWLoginGameMode(const APWLoginGameMode&); \
public:


#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWLoginGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWLoginGameMode(APWLoginGameMode&&); \
	NO_API APWLoginGameMode(const APWLoginGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWLoginGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWLoginGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWLoginGameMode)


#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWLoginGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWLoginGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWLoginGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
