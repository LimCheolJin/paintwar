// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWShooterAnimInstance_generated_h
#error "PWShooterAnimInstance.generated.h already included, missing '#pragma once' in PWShooterAnimInstance.h"
#endif
#define PAINTWAR_PWShooterAnimInstance_generated_h

#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWShooterAnimInstance(); \
	friend struct Z_Construct_UClass_UPWShooterAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWShooterAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWShooterAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUPWShooterAnimInstance(); \
	friend struct Z_Construct_UClass_UPWShooterAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWShooterAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWShooterAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWShooterAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWShooterAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWShooterAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWShooterAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWShooterAnimInstance(UPWShooterAnimInstance&&); \
	NO_API UPWShooterAnimInstance(const UPWShooterAnimInstance&); \
public:


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWShooterAnimInstance(UPWShooterAnimInstance&&); \
	NO_API UPWShooterAnimInstance(const UPWShooterAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWShooterAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWShooterAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPWShooterAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IsFire() { return STRUCT_OFFSET(UPWShooterAnimInstance, IsFire); }


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_15_PROLOG
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_INCLASS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWShooterAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AnimInstance_PWShooterAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
