// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWShooterCharacter_generated_h
#error "PWShooterCharacter.generated.h already included, missing '#pragma once' in PWShooterCharacter.h"
#endif
#define PAINTWAR_PWShooterCharacter_generated_h

#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUnAbleFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnAbleFire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUnAbleFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnAbleFire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWShooterCharacter(); \
	friend struct Z_Construct_UClass_APWShooterCharacter_Statics; \
public: \
	DECLARE_CLASS(APWShooterCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWShooterCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPWShooterCharacter(); \
	friend struct Z_Construct_UClass_APWShooterCharacter_Statics; \
public: \
	DECLARE_CLASS(APWShooterCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWShooterCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWShooterCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWShooterCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWShooterCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWShooterCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWShooterCharacter(APWShooterCharacter&&); \
	NO_API APWShooterCharacter(const APWShooterCharacter&); \
public:


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWShooterCharacter(APWShooterCharacter&&); \
	NO_API APWShooterCharacter(const APWShooterCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWShooterCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWShooterCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWShooterCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShooterAnim() { return STRUCT_OFFSET(APWShooterCharacter, ShooterAnim); }


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_13_PROLOG
#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_INCLASS \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWShooterCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Characters_PWShooterCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
