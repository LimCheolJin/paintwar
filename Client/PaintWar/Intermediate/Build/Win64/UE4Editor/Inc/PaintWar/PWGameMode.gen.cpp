// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWGameMode() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWGameMode_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWGameMode_ChangeMenuWidget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWGameMode_SendWorldInfo();
	PAINTWAR_API UClass* Z_Construct_UClass_APWGameTimeManager_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWGameInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWSpawnPoint_NoRegister();
// End Cross Module References
	void APWGameMode::StaticRegisterNativesAPWGameMode()
	{
		UClass* Class = APWGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ChangeMenuWidget", &APWGameMode::execChangeMenuWidget },
			{ "SendWorldInfo", &APWGameMode::execSendWorldInfo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics
	{
		struct PWGameMode_eventChangeMenuWidget_Parms
		{
			TSubclassOf<UUserWidget>  NewWidgetClass;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_NewWidgetClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::NewProp_NewWidgetClass = { "NewWidgetClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameMode_eventChangeMenuWidget_Parms, NewWidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::NewProp_NewWidgetClass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWGameMode, nullptr, "ChangeMenuWidget", sizeof(PWGameMode_eventChangeMenuWidget_Parms), Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWGameMode_ChangeMenuWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWGameMode_ChangeMenuWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWGameMode, nullptr, "SendWorldInfo", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWGameMode_SendWorldInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWGameMode_SendWorldInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWGameMode_NoRegister()
	{
		return APWGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APWGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_GameTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_GameTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SendTimerHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SendTimerHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistOnGameLevelCharacters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExistOnGameLevelCharacters;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExistOnGameLevelCharacters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PWGameInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PWGameInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GreenTeamSpawnPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GreenTeamSpawnPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RedTeamSpawnPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RedTeamSpawnPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWGameMode_ChangeMenuWidget, "ChangeMenuWidget" }, // 4108656196
		{ &Z_Construct_UFunction_APWGameMode_SendWorldInfo, "SendWorldInfo" }, // 2576573807
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PWGameMode.h" },
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_m_GameTimer_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_m_GameTimer = { "m_GameTimer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, m_GameTimer), Z_Construct_UClass_APWGameTimeManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_m_GameTimer_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_m_GameTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_SendTimerHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_SendTimerHandle = { "SendTimerHandle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, SendTimerHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_SendTimerHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_SendTimerHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
		{ "ToolTip", "UPROPERTY()\n               class APWShooterCharacter* ShooterCharacterRed;\n\n       UPROPERTY()\n               class APWShooterCharacter* ShooterCharacterGreen;\n\n       UPROPERTY()\n               class APWRollerCharacter* RollerCharacterRed;\n\n       UPROPERTY()\n               class APWRollerCharacter* RollerCharacterGreen;\n\n       UPROPERTY()\n               class APWBomberCharacter* BomberCharacterRed;\n\n       UPROPERTY()\n               class APWBomberCharacter* BomberCharacterGreen;" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters = { "ExistOnGameLevelCharacters", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, ExistOnGameLevelCharacters), METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters_Inner = { "ExistOnGameLevelCharacters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_APWBaseCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_PWGameInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_PWGameInstance = { "PWGameInstance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, PWGameInstance), Z_Construct_UClass_UPWGameInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_PWGameInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_PWGameInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_GreenTeamSpawnPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_GreenTeamSpawnPoint = { "GreenTeamSpawnPoint", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, GreenTeamSpawnPoint), Z_Construct_UClass_APWSpawnPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_GreenTeamSpawnPoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_GreenTeamSpawnPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_RedTeamSpawnPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_RedTeamSpawnPoint = { "RedTeamSpawnPoint", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, RedTeamSpawnPoint), Z_Construct_UClass_APWSpawnPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_RedTeamSpawnPoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_RedTeamSpawnPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameMode_Statics::NewProp_CurrentWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWGameMode_Statics::NewProp_CurrentWidget = { "CurrentWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameMode, CurrentWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::NewProp_CurrentWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::NewProp_CurrentWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_m_GameTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_SendTimerHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_ExistOnGameLevelCharacters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_PWGameInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_GreenTeamSpawnPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_RedTeamSpawnPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameMode_Statics::NewProp_CurrentWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWGameMode_Statics::ClassParams = {
		&APWGameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWGameMode_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::PropPointers),
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_APWGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWGameMode, 4180741407);
	template<> PAINTWAR_API UClass* StaticClass<APWGameMode>()
	{
		return APWGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWGameMode(Z_Construct_UClass_APWGameMode, &APWGameMode::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
