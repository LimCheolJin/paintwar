// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWAnimInstance_generated_h
#error "PWAnimInstance.generated.h already included, missing '#pragma once' in PWAnimInstance.h"
#endif
#define PAINTWAR_PWAnimInstance_generated_h

#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWAnimInstance(); \
	friend struct Z_Construct_UClass_UPWAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUPWAnimInstance(); \
	friend struct Z_Construct_UClass_UPWAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWAnimInstance(UPWAnimInstance&&); \
	NO_API UPWAnimInstance(const UPWAnimInstance&); \
public:


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWAnimInstance(UPWAnimInstance&&); \
	NO_API UPWAnimInstance(const UPWAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPWAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentPawnSpeed() { return STRUCT_OFFSET(UPWAnimInstance, CurrentPawnSpeed); } \
	FORCEINLINE static uint32 __PPO__IsInAir() { return STRUCT_OFFSET(UPWAnimInstance, IsInAir); } \
	FORCEINLINE static uint32 __PPO__IsDead() { return STRUCT_OFFSET(UPWAnimInstance, IsDead); } \
	FORCEINLINE static uint32 __PPO__IsReload() { return STRUCT_OFFSET(UPWAnimInstance, IsReload); }


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_17_PROLOG
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_INCLASS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AnimInstance_PWAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
