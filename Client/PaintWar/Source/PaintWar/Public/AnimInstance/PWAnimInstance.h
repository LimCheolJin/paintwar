// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "Animation/AnimInstance.h"
#include "PWAnimInstance.generated.h"

/**
 * 
 */

//우리가 사용한 독립적인 애님인스턴스는 나중에 서버에서 캐릭터별로 어떤 애니메이션을 지정해라
//할때 사용할 것이다. 여기선 공통적인 뛰기 점프 의 애니메이션을 위한 정보를가지고잇을것이고
//이를 상속받아 만든 AnimInstance에는 각각 shooter, bomber, Roller의 애님인스턴스를 각각 만들어
// 처리해 줄 것 이다. 물론 사용할당시에 부모는 UAnimInstance형으로 선언하고 접근해야한다.
UCLASS()
class PAINTWAR_API UPWAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	//생성자
	UPWAnimInstance();

	//애님인스턴스는 틱마다 호출되는 NativeUpdateAnimation함수를 가상함수로 제공한다.
	//이를 선언하고 정의하는 이유는 애님인스턴스에 데이터를 tick마다 set해주기위함이다.
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

private:
	//현재 폰의 속도를 저장하여 애님그래프에 알려주기위함.
	//에디터 어디서든지 수정할수 있고 블루프린트에서 읽기,쓰기전용 으로 만들어주었다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float CurrentPawnSpeed;

	//현재 폰의 공중에 떠잇는지 여부를 확인하기위함. 확인후
	//애님그래프에 적용하기위함이다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsInAir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsReload;
};
