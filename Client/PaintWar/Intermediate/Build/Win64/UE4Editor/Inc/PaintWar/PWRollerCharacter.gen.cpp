// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Characters/PWRollerCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWRollerCharacter() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerCharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerCharacter();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerCharacter_AblePainting();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWRollerCharacter_OnPainting();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWRollerAnimInstance_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPostProcessComponent_NoRegister();
// End Cross Module References
	void APWRollerCharacter::StaticRegisterNativesAPWRollerCharacter()
	{
		UClass* Class = APWRollerCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AblePainting", &APWRollerCharacter::execAblePainting },
			{ "OnPainting", &APWRollerCharacter::execOnPainting },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWRollerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerCharacter, nullptr, "AblePainting", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerCharacter_AblePainting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerCharacter_AblePainting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWRollerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWRollerCharacter, nullptr, "OnPainting", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWRollerCharacter_OnPainting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWRollerCharacter_OnPainting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWRollerCharacter_NoRegister()
	{
		return APWRollerCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APWRollerCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerAnim_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerAnim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollerPostProcessComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RollerPostProcessComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWRollerCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseCharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWRollerCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWRollerCharacter_AblePainting, "AblePainting" }, // 865200126
		{ &Z_Construct_UFunction_APWRollerCharacter_OnPainting, "OnPainting" }, // 15916691
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Characters/PWRollerCharacter.h" },
		{ "ModuleRelativePath", "Public/Characters/PWRollerCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerAnim_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWRollerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerAnim = { "RollerAnim", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerCharacter, RollerAnim), Z_Construct_UClass_UPWRollerAnimInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerAnim_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerAnim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerPostProcessComponent_MetaData[] = {
		{ "Category", "PWRollerCharacter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Characters/PWRollerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerPostProcessComponent = { "RollerPostProcessComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWRollerCharacter, RollerPostProcessComponent), Z_Construct_UClass_UPostProcessComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerPostProcessComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerPostProcessComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWRollerCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerAnim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWRollerCharacter_Statics::NewProp_RollerPostProcessComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWRollerCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWRollerCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWRollerCharacter_Statics::ClassParams = {
		&APWRollerCharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWRollerCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWRollerCharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWRollerCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWRollerCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWRollerCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWRollerCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWRollerCharacter, 219296061);
	template<> PAINTWAR_API UClass* StaticClass<APWRollerCharacter>()
	{
		return APWRollerCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWRollerCharacter(Z_Construct_UClass_APWRollerCharacter, &APWRollerCharacter::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWRollerCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWRollerCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
