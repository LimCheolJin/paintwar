// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPlayerController_generated_h
#error "PWPlayerController.generated.h already included, missing '#pragma once' in PWPlayerController.h"
#endif
#define PAINTWAR_PWPlayerController_generated_h

#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEscMenuClose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EscMenuClose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuHide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuHide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuClose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuClose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuOpen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuOpen(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEscMenuClose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EscMenuClose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuHide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuHide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuClose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuClose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execEnterMenuOpen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EnterMenuOpen(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPlayerController(); \
	friend struct Z_Construct_UClass_APWPlayerController_Statics; \
public: \
	DECLARE_CLASS(APWPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPlayerController)


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPlayerController(); \
	friend struct Z_Construct_UClass_APWPlayerController_Statics; \
public: \
	DECLARE_CLASS(APWPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPlayerController)


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPlayerController(APWPlayerController&&); \
	NO_API APWPlayerController(const APWPlayerController&); \
public:


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPlayerController(APWPlayerController&&); \
	NO_API APWPlayerController(const APWPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPlayerController)


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
