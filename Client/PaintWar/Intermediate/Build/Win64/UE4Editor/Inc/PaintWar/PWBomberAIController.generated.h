// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBomberAIController_generated_h
#error "PWBomberAIController.generated.h already included, missing '#pragma once' in PWBomberAIController.h"
#endif
#define PAINTWAR_PWBomberAIController_generated_h

#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBomberAIController(); \
	friend struct Z_Construct_UClass_APWBomberAIController_Statics; \
public: \
	DECLARE_CLASS(APWBomberAIController, APWBaseAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPWBomberAIController(); \
	friend struct Z_Construct_UClass_APWBomberAIController_Statics; \
public: \
	DECLARE_CLASS(APWBomberAIController, APWBaseAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBomberAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBomberAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberAIController(APWBomberAIController&&); \
	NO_API APWBomberAIController(const APWBomberAIController&); \
public:


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberAIController(APWBomberAIController&&); \
	NO_API APWBomberAIController(const APWBomberAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBomberAIController)


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_11_PROLOG
#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_INCLASS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBomberAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AIControllers_PWBomberAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
