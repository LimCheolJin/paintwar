// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_1st.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_1st : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintLeft_1st();
};
