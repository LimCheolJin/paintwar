// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWBaseWeapon.h"
#include "PWRangeWeapon.generated.h"

UCLASS()
class PAINTWAR_API APWRangeWeapon : public APWBaseWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWRangeWeapon();

	UPROPERTY(EditAnywhere, Category = Weapon)
	float Range;

	class UArrowComponent* GetProjectileSpawnPoint();

protected:
	UPROPERTY(EditAnywhere, Category = Projectile)
	class UArrowComponent* ProjectileSpawnPoint;

	virtual void Tick(float DeltaTime) override;
	
};
