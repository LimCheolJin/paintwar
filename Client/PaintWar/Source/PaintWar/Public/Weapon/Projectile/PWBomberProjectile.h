// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "Weapon/Projectile/PWBaseProjectile.h"
#include "PWBomberProjectile.generated.h"

/**
 * 
 */

UCLASS()
class PAINTWAR_API APWBomberProjectile : public APWBaseProjectile
{
	GENERATED_BODY()
public:
	APWBomberProjectile();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void PaintProcess();

	UFUNCTION()
	void HideProjectile();

	void Skill();

	UPROPERTY()
		class USoundCue* AttackSound;

	UAudioComponent* AttackSoundComponent;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = Projectile)
	class UBoxComponent* ExplosionRangeBox;

	//virtual void BeginDestroy() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	float CurrentProjectileSpeedSize;

	bool IsDestroy = false;

	UPROPERTY()
	class UParticleSystem* ParticleRed;

	UPROPERTY()
	class UParticleSystem* ParticleGreen;

	struct FTimerHandle DestoyEventTimerHandle;


};
