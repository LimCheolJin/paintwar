// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/AnimInstance/PWShooterAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWShooterAnimInstance() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_UPWShooterAnimInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWShooterAnimInstance();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWAnimInstance();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1();
	PAINTWAR_API UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify();
// End Cross Module References
	void UPWShooterAnimInstance::StaticRegisterNativesUPWShooterAnimInstance()
	{
		UClass* Class = UPWShooterAnimInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AnimNotify_FireNotify", &UPWShooterAnimInstance::execAnimNotify_FireNotify },
			{ "AnimNotify_FireNotify1", &UPWShooterAnimInstance::execAnimNotify_FireNotify1 },
			{ "AnimNotify_ReloadNotify", &UPWShooterAnimInstance::execAnimNotify_ReloadNotify },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimInstance/PWShooterAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWShooterAnimInstance, nullptr, "AnimNotify_FireNotify", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimInstance/PWShooterAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWShooterAnimInstance, nullptr, "AnimNotify_FireNotify1", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimInstance/PWShooterAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPWShooterAnimInstance, nullptr, "AnimNotify_ReloadNotify", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPWShooterAnimInstance_NoRegister()
	{
		return UPWShooterAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPWShooterAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsFire_MetaData[];
#endif
		static void NewProp_IsFire_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsFire;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPWShooterAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPWAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPWShooterAnimInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify, "AnimNotify_FireNotify" }, // 3693178255
		{ &Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_FireNotify1, "AnimNotify_FireNotify1" }, // 3992754408
		{ &Z_Construct_UFunction_UPWShooterAnimInstance_AnimNotify_ReloadNotify, "AnimNotify_ReloadNotify" }, // 691645031
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWShooterAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "AnimInstance/PWShooterAnimInstance.h" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWShooterAnimInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWShooterAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire_SetBit(void* Obj)
	{
		((UPWShooterAnimInstance*)Obj)->IsFire = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire = { "IsFire", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPWShooterAnimInstance), &Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPWShooterAnimInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWShooterAnimInstance_Statics::NewProp_IsFire,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPWShooterAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPWShooterAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPWShooterAnimInstance_Statics::ClassParams = {
		&UPWShooterAnimInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPWShooterAnimInstance_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UPWShooterAnimInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPWShooterAnimInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPWShooterAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPWShooterAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPWShooterAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPWShooterAnimInstance, 409516623);
	template<> PAINTWAR_API UClass* StaticClass<UPWShooterAnimInstance>()
	{
		return UPWShooterAnimInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPWShooterAnimInstance(Z_Construct_UClass_UPWShooterAnimInstance, &UPWShooterAnimInstance::StaticClass, TEXT("/Script/PaintWar"), TEXT("UPWShooterAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPWShooterAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
