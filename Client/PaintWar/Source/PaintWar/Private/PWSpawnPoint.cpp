// Fill out your copyright notice in the Description page of Project Settings.


#include "PWSpawnPoint.h"
#include "PWBaseCharacter.h"

// Sets default values
APWSpawnPoint::APWSpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerVolume"));
	//TriggerBox->SetBoxExtent(FVector(1200.0f, 1200.0f, 1000.0f));
	//TriggerBox->SetBoxExtent(FVector(150.0f, 150.0f, 100.0f));
	TriggerBox->SetBoxExtent(FVector(75.0f, 75.0f, 50.0f));
	RootComponent = TriggerBox;

	TriggerBox->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));
	TriggerBox->SetGenerateOverlapEvents(true);
	this->SetActorHiddenInGame(false);
	TeamColor = ETeam::NONE;
}

// Called when the game starts or when spawned
void APWSpawnPoint::BeginPlay()
{
	Super::BeginPlay();

	/*if (!GetName().Compare(FString("RedSpawnPoint")))
	{
		this->SetTeamColor(ETeam::RED);
	}
	else if (!GetName().Compare(FString("GreenSpawnPoint")))
	{
		this->SetTeamColor(ETeam::GREEN);
	}*/
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &APWSpawnPoint::OnOverlapEnd);
}

// Called every frame
void APWSpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void APWSpawnPoint::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	/*const auto& BaseCharacter = Cast<APWBaseCharacter>(OtherActor);
	if (BaseCharacter == nullptr)
		return;


	BaseCharacter->SetActorEnableCollision(true);*/
	

		//Weapon->SetActorEnableCollision(true);
	//this->SetActorEnableCollision(true);	
}

ETeam APWSpawnPoint::GetTeamColor() const
{
	return TeamColor;
}

void APWSpawnPoint::SetTeamColor(ETeam Color)
{
	TeamColor = Color;
}