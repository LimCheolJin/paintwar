// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
#ifdef PAINTWAR_PWGameMode_generated_h
#error "PWGameMode.generated.h already included, missing '#pragma once' in PWGameMode.h"
#endif
#define PAINTWAR_PWGameMode_generated_h

#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSendWorldInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SendWorldInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSendWorldInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SendWorldInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWGameMode(); \
	friend struct Z_Construct_UClass_APWGameMode_Statics; \
public: \
	DECLARE_CLASS(APWGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWGameMode)


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPWGameMode(); \
	friend struct Z_Construct_UClass_APWGameMode_Statics; \
public: \
	DECLARE_CLASS(APWGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWGameMode)


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWGameMode(APWGameMode&&); \
	NO_API APWGameMode(const APWGameMode&); \
public:


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWGameMode(APWGameMode&&); \
	NO_API APWGameMode(const APWGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWGameMode)


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RedTeamSpawnPoint() { return STRUCT_OFFSET(APWGameMode, RedTeamSpawnPoint); } \
	FORCEINLINE static uint32 __PPO__GreenTeamSpawnPoint() { return STRUCT_OFFSET(APWGameMode, GreenTeamSpawnPoint); } \
	FORCEINLINE static uint32 __PPO__PWGameInstance() { return STRUCT_OFFSET(APWGameMode, PWGameInstance); } \
	FORCEINLINE static uint32 __PPO__ExistOnGameLevelCharacters() { return STRUCT_OFFSET(APWGameMode, ExistOnGameLevelCharacters); } \
	FORCEINLINE static uint32 __PPO__SendTimerHandle() { return STRUCT_OFFSET(APWGameMode, SendTimerHandle); } \
	FORCEINLINE static uint32 __PPO__m_GameTimer() { return STRUCT_OFFSET(APWGameMode, m_GameTimer); }


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_15_PROLOG
#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_INCLASS \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWGameMode_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
