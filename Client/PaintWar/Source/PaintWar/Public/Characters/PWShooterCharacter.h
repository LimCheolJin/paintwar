// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWBaseCharacter.h"
#include "PWGameTimeManager.h"
#include "PWShooterCharacter.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWShooterCharacter : public APWBaseCharacter
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaTime) override;

	bool IsFire;
public:
	APWShooterCharacter();


	UFUNCTION(BlueprintCallable)
	virtual void Fire() override;

	bool GetIsFire() const;

	void SetIsFire(bool);

	UFUNCTION(BlueprintCallable)
	void UnAbleFire();

	void OnFire();

	void SpawnShooterProjectile(FVector);
	
	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* RedParticle;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* GreenParticle;

	virtual void Skill() override;

	int SkillTime;

	void SKillCountDown();

private:

	FTimerHandle SpawnTimer;

	UPROPERTY()
	class UPWShooterAnimInstance* ShooterAnim;
};
