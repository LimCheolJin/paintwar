// Fill out your copyright notice in the Description page of Project Settings.


#include "PWNetWork.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformAffinity.h"
#include "Runtime/Core/Public/HAL/RunnableThread.h"
#include "PWGameInstance.h"
#include "PWGameMode.h"
#include <iostream>
#include <map>

PWNetWork::PWNetWork()
{
	m_ClientNetWorkThread = nullptr;

}

PWNetWork::~PWNetWork()
{
	/*delete m_ClientNetWorkThread;
	m_ClientNetWorkThread = nullptr;*/
	if (m_ClientNetWorkThread)
		delete m_ClientNetWorkThread;
}


bool PWNetWork::Init()
{
	//FRunnable::Init();
	//PWLOG(Error, TEXT("Thread Init!"));
	return true;
}
uint32 PWNetWork::Run()
{
	FPlatformProcess::Sleep(0.03);
	while((m_bIsRun == true) && (IsThreadRun == true) )
	{ //1000초 기다린다.
		m_NetWorkIndex = WSAWaitForMultipleEvents(1, &m_hEvent, FALSE, INFINITE, FALSE);

		if (m_NetWorkIndex == WSA_WAIT_FAILED)
		{
			//PWLOG(Error, TEXT("WSAWaitForMultipleEvents WSA_WAIT_FAILED"));
			continue;
		}
		else if (m_NetWorkIndex == WSA_WAIT_TIMEOUT)
		{
			//PWLOG(Error, TEXT("WSAWaitForMultipleEvents WSA_WAIT_TIMEOUT"));
			continue;
		}

		int retval = WSAEnumNetworkEvents(m_ClientSocket, m_hEvent, &m_NetWorkEvents);
		if (retval == SOCKET_ERROR)
		{
			//PWLOG(Error, TEXT("WSAEnumNetworkEvents Error"));
			break;
		}

		//FD_READ 이벤트가 발생했을떄.
		if (m_NetWorkEvents.lNetworkEvents & FD_READ)
		{
			if (m_NetWorkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
				//PWLOG(Error, TEXT("FD_READ_ERROR"));
				continue;
			}

			DWORD RecvBytes = 0;
			DWORD flags = 0;
			int PacketSize = 0;
			if (WSARecv(m_ClientSocket, &m_WSARecvBuf, 1, &RecvBytes, &flags, NULL, NULL) == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
				{
					//PWLOG(Error, TEXT("WSARecv Error"));
				}	
			}
			int IOBytes = RecvBytes;
			char* packet = reinterpret_cast<char*>(m_RecvBuf);

			if (m_PrevPacketSize != 0)
				PacketSize = m_PacketBuf[0];
			while (IOBytes > 0) //받은게 있다면
			{
				if (PacketSize == 0) // 처음 받은거면
					PacketSize = *packet;  //현재 받은 패킷의 사이즈

				//DWORD required = PacketSize - m_PrevPacketSize; //필요한 패킷크기는 받아야할 패킷의 크기 - 받았었던 패킷의크기
				if (PacketSize <= IOBytes + m_PrevPacketSize)   //현재 받은양  < 필요한 양,   미완성
				{
					memcpy(m_PacketBuf + m_PrevPacketSize, packet,PacketSize - m_PrevPacketSize); //지난번에 받았던게 있을수도있으므로 그 다음번지부터 받는다.
					//m_PrevPacketSize += RecvBytes;  //전에받았던 양을 늘린다.
					//break;
					packet += PacketSize - m_PrevPacketSize;
					IOBytes -= PacketSize - m_PrevPacketSize;
					PacketSize = 0;
					RecvPacketProcess(m_PacketBuf);
					m_PrevPacketSize = 0;
				}
				else // 
				{
					memcpy(m_PacketBuf + m_PrevPacketSize, packet, IOBytes);
					m_PrevPacketSize += IOBytes;
					IOBytes = 0;
					packet += IOBytes;
					

					/*m_PrevPacketSize = 0;
					RecvBytes -= required;
					packet += required;
					PacketSize = 0;*/
				}
			}

		}

		if (m_NetWorkEvents.lNetworkEvents & FD_CLOSE)
		{
			//PWLOG(Error, TEXT("FD_CLOSE EVENT"));
			break;
		}
	}
	return 0;
}
void PWNetWork::Stop()
{
	//FRunnable::Stop();
	//FRunnable::Stop();
	IsThreadRun = false;
	m_StopThreadCounter.Increment();
	//PWLOG(Error, TEXT("Thread Stop!"));
}
//void PWNetWork::Exit()
//{
//	//FRunnable::Exit();
//	PWLOG(Error, TEXT("Thread Exit!"));
//}

bool PWNetWork::InitClientSocket()
{
	WSADATA wsa;
	//PWLOG(Error, TEXT("PWNetWork's InitClientSocket"));
	// Winsock Init  2.2 버전사용
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
		//PWLOG(Error, TEXT("WinSock Init Error"));
		return false;
	}

	m_ClientSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (INVALID_SOCKET == m_ClientSocket)
	{
		//PWLOG(Error, TEXT("Make Socket Error"));
		return false;
	}

	m_WSARecvBuf.buf = m_RecvBuf;
	m_WSARecvBuf.len = MAX_BUFFER;

	m_WSASendBuf.buf = m_SendBuf;

	return true;
}

bool PWNetWork::CreateEventSelect()
{
	//PWLOG(Error, TEXT("PWNetWork's CreateEventSelect"));
	int retval;
	m_hEvent = WSACreateEvent();
	if (m_hEvent == WSA_INVALID_EVENT)
	{
		//PWLOG(Error, TEXT("WSACreateEvent() Error"));
		return false;
	}

	retval = WSAEventSelect(m_ClientSocket, m_hEvent, FD_READ || FD_CLOSE);
	if (retval == SOCKET_ERROR)
	{
		//PWLOG(Error, TEXT("WSAEventSelect Error"));
		return false;
	}

	return true;
}

bool PWNetWork::Connect(const char* serverIP)
{
	//PWLOG(Error, TEXT("PWNetWork's Connect"));
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(SOCKADDR_IN));

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(SERVER_PORT);
	serveraddr.sin_addr.s_addr = inet_addr(serverIP);

	int retval = connect(m_ClientSocket, (SOCKADDR*)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR)
	{
		//PWLOG(Error, TEXT("Connect Error"));
		return false;
	}
	//PWLOG(Error, TEXT("Connect Succeeded"));
	m_bIsRun = true;
	return true;

}


bool PWNetWork::StartThread()
{
	//PWLOG(Error, TEXT("PWNetWork's StartThread"));
	if (m_ClientNetWorkThread != nullptr)
		return false;
	if (IsThreadRun == true)
		return false;
	m_ClientNetWorkThread = FRunnableThread::Create(this, TEXT("PWNetWork"), 0, TPri_BelowNormal);
	IsThreadRun = true;
	return true;
}

void PWNetWork::SetPWGameInstance(UPWGameInstance* instance)
{
	m_PWGameInstance = instance;

}

void PWNetWork::SetPWGameMode(APWGameMode* gameMode)
{
	if(gameMode != nullptr)
		m_PWGameMode = gameMode;
}

void PWNetWork::DisConnect()
{
	//IsThreadRun = false;
	m_bIsRun = false;

	if (m_ClientNetWorkThread != nullptr)
		this->StopThread();

	WSACloseEvent(m_hEvent);
	if (m_ClientSocket != INVALID_SOCKET)
		closesocket(m_ClientSocket);

	WSACleanup();

	//PWLOG(Error, TEXT("Client NetWork is DisConnected"));
}

void PWNetWork::StopThread()
{
	
	if (m_ClientNetWorkThread == nullptr)
		return;
	/*if (IsThreadRun == false)
		return;*/

	//IsThreadRun = false;
	//m_ClientNetWorkThread->Kill();*/
	m_ClientNetWorkThread->Kill(false);
	delete m_ClientNetWorkThread;
	m_ClientNetWorkThread = nullptr;
	//m_ClientNetWorkThread->WaitForCompletion();
	//PWLOG(Error, TEXT("m_ClientNetWorkThread is  died !"));
	/*delete m_ClientNetWorkThread;
	m_ClientNetWorkThread = nullptr;*/
	
	
	//if (this != nullptr)
	//{
	//	this->Stop();
	//	//m_ClientNetWorkThread->WaitForCompletion();
	//	/*delete m_ClientNetWorkThread;
	//	m_ClientNetWorkThread = nullptr;*/
	//	delete this;
	//	
	//}
	m_StopThreadCounter.Reset();
}

void PWNetWork::RecvPacketProcess(char* Packet)
{
	char PacketType = Packet[1];

	switch ((e_PacketType)PacketType)
	{
	case e_PacketType::e_LoginOKPacket:
	{
		/*SC_PACKET_PLAYERLOGIN LoginPacket;
		memcpy(&LoginPacket, Packet, sizeof(SC_PACKET_PLAYERLOGIN));*/
		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_LoginPacketProcess(Packet);

		}
		break;
	}
	case e_PacketType::e_EnterPacket:
	{
		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_OtherPlayerEnterPacketProcess(Packet);

		}
	}
	break;
	case e_PacketType::e_LeavePacket:
	{
		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_OtherPlayerLeavePacketProcess(Packet);
		}
		break;
	}
	case e_PacketType::e_SelectTeamPacket:
	{
		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_SelectTeamTypePacketProcess(Packet);
		}
		break;
	}
	case e_PacketType::e_SelectCharacterTypePacket:
	{
		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_SelectCharacterTypePacketProcess(Packet);
		}
		break;
	}
	case e_PacketType::e_StartGamePacket:
	{

		if (m_PWGameInstance != nullptr)
		{
			m_PWGameInstance->Recv_StartGamePacketProcess(Packet);
		}
		break;
	}
	case e_PacketType::e_PlayerMovePacket:
	{
		if (m_PWGameMode != nullptr)
		{
			m_PWGameMode->Recv_PlayerMovePacketProcess(Packet);
		}
	}
	break;
	case e_PacketType::e_ShooterFire:
	{
		if (m_PWGameMode != nullptr)
		{
			m_PWGameMode->Recv_ShooterFirePacketProcess(Packet);
		}
	}
	break;
	case e_PacketType::e_RollerPaint:
	{
		if (m_PWGameMode != nullptr)
		{
			m_PWGameMode->Recv_RollerPaintPacketProcess(Packet);
		}
	}
	break;
	case e_PacketType::e_BomberThrow:
	{
		if (m_PWGameMode != nullptr)
		{
			m_PWGameMode->Recv_BomberThrowPacketProcess(Packet);
		}
	}
	break;
	case e_PacketType::e_Reloading:
	{
		if (m_PWGameMode != nullptr)
		{
			m_PWGameMode->Recv_ReloadingPacketProcess(Packet);
		}
	}
	break;
	case e_PacketType::e_GameTimer:
	{
		if (m_PWGameMode != nullptr)
			m_PWGameMode->Recv_GameTimerPacketProcess(Packet);
	}
	break;
	case e_PacketType::e_EndGamePacket:
	{
		if (m_PWGameInstance != nullptr)
			m_PWGameInstance->Recv_GameEndPacketProcess(Packet);
	}
	break;
	case e_PacketType::e_ResultGame:
	{
		if (m_PWGameInstance != nullptr)
			m_PWGameInstance->Recv_GameResultPacketProcess(Packet);
	}
	break;
	case e_PacketType::e_ChatPacket:
	{
		if (m_PWGameMode != nullptr)
			m_PWGameMode->Recv_ChatPacketProcess(Packet,true);
	}
	break;
	}
}

bool PWNetWork::Send_Packet()
{
	/*char* buf = reinterpret_cast<char*>(packet);
	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = buf[0];
	memcpy(m_SendBuf, buf, sizeof(buf[0]));*/
	DWORD SentBytes = 0;
	DWORD flags = 0;

	if (WSASend(m_ClientSocket, &m_WSASendBuf, 1, &SentBytes, flags, NULL, NULL) != 0)
	{
		//PWLOG(Error, TEXT("Send Packet Error"));
		return false;
	}
	return true;

}

const bool PWNetWork::Send_Login()
{
	if (m_bIsRun == false)
		return false;
	CS_PACKET_LOGIN packet{};
	memset(&packet, 0, sizeof(CS_PACKET_LOGIN));
	packet.m_PacketSize = sizeof(CS_PACKET_LOGIN);
	packet.m_ePacketType = e_PacketType::e_LogInPacket;
	FString tempID = m_PWGameInstance->GetLoginID();
	PWLOG(Error, TEXT("%s    , %d"), *tempID, tempID.Len());
	int len = tempID.Len();
	const char* ID = TCHAR_TO_ANSI(* (tempID) );

	memcpy(packet.m_LoginID,ID,len);
	packet.m_LoginID[len] = '\0';

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_LOGIN);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_LOGIN));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Login Failed "));
		return false;
	}
	return true;

}
const bool PWNetWork::Send_ReadyPacket(e_GameStateInfo currentState, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_READY packet{};
	memset(&packet, 0, sizeof(CS_PACKET_READY));
	packet.m_PacketSize = sizeof(CS_PACKET_READY);
	packet.m_ePacketType = e_PacketType::e_ReadyPacket;
	packet.m_ID = ID;
	packet.m_PlayerState = currentState;

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_READY);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_READY));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send ReadyInfo Error"));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_SelectTeamTypePacket(ETeam eTeamType, int ID)
{
	if (m_bIsRun == false)
		return false;
	//PWLOG(Error, TEXT("PWNetWork's Send_SelectTeamTypePacket"));
	CS_PACKET_SELECT_TEAM packet{};
	packet.m_ePacketType = e_PacketType::e_SelectTeamPacket;
	packet.m_eTeamType = eTeamType;
	packet.m_PacketSize = sizeof(CS_PACKET_SELECT_TEAM);
	packet.m_ID = ID;

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_SELECT_TEAM);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_SELECT_TEAM));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Login Failed "));
		return false;
	}
	return true;
}
const bool PWNetWork::Send_SelectCharacterTypePacket(ESelectCharacter  eCharacterType, int ID)
{
	if (m_bIsRun == false)
		return false;
	//PWLOG(Error, TEXT("PWNetWork's Send_SelectCharacterTypePacket"));
	CS_PACKET_SELECT_CHARACTERTYPE packet{};
	packet.m_eCharacterType = eCharacterType;//(e_PlayerCharacterType)eCharacterType;
	packet.m_ePacketType = e_PacketType::e_SelectCharacterTypePacket;
	packet.m_ID = ID;
	packet.m_PacketSize = sizeof(CS_PACKET_SELECT_CHARACTERTYPE);

	
	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_SELECT_CHARACTERTYPE);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_SELECT_CHARACTERTYPE));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Login Failed "));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_PlayerInfoPacket(ObjectInfo object, int ID, int skillcount, bool skillon, int amountpaint)
{
	if (m_bIsRun == false)
		return false;
	//PWLOG(Error, TEXT("ID: %d                   PWNetWork's Send_PlayerInfoPacket"), ID);
	CS_PACKET_PLAYER_INFO packet{};
	packet.m_ePacketType = e_PacketType::e_PlayerInfoPacket;
	packet.m_ID = ID;
	packet.m_ObjInfo = object;
	packet.m_SkillCount = skillcount;
	packet.m_SkillON = skillon;
	packet.m_AmountPaint = amountpaint;
	packet.m_PacketSize = sizeof(CS_PACKET_PLAYER_INFO);
	
	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_PLAYER_INFO);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_PLAYER_INFO));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send PlayerInfo Failed "));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_ShooterFirePacket(bool IsFire, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_SHOOTER_FIRE packet{};
	packet.m_ePacketType = e_PacketType::e_ShooterFire;
	packet.m_ID = ID;
	packet.m_IsFire = IsFire;
	packet.m_PacketSize = sizeof(CS_PACKET_SHOOTER_FIRE);

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_SHOOTER_FIRE);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_SHOOTER_FIRE));
	//PWLOG(Error, TEXT("ID is %d"), ID);
	//PWLOG(Error, packet.m_IsFire ? TEXT("m_IsFire true") : TEXT("m_IsFire false"));
	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send ShooterFire Failed "));
		return false;
	}
	return true;
}
const bool PWNetWork::Send_RollerPaintPacket(bool IsRolling, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_ROLLER_PAINT packet{};
	packet.m_ePacketType = e_PacketType::e_RollerPaint;
	packet.m_ID = ID;
	packet.m_IsRolling = IsRolling;
	packet.m_PacketSize = sizeof(CS_PACKET_ROLLER_PAINT);

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_ROLLER_PAINT);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_ROLLER_PAINT));

	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send RollerPaint Failed "));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_BomberThrowPacket(bool IsThrowing, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_BOBMER_THROW packet{};
	packet.m_ePacketType = e_PacketType::e_BomberThrow;
	packet.m_ID = ID;
	packet.m_IsThrowing = IsThrowing;
	packet.m_PacketSize = sizeof(CS_PACKET_BOBMER_THROW);

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_BOBMER_THROW);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_BOBMER_THROW));

	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send BomberThrow Failed "));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_ReloadInfoPacket(bool IsReloading, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_RELOAD packet{};
	packet.m_ePacketType = e_PacketType::e_Reloading;
	packet.m_ID = ID;
	packet.m_IsReloading = IsReloading;
	packet.m_PacketSize = sizeof(CS_PACKET_RELOAD);

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_RELOAD);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_RELOAD));

	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Reloading Failed "));
		return false;
	}
	return true;
}
const bool PWNetWork::Send_ResultGamePacket(int red, int green, int ID)
{
	if (m_bIsRun == false)
		return false;

	CS_PACKET_GAME_RESULT packet{};
	packet.m_ePacketType = e_PacketType::e_ResultGame;
	packet.m_PacketSize = sizeof(CS_PACKET_GAME_RESULT);
	packet.m_ResultGreenCount = green;
	packet.m_ResultRedCount = red;
	packet.m_ID = ID; //ID가 필요할까

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_GAME_RESULT);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_GAME_RESULT));

	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Result Failed "));
		return false;
	}
	return true;
}

const bool PWNetWork::Send_ChatPacket(FString chat)
{
	if (m_bIsRun == false)
		return false;
	
	auto result = TCHAR_TO_WCHAR(*chat); // 철 : 밥
	
	CS_PACKET_CHAT packet{};
	wcscpy(packet.m_Chat, result);
	packet.m_ePacketType = e_PacketType::e_ChatPacket;
	packet.m_PacketSize = sizeof(packet);

	m_WSASendBuf.buf = m_SendBuf;
	m_WSASendBuf.len = sizeof(CS_PACKET_CHAT);
	memcpy(m_SendBuf, &packet, sizeof(CS_PACKET_CHAT));

	if (Send_Packet() == false)
	{
		//PWLOG(Error, TEXT("Send Result Failed "));
		return false;
	}
	return true;
}