// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Characters/PWShooterCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWShooterCharacter() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWShooterCharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWShooterCharacter();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWShooterCharacter_Fire();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWShooterCharacter_UnAbleFire();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWShooterAnimInstance_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
// End Cross Module References
	void APWShooterCharacter::StaticRegisterNativesAPWShooterCharacter()
	{
		UClass* Class = APWShooterCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Fire", &APWShooterCharacter::execFire },
			{ "UnAbleFire", &APWShooterCharacter::execUnAbleFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWShooterCharacter_Fire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWShooterCharacter_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWShooterCharacter_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWShooterCharacter, nullptr, "Fire", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWShooterCharacter_Fire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWShooterCharacter_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWShooterCharacter_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWShooterCharacter_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWShooterCharacter, nullptr, "UnAbleFire", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWShooterCharacter_UnAbleFire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWShooterCharacter_UnAbleFire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWShooterCharacter_NoRegister()
	{
		return APWShooterCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APWShooterCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShooterAnim_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShooterAnim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GreenParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GreenParticle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RedParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RedParticle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWShooterCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseCharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWShooterCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWShooterCharacter_Fire, "Fire" }, // 3603529771
		{ &Z_Construct_UFunction_APWShooterCharacter_UnAbleFire, "UnAbleFire" }, // 2708065843
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Characters/PWShooterCharacter.h" },
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_ShooterAnim_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_ShooterAnim = { "ShooterAnim", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterCharacter, ShooterAnim), Z_Construct_UClass_UPWShooterAnimInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_ShooterAnim_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_ShooterAnim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_GreenParticle_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_GreenParticle = { "GreenParticle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterCharacter, GreenParticle), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_GreenParticle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_GreenParticle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_RedParticle_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Characters/PWShooterCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_RedParticle = { "RedParticle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterCharacter, RedParticle), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_RedParticle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_RedParticle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWShooterCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_ShooterAnim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_GreenParticle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterCharacter_Statics::NewProp_RedParticle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWShooterCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWShooterCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWShooterCharacter_Statics::ClassParams = {
		&APWShooterCharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWShooterCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWShooterCharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWShooterCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWShooterCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWShooterCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWShooterCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWShooterCharacter, 1294642886);
	template<> PAINTWAR_API UClass* StaticClass<APWShooterCharacter>()
	{
		return APWShooterCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWShooterCharacter(Z_Construct_UClass_APWShooterCharacter, &APWShooterCharacter::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWShooterCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWShooterCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
