// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPlayerController() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPlayerController_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuClose();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuHide();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuOpen();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWPlayerController_EscMenuClose();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
// End Cross Module References
	void APWPlayerController::StaticRegisterNativesAPWPlayerController()
	{
		UClass* Class = APWPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EnterMenuClose", &APWPlayerController::execEnterMenuClose },
			{ "EnterMenuHide", &APWPlayerController::execEnterMenuHide },
			{ "EnterMenuOpen", &APWPlayerController::execEnterMenuOpen },
			{ "EscMenuClose", &APWPlayerController::execEscMenuClose },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWPlayerController, nullptr, "EnterMenuClose", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuClose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWPlayerController_EnterMenuClose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWPlayerController, nullptr, "EnterMenuHide", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuHide()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWPlayerController_EnterMenuHide_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWPlayerController, nullptr, "EnterMenuOpen", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWPlayerController_EnterMenuOpen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWPlayerController_EnterMenuOpen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWPlayerController, nullptr, "EscMenuClose", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWPlayerController_EscMenuClose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWPlayerController_EscMenuClose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWPlayerController_NoRegister()
	{
		return APWPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_APWPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkillCountWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkillCountWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BulletWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BulletWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChatLogWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChatLogWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BloodWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BloodWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EscMenuWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EscMenuWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TabMenuWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TabMenuWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mycontroller_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mycontroller;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWPlayerController_EnterMenuClose, "EnterMenuClose" }, // 1054518466
		{ &Z_Construct_UFunction_APWPlayerController_EnterMenuHide, "EnterMenuHide" }, // 1998683725
		{ &Z_Construct_UFunction_APWPlayerController_EnterMenuOpen, "EnterMenuOpen" }, // 3269508036
		{ &Z_Construct_UFunction_APWPlayerController_EscMenuClose, "EscMenuClose" }, // 3096020786
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PWPlayerController.h" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_SkillCountWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_SkillCountWidget = { "SkillCountWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, SkillCountWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_SkillCountWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_SkillCountWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_BulletWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_BulletWidget = { "BulletWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, BulletWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_BulletWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_BulletWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_ChatLogWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_ChatLogWidget = { "ChatLogWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, ChatLogWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_ChatLogWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_ChatLogWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_BloodWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_BloodWidget = { "BloodWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, BloodWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_BloodWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_BloodWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_EscMenuWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_EscMenuWidget = { "EscMenuWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, EscMenuWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_EscMenuWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_EscMenuWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_TabMenuWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_TabMenuWidget = { "TabMenuWidget", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, TabMenuWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_TabMenuWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_TabMenuWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPlayerController_Statics::NewProp_Mycontroller_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWPlayerController_Statics::NewProp_Mycontroller = { "Mycontroller", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWPlayerController, Mycontroller), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::NewProp_Mycontroller_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::NewProp_Mycontroller_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWPlayerController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_SkillCountWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_BulletWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_ChatLogWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_BloodWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_EscMenuWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_TabMenuWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWPlayerController_Statics::NewProp_Mycontroller,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPlayerController_Statics::ClassParams = {
		&APWPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWPlayerController_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_APWPlayerController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPlayerController, 381789911);
	template<> PAINTWAR_API UClass* StaticClass<APWPlayerController>()
	{
		return APWPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPlayerController(Z_Construct_UClass_APWPlayerController, &APWPlayerController::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
