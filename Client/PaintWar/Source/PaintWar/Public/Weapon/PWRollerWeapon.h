// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PWMeleeWeapon.h"
#include "PWRollerWeapon.generated.h"

UCLASS()
class PAINTWAR_API APWRollerWeapon : public APWMeleeWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWRollerWeapon();

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

	UFUNCTION()
	void CountAmountPaint();

	UFUNCTION()
	void ProcessPaint();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
private:

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* RollerHeadMesh;

	UPROPERTY()
	float BrushSize;

	UPROPERTY()
	class UMaterialInterface* BrushMaterial;

	UPROPERTY()
	class UMaterialInstanceDynamic* BrushMaterialInstance;

	bool bOverlapWithOther;

	UPROPERTY(EditAnywhere, Category = Direction)
	class UArrowComponent* RollerDirection;

	UPROPERTY(EditAnywhere, Category = Material)
		class UMaterialInterface* RollerRed;

	UPROPERTY(EditAnywhere, Category = Material)
	class UMaterialInstanceConstant* RollerRedHead;

	UPROPERTY(EditAnywhere, Category = Material)
		class UMaterialInterface* RollerGreen;

	UPROPERTY(EditAnywhere, Category = Material)
	class UMaterialInstanceConstant* RollerGreenHead;

	UPROPERTY(EditAnywhere, Category = Brush)
	class UTexture2D* BrushTextureRed;

	UPROPERTY(EditAnywhere, Category = Brush)
	class UTexture2D* BrushTextureGreen;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystemComponent* PaintParticleLeft;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystemComponent* PaintParticleRight;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* ParticleRed;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* ParticleGreen;

	float RollerHeadRotateRate;

	UPROPERTY()
	FTimerHandle AmountPaintCountingHandle;

	UPROPERTY()
	FTimerHandle ProcessPaintHandle;

	bool TraceResult;

};
