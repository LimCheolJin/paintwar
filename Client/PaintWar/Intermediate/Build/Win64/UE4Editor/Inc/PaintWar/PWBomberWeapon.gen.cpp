// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Weapon/PWBomberWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWBomberWeapon() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberWeapon_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBomberWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWRangeWeapon();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWBomberWeapon::StaticRegisterNativesAPWBomberWeapon()
	{
	}
	UClass* Z_Construct_UClass_APWBomberWeapon_NoRegister()
	{
		return APWBomberWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APWBomberWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWBomberWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWRangeWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBomberWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/PWBomberWeapon.h" },
		{ "ModuleRelativePath", "Public/Weapon/PWBomberWeapon.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWBomberWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWBomberWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWBomberWeapon_Statics::ClassParams = {
		&APWBomberWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWBomberWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWBomberWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWBomberWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWBomberWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWBomberWeapon, 2715253603);
	template<> PAINTWAR_API UClass* StaticClass<APWBomberWeapon>()
	{
		return APWBomberWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWBomberWeapon(Z_Construct_UClass_APWBomberWeapon, &APWBomberWeapon::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWBomberWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWBomberWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
