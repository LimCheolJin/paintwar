// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWGameTimeManager.generated.h"

UCLASS()
class PAINTWAR_API APWGameTimeManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWGameTimeManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	int CountDown;

	UPROPERTY()
	FTimerHandle CountDownHandle;

	
	class PWGameEndThread* GameEndThread;

	/*long long TotalRedTeamPaint;
	long long TotalGreenTeamPaint;*/

	/*UPROPERTY()
	TArray<ETeam> TotalGameResult;*/
	long long GreenResultCount;
	long long RedResultCount;

	bool IsGameEnd;
	bool IsResultCompleteEnd;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void ShowCountDown();

	FCriticalSection ResultMutex;


	UFUNCTION(BlueprintCallable)
	int GetCount();

	void SetCount(int count);
	void SetIsGameEnd(bool);
	bool GetIsGameEnd()const;
	void GameEndProcess();
	void SetIsResultCompleteEnd(bool);

};
