// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBomberProjectile.h"
#include "Engine/Engine.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PWPaintCanvas.h"
#include "DrawDebugHelpers.h"
#include "PWBaseCharacter.h"
#include "PWPaintLeft_1st.h"
#include "PWPaintLeft_2nd.h"
#include "PWPaintLeft_3rd.h"
#include "PWPaintLeft_4th.h"
#include "PWPaintLeft_5th.h"
#include "PWPaintLeft_6th.h"
#include "PWPaintLeft_Bridge1.h"
#include "PWPaintLeft_Bridge2.h"
#include "PWPaintLeft_Bridge3.h"
#include "PWPaintLeft_Bridge4.h"
#include "PWPaintLeft_Bridge5.h"
#include "PWPaintRight_1st.h"
#include "PWPaintRight_2nd.h"
#include "PWPaintRight_3rd.h"
#include "PWPaintRight_4th.h"
#include "PWPaintRight_5th.h"
#include "PWPaintRight_6th.h"
#include "PWPaintRight_Bridge1.h"
#include "PWPaintRight_Bridge2.h"
#include "PWPaintRight_Bridge3.h"
#include "PWPaintRight_Bridge4.h"
#include "PWPaintRight_Bridge5.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "PWBomberCharacter.h"



APWBomberProjectile::APWBomberProjectile()
{
	CollisionSphere->InitSphereRadius(12.0f); // ���ͷΕ��� //StaticMesh'/Game/TestAssets/TestWeapon/Bomb/bomb_200324.bomb_200324'

	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_PROJECTILE(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_PROJECTILE(TEXT("/Game/TestAssets/TestWeapon/Bomb/bomb_200324.bomb_200324"));
	if (ST_PROJECTILE.Succeeded())
	{
		ProjectileMesh->SetStaticMesh(ST_PROJECTILE.Object);
	}

	ProjectileMovementComponent->SetUpdatedComponent(CollisionSphere); 
	ProjectileMovementComponent->InitialSpeed = 500.0f;  //���� 300
	ProjectileMovementComponent->MaxSpeed = 500.0f;//���� 300
	ProjectileMovementComponent->bRotationFollowsVelocity = true;//true; 
	ProjectileMovementComponent->bShouldBounce = true;
	ProjectileMovementComponent->Bounciness = 0.7f; // ���� 0.3f
	ProjectileMovementComponent->bIsSliding = true;
	ProjectileMovementComponent->ProjectileGravityScale = 0.5f;
	ProjectileMesh->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	ProjectileMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -1.0f));

	
	
	CollisionSphere->BodyInstance.SetCollisionProfileName(TEXT("BomberProjectile"));

	//Texture2D'/Game/TestAssets/TestFloorPaint/T_BluePaint.T_BluePaint'
	//Texture2D'/Game/TestAssets/TestFloorPaint/heightMapTest/RedSplat2.RedSplat2'


	//Material'/Game/TestAssets/TestWeapon/Bomb/M_BombRed.M_BombRed'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_BombMeshRed(TEXT("/Game/TestAssets/TestWeapon/Bomb/M_BombRed.M_BombRed"));
	if (M_BombMeshRed.Succeeded())//
	{
		ProjectileMeshRedMaterial = M_BombMeshRed.Object;
	}

	//Material'/Game/TestAssets/TestWeapon/Bomb/M_BombGreen.M_BombGreen'
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_BombMeshGreen(TEXT("/Game/TestAssets/TestWeapon/Bomb/M_BombGreen.M_BombGreen"));
	if (M_BombMeshGreen.Succeeded())
	{
		ProjectileMeshGreenMaterial = M_BombMeshGreen.Object;
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> T_BomberBrushRed(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/RedSplat2.RedSplat2"));
	if (T_BomberBrushRed.Succeeded())
	{
		BrushTextureRed = T_BomberBrushRed.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	//Texture2D'/Game/TestAssets/TestFloorPaint/heightMapTest/GreenSplat2.GreenSplat2'
	static ConstructorHelpers::FObjectFinder<UTexture2D> T_BomberBrushGreen(TEXT("/Game/TestAssets/TestFloorPaint/heightMapTest/GreenSplat2.GreenSplat2"));
	if (T_BomberBrushGreen.Succeeded())
	{
		BrushTextureGreen = T_BomberBrushGreen.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	//��ƼŬ
	static ConstructorHelpers::FObjectFinder<UParticleSystem> P_PaintRedParticle(TEXT("/Game/Particles/P_SplashRed.P_SplashRed"));
	if (P_PaintRedParticle.Succeeded())
	{
		//ProjectileParticle->SetTemplate(P_PaintRedParticle.Object);
		ParticleRed = P_PaintRedParticle.Object;
	}
	static ConstructorHelpers::FObjectFinder<UParticleSystem> P_PaintGreenParticle(TEXT("/Game/Particles/P_SplashGreen.P_SplashGreen"));
	if (P_PaintGreenParticle.Succeeded())
	{
		//ProjectileParticle->SetTemplate(P_PaintRedParticle.Object);
		ParticleGreen = P_PaintGreenParticle.Object;
	}
	ProjectileParticle->SetupAttachment(RootComponent);
	ProjectileParticle->bAutoActivate = false;
	//

	//BrushSize = 1000.0f;
	BrushSize = 500.0f;
	Damage = 50.0f;

	ExplosionRangeBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BomberProjectileExplosionRange"));
	ExplosionRangeBox->SetupAttachment(RootComponent);
	ExplosionRangeBox->SetBoxExtent(FVector(20.0f, 20.0f, 10.0f));
	ExplosionRangeBox->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	ExplosionRangeBox->bHiddenInGame = false;
	ExplosionRangeBox->SetCollisionProfileName("OverlapAll");
	ExplosionRangeBox->SetGenerateOverlapEvents(true);
	
	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSoundObject(TEXT("/Game/Sound/BomberAttackSound.BomberAttackSound"));
	if (AttackSoundObject.Succeeded())
	{
		AttackSound = AttackSoundObject.Object;

		AttackSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AttackSoundComponent"));
		AttackSoundComponent->SetupAttachment(RootComponent);

		AttackSoundComponent->SetSound(AttackSound);
	}
}

void APWBomberProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{

	/*FVector tempSpeed = this->GetVelocity();

	PWLOG(Warning, TEXT("SPEED size: %f"), tempSpeed.Size());*/
	//PWLOG(Warning, TEXT("Other Actor's name is %s "), *OtherActor->GetName());
	const auto& Owner = Cast<APWBaseCharacter>(GetOwner());
	
	if (Cast<APWPaintCanvas>(OtherActor) != nullptr)
	{
		
		//if( UKismetSystemLibrary::LineTraceSingle(this, TraceStart, TraceEnd, ETraceTypeQuery::TraceTypeQuery1, true, TempArr, EDrawDebugTrace::Persistent, HitResult, true)
		if (CurrentProjectileSpeedSize <= 10.0f) // 30.0f)
		{
			ProjectileParticle->SetActive(true);
			this->SetLifeSpan(0.6f);
			GetWorld()->GetTimerManager().SetTimer(DestoyEventTimerHandle, this, &APWBomberProjectile::HideProjectile, 0.05f, false);
			
			IsDestroy = true;
			
			//SetActorRotation(FRotator(0.0f, 0.0f, 0.0f), ETeleportType::TeleportPhysics);
			/*TSet<AActor*> OverlappedActors;
			TSubclassOf<AActor> FilterClass;
			ExplosionRangeBox->GetOverlappingActors(OverlappedActors, FilterClass);
			for (const auto& actor : OverlappedActors)
			{
				const auto& BaseCharacter = Cast<APWBaseCharacter>(actor);
				PWLOG(Warning, TEXT("Overlapping Actor name is %s "), *actor->GetName());
				if (BaseCharacter != nullptr &&  (BaseCharacter->GetTeam() != Owner->GetTeam() ))
				{
					BaseCharacter->TakeDamage(this->Damage);
				}
			}*/


			//FVector TraceStart;
			//FVector TraceEnd;
			//FHitResult HitResult;

			//TraceStart = GetActorLocation();
			//TraceEnd = GetActorLocation() + (GetActorUpVector() *  -10.0f);

			//FCollisionQueryParams CollisionQueryParams;
			//CollisionQueryParams.bTraceComplex = true;
			//CollisionQueryParams.AddIgnoredActor(this);
			//CollisionQueryParams.bReturnFaceIndex = true;

			//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, true);
			//if ((GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams)))
			//{
			//	FVector2D UV{ 0.0f, 0.0f };

			//	//GetActor�� �̸�����
			//	auto HitActor = HitResult.GetActor();

			//	if (Cast<APWPaintCanvas>(HitActor))
			//	{

			//		//���⼭ Ʈ������ֱ�
			//		bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);
			//		if (bFindUV)
			//		{
			//			PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
			//			Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
			//		}
			//	}

			//}
			//else
			//{
			//	PWLOG(Warning, TEXT("failed Line Trace"));
			//}
			//Destroy();
		}
		
	}
	else
	{
		//Destroy();
	}
	//PWLOG(Warning, TEXT("OnHit Event"));
	
}

void APWBomberProjectile::BeginPlay()
{
	APWBaseProjectile::BeginPlay();
	
	AttackSoundComponent->Stop();
	
	//CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APWBomberProjectile::OnOverlapBegin);
	CollisionSphere->OnComponentHit.AddDynamic(this, &APWBomberProjectile::OnHit);
	//this->SetLifeSpan(2.0f);
	this->SetLifeSpan(10.0f);
	ExplosionRangeBox->OnComponentBeginOverlap.AddDynamic(this, &APWBomberProjectile::OnOverlapBegin);
	
	const auto& BaseCharacter = Cast<APWBaseCharacter>(GetOwner());
	auto TeamColor = BaseCharacter->GetTeam();
	if (TeamColor == ETeam::RED)
	{
		ProjectileMesh->SetMaterial(0, ProjectileMeshRedMaterial);
		ProjectileParticle->SetTemplate(ParticleRed);
	}
	else if (TeamColor == ETeam::GREEN)
	{
		ProjectileMesh->SetMaterial(0, ProjectileMeshGreenMaterial);
		ProjectileParticle->SetTemplate(ParticleGreen);
	}
	
	//ExplosionRangeBox->OnComponentEndOverlap.AddDynamic(this, &APWBomberProjectile::OnOverlapEnd);
	//this->OnDestroyed.AddDynamic(this, &APWBomberProjectile::PaintProcess);

	
}

void APWBomberProjectile::HideProjectile()
{
	AttackSoundComponent->Play();
	ProjectileMesh->SetHiddenInGame(true);

}

void APWBomberProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
}



void APWBomberProjectile::Tick(float DeltaTime)
{
	APWBaseProjectile::Tick(DeltaTime);
	
	CurrentProjectileSpeedSize = this->GetVelocity().Size();
	/*if (CurrentProjectileSpeedSize <= 10.0f)
		Destroy();*/
	
}

//void APWBomberProjectile::BeginDestroy()
//{
//	PWLOG(Warning, TEXT("BeginDestroy"));
//	Super::BeginDestroy();
//}

void APWBomberProjectile::PaintProcess()
{
	
	if (IsDestroy)
	{

		//ProjectileParticle->SetActive(true);
		SetActorRotation(FRotator(0.0f, 0.0f, 0.0f), ETeleportType::TeleportPhysics);

		const auto& Owner = Cast<APWBaseCharacter>(GetOwner());
		TSet<AActor*> OverlappedActors;
		TSubclassOf<AActor> FilterClass;
		ExplosionRangeBox->GetOverlappingActors(OverlappedActors, FilterClass);
		for (const auto& actor : OverlappedActors)
		{
			const auto& BaseCharacter = Cast<APWBaseCharacter>(actor);
			//PWLOG(Warning, TEXT("Overlapping Actor name is %s "), *actor->GetName());
			if (BaseCharacter != nullptr && (BaseCharacter->GetTeam() != Owner->GetTeam()))
			{
				BaseCharacter->TakenDamage(Cast<APWBaseCharacter>(this->GetOwner()), this->Damage);
			}
		}

		

		FVector TraceStart;
		FVector TraceEnd;
		FHitResult HitResult;

		TraceStart = GetActorLocation();
		TraceEnd = GetActorLocation() + (GetActorUpVector() *  -15.0f);

		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.bTraceComplex = true;
		CollisionQueryParams.AddIgnoredActor(this);
		CollisionQueryParams.bReturnFaceIndex = true;

		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, true);
		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams);
		//if ((GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams)))
		if(Cast<APWPaintCanvas>(HitResult.GetActor()) != nullptr)
		{
			FVector2D UV{ 0.0f, 0.0f };

			//GetActor�� �̸�����
			auto HitActor = HitResult.GetActor();
			//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("paint canvas overlapped - %d"), Cast<APWBomberCharacter>(GetOwner())->Get_m_ID(), false));
			if (Cast<APWPaintLeft_1st>(HitActor)) // �̺κ� if - else if �� 22���� ��� ��������.. ..............
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_1st>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_2nd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintRight_3rd>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}
			else if (Cast<APWPaintLeft_4th>(HitActor))
			{
				bool bFindUV = UGameplayStatics::FindCollisionUV(HitResult, 0, UV);

				if (bFindUV)
				{
					//PWLOG(Warning, TEXT("UV (%f, %f) "), UV.X, UV.Y);
					Cast<APWPaintCanvas>(HitActor)->DrawBrush(UV, BrushSize, BrushMaterialInstance);
				}
			}

			//ProjectileParticle->SetActive(false);
			//ProjectileMesh->SetHiddenInGame(true);
		}
		
	}
}

void APWBomberProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	
	PaintProcess();

	
	//ProjectileParticle->SetActive(false);
	Super::EndPlay(EndPlayReason); 
}


void APWBomberProjectile::Skill()
{
	BrushSize = 1200.0f;
	ProjectileParticle->SetRelativeScale3D(FVector(1.5f, 1.5f,1.5f));
	ProjectileMesh->SetRelativeScale3D(FVector(0.35f, 0.35f, 0.35f));
	CollisionSphere->InitSphereRadius(48.0f);
	ExplosionRangeBox->SetBoxExtent(FVector(50.0f, 50.0f, 50.0f));
}