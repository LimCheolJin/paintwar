// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_5th.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_5th : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintLeft_5th();
};
