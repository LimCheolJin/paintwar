// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBomberWeapon_generated_h
#error "PWBomberWeapon.generated.h already included, missing '#pragma once' in PWBomberWeapon.h"
#endif
#define PAINTWAR_PWBomberWeapon_generated_h

#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWBomberWeapon(); \
	friend struct Z_Construct_UClass_APWBomberWeapon_Statics; \
public: \
	DECLARE_CLASS(APWBomberWeapon, APWRangeWeapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAPWBomberWeapon(); \
	friend struct Z_Construct_UClass_APWBomberWeapon_Statics; \
public: \
	DECLARE_CLASS(APWBomberWeapon, APWRangeWeapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWBomberWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWBomberWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWBomberWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberWeapon(APWBomberWeapon&&); \
	NO_API APWBomberWeapon(const APWBomberWeapon&); \
public:


#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWBomberWeapon(APWBomberWeapon&&); \
	NO_API APWBomberWeapon(const APWBomberWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWBomberWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWBomberWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWBomberWeapon)


#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_8_PROLOG
#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_INCLASS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWBomberWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Weapon_PWBomberWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
