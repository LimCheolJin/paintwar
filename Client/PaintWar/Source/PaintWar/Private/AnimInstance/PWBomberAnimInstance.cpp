// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBomberAnimInstance.h"
#include "PWBomberCharacter.h"


UPWBomberAnimInstance::UPWBomberAnimInstance()
{
	IsFire = false;
}

void UPWBomberAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	UPWAnimInstance::NativeUpdateAnimation(DeltaSeconds);
	auto BomberCharacter = Cast<APWBomberCharacter>(TryGetPawnOwner());

	if (BomberCharacter != nullptr)
	{
		IsFire = BomberCharacter->GetIsFire();

		/*if (!Montage_IsPlaying(AnimMontage))
		{
			if (IsFire)
			{
				Montage_Play(AnimMontage);
			}
		}*/
	}
}

void UPWBomberAnimInstance::AnimNotify_FireNotify()
{
	OnFireNotify.Broadcast();
}

void UPWBomberAnimInstance::AnimNotify_ReloadNotify()
{
	OnReloadNotify.Broadcast();
}