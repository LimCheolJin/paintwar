#pragma once
#include "PaintWar.h"

#define MAX_ID_LEN 30


enum class e_PacketType : uint8_t
{
	e_PlayerInfoPacket,
	e_SelectTeamPacket,
	e_SelectCharacterTypePacket,
	e_LogInPacket,
	e_LoginOKPacket,
	e_EnterPacket,
	e_LeavePacket,
	e_ReadyPacket,
	e_StartGamePacket,
	e_PlayerMovePacket,
	e_ShooterFire,
	e_RollerPaint,
	e_BomberThrow,
	e_Reloading,
	e_GameTimer,
	e_EndGamePacket,
	e_ResultGame,
	e_ChatPacket
};
//enum class e_PlayerTeamType : unsigned char
//{
//	e_None,
//	e_Red,
//	e_Green
//};
//enum class e_PlayerCharacterType : unsigned char
//{
//	e_None,
//	e_Shooter,
//	e_Roller,
//	e_Bomber
//};

enum class e_GameStateInfo : uint8_t
{
	e_None,
	e_Ready,
	e_UnReady
};

//클라:처음에 게임에 로그인을 한다.(start scene, 로그인 버튼을 누르면)
//서버: 로그인이 확인되었으면 클라에게 로그인 패킷을 SEND한다. 
//클라: 로그인패킷을 RECV한다. 
//클라: 플레이모드를 선택하는데 여기서 싱글모드를 선택하면 network->DisConnect()호출
//클라: 플레이모드를 멀티모드를 선택하면 네트워크를 지속한다.
//클라: (방에 대한 것은 생략 일단. 더 생각해봐야할거같음.)
//클라: 팀, 캐릭터타입 각각을 선택버튼을 누르면 서버에게 각각의 패킷을 SEND한다.
//서버: RECV하고 팀이면 팀, 캐릭터면 캐릭터를 다른 클라이언트에게 SEND한다.
//클라: 팀이면 팀, 캐릭터면 캐릭터에 따라 로비scene에서 각각 다른 캐릭터또는 이미지를 id에 맞게 띄운다.
//클라: READY버튼을 누르면 READY패킷을 SEND한다.
//서버: READY를 RECV한다. 모든 클라이언트가 READY상태인지 확인한다.
//서버: 맞으면 모든클라이언트들에게 게임을 시작하라는 상태를 보낸다. 
//서버: 시작하라는 상태를 보냄과 동시에 GameTime패킷을 보낸다.. 이는 1초에 한번씩 클라이언트에게 send해주어야한다.
//서버: 아니면 클라이언트들에게 모두 레디를 수신할떄까지 대기. 
//클라: 게임을 시작하라는 상태를 RECV하고 GameTime을 RECV한다.
//클라:OpenLevel("TestLevel")을 하고 게임을시작. 모든 6명의 캐릭터 스폰->각각 플레이어컨트롤러 적용.
//클라: 현재 플레이어의 정보를 60프레임마다 SEND, 
//서버: 현재 플레이어의 정보를 RECV, send한 플레이어를 제외하고 다른플레이어들에게 send한 플레이어의 정보를 send
//클라: 다른플레이어들의 정보를recv,  패킷 처리 후 적용
#pragma pack(push, 1)
struct ObjectPosition
{
	float x;
	float y;
	float z;
};
struct ObjectRotation
{
	float Pitch;
	float Yaw;
	float Roll;
};
struct ObjectVelocity
{
	float vx;
	float vy;
	float vz;
};
struct ObjectInfo
{
	ObjectPosition m_ObjectPosition;
	ObjectRotation m_ObjectRotation;
	ObjectVelocity m_ObjectVelocity;
};

struct CS_PACKET_READY
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	e_GameStateInfo m_PlayerState;
};


struct CS_PACKET_SELECT_TEAM
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ETeam  m_eTeamType;
};

struct CS_PACKET_LOGIN
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	char m_LoginID[MAX_ID_LEN];
	// id , 비밀번호 추가하기.
};

struct CS_PACKET_SELECT_CHARACTERTYPE
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ESelectCharacter  m_eCharacterType;
};
struct CS_PACKET_PLAYER_INFO
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ObjectInfo m_ObjInfo;
	int m_SkillCount;
	bool m_SkillON;
	int m_AmountPaint;
};

struct CS_PACKET_SHOOTER_FIRE
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsFire;
};
struct CS_PACKET_ROLLER_PAINT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsRolling;
};
struct CS_PACKET_BOBMER_THROW
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsThrowing;
};
struct CS_PACKET_RELOAD
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsReloading;
};
struct CS_PACKET_GAME_RESULT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	int m_ResultRedCount;
	int m_ResultGreenCount;
};

struct SC_PACKET_SELECT_TEAM
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ETeam  m_eTeamType;
};


struct SC_PACKET_SELECT_CHARACTERTYPE
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ESelectCharacter  m_eCharacterType;
};

struct SC_PACKET_LOGIN_OK
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsRoomHead = false;
	//클라이언트에게 보낼 초기정보들 써주기.
};

struct SC_PACKET_ENTER //다른플레이어 의 입장.
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsRoomHead;
	char m_LoginID[MAX_ID_LEN];
};
struct SC_PACKET_LEAVE // 다른플레이어의 탈출.
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
};

struct SC_PACKET_START_GAME
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	bool m_StartGame;
};
struct SC_PACKET_PLAYER_MOVE
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	ObjectInfo m_ObjInfo;
	bool m_SkillON;
	int m_SkillCount;
	int m_AmountPaint;
};

struct SC_PACKET_SHOOTER_FIRE
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsFire;
};

struct SC_PACKET_ROLLER_PAINT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsRolling;
};
struct SC_PACKET_BOBMER_THROW
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsThrowing;
};
struct SC_PACKET_RELOAD
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_IsReloading;
};
struct SC_PACKET_GAME_TIMER
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_TimerCount;
};
struct SC_PACKET_GAME_END
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ID;
	bool m_Resulter;
};
struct SC_PACKET_GAME_RESULT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	int m_ResultRedCount;
	int m_ResultGreenCount;
};
struct CS_PACKET_CHAT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	wchar_t m_Chat[32];
};
struct SC_PACKET_CHAT
{
	char m_PacketSize;
	e_PacketType m_ePacketType;
	wchar_t m_Chat[32];
};


//로그인시 게임인스턴스에 저장하기위함. map을 사용하기위함.
struct PlayerInfo
{
	int m_ID;
	char Login_ID[MAX_ID_LEN];
	ETeam  m_eTeamType = ETeam::NONE;
	ESelectCharacter  m_eCharacterType = ESelectCharacter::NONE;
	bool  m_IsRoomHead = false;
	e_GameStateInfo m_PlayerState = e_GameStateInfo::e_None;
	ObjectInfo ObjInfo;
	bool m_IsResulter = false;
	int m_SkillCount=0;
	bool m_SKillON = false;

	PlayerInfo() {} //: m_eTeamType(e_PlayerTeamType::e_Red), m_eCharacterType(e_PlayerCharacterType::e_Bomber){}
};

struct ObjectInformations
{
	ObjectInfo ObjInfo;
	bool IsFire;
	bool IsRolling;
	bool IsThrowing;
	bool IsReloading;
	ESelectCharacter CharacterType;
	int m_SkillCount ;
	bool m_SKillON ;
	int m_AmountPaint;

};





#pragma pack(pop)
