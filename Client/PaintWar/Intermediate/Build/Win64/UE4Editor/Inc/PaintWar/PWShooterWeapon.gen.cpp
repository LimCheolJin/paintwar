// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Weapon/PWShooterWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWShooterWeapon() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWShooterWeapon_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWShooterWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWRangeWeapon();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void APWShooterWeapon::StaticRegisterNativesAPWShooterWeapon()
	{
	}
	UClass* Z_Construct_UClass_APWShooterWeapon_NoRegister()
	{
		return APWShooterWeapon::StaticClass();
	}
	struct Z_Construct_UClass_APWShooterWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShooterProjectileParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShooterProjectileParticle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShooterGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShooterGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShooterRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShooterRed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWShooterWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWRangeWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Weapon/PWShooterWeapon.h" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterProjectileParticle_MetaData[] = {
		{ "Category", "Particle" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterProjectileParticle = { "ShooterProjectileParticle", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterWeapon, ShooterProjectileParticle), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterProjectileParticle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterProjectileParticle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleGreen_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleGreen = { "ParticleGreen", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterWeapon, ParticleGreen), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleGreen_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleRed_MetaData[] = {
		{ "Category", "Particle" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleRed = { "ParticleRed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterWeapon, ParticleRed), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleRed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterGreen_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterGreen = { "ShooterGreen", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterWeapon, ShooterGreen), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterGreen_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterRed_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/Weapon/PWShooterWeapon.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterRed = { "ShooterRed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWShooterWeapon, ShooterRed), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterRed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterRed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWShooterWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterProjectileParticle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ParticleRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWShooterWeapon_Statics::NewProp_ShooterRed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWShooterWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWShooterWeapon>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWShooterWeapon_Statics::ClassParams = {
		&APWShooterWeapon::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APWShooterWeapon_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWShooterWeapon_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWShooterWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWShooterWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWShooterWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWShooterWeapon, 3900014416);
	template<> PAINTWAR_API UClass* StaticClass<APWShooterWeapon>()
	{
		return APWShooterWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWShooterWeapon(Z_Construct_UClass_APWShooterWeapon, &APWShooterWeapon::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWShooterWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWShooterWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
