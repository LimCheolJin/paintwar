// Fill out your copyright notice in the Description page of Project Settings.


#include "PWJumpPad.h"
#include "PWBaseCharacter.h"

// Sets default values
APWJumpPad::APWJumpPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;;
	JumpPadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorMesh"));

	//StaticMesh'/Game/TestAssets/testmap/Obstalces/jump_200321.jump_200321'
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_Floor(TEXT("/Game/TestAssets/testmap/Obstalces/jump_200321.jump_200321"));
	if (ST_Floor.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{
		JumpPadMesh->SetStaticMesh(ST_Floor.Object);
	}

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	RootComponent = JumpPadMesh;
	CollisionBox->SetupAttachment(RootComponent);
	//CollisionBox->SetBoxExtent(FVector(50.0f, 50.0f, 32.0f));
	CollisionBox->SetBoxExtent(FVector(13.5f, 13.5f, 4.0f));
	CollisionBox->bHiddenInGame = false;

	JumpPadMesh->SetCollisionProfileName(TEXT("NoCollision"));
	CollisionBox->SetCollisionProfileName(TEXT("OverlapAll"));
	JumpHeight = 900.0f;

	//CollisionBox->SetRelativeScale3D(FVector(0.5f, 0.5f, 1.0f));
}

// Called when the game starts or when spawned
void APWJumpPad::BeginPlay()
{
	Super::BeginPlay();
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &APWJumpPad::BeginOverlap);
}

// Called every frame
void APWJumpPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void APWJumpPad::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Character = Cast<APWBaseCharacter>(OtherActor);

	if (Character != nullptr)
	{
		Character->LaunchCharacter(FVector(0.0f,0.0f, JumpHeight), false, false);
	}
}
