// Fill out your copyright notice in the Description page of Project Settings.


#include "PWConveyorBelt.h"
#include "Components/ArrowComponent.h"
#include "PWBaseCharacter.h"
//#include "Components/PrimitiveComponent.h"
// Sets default values
APWConveyorBelt::APWConveyorBelt()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ConveyorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	ConveyorDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	//StaticMesh'/Game/TestAssets/testmap/Obstalces/speed_200321.speed_200321'
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_Belt(TEXT("/Game/TestAssets/testmap/Obstalces/speed_200321.speed_200321"));
	if (ST_Belt.Succeeded())
	{
		ConveyorMesh->SetStaticMesh(ST_Belt.Object);
	}
	/*RootComponent = ConveyorMesh;
	CollisionBox->SetupAttachment(RootComponent);
	ConveyorDirection->SetupAttachment(RootComponent);*/

	RootComponent = CollisionBox;
	ConveyorMesh->SetupAttachment(RootComponent);
	CollisionBox->SetBoxExtent(FVector(30.0f, 13.0f, 4.0f));
	ConveyorDirection->SetupAttachment(RootComponent);

	ConveyorMesh->SetRelativeLocation(FVector(-57.0f, 309.0f, -2.5f));
	ConveyorMesh->SetCollisionProfileName(TEXT("NoCollision"));
	CollisionBox->SetCollisionProfileName(TEXT("OverlapAll"));
	//CollisionBox->SetRelativeScale3D(FVector(1.55f, 1.55f, 1.0f));
	//ConveyorDirection->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	ConveyorDirection->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));
	CollisionBox->bHiddenInGame = false;
	Speed = 80.0f;
	
}

// Called when the game starts or when spawned
void APWConveyorBelt::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APWConveyorBelt::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TSet<AActor*> OverlappingSet;
	CollisionBox->GetOverlappingActors(OverlappingSet);

	for (auto& actor : OverlappingSet)
	{
		if (Cast<APWBaseCharacter>(actor) != nullptr)
		{
			FVector ActorRightVector = -1 * GetActorRightVector();
			auto DeltaLocation = ActorRightVector * (Speed * DeltaTime);
			actor->AddActorWorldOffset(DeltaLocation);
		}
	}
	
}

