// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWBaseProjectile.generated.h"

UCLASS()
class PAINTWAR_API APWBaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APWBaseProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnywhere, Category = Projectile)
	USphereComponent* CollisionSphere;

	UPROPERTY(VisibleAnywhere, Category = Movement)
	class UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(VisibleAnywhere, Category = Projectile)
	UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(EditAnywhere, Category = Paint)
	float BrushSize;

	UPROPERTY(BlueprintReadWrite, Category = Canvas)
	class UMaterialInterface* BrushMaterial;

	UPROPERTY()
	class UMaterialInstanceDynamic* BrushMaterialInstance;

	UPROPERTY(BlueprintReadWrite, Category = Canvas)
	class UTexture2D* BrushTextureRed;

	UPROPERTY(BlueprintReadWrite, Category = Canvas)
	class UTexture2D* BrushTextureGreen;

	float Damage;

	UPROPERTY(EditAnywhere, Category = Material)
		class UMaterialInterface* ProjectileMeshGreenMaterial;
	//class UMaterialInstanceConstant* ProjectileMeshGreenMaterial;

	UPROPERTY(EditAnywhere, Category = Material)
		class UMaterialInterface* ProjectileMeshRedMaterial;
	//class UMaterialInstanceConstant* ProjectileMeshRedMaterial;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystemComponent* ProjectileParticle;

public:
	void FireInDirection(const FVector& ShootDirection);
	
	class USphereComponent* GetSphereCollision() const;

	class UProjectileMovementComponent* GetProjectileMovementComponent() const;

};
