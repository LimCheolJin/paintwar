// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBomberCharacter.h"
#include "PWBomberWeapon.h"
#include "PWBomberProjectile.h"
#include "PWSpawnPoint.h"
#include "Materials/MaterialInstanceConstant.h"
#include "Components/ArrowComponent.h"
#include "PWBomberAnimInstance.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PWGameInstance.h"
#include "PWBomberAIController.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "PWGameMode.h"

APWBomberCharacter::APWBomberCharacter()
{
	//AnimBlueprint'/Game/SpaceExplorer/Anims/BP_BomberCharacterAnim.BP_BomberCharacterAnim'
	static ConstructorHelpers::FClassFinder<UAnimInstance> BOMBER_ANIM(TEXT("/Game/SpaceExplorer/Anims/BP_BomberCharacterAnim.BP_BomberCharacterAnim_C"));
	if (BOMBER_ANIM.Succeeded())//AnimBlueprint'/Game/SpaceExplorer/Anims/BP_DefaultAnim.BP_DefaultAnim'
	{
		GetMesh()->SetAnimInstanceClass(BOMBER_ANIM.Class);
	}

	//static ConstructorHelpers::FObjectFinder<USoundCue> AttacSound()


	IsFire = false;
	CharacterType = ESelectCharacter::BOMBER;
	AmountPaint = 100.0f;

	AIControllerClass = APWBomberAIController::StaticClass();
	//AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSoundObject(TEXT("/Game/Sound/BomberShootSound.BomberShootSound"));
	if (AttackSoundObject.Succeeded())
	{
		AttackSound = AttackSoundObject.Object;

		AttackSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AttackSoundComponent"));
		AttackSoundComponent->SetupAttachment(RootComponent);

		AttackSoundComponent->SetSound(AttackSound);
	}
}

void APWBomberCharacter::BeginPlay()
{
	//APWBaseCharacter::BeginPlay();
	Super::BeginPlay();
	PWLOG(Error, TEXT("Bomber BeginPlay Start"));
	AttackSoundComponent->Stop();

	//스폰포인트를 가져오기위함인데 좋은방법이아닌거같음.
	TSubclassOf<AActor> ClassParam = APWSpawnPoint::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassParam, SpawnPointArray);
	if (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::SINGLE)
	{
		AIControllerClass = APWBomberAIController::StaticClass();
		AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	}

	if (!GetName().Compare(FString("RedBomber")))   //같으면 0을반환함.
	{
		SetTeam(ETeam::RED);
		PWLOG(Warning, TEXT("RED"));
		GetMesh()->SetMaterial(0, RedTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if (SpawnPoint->GetTeamColor() == ETeam::RED)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}
	else if (!GetName().Compare(FString("GreenBomber")))
	{
		SetTeam(ETeam::GREEN);
		PWLOG(Warning, TEXT("GREEN"));
		GetMesh()->SetMaterial(0, GreenTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if (SpawnPoint->GetTeamColor() == ETeam::GREEN)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}

	FName WeaponSocket(TEXT("hand_rSocketBomber"));

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	Weapon = GetWorld()->SpawnActor<APWBomberWeapon>(FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (nullptr != Weapon)
	{
		Weapon->SetOwner(this);
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	}

	//애님인스턴스 받아오기
	BomberAnim = Cast<UPWBomberAnimInstance>(GetMesh()->GetAnimInstance());
	if (BomberAnim != nullptr)
	{
		BomberAnim->OnFireNotify.AddUObject(this, &APWBomberCharacter::SpawnBomberProjectile);
		BomberAnim->OnReloadNotify.AddUObject(this, &APWBaseCharacter::ReloadProcess);
	}

}

void APWBomberCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APWBaseCharacter::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &APWBomberCharacter::Fire);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &APWBomberCharacter::UnAbleFire);
}

void APWBomberCharacter::Fire()
{
	//PWLOG(Warning, TEXT("ShooterFire!!!!!!"));
	//auto ProjectileSpawnPointInstance = Cast<APWBomberWeapon>(Weapon)->GetProjectileSpawnPoint();
	//auto TempTransform = ProjectileSpawnPointInstance->GetComponentTransform();
	//FVector ProjectileLocation = TempTransform.GetLocation();
	//FRotator ProjectileRotation = TempTransform.GetRotation().Rotator();
	//FActorSpawnParameters SpawnParams;
	//SpawnParams.Owner = this; // 일단무기로 해놧음 예제는 무기가아니고 캐릭터임
	//SpawnParams.Instigator = Instigator;

	////auto SockLocation = GetMesh()->GetSocketLocation(FName("hand_rSocketBomber"));
	////auto SockRotation = GetMesh()->GetSocketRotation(FName("hand_rSocketBomber"));
	//Projectile = GetWorld()->SpawnActor<APWBomberProjectile>(ProjectileLocation, ProjectileRotation, SpawnParams);//(GetArrowComponent()->GetComponentLocation(), GetArrowComponent()->GetComponentRotation(), SpawnParams); 
	
	//Projectile->GetProjectileMovementComponent()->SetVelocityInLocalSpace(FVector(0.0f,0.0f,0.0f));
	//Projectile->GetProjectileMovementComponent()->ProjectileGravityScale = 0.0f;

	//Projectile->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale,FName("hand_rSocketBomber"));
	//if (Projectile)
	//{
	//	//하드코딩했음 이거나중에 고칩시다.
	//	Projectile->SetOwner(this);
	//	PWLOG(Warning, TEXT("ShooterFire Playing"))
	//	FVector Direction = ProjectileRotation.Vector();
	//	Projectile->FireInDirection(Direction);
	//}
	//else
	//{
	//	PWLOG(Warning, TEXT("Projectile None"));
	//}
	

	if (!IsFire)
	{
		IsFire = true;
		auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (PWGameMode->Get_m_MyCharacter() == this))
			PWGameMode->SendBomberThrow(IsFire);
	}
}

bool APWBomberCharacter::GetIsFire() const
{
	return IsFire;
}
void APWBomberCharacter::UnAbleFire()
{
	if (IsFire)
	{
		IsFire = false;
		auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (PWGameMode->Get_m_MyCharacter() == this))
			PWGameMode->SendBomberThrow(IsFire);
	}
	//Projectile->GetProjectileMovementComponent()->SetVelocityInLocalSpace(FVector(1.0f, 0.0f, 0.0f));
	
	//Projectile->GetProjectileMovementComponent()->ProjectileGravityScale = 1.0f;
	//Projectile->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
	//PWLOG(Warning, TEXT("%f %f %f"), Projectile->GetVelocity().X, Projectile->GetVelocity().Y, Projectile->GetVelocity().Z);
}

void APWBomberCharacter::SpawnBomberProjectile()
{//여기에 if(AmountPaint > 0)  else 
	if (this->CurrentCharacterState == ECharacterState::DEATH)
		return;
	//PWLOG(Warning, TEXT("%d"), AmountPaint);
	AttackSoundComponent->Play();
	if (AmountPaint > 0.0f)
	{
		AmountPaint -= 10.0f;
		UsedPaint += 10.0f;

		if (UsedPaint >= 100) {
			if (SkillCount < 3)
				SkillCount += 1;
			UsedPaint = 0;
		}


		auto ProjectileSpawnPointInstance = Cast<APWBomberWeapon>(Weapon)->GetProjectileSpawnPoint();
		auto TempTransform = ProjectileSpawnPointInstance->GetComponentTransform();
		FVector ProjectileLocation = TempTransform.GetLocation();
		FRotator ProjectileRotation = TempTransform.GetRotation().Rotator();
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this; // 일단무기로 해놧음 예제는 무기가아니고 캐릭터임
		SpawnParams.Instigator = Instigator;

		Projectile = GetWorld()->SpawnActor<APWBomberProjectile>(ProjectileLocation, ProjectileRotation, SpawnParams);
		if (Projectile)
		{
			//하드코딩했음 이거나중에 고칩시다.
			Projectile->SetOwner(this);
			//PWLOG(Warning, TEXT("ShooterFire Playing"));
			FVector Direction = GetActorRotation().Vector();//ProjectileRotation.Vector();
			//Direction.X = -1.0f * Direction.X;
			//PWLOG(Warning, TEXT("%f, %f, %f"), Direction.X, Direction.Y, Direction.Z);
			Projectile->FireInDirection(Direction);
			if (SkillON == true) 
				Projectile->Skill();
			
		}
		else
		{
			PWLOG(Warning, TEXT("Projectile None"));
		}
		//PWLOG(Warning, TEXT("BOBMER'S PROJECTILE SPAWN!!!"));
	}
}
void APWBomberCharacter::SetIsFire(bool fire)
{
	IsFire = fire;
}

void APWBomberCharacter::Skill()
{
	if (SkillCount >= 1) {
		GetWorldTimerManager().SetTimer(SkillTimerHandle, this, &APWBomberCharacter::SKillCountDown, 1.0f, true);
		SkillTime = 5;
		SkillON = true;
		SkillCount -= 1;
	}
}

void APWBomberCharacter::SKillCountDown() {

	SkillTime--;
	if (SkillTime <= 0)
	{
		GetWorldTimerManager().ClearTimer(SkillTimerHandle);
		SkillON = false;
	}
}