// Fill out your copyright notice in the Description page of Project Settings.


#include "PWPaintRight_3rd.h"


APWPaintRight_3rd::APWPaintRight_3rd()
{//StaticMesh'/Game/TestAssets/testmap/TestMap0320/map_uv7_200319_right_5.map_uv7_200319_right_5'
	//StaticMesh'/Game/TestAssets/testmap/TestMap0313/map_test_navimesh_200313_right_5+6.map_test_navimesh_200313_right_5+6'
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/TestMap0320/map_uv7_200319_right_5.map_uv7_200319_right_5"));
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CANVAS(TEXT("/Game/TestAssets/testmap/testmap0227/eachFBX/right_3rd.right_3rd"));
	if (ST_CANVAS.Succeeded()) //StaticMesh'/Game/TestAssets/TestWeapon/RollerWeapon.RollerWeapon'
	{//루트지정해주기//StaticMesh'/Game/TestAssets/testmap/testmap0227/map5cm_uv_48105p_200226_left_1st.map5cm_uv_48105p_200226_left_1st'
		CanvasMesh->SetStaticMesh(ST_CANVAS.Object);
	}//StaticMesh'/Game/TestAssets/testmap/TestMap0314/map_uv6_navimesh_low_200314_right_5+6.map_uv6_navimesh_low_200314_right_5+6'
	//StaticMesh'/Game/TestAssets/testmap/testMap0318/map_low_uv7_200316_right_5.map_low_uv7_200316_right_5'
	//Material'/Game/TestAssets/TestFloorPaint/M_FloorMateriales/M_FloorRight3/M_FloorRight3.M_FloorRight3'
	CanvasMesh->bAllowCullDistanceVolume = 0;
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> M_Canvas(TEXT("/Game/TestAssets/TestFloorPaint/M_FloorMateriales/M_FloorRight3/M_FloorRight3.M_FloorRight3"));
	if (M_Canvas.Succeeded())
	{
		CanvasMaterial = M_Canvas.Object;
	}
}
