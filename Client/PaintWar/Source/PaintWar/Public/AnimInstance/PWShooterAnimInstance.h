// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "AnimInstance/PWAnimInstance.h"
#include "PWShooterAnimInstance.generated.h"

/**
 * 
 */
DECLARE_MULTICAST_DELEGATE(FOnReloadNotifyDelegate);
DECLARE_MULTICAST_DELEGATE(FOnFireNotifyDelegate);

UCLASS()
class PAINTWAR_API UPWShooterAnimInstance : public UPWAnimInstance
{
	GENERATED_BODY()
public:
	UPWShooterAnimInstance();

	FOnReloadNotifyDelegate OnReloadNotify;
	FOnFireNotifyDelegate OnFireNotify;
	FOnFireNotifyDelegate OnFireNotify1;

	UFUNCTION()
	void AnimNotify_ReloadNotify();

	UFUNCTION()
	void AnimNotify_FireNotify();

	UFUNCTION()
	void AnimNotify_FireNotify1();
protected:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsFire;

};
