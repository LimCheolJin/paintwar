// Fill out your copyright notice in the Description page of Project Settings.


#include "PWGameEndThread.h"
//#include "Engine/TextureRenderTarget2D.h"
//#include "PWRenderTargetPixelReader.h"
//#include "Kismet/KismetRenderingLibrary.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformAffinity.h"
#include "Runtime/Core/Public/HAL/RunnableThread.h"

PWGameEndThread::PWGameEndThread()
{
	GameEndThread = nullptr;
}

PWGameEndThread::~PWGameEndThread()
{
	delete GameEndThread;
	GameEndThread = nullptr;

}


bool PWGameEndThread::Init()
{
	PWLOG(Error, TEXT("Thread Init"));

	
	return true;
}
uint32 PWGameEndThread::Run()
{
	FPlatformProcess::Sleep(0.03);
	int redcount = 0;
	int greencount = 0;
	for (int i = 0; i < ColorBuffer.Num(); ++i)
	{
		//이 찾는 로직을 좀 바꿔야할거같음 TArray의 이점을 이용하여
		if (ColorBuffer[i].R > 0.6f)
			++redcount;
		else if (ColorBuffer[i].G > 0.6f)
			++greencount;
	}

	{
		FScopeLock ScopeLock(ResultMutex);
		*RedResultCount += redcount;
		*GreenResultCount += greencount;
	}

	PWLOG(Error, TEXT("Red count is %d , Green count is %d"), redcount, greencount);
	return 0;
}
void PWGameEndThread::Stop()
{

	PWLOG(Error, TEXT("Thread Stop"));
}
void PWGameEndThread::Exit()
{
	//GameEndThread->Kill(false);
	PWLOG(Error, TEXT("Thread Exit"));
}

bool PWGameEndThread::StartThread(TArray<FLinearColor>& ColorBuffer, long long* RedResultCount, long long* GreenResultCount, FCriticalSection* mutex)
{
	this->ColorBuffer = ColorBuffer;
	/*this->StartLoop = StartLoop;
	this->EndLoop = EndLoop;*/
	this->RedResultCount = RedResultCount;
	this->GreenResultCount = GreenResultCount;
	this->ResultMutex = mutex;
	GameEndThread = FRunnableThread::Create(this, TEXT("PWGameEndThread"), 0, TPri_BelowNormal);
	return true;
}


FRunnableThread* PWGameEndThread::GetGameEndThread() const
{
	return GameEndThread;
}