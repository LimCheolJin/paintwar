// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/AIControllers/PWRollerAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWRollerAIController() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerAIController_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWRollerAIController();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseAIController();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWRollerAIController::StaticRegisterNativesAPWRollerAIController()
	{
	}
	UClass* Z_Construct_UClass_APWRollerAIController_NoRegister()
	{
		return APWRollerAIController::StaticClass();
	}
	struct Z_Construct_UClass_APWRollerAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWRollerAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWBaseAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWRollerAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "AIControllers/PWRollerAIController.h" },
		{ "ModuleRelativePath", "Public/AIControllers/PWRollerAIController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWRollerAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWRollerAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWRollerAIController_Statics::ClassParams = {
		&APWRollerAIController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWRollerAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWRollerAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWRollerAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWRollerAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWRollerAIController, 4288754162);
	template<> PAINTWAR_API UClass* StaticClass<APWRollerAIController>()
	{
		return APWRollerAIController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWRollerAIController(Z_Construct_UClass_APWRollerAIController, &APWRollerAIController::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWRollerAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWRollerAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
