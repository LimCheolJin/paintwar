// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_Bridge4.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_Bridge4 : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintLeft_Bridge4();
};
