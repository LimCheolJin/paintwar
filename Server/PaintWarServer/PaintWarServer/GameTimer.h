#pragma once
#include <thread>
class GameTimer
{
public:
	GameTimer();
	~GameTimer();


	int GetTimerCount() const;
	void SetTimerCount(int);

	void ProcessTimer();
private:
	volatile int m_TimerCount;
};

