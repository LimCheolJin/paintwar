// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWBomberAnimInstance_generated_h
#error "PWBomberAnimInstance.generated.h already included, missing '#pragma once' in PWBomberAnimInstance.h"
#endif
#define PAINTWAR_PWBomberAnimInstance_generated_h

#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_FireNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_FireNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWBomberAnimInstance(); \
	friend struct Z_Construct_UClass_UPWBomberAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWBomberAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWBomberAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUPWBomberAnimInstance(); \
	friend struct Z_Construct_UClass_UPWBomberAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWBomberAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWBomberAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWBomberAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWBomberAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWBomberAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWBomberAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWBomberAnimInstance(UPWBomberAnimInstance&&); \
	NO_API UPWBomberAnimInstance(const UPWBomberAnimInstance&); \
public:


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWBomberAnimInstance(UPWBomberAnimInstance&&); \
	NO_API UPWBomberAnimInstance(const UPWBomberAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWBomberAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWBomberAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPWBomberAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IsFire() { return STRUCT_OFFSET(UPWBomberAnimInstance, IsFire); }


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_16_PROLOG
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_INCLASS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWBomberAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AnimInstance_PWBomberAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
