// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
struct FVector;
#ifdef PAINTWAR_PWShooterProjectile_generated_h
#error "PWShooterProjectile.generated.h already included, missing '#pragma once' in PWShooterProjectile.h"
#endif
#define PAINTWAR_PWShooterProjectile_generated_h

#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWShooterProjectile(); \
	friend struct Z_Construct_UClass_APWShooterProjectile_Statics; \
public: \
	DECLARE_CLASS(APWShooterProjectile, APWBaseProjectile, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWShooterProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWShooterProjectile(); \
	friend struct Z_Construct_UClass_APWShooterProjectile_Statics; \
public: \
	DECLARE_CLASS(APWShooterProjectile, APWBaseProjectile, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWShooterProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWShooterProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWShooterProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWShooterProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWShooterProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWShooterProjectile(APWShooterProjectile&&); \
	NO_API APWShooterProjectile(const APWShooterProjectile&); \
public:


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWShooterProjectile(APWShooterProjectile&&); \
	NO_API APWShooterProjectile(const APWShooterProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWShooterProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWShooterProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWShooterProjectile)


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ParticleRed() { return STRUCT_OFFSET(APWShooterProjectile, ParticleRed); } \
	FORCEINLINE static uint32 __PPO__ParticleGreen() { return STRUCT_OFFSET(APWShooterProjectile, ParticleGreen); }


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWShooterProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Weapon_Projectile_PWShooterProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
