// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintCanvas_generated_h
#error "PWPaintCanvas.generated.h already included, missing '#pragma once' in PWPaintCanvas.h"
#endif
#define PAINTWAR_PWPaintCanvas_generated_h

#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintCanvas(); \
	friend struct Z_Construct_UClass_APWPaintCanvas_Statics; \
public: \
	DECLARE_CLASS(APWPaintCanvas, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintCanvas)


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintCanvas(); \
	friend struct Z_Construct_UClass_APWPaintCanvas_Statics; \
public: \
	DECLARE_CLASS(APWPaintCanvas, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintCanvas)


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintCanvas(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintCanvas) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintCanvas); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintCanvas); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintCanvas(APWPaintCanvas&&); \
	NO_API APWPaintCanvas(const APWPaintCanvas&); \
public:


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintCanvas(APWPaintCanvas&&); \
	NO_API APWPaintCanvas(const APWPaintCanvas&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintCanvas); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintCanvas); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintCanvas)


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CanvasMesh() { return STRUCT_OFFSET(APWPaintCanvas, CanvasMesh); } \
	FORCEINLINE static uint32 __PPO__RenderTarget() { return STRUCT_OFFSET(APWPaintCanvas, RenderTarget); } \
	FORCEINLINE static uint32 __PPO__CanvasMaterial() { return STRUCT_OFFSET(APWPaintCanvas, CanvasMaterial); } \
	FORCEINLINE static uint32 __PPO__CanvasMaterialInstance() { return STRUCT_OFFSET(APWPaintCanvas, CanvasMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__InitCanvasMaterial() { return STRUCT_OFFSET(APWPaintCanvas, InitCanvasMaterial); } \
	FORCEINLINE static uint32 __PPO__InitCanvasMaterialInstance() { return STRUCT_OFFSET(APWPaintCanvas, InitCanvasMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__InitTexture() { return STRUCT_OFFSET(APWPaintCanvas, InitTexture); } \
	FORCEINLINE static uint32 __PPO__Canvas() { return STRUCT_OFFSET(APWPaintCanvas, Canvas); }


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_9_PROLOG
#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_INCLASS \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWPaintCanvas_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintCanvas>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWPaintCanvas_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
