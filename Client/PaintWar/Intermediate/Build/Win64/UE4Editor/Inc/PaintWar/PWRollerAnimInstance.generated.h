// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWRollerAnimInstance_generated_h
#error "PWRollerAnimInstance.generated.h already included, missing '#pragma once' in PWRollerAnimInstance.h"
#endif
#define PAINTWAR_PWRollerAnimInstance_generated_h

#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_PaintNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_PaintNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_PaintNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_PaintNotify(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_ReloadNotify) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_ReloadNotify(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWRollerAnimInstance(); \
	friend struct Z_Construct_UClass_UPWRollerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWRollerAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWRollerAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUPWRollerAnimInstance(); \
	friend struct Z_Construct_UClass_UPWRollerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPWRollerAnimInstance, UPWAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWRollerAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWRollerAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWRollerAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWRollerAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWRollerAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWRollerAnimInstance(UPWRollerAnimInstance&&); \
	NO_API UPWRollerAnimInstance(const UPWRollerAnimInstance&); \
public:


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWRollerAnimInstance(UPWRollerAnimInstance&&); \
	NO_API UPWRollerAnimInstance(const UPWRollerAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWRollerAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWRollerAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPWRollerAnimInstance)


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IsPainting() { return STRUCT_OFFSET(UPWRollerAnimInstance, IsPainting); }


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_16_PROLOG
#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_INCLASS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWRollerAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_AnimInstance_PWRollerAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
