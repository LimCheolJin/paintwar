// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PaintMap/Right/Bridge/PWPaintRight_Bridge5.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWPaintRight_Bridge5() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintRight_Bridge5_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintRight_Bridge5();
	PAINTWAR_API UClass* Z_Construct_UClass_APWPaintCanvas();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void APWPaintRight_Bridge5::StaticRegisterNativesAPWPaintRight_Bridge5()
	{
	}
	UClass* Z_Construct_UClass_APWPaintRight_Bridge5_NoRegister()
	{
		return APWPaintRight_Bridge5::StaticClass();
	}
	struct Z_Construct_UClass_APWPaintRight_Bridge5_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWPaintRight_Bridge5_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APWPaintCanvas,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWPaintRight_Bridge5_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaintMap/Right/Bridge/PWPaintRight_Bridge5.h" },
		{ "ModuleRelativePath", "Public/PaintMap/Right/Bridge/PWPaintRight_Bridge5.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWPaintRight_Bridge5_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWPaintRight_Bridge5>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWPaintRight_Bridge5_Statics::ClassParams = {
		&APWPaintRight_Bridge5::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWPaintRight_Bridge5_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWPaintRight_Bridge5_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWPaintRight_Bridge5()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWPaintRight_Bridge5_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWPaintRight_Bridge5, 2186100561);
	template<> PAINTWAR_API UClass* StaticClass<APWPaintRight_Bridge5>()
	{
		return APWPaintRight_Bridge5::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWPaintRight_Bridge5(Z_Construct_UClass_APWPaintRight_Bridge5, &APWPaintRight_Bridge5::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWPaintRight_Bridge5"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWPaintRight_Bridge5);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
