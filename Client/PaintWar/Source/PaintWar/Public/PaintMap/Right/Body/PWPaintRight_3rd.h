// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintRight_3rd.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintRight_3rd : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintRight_3rd();
};
