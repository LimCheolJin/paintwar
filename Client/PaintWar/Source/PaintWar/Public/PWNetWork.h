// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "PaintWar.h"

//#ifndef __PWNETWORK_H__
//#define __PWNETWORK_H__
//#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment(lib, "ws2_32.lib")
//#include "Windows/MinWindows.h"

#include <iostream>
#include <WS2tcpip.h>
#include "Runtime/Core/Public/HAL/Runnable.h"
#include "Protocol.h"
//#include <WinSock2.h>

//
#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 9000
#define MAX_BUFFER 4096
/**
 * 
 */


class PAINTWAR_API PWNetWork : public FRunnable
{
public:
	PWNetWork();
	virtual ~PWNetWork();


	//FRunnable override function
	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	//virtual void Exit() override;

	//싱글톤 네트워크 Get
	inline static PWNetWork* GetNetWork()
	{
		static PWNetWork NetWork;
		return &NetWork;
	}
	const bool Get_m_bIsRun() const
	{
		return m_bIsRun;
	}

	//소켓 함수들
	bool InitClientSocket();
	bool Connect(const char*);

	//쓰레드를 시작하는 것을 허용하는 함수
	bool StartThread();


	//void SendPlayerInfo(CharacterInfo player);

	void SetPWGameInstance(class UPWGameInstance* instance);

	void SetPWGameMode(class APWGameMode* gameMode);

	bool CreateEventSelect();

	void DisConnect();

	void StopThread();

	void RecvPacketProcess(char* Packet);


	const bool Send_SelectTeamTypePacket(ETeam  eTeamType, int ID);
	const bool Send_SelectCharacterTypePacket(ESelectCharacter  eCharacterType, int ID);
	const bool Send_Login();
	const bool Send_ReadyPacket(e_GameStateInfo currentState, int ID);
	const bool Send_PlayerInfoPacket(ObjectInfo object, int ID,int skillcount, bool skillon,int amountpaint);
	const bool Send_ShooterFirePacket(bool, int);
	const bool Send_RollerPaintPacket(bool, int);
	const bool Send_BomberThrowPacket(bool, int);
	const bool Send_ReloadInfoPacket(bool, int);
	const bool Send_ChatPacket(FString);
	const bool Send_ResultGamePacket(int, int, int);
	bool Send_Packet();//void* packet);
private:
	//서버와 송수신할 소켓
    SOCKET m_ClientSocket;
	
	//쓰레드를 따로 만들어서 처리를 해준다. 게임쓰레드에 영향이 없도록
	FRunnableThread* m_ClientNetWorkThread;
	
	//쓰레드를 멈추는 카운터
	FThreadSafeCounter m_StopThreadCounter;

	bool IsThreadRun;

	bool m_bIsRun;

	class UPWGameInstance* m_PWGameInstance;

	class APWGameMode* m_PWGameMode;
	//ready 정보를 서버에게 보낸다.
	//bool SendLoginInfo();

	int m_ID;
	WSANETWORKEVENTS m_NetWorkEvents;  //이벤트 셀렉트를위한 이벤트
	WSAEVENT m_hEvent; //이벤트 셀렉트를위한 이벤트

	WSABUF m_WSASendBuf; //운영체제와 통신하는 센드버퍼
	WSABUF m_WSARecvBuf;//운영체제와 통신하는 수신버퍼
	
	char m_RecvBuf[MAX_BUFFER]; //사용자정의 수신버퍼
	char m_SendBuf[MAX_BUFFER]; //사용자정의 송신버퍼

	//패킷완성을위함.
	char m_PacketBuf[MAX_BUFFER]; //
	int m_PrevPacketSize;

	int m_NetWorkIndex;
	
};
//#endif // !__PWNETWORK_H__