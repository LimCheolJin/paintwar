// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBaseCharacter.h"
#include "PWBaseWeapon.h"
#include "PWPlayerController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PWSpawnPoint.h"
#include "Materials/MaterialInstanceConstant.h"
#include "PWGameMode.h"
#include "PWGameInstance.h"
#include "Components/WidgetComponent.h"
#include "PWUserWidget.h"
#include "PWShooterCharacter.h"
#include "PWBomberCharacter.h"
#include "PWRollerCharacter.h"
#include "PWBaseWeapon.h"

// Sets default values
APWBaseCharacter::APWBaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//스프링암 , 카메라 메모리 할당후 캡슐컴포넌트에 붙힘.
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SPRINGARM"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CAMERA"));

	SpringArm->SetupAttachment(GetCapsuleComponent());
	Camera->SetupAttachment(SpringArm);
	Camera->SetHiddenInGame(true);
	SpringArm->SetHiddenInGame(true);

	const auto& CapsuleComponent = GetCapsuleComponent();
	//Scale
	//CapsuleComponent->SetCapsuleSize(30.0f, 90.0f);
	// 원래 3.0f 9.0f였음.
	CapsuleComponent->SetCollisionProfileName(TEXT("Pawn"));
	CapsuleComponent->SetCapsuleSize(4.0f, 8.0f);
	//CapsuleComponent->SetCollisionProfileName(TEXT("Pawn"));

	//01.17 추가함.
	CapsuleComponent->SetGenerateOverlapEvents(true);
	//


	//메쉬의 기본 위치,회전값을 줌.
	//Scale
	//GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -88.0f), FRotator(0.0f, -90.0f, 0.0f));
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -9.0f), FRotator(0.0f, -90.0f, 0.0f));
	///////////////////////////////////////////카메라 세팅 시작 /////////////////////////////////////
	SetCameraSetting();
	///////////////////////////////////////////카메라 세팅 끝 /////////////////////////////////////

	//스켈레탈메쉬, 애니메이션은 상속받는 붐버, 룰러, 슈터 클래스의 생성자에서 set해줄것임.
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM"));
	if (SK_MESH.Succeeded()) //성공하면SkeletalMesh'/Game/SpaceExplorer/Meshes/SpaceExplorer_CompleteHelmet2_SKM.SpaceExplorer_CompleteHelmet2_SKM'
	{
		GetMesh()->SetSkeletalMesh(SK_MESH.Object); // set 스켈레탈메쉬
	}

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);

	//Scale
	GetMesh()->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));

	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_RedTeam(TEXT("/Game/SpaceExplorer/Materials/SpaceExplorerMat_RedTeam.SpaceExplorerMat_RedTeam"));
	if (M_RedTeam.Succeeded())
	{
		RedTeamCharacterMaterialInstance = M_RedTeam.Object;
	}
	//MaterialInstanceConstant'/Game/SpaceExplorer/Materials/SpaceExplorerMat_GreenTeam.SpaceExplorerMat_GreenTeam'
	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> M_GreenTeam(TEXT("/Game/SpaceExplorer/Materials/SpaceExplorerMat_GreenTeam.SpaceExplorerMat_GreenTeam"));
	if (M_GreenTeam.Succeeded())
	{
		GreenTeamCharacterMaterialInstance = M_GreenTeam.Object;
	}


	//캐릭터의 z축점프값 설정.

	//Scale
	//GetCharacterMovement()->JumpZVelocity = 600.0f;
	GetCharacterMovement()->JumpZVelocity = 350.0f;

	//Scale
	GetCharacterMovement()->MaxWalkSpeed = 100.0f;
	GetCharacterMovement()->MaxStepHeight = 0.5f;
	//GetCharacterMovement()->bCanWalkOffLedges = false;
	GetCharacterMovement()->MaxAcceleration = 2048.0f;

	HealthPoint = 100.0f;
	AmountPaint = 100.0f;
	SkillCount = 0;
	UsedPaint = 0;
	SkillON = false;

	CurrentCharacterState = ECharacterState::ALIVE;
	CurrentTeam = ETeam::NONE;

	IsMove = true;

	////////////////////////////////  ID
	IDWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("IDWidget"));
	IDWidget->SetupAttachment(GetMesh());

	IDWidget->SetRelativeLocation(FVector(-30.f, 0.f, 230.f));
	IDWidget->SetWidgetSpace(EWidgetSpace::Screen);

	static ConstructorHelpers::FClassFinder<UUserWidget> ID_Widget(TEXT("/Game/UI/IDWidget.IDWidget_C"));
	if (ID_Widget.Succeeded())
	{
		IDWidget->SetWidgetClass(ID_Widget.Class);
		IDWidget->SetDrawSize(FVector2D(150.f, 50.f));
	}

}



void APWBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	PWLOG(Warning, TEXT("Character's BeginPlay"));

	//스폰포인트를 가져오기위함인데 좋은방법이아닌거같음.
	
	//if (CurrentTeam == ETeam::RED)
	//{
	//	/*SetTeam(ETeam::RED);
	//	PWLOG(Warning, TEXT("RED"));
	//	GetMesh()->SetMaterial(0, RedTeamCharacterMaterialInstance);
	//	for (const auto& Point : SpawnPointArray)
	//	{
	//		const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
	//		if (SpawnPoint->GetTeamColor() == ETeam::RED)
	//		{
	//			SpawnPointLocation = SpawnPoint->GetActorLocation();
	//		}
	//	}*/
	//}
	//else if (CurrentTeam == ETeam::GREEN)
	//{
	//	/*SetTeam(ETeam::GREEN);
	//	PWLOG(Warning, TEXT("GREEN"));
	//	GetMesh()->SetMaterial(0, GreenTeamCharacterMaterialInstance);
	//	for (const auto& Point : SpawnPointArray)
	//	{
	//		const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
	//		if (SpawnPoint->GetTeamColor() == ETeam::GREEN)
	//		{
	//			SpawnPointLocation = SpawnPoint->GetActorLocation();
	//		}
	//	}*/
	//}
	

	
	//FName WeaponSocket(TEXT("hand_rSocket"));
	//auto CurWeapon = GetWorld()->SpawnActor<APWBaseWeapon>(FVector::ZeroVector, FRotator::ZeroRotator);

	//if (nullptr != CurWeapon) {
	//	CurWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	//}

	//죽었을때 델리게이트 바인딩
	DeathDeleSingle.BindUFunction(this, FName("DeathEvent"));

	//킬했을때 델리게이트 바인딩
	KillEventDeleSingle.BindUFunction(this, FName("KillEvent"));

	GetWorld()->GetTimerManager().SetTimer(IsFallingEventTimerHandle, this, &APWBaseCharacter::IsCharacterFalling, 1.0f, true);

}

// Called every frame
void APWBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	
	/*auto IDUserWidget = Cast<UPWUserWidget>(IDWidget->GetUserWidgetObject());
	if (IDUserWidget != nullptr)
	{
		IDUserWidget->SetLoginID(GetLoginID());
	}*/
}
void APWBaseCharacter::SetMyLoginID_OnWidget()
{
	auto IDUserWidget = Cast<UPWUserWidget>(IDWidget->GetUserWidgetObject());
	if (IDUserWidget != nullptr)
	{
		IDUserWidget->SetLoginID(GetLoginID());
	}
}

void APWBaseCharacter::IsCharacterFalling()
{
	//PWLOG(Error, TEXT("FALLING"));
	if (CurrentCharacterState == ECharacterState::ALIVE && GetActorLocation().Z < 155.0f)
	{
		PWLOG(Error, TEXT("FALLING complete"));
		DeathEvent();
	}
}

void APWBaseCharacter::TakenDamage(APWBaseCharacter* DamageInstigator, float Damage)
{
 	HealthPoint -= Damage;
	APWPlayerController* pwc = Cast<APWPlayerController>(GetController());
	PWLOG(Error, TEXT("%s is affected for %s  %f Damage "), *DamageInstigator->GetName(), *this->GetName(), Damage);
	if (HealthPoint <= 60.0f && HealthPoint > 40.0f)
	{
		if (pwc == GetWorld()->GetFirstPlayerController())
			pwc->BloodWidget->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 0.3f));
	}
	else if (HealthPoint <= 40.0f)
	{
		if (pwc == GetWorld()->GetFirstPlayerController())
			pwc->BloodWidget->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 0.6f));
	}

	if (HealthPoint <= 0)
	{
		if( DamageInstigator->KillEventDeleSingle.IsBound() )
		{
			DamageInstigator->KillEventDeleSingle.Execute(DamageInstigator);
		}
		if (DeathDeleSingle.IsBound())
		{
			DeathDeleSingle.Execute();
		}
	}
}

void APWBaseCharacter::SetCameraSetting()
{
	//Scale
	//SpringArm->TargetArmLength = 350.0f;
	SpringArm->TargetArmLength = 100.0f;
	//Scale
	Camera->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	//Scale
	SpringArm->SetRelativeLocation(FVector(2.0f, 0.0f, 7.5f));

	SpringArm->SetRelativeRotation(FRotator(-0.15f, 0.0f, 0.0f));//(FRotator::ZeroRotator);
	SpringArm->bUsePawnControlRotation = true;  //스프링암은 현재 pawn의 rotation을 사용하겠다.
	SpringArm->bInheritPitch = true;
	SpringArm->bInheritRoll = true;
	SpringArm->bInheritYaw = true;
	SpringArm->bDoCollisionTest = true;
	bUseControllerRotationYaw = false; //플레이어컨트롤러의 yaw rotation을 사용하지않을것임.
	GetCharacterMovement()->bOrientRotationToMovement = true; //캐릭터가 움직이는 방향으로 캐릭터를 자동으로 회전시켜주는 기능
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // 회전율
	//GetController()->SetControlRotation(GetActorRotation());
	
}

// Called to bind functionality to input
void APWBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//축매핑 바인드
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APWBaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &APWBaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APWBaseCharacter::LookUp);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APWBaseCharacter::Turn);

	//액션매핑 바인드
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &APWBaseCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &APWBaseCharacter::StartReload);
	PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Released, this, &APWBaseCharacter::QuitReload);
	PlayerInputComponent->BindAction(TEXT("ZoomIn"), EInputEvent::IE_Pressed, this, &APWBaseCharacter::ZoomIn);
	PlayerInputComponent->BindAction(TEXT("ZoomOut"), EInputEvent::IE_Pressed, this, &APWBaseCharacter::ZoomOut);
	PlayerInputComponent->BindAction(TEXT("Skill"), EInputEvent::IE_Pressed, this, &APWBaseCharacter::Skill);
}


void APWBaseCharacter::MoveRight(float NewAxisValue)
{
	if(IsMove)
		AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::Y), NewAxisValue);
	
}

void APWBaseCharacter::MoveForward(float NewAxisValue)
{
	if (IsMove)
		AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X), NewAxisValue);
}
void APWBaseCharacter::LookUp(float NewAxisValue)
{
	if (IsMove)
		AddControllerPitchInput(NewAxisValue);
}
void APWBaseCharacter::Turn(float NewAxisValue)
{
	if (IsMove)
		AddControllerYawInput(NewAxisValue);
}

void APWBaseCharacter::DeathEvent()
{
	PWLOG(Warning, TEXT("%s Dead ...."), *GetName());
	
	/*Weapon->Destroy();
	Destroy();*/
	this->CurrentCharacterState = ECharacterState::DEATH;
	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	LatentInfo.ExecutionFunction = FName("DeathProcess");
	if (auto Controller = Cast<APWPlayerController>(GetController()))
	{
		DisableInput(Controller);
	}


	
	if (Cast<APWShooterCharacter>(this) != nullptr)
	{
		Cast<APWShooterCharacter>(this)->SetIsFire(false);
	}
	else if (Cast<APWRollerCharacter>(this) != nullptr)
	{
		Cast<APWRollerCharacter>(this)->SetbPainting(false);
	}
	else if (Cast<APWBomberCharacter>(this) != nullptr)
	{
		Cast<APWBomberCharacter>(this)->SetIsFire(false);
	}
	Weapon->SetActorEnableCollision(false);
	this->SetActorEnableCollision(false);

	
	auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (PWGameMode->Get_m_MyCharacter() == this)
	{
		auto PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());
		if (PWGameInstance->GetCurrentGameMode() == EGameMode::MULTI && PWGameInstance->GetIsGameStart())
		{
			if (Cast<APWShooterCharacter>(this) != nullptr)
			{
				PWGameMode->SendShooterFire(false);
				//Cast<APWShooterCharacter>(this)->UnAbleFire();
			}
			else if (Cast<APWRollerCharacter>(this) != nullptr)
			{
				Cast<APWRollerCharacter>(this)->UnablePainting();
			}
			else if (Cast<APWBomberCharacter>(this) != nullptr)
			{
				PWGameMode->SendBomberThrow(false);
				//Cast<APWBomberCharacter>(this)->UnAbleFire();
			}
		}
	}
	//UKismetSystemLibrary::Delay(GetWorld(), 2.1f, LatentInfo);
	GetWorld()->GetTimerManager().SetTimer(DeathEventTimerHandle, this, &APWBaseCharacter::DeathProcess, 3.0f, false);
	/*Weapon->SetActorHiddenInGame(true);
	Weapon->SetActorEnableCollision(false);
	Weapon->SetActorTickEnabled(false);
	this->SetActorHiddenInGame(true);
	this->SetActorEnableCollision(false);
	this->SetActorTickEnabled(false);*/
	


	//else if(AI컨트롤러이면)

	
}

void APWBaseCharacter::RespawnEvent()
{


	this->CurrentCharacterState = ECharacterState::ALIVE;

	//if 팀1이면
	SetActorLocation(SpawnPointLocation,false,nullptr,ETeleportType::ResetPhysics);
	//else if 팀2면

	if (auto Controller = Cast<APWPlayerController>(GetController()))
	{
		EnableInput(Controller);
	}

	Weapon->SetActorHiddenInGame(false);
	Weapon->SetActorEnableCollision(true);
	Weapon->SetActorTickEnabled(true);
	this->SetActorHiddenInGame(false);
	this->SetActorEnableCollision(true);
	this->SetActorTickEnabled(true);
	HealthPoint = 100.0f;
	AmountPaint = 100.0f;

	//else if(AI컨트롤러이면)
	GetWorld()->GetTimerManager().ClearTimer(RespawnEventTimerHandle);

	APWPlayerController* pwc = Cast<APWPlayerController>(GetController());
	if (pwc == GetWorld()->GetFirstPlayerController())
		pwc->BloodWidget->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 0.0f));
}

float APWBaseCharacter::GetHealthPoint() const
{
	return HealthPoint;
}

ECharacterState APWBaseCharacter::GetCurrentCharacterState() const
{
	return CurrentCharacterState;
}
void APWBaseCharacter::SetCurrentCharacterState(ECharacterState state)
{
	CurrentCharacterState = state;
}

void APWBaseCharacter::KillEvent(APWBaseCharacter* KillInstigator)
{
	KillInstigator->AddKillCount(KillInstigator);
}

void APWBaseCharacter::DeathProcess()
{
	this->CurrentCharacterState = ECharacterState::DEATH;
	Weapon->SetActorHiddenInGame(true);
	Weapon->SetActorTickEnabled(false);
	this->SetActorHiddenInGame(true);
	this->SetActorTickEnabled(false);
	GetWorld()->GetTimerManager().ClearTimer(DeathEventTimerHandle);
	this->AddDeathCount();
	GetWorld()->GetTimerManager().SetTimer(RespawnEventTimerHandle, this, &APWBaseCharacter::RespawnEvent, 5.0f, false);

}

ETeam APWBaseCharacter::GetTeam() const
{
	return CurrentTeam;
}

void APWBaseCharacter::SetTeam(ETeam Team)
{
	CurrentTeam = Team;
}

ESelectCharacter APWBaseCharacter::GetCharacterType() const
{
	return CharacterType;
}

void APWBaseCharacter::SetCharacterType(ESelectCharacter Type)
{
	CharacterType = Type;
}

void APWBaseCharacter::StartReload()
{
	IsReload = true;


	IsMove = false; //움직임을 제어함. 점프 상하좌우이동
	auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (Cast<APWBaseCharacter>(PWGameMode->Get_m_MyCharacter()) == this))
		PWGameMode->SendReloadInfo(IsReload);
	//ReloadProcess();
	//PWLOG(Warning, TEXT("Start Reload!"));
}

void APWBaseCharacter::QuitReload()
{
	IsReload = false;
	IsMove = true; //움직임을 가능하게함.
	if (auto Controller = Cast<APWPlayerController>(GetController()))
	{
		EnableInput(Controller);
	}

	auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (Cast<APWBaseCharacter>(PWGameMode->Get_m_MyCharacter()) == this))
		PWGameMode->SendReloadInfo(IsReload);

	//PWLOG(Warning, TEXT("Quit Reload!"));
}

void APWBaseCharacter::ReloadProcess()
{
	if (IsReload)
	{
		AmountPaint += 50.0f;
		
		if (AmountPaint >= 100.0f)
		{
			AmountPaint = 100.0f;
			IsReload = false;
			auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
			if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (Cast<APWBaseCharacter>(PWGameMode->Get_m_MyCharacter()) == this))
				PWGameMode->SendReloadInfo(IsReload);
		}
		PWLOG(Warning, TEXT("Reload Processing  %f "), AmountPaint);
		
		//GetWorldTimerManager().SetTimer(ReloadHandle, this, &APWBaseCharacter::ReloadProcess, 0.5f, true);
	}
	else
	{
		//GetWorldTimerManager().ClearTimer(ReloadHandle);
	}
}

bool APWBaseCharacter::GetIsReload() const
{
	return IsReload;
}

void APWBaseCharacter::Jump()
{
	if(IsMove)
		Super::Jump();

}

float APWBaseCharacter::GetAmountPaint() const
{
	return AmountPaint;
}

void APWBaseCharacter::SetAmountPaint(int amountpaint)
{
	AmountPaint = amountpaint;
}

void APWBaseCharacter::DiscountAmountPaint(float UsingPaintAmount)
{
	AmountPaint -= UsingPaintAmount;
	UsedPaint += UsingPaintAmount;

	if (UsedPaint >= 100) {
		if (SkillCount < 3)
			SkillCount += 1;
		UsedPaint = 0;
	}
}

int APWBaseCharacter::GetKill()
{
	return Kill;
}

int APWBaseCharacter::GetDeath()
{
	return Death;
}

void APWBaseCharacter::AddKillCount(APWBaseCharacter* Instigator)
{
	if (Instigator != nullptr)
	{
		int Count = Instigator->GetKillCount();
		++Count;
		Instigator->SetKillCount(Count);
	}
}
void APWBaseCharacter::AddDeathCount()
{
	++Death;
}

int APWBaseCharacter::GetKillCount() const
{
	return Kill;
}

int APWBaseCharacter::GetDeathCount() const
{
	return Death;
}
void APWBaseCharacter::SetKillCount(int KillCount)
{
	Kill = KillCount;
}

int APWBaseCharacter::Get_m_ID() const
{
	return m_ID;
}

void APWBaseCharacter::Set_m_ID(int ID)
{
	m_ID = ID;
}

void APWBaseCharacter::ZoomIn()
{
	if (SpringArm->TargetArmLength > 20)
		SpringArm->TargetArmLength -= 5.f;
}

void APWBaseCharacter::ZoomOut()
{
	if (SpringArm->TargetArmLength < 140)
		SpringArm->TargetArmLength += 5.f;
}

FString APWBaseCharacter::GetLoginID() {
	return LoginID;
}

void APWBaseCharacter::SetLoginID(FString id) {
	LoginID = id;
}


APWBaseWeapon* APWBaseCharacter::GetWeapon() const{
	return Weapon;
}

void APWBaseCharacter::Skill() {
	//GetCharacterMovement()->MaxWalkSpeed = 500.0f;
}

void APWBaseCharacter::Skilloff() {
	
}


bool APWBaseCharacter::GetSkillON() const{
	return SkillON;
}

void APWBaseCharacter::SetSkillON(bool skillon) {
	SkillON = skillon;
}


int APWBaseCharacter::GetSkillCount() const {
	return SkillCount;
}

void APWBaseCharacter::SetSkillCount(int skillcount) {
	SkillCount = skillcount;
}



