// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintRight_Bridge5_generated_h
#error "PWPaintRight_Bridge5.generated.h already included, missing '#pragma once' in PWPaintRight_Bridge5.h"
#endif
#define PAINTWAR_PWPaintRight_Bridge5_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge5(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge5_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge5, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge5)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge5(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge5_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge5, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge5)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintRight_Bridge5(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintRight_Bridge5) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge5); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge5); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge5(APWPaintRight_Bridge5&&); \
	NO_API APWPaintRight_Bridge5(const APWPaintRight_Bridge5&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge5(APWPaintRight_Bridge5&&); \
	NO_API APWPaintRight_Bridge5(const APWPaintRight_Bridge5&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge5); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge5); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintRight_Bridge5)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintRight_Bridge5>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge5_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
