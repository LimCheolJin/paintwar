// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintLeft_1st_generated_h
#error "PWPaintLeft_1st.generated.h already included, missing '#pragma once' in PWPaintLeft_1st.h"
#endif
#define PAINTWAR_PWPaintLeft_1st_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_1st(); \
	friend struct Z_Construct_UClass_APWPaintLeft_1st_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_1st, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_1st)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_1st(); \
	friend struct Z_Construct_UClass_APWPaintLeft_1st_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_1st, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_1st)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintLeft_1st(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintLeft_1st) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_1st); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_1st); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_1st(APWPaintLeft_1st&&); \
	NO_API APWPaintLeft_1st(const APWPaintLeft_1st&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_1st(APWPaintLeft_1st&&); \
	NO_API APWPaintLeft_1st(const APWPaintLeft_1st&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_1st); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_1st); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintLeft_1st)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintLeft_1st>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Left_Body_PWPaintLeft_1st_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
