// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintRight_Bridge1_generated_h
#error "PWPaintRight_Bridge1.generated.h already included, missing '#pragma once' in PWPaintRight_Bridge1.h"
#endif
#define PAINTWAR_PWPaintRight_Bridge1_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge1(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge1_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge1, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge1)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintRight_Bridge1(); \
	friend struct Z_Construct_UClass_APWPaintRight_Bridge1_Statics; \
public: \
	DECLARE_CLASS(APWPaintRight_Bridge1, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintRight_Bridge1)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintRight_Bridge1(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintRight_Bridge1) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge1); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge1); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge1(APWPaintRight_Bridge1&&); \
	NO_API APWPaintRight_Bridge1(const APWPaintRight_Bridge1&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintRight_Bridge1(APWPaintRight_Bridge1&&); \
	NO_API APWPaintRight_Bridge1(const APWPaintRight_Bridge1&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintRight_Bridge1); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintRight_Bridge1); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintRight_Bridge1)


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintRight_Bridge1>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Right_Bridge_PWPaintRight_Bridge1_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
