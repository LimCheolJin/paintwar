// Fill out your copyright notice in the Description page of Project Settings.


#include "PWRollerCharacter.h"
#include "PWRollerWeapon.h"
#include "PWSpawnPoint.h"
#include "Materials/MaterialInstanceConstant.h"
#include "PWRollerAIController.h"
#include "PWGameMode.h"
#include "PWGameInstance.h"
#include "Engine/PostProcessVolume.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "Components/PostProcessComponent.h"


APWRollerCharacter::APWRollerCharacter()
{//AnimBlueprint'/Game/SpaceExplorer/Anims/BP_RollerCharacterAnim.BP_RollerCharacterAnim'
	static ConstructorHelpers::FClassFinder<UAnimInstance> Roller_Anim(TEXT("/Game/SpaceExplorer/Anims/BP_RollerCharacterAnim.BP_RollerCharacterAnim_C"));
	if (Roller_Anim.Succeeded())//AnimBlueprint'/Game/SpaceExplorer/Anims/BP_DefaultAnim.BP_DefaultAnim'
	{
		GetMesh()->SetAnimInstanceClass(Roller_Anim.Class);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSoundObject(TEXT("/Game/Sound/RollerAttackSound.RollerAttackSound"));
	if (AttackSoundObject.Succeeded())
	{
		AttackSound = AttackSoundObject.Object;

		AttackSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AttackSoundComponent"));
		AttackSoundComponent->SetupAttachment(RootComponent);

		AttackSoundComponent->SetSound(AttackSound);
	}

	bPainting = false;

	CharacterType = ESelectCharacter::ROLLER;
	AmountPaint = 100.0f;

	
	AIControllerClass = APWRollerAIController::StaticClass();
	//AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	RollerPostProcessComponent = CreateDefaultSubobject<UPostProcessComponent>(TEXT("UPostProcessComponent"));
	RollerPostProcessComponent->SetupAttachment(GetCapsuleComponent());

	RollerPostProcessComponent->Settings.bOverride_MotionBlurAmount = false;
	RollerPostProcessComponent->Settings.bOverride_MotionBlurMax = false;
	RollerPostProcessComponent->Settings.bOverride_MotionBlurPerObjectSize = false;
	RollerPostProcessComponent->Settings.MotionBlurAmount=1;
	RollerPostProcessComponent->Settings.MotionBlurMax=100;
	RollerPostProcessComponent->Settings.MotionBlurPerObjectSize=100;
	RollerPostProcessComponent->bUnbound = false;
	RollerPostProcessComponent->BlendRadius = 120;

	SkillTime = 0;

}

void APWRollerCharacter::BeginPlay()
{
	AttackSoundComponent->Stop();
	//APWBaseCharacter::BeginPlay();
	Super::BeginPlay();
	PWLOG(Error, TEXT("Roller BeginPlay Start"));
	TSubclassOf<AActor> ClassParam = APWSpawnPoint::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassParam, SpawnPointArray);

	if (!GetName().Compare(FString("RedRoller")))   //같으면 0을반환함.
	{
		SetTeam(ETeam::RED);
		PWLOG(Warning, TEXT("RED"));
		GetMesh()->SetMaterial(0, RedTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if (SpawnPoint->GetTeamColor() == ETeam::RED)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}
	else if (!GetName().Compare(FString("GreenRoller")))
	{
		SetTeam(ETeam::GREEN);
		PWLOG(Warning, TEXT("GREEN"));
		GetMesh()->SetMaterial(0, GreenTeamCharacterMaterialInstance);
		for (const auto& Point : SpawnPointArray)
		{
			const auto& SpawnPoint = Cast<APWSpawnPoint>(Point);
			if(SpawnPoint->GetTeamColor() == ETeam::GREEN)
			{
				SpawnPointLocation = SpawnPoint->GetActorLocation();
				break;
			}
		}
	}
	
	FName WeaponSocket(TEXT("hand_rSocketRoller"));

	//auto CurWeapon = GetWorld()->SpawnActor<APWRollerWeapon>(FVector(-15.0f, -160.0f, -100.0f), FRotator(0.0f,0.0f,180.0f));

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	Weapon = GetWorld()->SpawnActor<APWRollerWeapon>(FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (nullptr != Weapon)
	{
		Weapon->SetOwner(this);
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocket);
	}

	RollerAnim = Cast<UPWRollerAnimInstance>(GetMesh()->GetAnimInstance());
	if (RollerAnim != nullptr)
	{
		RollerAnim->OnPaintNotify.AddUObject(this, &APWRollerCharacter::OnPainting);
		RollerAnim->OnReloadNotify.AddUObject(this, &APWBaseCharacter::ReloadProcess);
	}


}

void APWRollerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APWBaseCharacter::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Brush"), EInputEvent::IE_Pressed, this, &APWRollerCharacter::AblePainting);
	PlayerInputComponent->BindAction(TEXT("Brush"), EInputEvent::IE_Released, this, &APWRollerCharacter::UnablePainting);

}

void APWRollerCharacter::UnablePainting()
{
	//if (bPainting)
		bPainting = false;

	auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (PWGameMode->Get_m_MyCharacter() == this))
		PWGameMode->SendRollerPaint(bPainting);
	AttackSoundComponent->Stop();
	//PWLOG(Warning, bPainting ? TEXT("True") : TEXT("False"));
}
void APWRollerCharacter::SetbPainting(bool paint)
{
	bPainting = paint;
}
void APWRollerCharacter::AblePainting()
{
	
	//if (!bPainting)
	//{
		bPainting = true;
		auto PWGameMode = Cast<APWGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (PWGameMode != nullptr && (Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI) && (PWGameMode->Get_m_MyCharacter() == this) )
			PWGameMode->SendRollerPaint(bPainting);
		
		AttackSoundComponent->Play();
	//}
	//PWLOG(Warning, bPainting ? TEXT("True") : TEXT("False"));
	//PWLOG(Warning, TEXT("Painting Floor !!"));
}

bool APWRollerCharacter::GetbPainting() const
{
	return bPainting;
}
void APWRollerCharacter::RespawnEvent()
{
	APWBaseCharacter::RespawnEvent();
	bPainting = false;
}

void APWRollerCharacter::OnPainting()
{
	if(Weapon != nullptr)
		Cast<APWRollerWeapon>(Weapon)->ProcessPaint();
}

void APWRollerCharacter::Skill()
{
	
	if (SkillCount >= 1 && SkillON == false) { //스킬활성화 조건 
		GetWorldTimerManager().SetTimer(SkillTimerHandle, this, &APWRollerCharacter::SKillCountDown, 1.0f, true);
		GetCharacterMovement()->MaxWalkSpeed = 500.0f;
		RollerPostProcessComponent->Settings.bOverride_MotionBlurAmount = true;
		RollerPostProcessComponent->Settings.bOverride_MotionBlurMax = true;
		RollerPostProcessComponent->Settings.bOverride_MotionBlurPerObjectSize = true;
		SkillCount -= 1;
		SkillTime = 5;
		SkillON = true;
	}
}

void APWRollerCharacter::Skilloff() {

	GetWorldTimerManager().ClearTimer(SkillTimerHandle);
	GetCharacterMovement()->MaxWalkSpeed = 100.0f;
	RollerPostProcessComponent->Settings.bOverride_MotionBlurAmount = false;
	RollerPostProcessComponent->Settings.bOverride_MotionBlurMax = false;
	RollerPostProcessComponent->Settings.bOverride_MotionBlurPerObjectSize = false;
	SkillON = false;
}

void APWRollerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APWRollerCharacter::SKillCountDown() {
	
	SkillTime--;
	if (SkillTime <= 0)
	{
		Skilloff();
		SkillON = false;
	}
}