// Fill out your copyright notice in the Description page of Project Settings.


#include "PWGameMode.h"
#include "Engine/Engine.h"
#include "PWShooterCharacter.h"
#include "PWRollerCharacter.h"
#include "PWBomberCharacter.h"
#include "PWPlayerController.h"
#include "PWGameInstance.h"
#include "PWNetWork.h"
#include "PWSpawnPoint.h"
#include "PWBomberAIController.h"
#include "PWRollerAIController.h"
#include "PWShooterAIController.h"
#include "Protocol.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "PWPaintLeft_1st.h"
#include "PWPaintLeft_2nd.h"
#include "PWPaintLeft_3rd.h"
#include "PWPaintLeft_4th.h"

#include "PWPaintRight_1st.h"
#include "PWPaintRight_2nd.h"
#include "PWPaintRight_3rd.h"
#include "Engine/Canvas.h"
#include "APWAutoTextureAtlas.h"
#include "PWGameTimeManager.h"

//게임모드->폰->플레이어컨트롤러  beginplay순서
//*************************************************************************************** 최 지 운 을 위한 주석  필독***************************************************************************************8 
/*
PWGameInstance를 만들어놓았음.
이 GameInstance는  모든 레벨 간의 전환 시에도 변수를 저장해 둘수 있는 기능이있다.
결국에 레벨이 바뀌면서 게임모드, 플레이어컨트롤러, 폰 등 파괴되는 것들에 대해서 저장 해야될것을 저장해놓을수있다는소리임.
이 GameInstance를 이용하면 결국 ui scene에서 게임 캐릭터를 선택하고  색깔을 선택하는 정보를 gameinstance에 저장해놓고
현재 게임 scene 레벨로 넘어왔을때 이 정보를 beginplay에서 set을 해준다면 원하는 캐릭터에 possess 할수 있다.
*/

APWGameMode::APWGameMode()
{
	//character를 생성하고 DefaultPawn속성에 지정하는것이아닌.
	//클래스 정보를 저장하도록 한다. 이렇게 하는 이유는 멀티플레이까지
	//고려하여 게임에 몇명의 플레이어가 들어올지 모르기 때문에 미리 폰을
	//만들어 두는 것이 아닌 클래스 정보만 저장해놓고 이를 기반으로 폰을 생성하는것이
	//합리적이기 때문이다.
	//언리얼 오브젝트의 클래스정보는 언리얼 헤더툴에 의해 자동으로 생성되고
	//이는 언리얼 오브젝트마다 자동으로 생성된 StaticClass()로 가져올수있다.
	//DefaultPawnClass = APWRollerCharacter::StaticClass();//APWBaseCharacter::StaticClass();

	PlayerControllerClass = APWPlayerController::StaticClass();
	//DefaultPawnClass = APWShooterCharacter::StaticClass();
	CurrentGameMode = EGameMode::NONE;
	//AIControllerClass = APWBaseAIController::StaticClass();
	PrimaryActorTick.bCanEverTick = true;
	m_GameTimer = nullptr;

	////////////////////////////////left base
	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> T_LeftBaseRenderTarget(TEXT("/Game/TestAssets/TestFloorPaint/LeftRenderTarget.LeftRenderTarget"));
	if (T_LeftBaseRenderTarget.Succeeded())
	{
		m_LeftBaseRenderTarget = T_LeftBaseRenderTarget.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}

	/////////////////////////right base
	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> T_RightBaseRenderTarget(TEXT("/Game/TestAssets/TestFloorPaint/RightRenderTarget.RightRenderTarget"));
	if (T_RightBaseRenderTarget.Succeeded())
	{
		m_RightBaseRenderTarget = T_RightBaseRenderTarget.Object;
		//PWLOG(Warning, TEXT("SUCCEED ROAD T_BRUSH"));
	}
}

APWGameMode::~APWGameMode()
{
	m_AllCharacters.clear();

	//m_TempPlayerInfoes.clear();
}


void APWGameMode::PostLogin(APlayerController* NewPlayer)
{
	PWLOG(Warning, TEXT("LOGIN BEGIN"));
	Super::PostLogin(NewPlayer);
	PWLOG(Warning, TEXT("LOGIN END"));
}

void APWGameMode::BeginPlay()
{
	Super::BeginPlay();

	
	PWLOG(Error, TEXT("GAME MODE BEGINPLAY"));
	m_GameTimer = GetWorld()->SpawnActor<APWGameTimeManager>();

	//아래에서 UI적용 하겠음
	PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());
	if (PWGameInstance != nullptr)
	{
		CurrentCharacter = PWGameInstance->GetCurrentSelectCharacter();
		CurrentTeam = PWGameInstance->GetCurrentSelectTeam();	

		CurrentGameMode = PWGameInstance->GetCurrentGameMode();
	}//GetWorld()->SpawnActor<APWShooterWeapon>(FVector::ZeroVector, FRotator::ZeroRotator);

	FStringClassReference HudWidgetClassRef(TEXT("/Game/UI/MainHud.MainHud_C"));
	if (UClass* HudWidgetClass = HudWidgetClassRef.TryLoadClass<UUserWidget>())
	{
		MainHudWidget = CreateWidget<UUserWidget>(GetWorld(), HudWidgetClass);
		MainHudWidget->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 0.0f));
		MainHudWidget->AddToViewport();
	}

	MainHudWidget->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));

	if (CurrentGameMode == EGameMode::SINGLE)
	{
		
		//슈터스폰
		FActorSpawnParameters ShooterRedSpawnParams;
		ShooterRedSpawnParams.Name = FName(TEXT("RedShooter"));
		//ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(292.0f, -752.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
		auto ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(418.0f, -456.f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
		auto shootAIred = GetWorld()->SpawnActor< APWShooterAIController>();
		shootAIred->OnPossess(ShooterCharacterRed);
		//ExistOnGameLevelCharacters.Emplace(ShooterCharacterRed);

		FActorSpawnParameters ShooterGreenSpawnParams;
		ShooterGreenSpawnParams.Name = FName(TEXT("GreenShooter"));
		//ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-159.0f, 192.0f, 115.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
		auto ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector (-2114.0f, 186.0f, 506.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
		auto shootAIgreen = GetWorld()->SpawnActor< APWShooterAIController>();
		shootAIgreen->OnPossess(ShooterCharacterGreen);
		//ExistOnGameLevelCharacters.Emplace(ShooterCharacterGreen);

		////롤러스폰
		FActorSpawnParameters RollerRedSpawnParams;
		RollerRedSpawnParams.Name = FName(TEXT("RedRoller"));
		//RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(292.0f, -711.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
		auto RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(418.0f, -496.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
		auto rollerAIred = GetWorld()->SpawnActor< APWRollerAIController>();
		rollerAIred->OnPossess(RollerCharacterRed);
		//ExistOnGameLevelCharacters.Emplace(RollerCharacterRed);

		FActorSpawnParameters RollerGreenSpawnParams;
		RollerGreenSpawnParams.Name = FName(TEXT("GreenRoller"));
		//RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-159.0f, 229.0f, 115.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
		auto RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-2114.0f, 226.0f, 506.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
		auto rollerAIgreen = GetWorld()->SpawnActor< APWRollerAIController>();
		rollerAIgreen->OnPossess(RollerCharacterGreen);
		//ExistOnGameLevelCharacters.Emplace(RollerCharacterGreen);

		////붐버스폰
		FActorSpawnParameters BomberRedSpawnParams;
		BomberRedSpawnParams.Name = FName(TEXT("RedBomber"));
		//BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(292.0f, -670.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
		auto BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(418.0f, -416.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
		auto bomberAIred = GetWorld()->SpawnActor< APWBomberAIController>();
		bomberAIred->OnPossess(BomberCharacterRed);
		//ExistOnGameLevelCharacters.Emplace(BomberCharacterRed);

		FActorSpawnParameters BomberGreenSpawnParams;
		BomberGreenSpawnParams.Name = FName(TEXT("GreenBomber"));
		//BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-159.0f, 264.0f, 115.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
		auto BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector (-2114.0f, 146.0f, 506.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
		auto bomberAIgreen = GetWorld()->SpawnActor< APWBomberAIController>();
		bomberAIgreen->OnPossess(BomberCharacterGreen);
		//ExistOnGameLevelCharacters.Emplace(BomberCharacterGreen);

		PWController = Cast<APWPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (PWController != nullptr)
		{
			PWController->FindCharacters();
		}
	}
	else if (CurrentGameMode == EGameMode::MULTI && PWGameInstance->GetIsGameStart())
	{


		/*TSubclassOf<AActor> gt = APWGameTimeManager::StaticClass();

		TArray<AActor*> OutputActors;

		UGameplayStatics::GetAllActorsOfClass(GetWorld(), gt, OutputActors);

		m_GameTimer = Cast<APWGameTimeManager>(OutputActors[0]);*/

		/*if (PWGameInstance->GetStartGameScene())
		{
			PWGameInstance->PWNetWork->SetPWGameMode(this);*/
		auto AllSessionPlayers = PWGameInstance->m_PlayerInformations;
		m_PWNetWork = PWNetWork::GetNetWork();
		m_PWNetWork->SetPWGameMode(this);
		for (auto& Client : AllSessionPlayers)
		{
			if (PWGameInstance->m_MyPlayerInfo != Client.second)
			{
				if (Client.second->m_eTeamType == ETeam::RED)
				{
					if (Client.second->m_eCharacterType == ESelectCharacter::SHOOTER)
					{
						FActorSpawnParameters ShooterRedSpawnParams;
						ShooterRedSpawnParams.Name = FName(TEXT("RedShooter"));
						//ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(292.0f, -752.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						//auto ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(698.0f, -1803.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						auto ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(418.0f, -456.f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(ShooterCharacterRed);
						//PWLOG(Error, TEXT("Spawn Shooter red"));

						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(ShooterCharacterRed);
						ShooterCharacterRed->Set_m_ID(Client.second->m_ID);
						ShooterCharacterRed->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = ShooterCharacterRed;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::ROLLER)
					{
						////롤러스폰
						FActorSpawnParameters RollerRedSpawnParams;
						RollerRedSpawnParams.Name = FName(TEXT("RedRoller"));
						//RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(292.0f, -711.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						//auto RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(698.0f, -1840.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						auto RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(418.0f, -496.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(RollerCharacterRed);
						//PWLOG(Error, TEXT("Spawn Roller red"));

						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(RollerCharacterRed);
						RollerCharacterRed->Set_m_ID(Client.second->m_ID);
						RollerCharacterRed->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = RollerCharacterRed;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::BOMBER)
					{
						FActorSpawnParameters BomberRedSpawnParams;
						BomberRedSpawnParams.Name = FName(TEXT("RedBomber"));
						//BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(292.0f, -670.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
						//auto BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(698.0f, -1760.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
						auto BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(418.0f, -416.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(BomberCharacterRed);
						//PWLOG(Error, TEXT("Spawn Bomber red"));
						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(BomberCharacterRed);
						BomberCharacterRed->Set_m_ID(Client.second->m_ID);
						BomberCharacterRed->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = BomberCharacterRed;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
				}
				else if (Client.second->m_eTeamType == ETeam::GREEN)
				{
					if (Client.second->m_eCharacterType == ESelectCharacter::SHOOTER)
					{
						FActorSpawnParameters ShooterGreenSpawnParams;
						ShooterGreenSpawnParams.Name = FName(TEXT("GreenShooter"));
						//ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-159.0f, 192.0f, 115.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
						//auto ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-438.0f, 617.0f, 436.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
						auto ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-2114.0f, 186.0f, 506.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(ShooterCharacterGreen);
						//PWLOG(Error, TEXT("Spawn shooter green"));

						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(ShooterCharacterGreen);
						ShooterCharacterGreen->Set_m_ID(Client.second->m_ID);
						ShooterCharacterGreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = ShooterCharacterGreen;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::ROLLER)
					{
						FActorSpawnParameters RollerGreenSpawnParams;
						RollerGreenSpawnParams.Name = FName(TEXT("GreenRoller"));
						//RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-159.0f, 229.0f, 115.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
						//auto RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-438.0f, 577.0f, 436.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
						auto RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-2114.0f, 226.0f, 506.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(RollerCharacterGreen);
						//PWLOG(Error, TEXT("Spawn Roller green"));

						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(RollerCharacterGreen);
						RollerCharacterGreen->Set_m_ID(Client.second->m_ID);
						RollerCharacterGreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = RollerCharacterGreen;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::BOMBER)
					{
						FActorSpawnParameters BomberGreenSpawnParams;
						BomberGreenSpawnParams.Name = FName(TEXT("GreenBomber"));
						//BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-159.0f, 264.0f, 115.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						//auto BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-438.0f, 537.0f, 436.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						auto BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-2114.0f, 146.0f, 506.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(BomberCharacterGreen);
						//PWLOG(Error, TEXT("Spawn Bomber green"));

						auto controller = SpawnPlayerController(ENetRole::ROLE_None, FVector::ZeroVector, FRotator::ZeroRotator);
						auto PWOtherController = Cast<APWPlayerController>(controller);
						PWOtherController->OnPossess(BomberCharacterGreen);
						BomberCharacterGreen->Set_m_ID(Client.second->m_ID);
						BomberCharacterGreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = BomberCharacterGreen;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}

				}
			}
			else
			{
				if (Client.second->m_eTeamType == ETeam::RED)
				{
					if (Client.second->m_eCharacterType == ESelectCharacter::SHOOTER)
					{
						FActorSpawnParameters ShooterRedSpawnParams;
						ShooterRedSpawnParams.Name = FName(TEXT("RedShooter"));
						//ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(292.0f, -752.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						//auto shootred = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(698.0f, -1803.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						auto ShooterCharacterRed = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(418.0f, -456.f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), ShooterRedSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(ShooterCharacterRed);
						//PWLOG(Error, TEXT("Spawn Shooter red"));
						ShooterCharacterRed->Set_m_ID(Client.second->m_ID);
						ShooterCharacterRed->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = ShooterCharacterRed;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::ROLLER)
					{
						////롤러스폰
						FActorSpawnParameters RollerRedSpawnParams;
						RollerRedSpawnParams.Name = FName(TEXT("RedRoller"));
						//RollerCharacterRed = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(292.0f, -711.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						//auto rollerred = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(698.0f, -1840.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						auto rollerred = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(418.0f, -496.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), RollerRedSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(RollerCharacterRed);
						//PWLOG(Error, TEXT("Spawn Roller red"));


						rollerred->Set_m_ID(Client.second->m_ID);
						rollerred->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = rollerred;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::BOMBER)
					{
						FActorSpawnParameters BomberRedSpawnParams;
						BomberRedSpawnParams.Name = FName(TEXT("RedBomber"));
						//BomberCharacterRed = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(292.0f, -670.0f, 115.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
						//auto bomberred = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(698.0f, -1760.0f, 436.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);
						auto bomberred = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(418.0f, -416.0f, 506.0f), FRotator(0.0f, 180.0f, 0.0f), BomberRedSpawnParams);

						bomberred->Set_m_ID(Client.second->m_ID);
						bomberred->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = bomberred;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
				}
				else if (Client.second->m_eTeamType == ETeam::GREEN)
				{
					if (Client.second->m_eCharacterType == ESelectCharacter::SHOOTER)
					{
						FActorSpawnParameters ShooterGreenSpawnParams;
						ShooterGreenSpawnParams.Name = FName(TEXT("GreenShooter"));
						//ShooterCharacterGreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-159.0f, 192.0f, 115.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
						//auto shootergreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-438.0f, 617.0f, 436.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);
						auto shootergreen = GetWorld()->SpawnActor<APWShooterCharacter>(FVector(-2114.0f, 186.0f, 506.0f), FRotator::ZeroRotator, ShooterGreenSpawnParams);

						shootergreen->Set_m_ID(Client.second->m_ID);
						shootergreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = shootergreen;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::ROLLER)
					{
						FActorSpawnParameters RollerGreenSpawnParams;
						RollerGreenSpawnParams.Name = FName(TEXT("GreenRoller"));
						//RollerCharacterGreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-159.0f, 229.0f, 115.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
						//auto rollergreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-438.0f, 577.0f, 436.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);
						auto rollergreen = GetWorld()->SpawnActor<APWRollerCharacter>(FVector(-2114.0f, 226.0f, 506.0f), FRotator::ZeroRotator, RollerGreenSpawnParams);


						rollergreen->Set_m_ID(Client.second->m_ID);
						rollergreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = rollergreen;

						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
					else if (Client.second->m_eCharacterType == ESelectCharacter::BOMBER)
					{
						FActorSpawnParameters BomberGreenSpawnParams;
						BomberGreenSpawnParams.Name = FName(TEXT("GreenBomber"));
						//BomberCharacterGreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-159.0f, 264.0f, 115.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						//auto bombergreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-438.0f, 537.0f, 436.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						auto bombergreen = GetWorld()->SpawnActor<APWBomberCharacter>(FVector(-2114.0f, 146.0f, 506.0f), FRotator::ZeroRotator, BomberGreenSpawnParams);
						//ExistOnGameLevelCharacters.Emplace(BomberCharacterGreen);
						//PWLOG(Error, TEXT("Spawn Bomber green"));


						bombergreen->Set_m_ID(Client.second->m_ID);
						bombergreen->SetCurrentCharacterState(ECharacterState::ALIVE);
						m_AllCharacters[Client.second->m_ID] = bombergreen;
						FString ID = FString(Client.second->Login_ID);
						m_AllCharacters[Client.second->m_ID]->SetLoginID(ID);
						m_AllCharacters[Client.second->m_ID]->SetMyLoginID_OnWidget();
					}
				}
			}
			
			/*IsMultiGameStart = PWGameInstance->GetIsGameStart();
			auto PWController = Cast<APWPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
			if (PWController != nullptr)
			{
				PWController->FindCharacters();
			}
			if (IsMultiGameStart == true)
			{
				GetWorldTimerManager().SetTimer(SendTimerHandle, this, &APWGameMode::SendWorldInfo, 0.016f, true);
			}*/
		}
		IsMultiGameStart = PWGameInstance->GetIsGameStart();
		auto PWController = Cast<APWPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (PWController != nullptr)
		{
			PWController->FindCharacters();
		}
		if (IsMultiGameStart == true)
		{
			GetWorldTimerManager().SetTimer(SendTimerHandle, this, &APWGameMode::SendWorldInfo, 0.016f, true);
		}
	}

	PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());
	////////////////////0729 test
	TArray<AActor*> textureAtlasS;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAPWAutoTextureAtlas::StaticClass(), textureAtlasS);
	auto AutotextureAtlas = Cast<AAPWAutoTextureAtlas>(textureAtlasS[0]);
	//leftbase
	{
		FDrawToRenderTargetContext Context;
		FVector2D Size;
		UCanvas* Canvas;
		UKismetRenderingLibrary::BeginDrawCanvasToRenderTarget(GetWorld(), m_LeftBaseRenderTarget, Canvas, Size, Context);

		if (Canvas)
		{
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[0], FVector2D(0.0f, 0.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[1], FVector2D(2048.0f, 0.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[2], FVector2D(0.0f, 2048.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[3], FVector2D(2048.0f, 2048.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
		}
		UKismetRenderingLibrary::EndDrawCanvasToRenderTarget(GetWorld(), Context);
	}

	//rightbase
	{
		FDrawToRenderTargetContext Context;
		FVector2D Size;
		UCanvas* Canvas;
		UKismetRenderingLibrary::BeginDrawCanvasToRenderTarget(GetWorld(), m_RightBaseRenderTarget, Canvas, Size, Context);

		if (Canvas)
		{
			//Canvas->K2_DrawMaterial(BrushMaterialInstance, ((Size * DrawLocation) - (BrushSize / 12.0f)), FVector2D(BrushSize, BrushSize), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[4], FVector2D(0.0f, 0.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[5], FVector2D(2048.0f, 0.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
			Canvas->K2_DrawTexture(AutotextureAtlas->m_TestTextures[6], FVector2D(0.0f, 2048.0f), FVector2D(2048.0f, 2048.0f), FVector2D(0.0f, 0.0f));
		}
		UKismetRenderingLibrary::EndDrawCanvasToRenderTarget(GetWorld(), Context);
	}



}

void APWGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//PWLOG(Error, TEXT("GAME MODE TICK"));
	if (!IsMultiGameStart)
		return;
	if (m_PWNetWork == nullptr)
		return;
	auto timer = GetGameTimer();
	if (timer == nullptr)
		return;
	
	
	UpdatePlayers();

}

void APWGameMode::UpdatePlayers()
{
	for (auto Client : m_AllCharacters)
	{
		if (CurrentPlayerID != Client.second->Get_m_ID())
		{
			//이동처리
			int ID = Client.second->Get_m_ID();
			auto Pos = m_TempPlayerInfoes[ID].ObjInfo.m_ObjectPosition;
			auto Rot = m_TempPlayerInfoes[ID].ObjInfo.m_ObjectRotation;
			auto Vel = m_TempPlayerInfoes[ID].ObjInfo.m_ObjectVelocity;
			
			Client.second->SetActorLocationAndRotation(FVector(Pos.x, Pos.y, Pos.z), FRotator(Rot.Pitch, Rot.Yaw, Rot.Roll), false, nullptr, ETeleportType::TeleportPhysics);//SetActorLocation(FVector(Pos.x,Pos.y,Pos.z) );
			Client.second->AddMovementInput(FVector(Vel.vx, Vel.vy, Vel.vz));
			//이동처리끝
		
			//장전 (장전은 모든 캐릭터가 공유한다. )
			auto BaseCharacter = Cast<APWBaseCharacter>(Client.second);
			if (BaseCharacter != nullptr)
			{
				if (m_TempPlayerInfoes[ID].IsReloading)
				{
					PWLOG(Warning, TEXT("APWGameMode's Update()"));
					BaseCharacter->StartReload();
				}
				else
				{
					BaseCharacter->QuitReload();
				}
			}
			////////////////////////스킬   잘못되었따
			Client.second->SetSkillON(m_TempPlayerInfoes[ID].m_SKillON);
			PWLOG(Warning, TEXT("skill on : %d"), m_TempPlayerInfoes[ID].m_SKillON);
			Client.second->SetSkillCount(m_TempPlayerInfoes[ID].m_SkillCount);
			PWLOG(Warning, TEXT("skill count : %d"), m_TempPlayerInfoes[ID].m_SkillCount);
			Client.second->SetAmountPaint(m_TempPlayerInfoes[ID].m_AmountPaint);
				

			//현재 살아있으면 애니메이션이나 다른걸 처리. 죽었으면 안해야함.
			if (Client.second->GetCurrentCharacterState() == ECharacterState::ALIVE)
			{
				//슈터 Fire처리
				if (Client.second->GetCharacterType() == ESelectCharacter::SHOOTER)
				{//
					auto Shooter = Cast<APWShooterCharacter>(Client.second);
					//PWLOG(Error, TEXT("ID is %d"), ID);
					//PWLOG(Error, m_TempPlayerInfoes[ID].IsFire ? TEXT("Is Fire is TRUE") : TEXT("is Fire is FALSE"));
					if (Shooter != nullptr)
					{
						if (m_TempPlayerInfoes[ID].IsFire)
						{//한번만 호출
							Shooter->Fire();
							//발사체가 스폰이 허락되면


						}
						else
						{
							Shooter->UnAbleFire();
						}
					}
				}
				else if (Client.second->GetCharacterType() == ESelectCharacter::ROLLER)
				{//롤러 처리
					auto Roller = Cast<APWRollerCharacter>(Client.second);
					/*PWLOG(Error, TEXT("ID is %d"), ID);
					PWLOG(Error, m_TempPlayerInfoes[ID].IsFire ? TEXT("Is Fire is TRUE") : TEXT("is Fire is FALSE"));*/
					if (Roller != nullptr)
					{
						if (m_TempPlayerInfoes[ID].IsRolling)
						{//한번만 호출
							Roller->AblePainting();
						}
						else
						{
							Roller->UnablePainting();
						}
					}
				}
				else if (Client.second->GetCharacterType() == ESelectCharacter::BOMBER)
				{
					auto Bomber = Cast<APWBomberCharacter>(Client.second);
					if (Bomber != nullptr)
					{
						if (m_TempPlayerInfoes[ID].IsThrowing)
							Bomber->Fire();
						else
							Bomber->UnAbleFire();
					}
				}
			}
			
		}
	}

	
}

void APWGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

	Super::EndPlay(EndPlayReason);

	PWLOG(Error, TEXT("GAME MODE ENDPLAY"));

}


EGameMode APWGameMode::GetCurrentGameMode() const
{
	return CurrentGameMode;
}

void APWGameMode::SetCurrentGameMode(EGameMode Mode)
{
	CurrentGameMode = Mode;
}

void APWGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	//지금 뭐가 떠있으면 삭제
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}

	//새로 위젯이 들어오면 띄우기
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}


/////////////////////////////For NetWork Function //////////////////////////////////////
std::map<int, APWBaseCharacter*> APWGameMode::Get_m_Clients()
{
	return m_AllCharacters;
}

int APWGameMode::GetCurrentPlayerID() const
{
	return CurrentPlayerID;
}

void APWGameMode::SetCurrentPlayerID(int id)
{
	CurrentPlayerID = id;
}

APWBaseCharacter* APWGameMode::Get_m_MyCharacter() const
{
	return m_MyCharacter;
}

void APWGameMode::Set_m_MyCharacter(APWBaseCharacter* ch)
{
	m_MyCharacter = ch;
}
void APWGameMode::SendPlayerInfo()
{
	
	auto timer = GetGameTimer();
	if (timer == nullptr)
		return;
	if ((m_MyCharacter != nullptr) && (timer->GetIsGameEnd() == false) && IsMultiGameStart)
	{
		ObjectInfo Object;

		Object.m_ObjectPosition.x= m_MyCharacter->GetActorLocation().X;
		Object.m_ObjectPosition.y = m_MyCharacter->GetActorLocation().Y;
		Object.m_ObjectPosition.z = m_MyCharacter->GetActorLocation().Z;

		Object.m_ObjectRotation.Pitch = m_MyCharacter->GetActorRotation().Pitch;
		Object.m_ObjectRotation.Yaw = m_MyCharacter->GetActorRotation().Yaw;
		Object.m_ObjectRotation.Roll = m_MyCharacter->GetActorRotation().Roll;

		Object.m_ObjectVelocity.vx = m_MyCharacter->GetVelocity().X;
		Object.m_ObjectVelocity.vy = m_MyCharacter->GetVelocity().Y;
		Object.m_ObjectVelocity.vz = m_MyCharacter->GetVelocity().Z;

		
		m_PWNetWork->Send_PlayerInfoPacket(Object, m_MyCharacter->Get_m_ID(), m_MyCharacter->GetSkillCount(), m_MyCharacter->GetSkillON(), m_MyCharacter->GetAmountPaint());
			//PWLOG(Error, TEXT("GameMode's SendPlayerInfo Error"));

	}
}

void APWGameMode::SendShooterFire(bool IsFire)
{
	if (m_MyCharacter != nullptr)
	{
		if(IsMultiGameStart)
			m_PWNetWork->Send_ShooterFirePacket(IsFire, m_MyCharacter->Get_m_ID());
			//PWLOG(Error, TEXT("GameMode's SendShooterFire Error"));
	}
}
void APWGameMode::SendRollerPaint(bool IsRolling)
{
	if (m_MyCharacter != nullptr)
	{
		//네트워크->send roller
		if (IsMultiGameStart)
			m_PWNetWork->Send_RollerPaintPacket(IsRolling, m_MyCharacter->Get_m_ID());
			//PWLOG(Error, TEXT("GameMode's SendRollerPaint Error"));
	}
}
void APWGameMode::SendBomberThrow(bool IsThrowing)
{
	if (m_MyCharacter != nullptr)
	{
		//네트워크->send Bomber
		if (IsMultiGameStart)
			m_PWNetWork->Send_BomberThrowPacket(IsThrowing, m_MyCharacter->Get_m_ID());
		
	}

}
void APWGameMode::SendReloadInfo(bool IsReload)
{
	if (m_MyCharacter != nullptr)
	{
		//네트워크->send Bomber
		if (IsMultiGameStart)
			m_PWNetWork->Send_ReloadInfoPacket(IsReload, m_MyCharacter->Get_m_ID());
		
	}
}

void APWGameMode::Recv_PlayerMovePacketProcess(char* MovePacket)
{// 어떤 자료구조에 받아와서 여기서 set을 해주고 tick에서 업데이트해주자.
	SC_PACKET_PLAYER_MOVE* packet = reinterpret_cast<SC_PACKET_PLAYER_MOVE*>(MovePacket);
	if (m_PWNetWork != nullptr)
	{
		if (IsMultiGameStart) {
			m_TempPlayerInfoes[packet->m_ID].ObjInfo = packet->m_ObjInfo;
			m_TempPlayerInfoes[packet->m_ID].m_SkillCount = packet->m_SkillCount;
			m_TempPlayerInfoes[packet->m_ID].m_SKillON = packet->m_SkillON;
			m_TempPlayerInfoes[packet->m_ID].m_AmountPaint = packet->m_AmountPaint;
		}
		
	}
}
void APWGameMode::Recv_ShooterFirePacketProcess(char* ShooterFirePacket)
{
	SC_PACKET_SHOOTER_FIRE* packet = reinterpret_cast<SC_PACKET_SHOOTER_FIRE*>(ShooterFirePacket);
	if (m_PWNetWork != nullptr)
	{
		//PWLOG(Error, packet->m_IsFire ? TEXT("packet-> IsFire is True") : TEXT("packet-> IsFire is false"))
		if (IsMultiGameStart)
			m_TempPlayerInfoes[packet->m_ID].IsFire = packet->m_IsFire;

	}
}
void APWGameMode::Recv_RollerPaintPacketProcess(char* RollerPaintPacket)
{
	SC_PACKET_ROLLER_PAINT* packet = reinterpret_cast<SC_PACKET_ROLLER_PAINT*>(RollerPaintPacket);
	if (m_PWNetWork != nullptr)
	{
		if (IsMultiGameStart)
			m_TempPlayerInfoes[packet->m_ID].IsRolling = packet->m_IsRolling;
	}
}
void APWGameMode::Recv_BomberThrowPacketProcess(char* BomberThrowPacket)
{
	SC_PACKET_BOBMER_THROW* packet = reinterpret_cast<SC_PACKET_BOBMER_THROW*>(BomberThrowPacket);
	if (m_PWNetWork != nullptr)
	{
		if (IsMultiGameStart)
			m_TempPlayerInfoes[packet->m_ID].IsThrowing = packet->m_IsThrowing;
	}
}

void APWGameMode::Recv_ReloadingPacketProcess(char* ReloadPacket)
{
	SC_PACKET_RELOAD* packet = reinterpret_cast<SC_PACKET_RELOAD*>(ReloadPacket);
	if (m_PWNetWork != nullptr)
	{
		if (IsMultiGameStart)
			m_TempPlayerInfoes[packet->m_ID].IsReloading = packet->m_IsReloading;
	}
}

void APWGameMode::Recv_GameTimerPacketProcess(char* TimerPacket)
{
	//0506
	if (!IsMultiGameStart)
		return;
	SC_PACKET_GAME_TIMER* packet = reinterpret_cast<SC_PACKET_GAME_TIMER*>(TimerPacket);
	if (m_PWNetWork != nullptr)
	{
		if (m_GameTimer != nullptr)
		{
			m_GameTimer->SetCount(packet->m_TimerCount);
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("Timer Count is %d "), m_GameTimer->GetCount(), false));
		}
		else
		{
			m_GameTimer = GetWorld()->SpawnActor<APWGameTimeManager>();
		}
	}
}

void APWGameMode::Recv_ChatPacketProcess(char* ChatPacket,bool chatset)
{
	if (!IsMultiGameStart)
		return;
	SC_PACKET_CHAT* packet = reinterpret_cast<SC_PACKET_CHAT*>(ChatPacket);
	if (m_PWNetWork != nullptr)
	{
		if (m_GameTimer != nullptr)
		{
			wcscpy(m_RecvChat,packet->m_Chat);
			ChatSet = chatset;
		}
	}
}
//void APWGameMode::Recv_GameEndPacketProcess(char* EndPacket)
//{
//	if (m_PWNetWork != nullptr)
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("Game ENd  "), false));
//		//잠시 주석
//		m_GameTimer->SetIsGameEnd(true);
//		
//		//밑에땜에 runtime error 나는거같음.
//		Cast<UPWGameInstance>(GetGameInstance())->SetIsGameStart(false);
//	}
//
//}
void APWGameMode::Recv_OtherPlayerLeavePacketProcess(int ID)
{
	auto iterTemp = m_TempPlayerInfoes.find(ID); 
	if (iterTemp != m_TempPlayerInfoes.end())
	{
		m_TempPlayerInfoes.erase(ID);
	}
	auto iterCharacter = m_AllCharacters.find(ID);
	if (iterCharacter != m_AllCharacters.end())
	{
		iterCharacter->second->Destroy();
		m_AllCharacters.erase(ID);
	}
}
void APWGameMode::SendWorldInfo()
{//월드상에 보낼것이있다면 이곳에서
	SendPlayerInfo();

}

APWGameTimeManager* APWGameMode::GetGameTimer() const
{
	return m_GameTimer;
}

void APWGameMode::SetIsMultiGameStart(bool start)
{
	IsMultiGameStart = start;
}
///////////////////////////////////////////////////////////////////////////////////////