// Fill out your copyright notice in the Description page of Project Settings.


#include "PWGameTimeManager.h"
#include "PWGameInstance.h"
#include "PWBaseCharacter.h"
#include "PWGameEndThread.h"
#include "HAL/RunnableThread.h"
#include "PWPlayerController.h"
#include "PWBaseAIController.h"
#include "BrainComponent.h"
#include "Engine/TextureRenderTarget2D.h"
//#include "Kismet/KismetRenderingLibrary.h"
//#include "Engine.h"
// Sets default values
APWGameTimeManager::APWGameTimeManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CountDown = 60;
	GreenResultCount = 0;
	RedResultCount = 0;
	IsGameEnd = false;
	IsResultCompleteEnd = false;
}

// Called when the game starts or when spawned
void APWGameTimeManager::BeginPlay()
{
	Super::BeginPlay();
	/*if(Cast<UPWGameInstance>(GetGameInstance())->GetCurrentGameMode() == EGameMode::MULTI)
		CountDown = 300;*/
	GetWorldTimerManager().SetTimer(CountDownHandle, this, &APWGameTimeManager::ShowCountDown, 1.0f, true);
}

// Called every frame
void APWGameTimeManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APWGameTimeManager::ShowCountDown()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Remaining GameTime is  %d"), CountDown));
	//PWLOG(Warning, TEXT("Game remainning time is %d "), CountDown);
	
	auto PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());
	if (PWGameInstance->GetCurrentGameMode() == EGameMode::SINGLE)
	{
		--CountDown;
		if (CountDown < 1) //게임시간이끝났다.
		{
			GetWorldTimerManager().ClearTimer(CountDownHandle); // 타이머를 초기화한다.
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("Game is Finished !!!!!!!!!!!!!!!1")));

			GameEndProcess();
		}
	}
	else if (PWGameInstance->GetCurrentGameMode() == EGameMode::MULTI && IsGameEnd)
	{
		if (PWGameInstance->m_MyPlayerInfo->m_IsResulter == true)
		{
			GetWorldTimerManager().ClearTimer(CountDownHandle); // 타이머를 초기화한다.
			GameEndProcess();
		}
		else if(IsResultCompleteEnd == true) //&& PWGameInstance->m_MyPlayerInfo->m_IsResulter == false)
		{
			//계산한 결과를 set해주어야한다. 결과계산하는 클라이언트가 아니기때문에.
			GetWorldTimerManager().ClearTimer(CountDownHandle); // 타이머를 초기화한다.
			PWGameInstance->ClearRenderTargets();
			UGameplayStatics::OpenLevel(GetWorld(), FName("ResultScene"));
		}
	}

}

int APWGameTimeManager::GetCount()
{

	return CountDown;

}
void APWGameTimeManager::SetCount(int count)
{
	CountDown = count;
}

void APWGameTimeManager::GameEndProcess()
{
	//모든 AI와 플레이어의 입력을 멈추어야한다.
			//그러기위해 일단 모든 컨트롤러를 가져와야한다.
	auto PWGameInstance = Cast<UPWGameInstance>(GetGameInstance());

	TArray<AActor*> OutputActors;
	TSubclassOf<AActor> ClassCharacter = APWBaseCharacter::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassCharacter, OutputActors);

	for (const auto& Actor : OutputActors)
	{
		const auto& TempPawn = Cast<APWBaseCharacter>(Actor);
		const auto& TempController = TempPawn->GetController();


		if (Cast<APWBaseAIController>(TempController))
		{
			Cast<APWBaseAIController>(TempController)->GetBrainComponent()->StopLogic(FString(TEXT("Game Is End")));
		}
		else if (Cast<APWPlayerController>(TempController))
		{
			TempPawn->DisableInput(Cast<APWPlayerController>(TempController));
		}
	}
	//그리고 모든렌더타겟의 검사를 시작한다.
	const auto& RenderTargets = PWGameInstance->GetAllRenderTargets();

	//렌더타겟의 검사를 위해 게임스레드를 멈추는것은 막대한 영향이 끼친다.
	//렌더타겟의 크기는 2048 x 2048 인데 이걸 다 검사하려면 하루 왠종일 걸릴 것이다.
	//그래서 따로 쓰레드를 만들어서 계산만 하는 쓰레드를 만든다.
	// 쓰레드 갯수는 렌더타겟의 갯수만큼이다.
	PWGameEndThread** GameEndThreads = new PWGameEndThread*[RenderTargets.Num()];
	TArray< TArray<FLinearColor>> ColorBuffers;
	
	ColorBuffers.SetNum(RenderTargets.Num());
	//ColorBuffers.

	for (int i = 0; i < RenderTargets.Num(); ++i)
	{
		RenderTargets[i]->GameThread_GetRenderTargetResource()->ReadLinearColorPixels(ColorBuffers[i]);
		GameEndThreads[i] = new PWGameEndThread;
		GameEndThreads[i]->StartThread(ColorBuffers[i], &RedResultCount, &GreenResultCount, &ResultMutex);
	}
	//


	//모든쓰레드가 일을 마칠때 까지 기다려야한다.
	for (int i = 0; i < RenderTargets.Num(); ++i)
	{
		GameEndThreads[i]->GetGameEndThread()->WaitForCompletion();
	}

	//일을 다했으니 삭제해주어야한다. 
	for (int i = 0; i < RenderTargets.Num(); ++i)
	{
		delete GameEndThreads[i];
	}
	delete[] GameEndThreads;

	//여기에서 센드해주어야한다. 결과값을.
	PWGameInstance->SetRedResultCount(RedResultCount);
	PWGameInstance->SetGreenResultCount(GreenResultCount);

	
	PWGameInstance->ClearRenderTargets();

	if (PWGameInstance->GetCurrentGameMode() == EGameMode::MULTI)
	{
		PWGameInstance->Send_ResultGame();
		PWGameInstance->ForRestartInit();
	}
	UGameplayStatics::OpenLevel(GetWorld(), FName("ResultScene"));
	
	//결과값을 위한것이다.
	//PWLOG(Error, TEXT("RedResultCount is %d "), RedResultCount);
	//PWLOG(Error, TEXT("GreenResultCount is %d "), GreenResultCount);
	//PWLOG(Error, RedResultCount > GreenResultCount ? TEXT("Red Team Win !!!!!!!!!") : TEXT("Green Team Win !!!!!!!!"));
}
void APWGameTimeManager::SetIsGameEnd(bool end)
{
	IsGameEnd = end;
}
bool APWGameTimeManager::GetIsGameEnd()const
{
	return IsGameEnd;
}
void APWGameTimeManager::SetIsResultCompleteEnd(bool complete)
{
	IsResultCompleteEnd = complete;
}