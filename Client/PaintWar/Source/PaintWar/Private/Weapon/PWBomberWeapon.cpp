// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBomberWeapon.h"
#include "Components/ArrowComponent.h"
// Sets default values
APWBomberWeapon::APWBomberWeapon()
{
	//StaticMesh'/Game/TestAssets/TestWeapon/Weapons/Gun/FPWeapon/Bomb/Bomb_01.Bomb_01'
	//Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_BOMB(TEXT("/Game/TestAssets/TestWeapon/Weapons/Gun/FPWeapon/Bomb/Bomb_01.Bomb_01"));
	if (ST_BOMB.Succeeded()) 
	{
		WeaponSTMesh->SetStaticMesh(ST_BOMB.Object);
	}
	WeaponSTMesh->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
	ProjectileSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileSpawnPoint"));
	//ProjectileSpawnPoint->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
	//ProjectileSpawnPoint->SetRelativeLocation(FVector(0.0f, 0.0f, 5.0f));
	ProjectileSpawnPoint->SetupAttachment(WeaponSTMesh);
	ProjectileSpawnPoint->SetRelativeLocation(FVector(0.0f, 0.0f, 55.0f));
	ProjectileSpawnPoint->SetRelativeRotation(FRotator(0.0f, 0.0f, 90.0f));
	ProjectileSpawnPoint->bHiddenInGame = false;
}



