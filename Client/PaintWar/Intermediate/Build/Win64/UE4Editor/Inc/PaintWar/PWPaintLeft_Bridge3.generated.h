// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWPaintLeft_Bridge3_generated_h
#error "PWPaintLeft_Bridge3.generated.h already included, missing '#pragma once' in PWPaintLeft_Bridge3.h"
#endif
#define PAINTWAR_PWPaintLeft_Bridge3_generated_h

#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_RPC_WRAPPERS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_Bridge3(); \
	friend struct Z_Construct_UClass_APWPaintLeft_Bridge3_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_Bridge3, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_Bridge3)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWPaintLeft_Bridge3(); \
	friend struct Z_Construct_UClass_APWPaintLeft_Bridge3_Statics; \
public: \
	DECLARE_CLASS(APWPaintLeft_Bridge3, APWPaintCanvas, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWPaintLeft_Bridge3)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWPaintLeft_Bridge3(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWPaintLeft_Bridge3) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_Bridge3); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_Bridge3); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_Bridge3(APWPaintLeft_Bridge3&&); \
	NO_API APWPaintLeft_Bridge3(const APWPaintLeft_Bridge3&); \
public:


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWPaintLeft_Bridge3(APWPaintLeft_Bridge3&&); \
	NO_API APWPaintLeft_Bridge3(const APWPaintLeft_Bridge3&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWPaintLeft_Bridge3); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWPaintLeft_Bridge3); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWPaintLeft_Bridge3)


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_PRIVATE_PROPERTY_OFFSET
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_12_PROLOG
#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_INCLASS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWPaintLeft_Bridge3>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PaintMap_Left_Bridge_PWPaintLeft_Bridge3_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
