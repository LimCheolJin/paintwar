// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWAR_PWRollerCharacter_generated_h
#error "PWRollerCharacter.generated.h already included, missing '#pragma once' in PWRollerCharacter.h"
#endif
#define PAINTWAR_PWRollerCharacter_generated_h

#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPainting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPainting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAblePainting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AblePainting(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPainting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPainting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAblePainting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AblePainting(); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWRollerCharacter(); \
	friend struct Z_Construct_UClass_APWRollerCharacter_Statics; \
public: \
	DECLARE_CLASS(APWRollerCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWRollerCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPWRollerCharacter(); \
	friend struct Z_Construct_UClass_APWRollerCharacter_Statics; \
public: \
	DECLARE_CLASS(APWRollerCharacter, APWBaseCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(APWRollerCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWRollerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWRollerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWRollerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWRollerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWRollerCharacter(APWRollerCharacter&&); \
	NO_API APWRollerCharacter(const APWRollerCharacter&); \
public:


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWRollerCharacter(APWRollerCharacter&&); \
	NO_API APWRollerCharacter(const APWRollerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWRollerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWRollerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWRollerCharacter)


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RollerAnim() { return STRUCT_OFFSET(APWRollerCharacter, RollerAnim); }


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_11_PROLOG
#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_INCLASS \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class APWRollerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_Characters_PWRollerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
