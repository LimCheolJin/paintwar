// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/Characters/PWBaseCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWBaseCharacter() {}
// Cross Module References
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ECharacterState();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_DeathEvent();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_DeathProcess();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetDeath();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetIsReload();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetKill();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetLoginID();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetSkillCount();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetSkillON();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetTeam();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ETeam();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_GetWeapon();
	PAINTWAR_API UClass* Z_Construct_UClass_APWBaseWeapon_NoRegister();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_KillEvent();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_QuitReload();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_RespawnEvent();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_SetLoginID();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWBaseCharacter_StartReload();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	PAINTWAR_API UEnum* Z_Construct_UEnum_PaintWar_ESelectCharacter();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceConstant_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	ENGINE_API UClass* Z_Construct_UClass_USoundCue_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWidgetComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	static UEnum* ECharacterState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PaintWar_ECharacterState, Z_Construct_UPackage__Script_PaintWar(), TEXT("ECharacterState"));
		}
		return Singleton;
	}
	template<> PAINTWAR_API UEnum* StaticEnum<ECharacterState>()
	{
		return ECharacterState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECharacterState(ECharacterState_StaticEnum, TEXT("/Script/PaintWar"), TEXT("ECharacterState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PaintWar_ECharacterState_Hash() { return 3465838524U; }
	UEnum* Z_Construct_UEnum_PaintWar_ECharacterState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PaintWar();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECharacterState"), 0, Get_Z_Construct_UEnum_PaintWar_ECharacterState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECharacterState::ALIVE", (int64)ECharacterState::ALIVE },
				{ "ECharacterState::DEATH", (int64)ECharacterState::DEATH },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ALIVE.DisplayName", "Alive" },
				{ "DEATH.DisplayName", "Death" },
				{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PaintWar,
				nullptr,
				"ECharacterState",
				"ECharacterState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void APWBaseCharacter::StaticRegisterNativesAPWBaseCharacter()
	{
		UClass* Class = APWBaseCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeathEvent", &APWBaseCharacter::execDeathEvent },
			{ "DeathProcess", &APWBaseCharacter::execDeathProcess },
			{ "GetAmountPaint", &APWBaseCharacter::execGetAmountPaint },
			{ "GetDeath", &APWBaseCharacter::execGetDeath },
			{ "GetIsReload", &APWBaseCharacter::execGetIsReload },
			{ "GetKill", &APWBaseCharacter::execGetKill },
			{ "GetLoginID", &APWBaseCharacter::execGetLoginID },
			{ "GetSkillCount", &APWBaseCharacter::execGetSkillCount },
			{ "GetSkillON", &APWBaseCharacter::execGetSkillON },
			{ "GetTeam", &APWBaseCharacter::execGetTeam },
			{ "GetWeapon", &APWBaseCharacter::execGetWeapon },
			{ "IsCharacterFalling", &APWBaseCharacter::execIsCharacterFalling },
			{ "KillEvent", &APWBaseCharacter::execKillEvent },
			{ "QuitReload", &APWBaseCharacter::execQuitReload },
			{ "RespawnEvent", &APWBaseCharacter::execRespawnEvent },
			{ "SetLoginID", &APWBaseCharacter::execSetLoginID },
			{ "StartReload", &APWBaseCharacter::execStartReload },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "DeathEvent", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_DeathEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_DeathEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "DeathProcess", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_DeathProcess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_DeathProcess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics
	{
		struct PWBaseCharacter_eventGetAmountPaint_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetAmountPaint_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetAmountPaint", sizeof(PWBaseCharacter_eventGetAmountPaint_Parms), Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics
	{
		struct PWBaseCharacter_eventGetDeath_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetDeath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetDeath", sizeof(PWBaseCharacter_eventGetDeath_Parms), Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetDeath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetDeath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics
	{
		struct PWBaseCharacter_eventGetIsReload_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWBaseCharacter_eventGetIsReload_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWBaseCharacter_eventGetIsReload_Parms), &Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetIsReload", sizeof(PWBaseCharacter_eventGetIsReload_Parms), Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetIsReload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetIsReload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics
	{
		struct PWBaseCharacter_eventGetKill_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetKill_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetKill", sizeof(PWBaseCharacter_eventGetKill_Parms), Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetKill()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetKill_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics
	{
		struct PWBaseCharacter_eventGetLoginID_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetLoginID_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetLoginID", sizeof(PWBaseCharacter_eventGetLoginID_Parms), Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetLoginID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetLoginID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics
	{
		struct PWBaseCharacter_eventGetSkillCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetSkillCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetSkillCount", sizeof(PWBaseCharacter_eventGetSkillCount_Parms), Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetSkillCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetSkillCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics
	{
		struct PWBaseCharacter_eventGetSkillON_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PWBaseCharacter_eventGetSkillON_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PWBaseCharacter_eventGetSkillON_Parms), &Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetSkillON", sizeof(PWBaseCharacter_eventGetSkillON_Parms), Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetSkillON()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetSkillON_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics
	{
		struct PWBaseCharacter_eventGetTeam_Parms
		{
			ETeam ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetTeam_Parms, ReturnValue), Z_Construct_UEnum_PaintWar_ETeam, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetTeam", sizeof(PWBaseCharacter_eventGetTeam_Parms), Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetTeam()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetTeam_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics
	{
		struct PWBaseCharacter_eventGetWeapon_Parms
		{
			APWBaseWeapon* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventGetWeapon_Parms, ReturnValue), Z_Construct_UClass_APWBaseWeapon_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "GetWeapon", sizeof(PWBaseCharacter_eventGetWeapon_Parms), Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_GetWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_GetWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "IsCharacterFalling", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics
	{
		struct PWBaseCharacter_eventKillEvent_Parms
		{
			APWBaseCharacter* KillInstigator;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_KillInstigator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::NewProp_KillInstigator = { "KillInstigator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventKillEvent_Parms, KillInstigator), Z_Construct_UClass_APWBaseCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::NewProp_KillInstigator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "KillEvent", sizeof(PWBaseCharacter_eventKillEvent_Parms), Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_KillEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_KillEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "QuitReload", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_QuitReload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_QuitReload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "RespawnEvent", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_RespawnEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_RespawnEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics
	{
		struct PWBaseCharacter_eventSetLoginID_Parms
		{
			FString id;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWBaseCharacter_eventSetLoginID_Parms, id), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::NewProp_id,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "SetLoginID", sizeof(PWBaseCharacter_eventSetLoginID_Parms), Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_SetLoginID()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_SetLoginID_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWBaseCharacter, nullptr, "StartReload", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWBaseCharacter_StartReload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWBaseCharacter_StartReload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWBaseCharacter_NoRegister()
	{
		return APWBaseCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APWBaseCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentTeam_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentTeam;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentTeam_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnPointLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpawnPointLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnPointArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnPointArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnPointArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CharacterType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CharacterType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkillMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkillMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GreenTeamCharacterMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GreenTeamCharacterMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RedTeamCharacterMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RedTeamCharacterMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentCharacterState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentCharacterState;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CurrentCharacterState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Weapon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Weapon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReloadHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReloadHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttackSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttackSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkillTimerHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkillTimerHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IDWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IDWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpringArm_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpringArm;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWBaseCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWBaseCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWBaseCharacter_DeathEvent, "DeathEvent" }, // 2148335936
		{ &Z_Construct_UFunction_APWBaseCharacter_DeathProcess, "DeathProcess" }, // 3711209837
		{ &Z_Construct_UFunction_APWBaseCharacter_GetAmountPaint, "GetAmountPaint" }, // 642010409
		{ &Z_Construct_UFunction_APWBaseCharacter_GetDeath, "GetDeath" }, // 2934748525
		{ &Z_Construct_UFunction_APWBaseCharacter_GetIsReload, "GetIsReload" }, // 1555450663
		{ &Z_Construct_UFunction_APWBaseCharacter_GetKill, "GetKill" }, // 3716554215
		{ &Z_Construct_UFunction_APWBaseCharacter_GetLoginID, "GetLoginID" }, // 2067843926
		{ &Z_Construct_UFunction_APWBaseCharacter_GetSkillCount, "GetSkillCount" }, // 1133420353
		{ &Z_Construct_UFunction_APWBaseCharacter_GetSkillON, "GetSkillON" }, // 1824264939
		{ &Z_Construct_UFunction_APWBaseCharacter_GetTeam, "GetTeam" }, // 4198992778
		{ &Z_Construct_UFunction_APWBaseCharacter_GetWeapon, "GetWeapon" }, // 2440098341
		{ &Z_Construct_UFunction_APWBaseCharacter_IsCharacterFalling, "IsCharacterFalling" }, // 4037582457
		{ &Z_Construct_UFunction_APWBaseCharacter_KillEvent, "KillEvent" }, // 2682112924
		{ &Z_Construct_UFunction_APWBaseCharacter_QuitReload, "QuitReload" }, // 2475651549
		{ &Z_Construct_UFunction_APWBaseCharacter_RespawnEvent, "RespawnEvent" }, // 2999733961
		{ &Z_Construct_UFunction_APWBaseCharacter_SetLoginID, "SetLoginID" }, // 3622025676
		{ &Z_Construct_UFunction_APWBaseCharacter_StartReload, "StartReload" }, // 2290640303
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Characters/PWBaseCharacter.h" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam_MetaData[] = {
		{ "Category", "Team" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam = { "CurrentTeam", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, CurrentTeam), Z_Construct_UEnum_PaintWar_ETeam, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointLocation = { "SpawnPointLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, SpawnPointLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray = { "SpawnPointArray", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, SpawnPointArray), METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray_Inner = { "SpawnPointArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType_MetaData[] = {
		{ "Category", "Type" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType = { "CharacterType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, CharacterType), Z_Construct_UEnum_PaintWar_ESelectCharacter, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillMaterialInstance = { "SkillMaterialInstance", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, SkillMaterialInstance), Z_Construct_UClass_UMaterialInstanceConstant_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_GreenTeamCharacterMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_GreenTeamCharacterMaterialInstance = { "GreenTeamCharacterMaterialInstance", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, GreenTeamCharacterMaterialInstance), Z_Construct_UClass_UMaterialInstanceConstant_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_GreenTeamCharacterMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_GreenTeamCharacterMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_RedTeamCharacterMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
		{ "ToolTip", "#include \"Materials/MaterialInstanceConstant.h\"" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_RedTeamCharacterMaterialInstance = { "RedTeamCharacterMaterialInstance", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, RedTeamCharacterMaterialInstance), Z_Construct_UClass_UMaterialInstanceConstant_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_RedTeamCharacterMaterialInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_RedTeamCharacterMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState = { "CurrentCharacterState", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, CurrentCharacterState), Z_Construct_UEnum_PaintWar_ECharacterState, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_HealthPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_HealthPoint = { "HealthPoint", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, HealthPoint), METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_HealthPoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_HealthPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Weapon_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Weapon = { "Weapon", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, Weapon), Z_Construct_UClass_APWBaseWeapon_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Weapon_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Weapon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_ReloadHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_ReloadHandle = { "ReloadHandle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, ReloadHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_ReloadHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_ReloadHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_AttackSound_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_AttackSound = { "AttackSound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, AttackSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_AttackSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_AttackSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillTimerHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillTimerHandle = { "SkillTimerHandle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, SkillTimerHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillTimerHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillTimerHandle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_IDWidget_MetaData[] = {
		{ "Category", "UI" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_IDWidget = { "IDWidget", nullptr, (EPropertyFlags)0x00100000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, IDWidget), Z_Construct_UClass_UWidgetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_IDWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_IDWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Camera_MetaData[] = {
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
		{ "ToolTip", "\xc4\xab?\xde\xb6?" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x00100000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, Camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Camera_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Camera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpringArm_MetaData[] = {
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Characters/PWBaseCharacter.h" },
		{ "ToolTip", "\xc4\xab?\xde\xb6??? ???????? ?\xce\xb8\xf0\xb0\xa1\xb5? ????" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpringArm = { "SpringArm", nullptr, (EPropertyFlags)0x00100000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWBaseCharacter, SpringArm), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpringArm_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpringArm_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWBaseCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentTeam_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpawnPointArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CharacterType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_GreenTeamCharacterMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_RedTeamCharacterMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_CurrentCharacterState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_HealthPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Weapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_ReloadHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_AttackSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SkillTimerHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_IDWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWBaseCharacter_Statics::NewProp_SpringArm,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWBaseCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWBaseCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWBaseCharacter_Statics::ClassParams = {
		&APWBaseCharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWBaseCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWBaseCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWBaseCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWBaseCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWBaseCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWBaseCharacter, 1921313828);
	template<> PAINTWAR_API UClass* StaticClass<APWBaseCharacter>()
	{
		return APWBaseCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWBaseCharacter(Z_Construct_UClass_APWBaseCharacter, &APWBaseCharacter::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWBaseCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWBaseCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
