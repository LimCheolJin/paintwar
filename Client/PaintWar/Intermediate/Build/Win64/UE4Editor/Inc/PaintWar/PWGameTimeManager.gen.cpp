// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/PWGameTimeManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWGameTimeManager() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_APWGameTimeManager_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_APWGameTimeManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWGameTimeManager_GetCount();
	PAINTWAR_API UFunction* Z_Construct_UFunction_APWGameTimeManager_ShowCountDown();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
// End Cross Module References
	void APWGameTimeManager::StaticRegisterNativesAPWGameTimeManager()
	{
		UClass* Class = APWGameTimeManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCount", &APWGameTimeManager::execGetCount },
			{ "ShowCountDown", &APWGameTimeManager::execShowCountDown },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics
	{
		struct PWGameTimeManager_eventGetCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PWGameTimeManager_eventGetCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameTimeManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWGameTimeManager, nullptr, "GetCount", sizeof(PWGameTimeManager_eventGetCount_Parms), Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWGameTimeManager_GetCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWGameTimeManager_GetCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PWGameTimeManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APWGameTimeManager, nullptr, "ShowCountDown", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APWGameTimeManager_ShowCountDown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APWGameTimeManager_ShowCountDown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APWGameTimeManager_NoRegister()
	{
		return APWGameTimeManager::StaticClass();
	}
	struct Z_Construct_UClass_APWGameTimeManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CountDownHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CountDownHandle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APWGameTimeManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APWGameTimeManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APWGameTimeManager_GetCount, "GetCount" }, // 289723344
		{ &Z_Construct_UFunction_APWGameTimeManager_ShowCountDown, "ShowCountDown" }, // 2201015956
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameTimeManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PWGameTimeManager.h" },
		{ "ModuleRelativePath", "Public/PWGameTimeManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APWGameTimeManager_Statics::NewProp_CountDownHandle_MetaData[] = {
		{ "ModuleRelativePath", "Public/PWGameTimeManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APWGameTimeManager_Statics::NewProp_CountDownHandle = { "CountDownHandle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APWGameTimeManager, CountDownHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_APWGameTimeManager_Statics::NewProp_CountDownHandle_MetaData, ARRAY_COUNT(Z_Construct_UClass_APWGameTimeManager_Statics::NewProp_CountDownHandle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APWGameTimeManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APWGameTimeManager_Statics::NewProp_CountDownHandle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APWGameTimeManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APWGameTimeManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APWGameTimeManager_Statics::ClassParams = {
		&APWGameTimeManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APWGameTimeManager_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_APWGameTimeManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_APWGameTimeManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APWGameTimeManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APWGameTimeManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APWGameTimeManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APWGameTimeManager, 857938158);
	template<> PAINTWAR_API UClass* StaticClass<APWGameTimeManager>()
	{
		return APWGameTimeManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APWGameTimeManager(Z_Construct_UClass_APWGameTimeManager, &APWGameTimeManager::StaticClass, TEXT("/Script/PaintWar"), TEXT("APWGameTimeManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APWGameTimeManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
