// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_Bridge5.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_Bridge5 : public APWPaintCanvas
{
	GENERATED_BODY()

public:
	APWPaintLeft_Bridge5();
	
};
