// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintRight_2nd.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintRight_2nd : public APWPaintCanvas
{
	GENERATED_BODY()

public:
	APWPaintRight_2nd();
	
};
