// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_2nd.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_2nd : public APWPaintCanvas
{
	GENERATED_BODY()

public:
	APWPaintLeft_2nd();
	
};
