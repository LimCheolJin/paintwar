// Fill out your copyright notice in the Description page of Project Settings.


#include "PWMeleeWeapon.h"

// Sets default values
APWMeleeWeapon::APWMeleeWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RollerCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RollerCollsion"));
	RollerCollision->SetupAttachment(WeaponSTMesh);
}

void APWMeleeWeapon::Tick(float DeltaTime)
{
	APWBaseWeapon::Tick(DeltaTime);
}

void APWMeleeWeapon::BeginPlay()
{
	APWBaseWeapon::BeginPlay();
}

