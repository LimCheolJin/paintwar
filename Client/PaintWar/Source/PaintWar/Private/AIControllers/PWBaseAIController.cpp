// Fill out your copyright notice in the Description page of Project Settings.


#include "PWBaseAIController.h"
#include "PWGameInstance.h"
#include "PWBaseCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"

APWBaseAIController::APWBaseAIController()
{

}

void APWBaseAIController::BeginPlay()
{
	Super::BeginPlay();

}

void APWBaseAIController::OnUnPossess()
{
	Super::OnUnPossess();

	PWLOG(Error, TEXT("Base AIController UnOnPossess"));

}