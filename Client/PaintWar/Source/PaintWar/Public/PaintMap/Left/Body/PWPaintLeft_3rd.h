// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "PWPaintCanvas.h"
#include "PWPaintLeft_3rd.generated.h"

/**
 * 
 */
UCLASS()
class PAINTWAR_API APWPaintLeft_3rd : public APWPaintCanvas
{
	GENERATED_BODY()
	
public:
	APWPaintLeft_3rd();
};
