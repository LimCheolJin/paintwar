// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWar/Public/AnimInstance/PWAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePWAnimInstance() {}
// Cross Module References
	PAINTWAR_API UClass* Z_Construct_UClass_UPWAnimInstance_NoRegister();
	PAINTWAR_API UClass* Z_Construct_UClass_UPWAnimInstance();
	ENGINE_API UClass* Z_Construct_UClass_UAnimInstance();
	UPackage* Z_Construct_UPackage__Script_PaintWar();
// End Cross Module References
	void UPWAnimInstance::StaticRegisterNativesUPWAnimInstance()
	{
	}
	UClass* Z_Construct_UClass_UPWAnimInstance_NoRegister()
	{
		return UPWAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPWAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsReload_MetaData[];
#endif
		static void NewProp_IsReload_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsReload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsDead_MetaData[];
#endif
		static void NewProp_IsDead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsDead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsInAir_MetaData[];
#endif
		static void NewProp_IsInAir_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsInAir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentPawnSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentPawnSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPWAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "AnimInstance/PWAnimInstance.h" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWAnimInstance.h" },
		{ "ToolTip", "//?\xec\xb8\xae?? ?????? ???????? ?\xd6\xb4??\xce\xbd??\xcf\xbd??? ???\xdf\xbf? ???????? \xc4\xb3???\xcd\xba??? ?\xee\xb6\xb2 ?\xd6\xb4\xcf\xb8??\xcc\xbc??? ?????\xd8\xb6?\n//?\xd2\xb6? ?????? ???\xcc\xb4?. ???\xe2\xbc\xb1 ???????? ?\xd9\xb1? ???? ?? ?\xd6\xb4\xcf\xb8??\xcc\xbc??? ???? ???????????????????\xcc\xb0?\n//?\xcc\xb8? ???\xd3\xb9\xde\xbe? ???? AnimInstance???? ???? shooter, bomber, Roller?? ?\xd6\xb4??\xce\xbd??\xcf\xbd??? ???? ??????\n// \xc3\xb3???? ?? ?? ?\xcc\xb4?. ???? ?????\xd2\xb4??\xc3\xbf? ?\xce\xb8??? UAnimInstance?????? ?????\xcf\xb0? ?????\xd8\xbe??\xd1\xb4?." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload_SetBit(void* Obj)
	{
		((UPWAnimInstance*)Obj)->IsReload = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload = { "IsReload", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPWAnimInstance), &Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead_SetBit(void* Obj)
	{
		((UPWAnimInstance*)Obj)->IsDead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead = { "IsDead", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPWAnimInstance), &Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWAnimInstance.h" },
		{ "ToolTip", "???? ???? ???\xdf\xbf? ???\xd5\xb4??? ???\xce\xb8? \xc8\xae???\xcf\xb1?????. \xc8\xae????\n?\xd6\xb4\xd4\xb1\xd7\xb7????? ?????\xcf\xb1??????\xcc\xb4?." },
	};
#endif
	void Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir_SetBit(void* Obj)
	{
		((UPWAnimInstance*)Obj)->IsInAir = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir = { "IsInAir", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPWAnimInstance), &Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_CurrentPawnSpeed_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "Public/AnimInstance/PWAnimInstance.h" },
		{ "ToolTip", "???? ???? ?\xd3\xb5??? ?????\xcf\xbf? ?\xd6\xb4\xd4\xb1\xd7\xb7????? ?\xcb\xb7??\xd6\xb1?????.\n?????? ???\xf0\xbc\xad\xb5??? ?????\xd2\xbc? ?\xd6\xb0? ????????\xc6\xae???? ?\xd0\xb1?,???????? ???? ???????\xd6\xbe???." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_CurrentPawnSpeed = { "CurrentPawnSpeed", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPWAnimInstance, CurrentPawnSpeed), METADATA_PARAMS(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_CurrentPawnSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_CurrentPawnSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPWAnimInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsReload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsDead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_IsInAir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPWAnimInstance_Statics::NewProp_CurrentPawnSpeed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPWAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPWAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPWAnimInstance_Statics::ClassParams = {
		&UPWAnimInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPWAnimInstance_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPWAnimInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPWAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPWAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPWAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPWAnimInstance, 1805510397);
	template<> PAINTWAR_API UClass* StaticClass<UPWAnimInstance>()
	{
		return UPWAnimInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPWAnimInstance(Z_Construct_UClass_UPWAnimInstance, &UPWAnimInstance::StaticClass, TEXT("/Script/PaintWar"), TEXT("UPWAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPWAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
