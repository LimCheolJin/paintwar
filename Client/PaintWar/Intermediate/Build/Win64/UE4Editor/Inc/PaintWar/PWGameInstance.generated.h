// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGameMode : uint8;
enum class ETeam : uint8;
enum class ESelectCharacter : uint8;
#ifdef PAINTWAR_PWGameInstance_generated_h
#error "PWGameInstance.generated.h already included, missing '#pragma once' in PWGameInstance.h"
#endif
#define PAINTWAR_PWGameInstance_generated_h

#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetIsGameStart) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsGameStart(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_ReadyInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_ReadyInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_InitLogin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_InitLogin(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_SelectCharacterTypePacket) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_SelectCharacterTypePacket(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_SelectTeamTypePacket) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_SelectTeamTypePacket(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConnect) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_serverIP); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Connect(Z_Param_serverIP); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitNetWork) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->InitNetWork(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLoginID) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_id); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetLoginID(Z_Param_id); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoginID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetLoginID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetGreenResultCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetGreenResultCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRedResultCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetRedResultCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetGreenResultCount) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_num); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetGreenResultCount(Z_Param_num); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetRedResultCount) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_num); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetRedResultCount(Z_Param_num); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentGameMode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGameMode*)Z_Param__Result=P_THIS->GetCurrentGameMode(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentSelectTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ETeam*)Z_Param__Result=P_THIS->GetCurrentSelectTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentSelectCharacter) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ESelectCharacter*)Z_Param__Result=P_THIS->GetCurrentSelectCharacter(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentGameMode) \
	{ \
		P_GET_ENUM(EGameMode,Z_Param_Mode); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentGameMode(EGameMode(Z_Param_Mode)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentSelectTeam) \
	{ \
		P_GET_ENUM(ETeam,Z_Param_SelectTeam); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentSelectTeam(ETeam(Z_Param_SelectTeam)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentSelectCharacter) \
	{ \
		P_GET_ENUM(ESelectCharacter,Z_Param_SelectCharacter); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentSelectCharacter(ESelectCharacter(Z_Param_SelectCharacter)); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetIsGameStart) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsGameStart(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_ReadyInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_ReadyInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_InitLogin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_InitLogin(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_SelectCharacterTypePacket) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_SelectCharacterTypePacket(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSend_SelectTeamTypePacket) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Send_SelectTeamTypePacket(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConnect) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_serverIP); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Connect(Z_Param_serverIP); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitNetWork) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->InitNetWork(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLoginID) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_id); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetLoginID(Z_Param_id); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoginID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetLoginID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetGreenResultCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetGreenResultCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRedResultCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetRedResultCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetGreenResultCount) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_num); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetGreenResultCount(Z_Param_num); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetRedResultCount) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_num); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetRedResultCount(Z_Param_num); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentGameMode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGameMode*)Z_Param__Result=P_THIS->GetCurrentGameMode(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentSelectTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ETeam*)Z_Param__Result=P_THIS->GetCurrentSelectTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentSelectCharacter) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ESelectCharacter*)Z_Param__Result=P_THIS->GetCurrentSelectCharacter(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentGameMode) \
	{ \
		P_GET_ENUM(EGameMode,Z_Param_Mode); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentGameMode(EGameMode(Z_Param_Mode)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentSelectTeam) \
	{ \
		P_GET_ENUM(ETeam,Z_Param_SelectTeam); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentSelectTeam(ETeam(Z_Param_SelectTeam)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentSelectCharacter) \
	{ \
		P_GET_ENUM(ESelectCharacter,Z_Param_SelectCharacter); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetCurrentSelectCharacter(ESelectCharacter(Z_Param_SelectCharacter)); \
		P_NATIVE_END; \
	}


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPWGameInstance(); \
	friend struct Z_Construct_UClass_UPWGameInstance_Statics; \
public: \
	DECLARE_CLASS(UPWGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWGameInstance)


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUPWGameInstance(); \
	friend struct Z_Construct_UClass_UPWGameInstance_Statics; \
public: \
	DECLARE_CLASS(UPWGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PaintWar"), NO_API) \
	DECLARE_SERIALIZER(UPWGameInstance)


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPWGameInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPWGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWGameInstance(UPWGameInstance&&); \
	NO_API UPWGameInstance(const UPWGameInstance&); \
public:


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPWGameInstance(UPWGameInstance&&); \
	NO_API UPWGameInstance(const UPWGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPWGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPWGameInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPWGameInstance)


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentSelectCharacter() { return STRUCT_OFFSET(UPWGameInstance, CurrentSelectCharacter); } \
	FORCEINLINE static uint32 __PPO__CurrentSelectTeam() { return STRUCT_OFFSET(UPWGameInstance, CurrentSelectTeam); } \
	FORCEINLINE static uint32 __PPO__CurrentGameMode() { return STRUCT_OFFSET(UPWGameInstance, CurrentGameMode); } \
	FORCEINLINE static uint32 __PPO__AllRenderTargets() { return STRUCT_OFFSET(UPWGameInstance, AllRenderTargets); }


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_15_PROLOG
#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_RPC_WRAPPERS \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_INCLASS \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_PRIVATE_PROPERTY_OFFSET \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_INCLASS_NO_PURE_DECLS \
	PaintWar_Source_PaintWar_Public_PWGameInstance_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWAR_API UClass* StaticClass<class UPWGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWar_Source_PaintWar_Public_PWGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
