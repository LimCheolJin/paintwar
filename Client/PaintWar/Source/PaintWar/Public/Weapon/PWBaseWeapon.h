// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaintWar.h"
#include "GameFramework/Actor.h"
#include "PWBaseWeapon.generated.h"

UCLASS()
class PAINTWAR_API APWBaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APWBaseWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(EditAnywhere, Category = Weapon)
	float Damage;

//현재는 스태틱메쉬만정의해둔상태임. 스켈레탈메쉬가생긴다면
	//코드를 수정할필요가있을거같음.
	UPROPERTY(EditAnywhere, Category = Weapon)
	UStaticMeshComponent* WeaponSTMesh;

	UFUNCTION(BlueprintCallable)
	UStaticMeshComponent* GetWeaponSTMesh();
};